# Portfolio

해당 Repository는 저의 포트폴리오를 한군데 모아두기 위한 곳입니다.

대부분의 작업 Repository가 private에서 public 으로 전환하기 어려운 부분이있어 공개 가능한 부분만 모아뒀습니다.

저의 작업기록을 자세히 보고 싶으신 분은

winwoo4946@gmail.com 으로 연락주시면 private Repository를 초대드리겠습니다.

감사합니다.