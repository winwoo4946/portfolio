protoc -I=./ --csharp_out=./ ./Protocol.proto
IF ERRORLEVEL 1 PAUSE

START ../bin/Debug/net7.0/PacketGenerator.exe ./Protocol.proto
XCOPY /Y Protocol.cs "../../../Client/Assets/Script/Packet"
XCOPY /Y Protocol.cs "../../Server/Packet"
XCOPY /Y ClientPacketManager.cs "../../../Client/Assets/Script/Packet"
XCOPY /Y ServerPacketManager.cs "../../Server/Packet"