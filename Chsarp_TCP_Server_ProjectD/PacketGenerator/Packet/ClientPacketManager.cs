using Google.Protobuf;
using Google.Protobuf.Protocol;
using ServerCore;
using System;
using System.Collections.Generic;

class PacketManager
{
	#region Singleton
	static PacketManager _instance = new PacketManager();
	public static PacketManager Instance { get { return _instance; } }
	#endregion

	PacketManager()
	{
		Register();
	}

	Dictionary<ushort, Action<PacketSession, ArraySegment<byte>, ushort>> _onRecv = new Dictionary<ushort, Action<PacketSession, ArraySegment<byte>, ushort>>();
	Dictionary<ushort, Action<PacketSession, IMessage>> _handler = new Dictionary<ushort, Action<PacketSession, IMessage>>();

	public Action<PacketSession, IMessage, ushort> CustomHandler { get; set; }

	public void Register()
	{		
		_onRecv.Add((ushort)MsgId.SMatchGame, MakePacket<S_MatchGame>);
		_handler.Add((ushort)MsgId.SMatchGame, PacketHandler.S_MatchGameHandler);		
		_onRecv.Add((ushort)MsgId.SEnterGame, MakePacket<S_EnterGame>);
		_handler.Add((ushort)MsgId.SEnterGame, PacketHandler.S_EnterGameHandler);		
		_onRecv.Add((ushort)MsgId.SReadyGame, MakePacket<S_ReadyGame>);
		_handler.Add((ushort)MsgId.SReadyGame, PacketHandler.S_ReadyGameHandler);		
		_onRecv.Add((ushort)MsgId.SLeaveGame, MakePacket<S_LeaveGame>);
		_handler.Add((ushort)MsgId.SLeaveGame, PacketHandler.S_LeaveGameHandler);		
		_onRecv.Add((ushort)MsgId.SPlaceUnit, MakePacket<S_PlaceUnit>);
		_handler.Add((ushort)MsgId.SPlaceUnit, PacketHandler.S_PlaceUnitHandler);		
		_onRecv.Add((ushort)MsgId.SMove, MakePacket<S_Move>);
		_handler.Add((ushort)MsgId.SMove, PacketHandler.S_MoveHandler);		
		_onRecv.Add((ushort)MsgId.SAttackGameObject, MakePacket<S_AttackGameObject>);
		_handler.Add((ushort)MsgId.SAttackGameObject, PacketHandler.S_AttackGameObjectHandler);		
		_onRecv.Add((ushort)MsgId.SEndGame, MakePacket<S_EndGame>);
		_handler.Add((ushort)MsgId.SEndGame, PacketHandler.S_EndGameHandler);		
		_onRecv.Add((ushort)MsgId.SLevelUpTower, MakePacket<S_LevelUpTower>);
		_handler.Add((ushort)MsgId.SLevelUpTower, PacketHandler.S_LevelUpTowerHandler);		
		_onRecv.Add((ushort)MsgId.SNuclear, MakePacket<S_Nuclear>);
		_handler.Add((ushort)MsgId.SNuclear, PacketHandler.S_NuclearHandler);		
		_onRecv.Add((ushort)MsgId.SNuclearCount, MakePacket<S_NuclearCount>);
		_handler.Add((ushort)MsgId.SNuclearCount, PacketHandler.S_NuclearCountHandler);		
		_onRecv.Add((ushort)MsgId.SSync, MakePacket<S_Sync>);
		_handler.Add((ushort)MsgId.SSync, PacketHandler.S_SyncHandler);		
		_onRecv.Add((ushort)MsgId.SCheat, MakePacket<S_Cheat>);
		_handler.Add((ushort)MsgId.SCheat, PacketHandler.S_CheatHandler);
	}

	public void OnRecvPacket(PacketSession session, ArraySegment<byte> buffer)
	{
		ushort count = 0;

		ushort size = BitConverter.ToUInt16(buffer.Array, buffer.Offset);
		count += 2;
		ushort id = BitConverter.ToUInt16(buffer.Array, buffer.Offset + count);
		count += 2;

		Action<PacketSession, ArraySegment<byte>, ushort> action = null;
		if (_onRecv.TryGetValue(id, out action))
			action.Invoke(session, buffer, id);
	}

	void MakePacket<T>(PacketSession session, ArraySegment<byte> buffer, ushort id) where T : IMessage, new()
	{
		T pkt = new T();
		pkt.MergeFrom(buffer.Array, buffer.Offset + 4, buffer.Count - 4);
		if (CustomHandler != null)
		{
			CustomHandler.Invoke(session, pkt, id);
		}
		else
		{
            Action<PacketSession, IMessage> action = null;
            if (_handler.TryGetValue(id, out action))
                action.Invoke(session, pkt);
        }
	}

	public Action<PacketSession, IMessage> GetPacketHandler(ushort id)
	{
		Action<PacketSession, IMessage> action = null;
		if (_handler.TryGetValue(id, out action))
			return action;
		return null;
	}
}