﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ServerCore
{
    /// <summary>
    /// TCP Socket을 생성하여 클라이언트 연결 요청을 Listen함
    /// </summary>
    public class Listener
    {
        Socket _listenSocket;
        Func<Session> _sessionFactory;

        /// <summary>
        /// Socket 생성 및 클라이언트 연결 대기
        /// </summary>
        /// <param name="endPoint">서버 주소 정보</param>
        /// <param name="sessionFactory">SocketManager.Generate</param>
        /// <param name="register">Accept 이벤트 연결 개수</param>
        /// <param name="backlog">최대 연결 요청 개수</param>
        public void Init(IPEndPoint endPoint, Func<Session> sessionFactory, int register = 10, int backlog = 100)
        {
            _listenSocket = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            _sessionFactory += sessionFactory;
            _listenSocket.Bind(endPoint);
            // 소켓 오픈
            // backlog: 최대 대기수
            _listenSocket.Listen(backlog);

            for (int i = 0; i < register; ++i)
            {
                // register만큼 연결 이벤트 생성
                SocketAsyncEventArgs args = new SocketAsyncEventArgs();
                args.Completed += new EventHandler<SocketAsyncEventArgs>(OnAcceptCompleted);
                RegisterAccept(args);
            }
        }

        /// <summary>
        /// Socket 연결 요청
        /// AcceptAsync pending이 false일 경우 OnCompleted가 호출이 안되서
        /// 강제로 OnAcceptCompleted를 호출해 줘야함
        /// https://learn.microsoft.com/ko-kr/dotnet/api/system.net.sockets.socket.acceptasync?view=net-7.0
        /// </summary>
        /// <param name="args">비동기 이벤트</param>
        void RegisterAccept(SocketAsyncEventArgs args)
        {
            args.AcceptSocket = null;

            bool pending = _listenSocket.AcceptAsync(args);
            if (!pending)
                OnAcceptCompleted(null, args);
        }

        /// <summary>
        /// Socket연결이 완료 되면 호출됨
        /// Socket연결이 정상적으로 완료 됐을 경우 Session을 생성
        /// Session생성은 Init때 전달받은 _sessionFactory로 생성
        /// Session생성 후 이후 해당 클라이언트와의 통신은 Session이 담당하고 
        /// Listener Socket은 다시 다른 클라이언트 연결 요청을 대기함
        /// </summary>
        void OnAcceptCompleted(object sender, SocketAsyncEventArgs args)
        {
            try
            {
                if (args.SocketError == SocketError.Success)
                {
                    Session session = _sessionFactory.Invoke();

                    if (args.AcceptSocket != null && args.AcceptSocket.Connected)
                    {
                        session.Start(args.AcceptSocket);
                        session.OnConnected(args.AcceptSocket.RemoteEndPoint);
                    }
                    else
                    {
                        // 소켓이 닫혀 있거나 연결되지 않은 경우의 처리
                        Console.WriteLine("소켓이 연결되지 않았거나 이미 닫혀 있습니다.");
                    }
                }
                else
                {
                    Console.WriteLine(args.SocketError.ToString());
                }
            }
            catch (ObjectDisposedException ex)
            {
                Console.WriteLine($"소켓 오류: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"기타 오류: {ex.Message}");
            }
            finally
            {
                RegisterAccept(args);
            }
        }
    }
}
