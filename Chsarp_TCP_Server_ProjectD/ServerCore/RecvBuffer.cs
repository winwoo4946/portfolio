﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServerCore
{
    /// <summary>
    /// TCP 특성상 1번에 통신에 전체 데이터 byte가 오지 않을 수 있기 때문에 다음 통신에 전달받는 data까지 기다려서 recv처리
    /// ex) clinet에서 100byte를 보냈어도 1번째 통신에선 80byte가오고 2번째 통신에 나머지 20byte가 올 수 있음
    /// </summary>
    public class RecvBuffer
    {
        /** 
         * [rw][] [] [] [] [] [] [] [] [] []
         * [r] [] [] [] [w][] [] [] [] [] [] read 부터 write까지의 byte가 전체 데이터에 대한 byte인지 체크

         * [] [] [] [] [rw][] [] [] [] [] [] 1-1)전체 데이터가 4byte라면 4byte 데이터 처리 후 read는 write위치로

         * [r] [] [] [] [w][] [] [] [] [] [] 2-1)전체 데이터가 6byte라면 4byte 데이터를 처리하지 않고 대기
         * [r] [] [] [] [] [] [w][] [] [] [] 2-2)나머지 2byte까지 받고
         * [] [] [] [] [] [] [rw][] [] [] [] 2-3)전체 6byte 처리 후 read는 write위치로
         * 
         * [] [] [] [] [] [] [rw] [] [] [] []
         * [rw][] [] [] [] [] [] [] [] [] []  rw가 같은 위치일 경우 처음 위치로 돌아옴
         * 
         * [r] [] [] [w] [] [] [] [] [] [] [] 3-1)총 3byte를 받았고 처리 가능한 데이터가 2byte일 경우
         * [] [] [r] [w] [] [] [] [] [] [] [] 3-2)처리 가능한 2byte를 처리하고 read는 2byte이동
         * [] [] [r] [] [] [w] [] [] [] [] [] 
         * [] [] [] [] [r] [w] [] [] [] [] [] 3-2)다음 통신에 또 2byte를 받고 2byte만 처리 가능하면 read와 write 위치가 점점 뒤로 밀림
         * [r] [w] [] [] [] [] [] [] [] [] [] 3-3)이 경우는 read 와 write위치를 처음 기준으로 다시 이동시켜줌
         */
        ArraySegment<byte> _buffer;
        int _readPos;
        int _writePos;

        public RecvBuffer(int bufferSize)
        {
            _buffer = new ArraySegment<byte>(new byte[bufferSize], 0, bufferSize);
        }

        // [r] [] [] [w] [] [] [] [] [] [] [] 일때
        // <Data>[r] [] []</Data> <Free>[w] [] [] [] [] [] [] []</Free>  
        // read의 위치
        public int DataOffset => _buffer.Offset + _readPos;
        // read ~ write까지의 사이즈
        public int DataSize => _writePos - _readPos;

        // write의 위치
        public int FreeOffset => _buffer.Offset + _writePos;
        // wirte ~ buffer의 마지막
        public int FreeSize => _buffer.Count - _writePos;

        public ArraySegment<byte> ReadSegment => new ArraySegment<byte>(_buffer.Array, DataOffset, DataSize);
        public ArraySegment<byte> WriteSegment => new ArraySegment<byte>(_buffer.Array, FreeOffset, FreeSize);

        public void Clean()
        {
            int dataSize = DataSize;
            if (dataSize == 0)
            {
                // 남은 데이터가 없으면 커서 위치만 리셋
                _readPos = _writePos = 0;
                return;
            }

            // 남은 데이터가 있으면 시작위치로 복사
            Array.Copy(_buffer.Array, DataOffset, _buffer.Array, _buffer.Offset, dataSize);
            _readPos = 0;
            _writePos = dataSize;
        }

        public bool OnRead(int numOfBytes)
        {
            if (numOfBytes > DataSize)
                return false;

            _readPos += numOfBytes;
            return true;
        }

        public bool OnWrite(int numOfBytes)
        {
            if(numOfBytes > FreeSize) 
                return false;

            _writePos += numOfBytes;
            return true;
        }
    }
}
