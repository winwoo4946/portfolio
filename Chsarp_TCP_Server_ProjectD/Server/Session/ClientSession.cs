﻿using ServerCore;
using System.Net;
using Google.Protobuf.Protocol;
using Google.Protobuf;
using Server.Game;
using CommonScripts;

namespace Server
{
    public class ClientSession : PacketSession
	{
		public Player MyPlayer { get; set; }
		public int SessionId { get; set; }

		public void Send(IMessage packet)
		{
			string msgName = packet.Descriptor.Name.Replace("_", string.Empty);
			MsgId msgId = (MsgId)Enum.Parse(typeof(MsgId), msgName);
			//Util.Log(packet, msgId);

            ushort size = (ushort)packet.CalculateSize();
			// [size(2byte)][msgId(2byte)][packet]
            byte[] sendBuffer = new byte[size + 4];
			// sendBuffer의 사이즈(length)를 앞 2byte에 ushort type으로 넣어줌
            Array.Copy(BitConverter.GetBytes((ushort)(size + 4)), 0, sendBuffer, 0, sizeof(ushort));
			// packet의 msgId 를 다음 2byte에 ushort type으로 넣어줌
            Array.Copy(BitConverter.GetBytes((ushort)msgId), 0, sendBuffer, 2, sizeof(ushort));
			// 나머지 공간은 packet byteArray 넣어줌
            Array.Copy(packet.ToByteArray(), 0, sendBuffer, 4, size);

            Send(new ArraySegment<byte>(sendBuffer));
        }

		public override void OnConnected(EndPoint endPoint)
		{
			Util.Log("OnConnected", endPoint);
			MyPlayer = new Player();
            MyPlayer.Session = this;
		}

		public override void OnRecvPacket(ArraySegment<byte> buffer)
		{
			PacketManager.Instance.OnRecvPacket(this, buffer);
		}

		public override void OnDisconnected(EndPoint endPoint)
		{
			SessionManager.Instance.Remove(this);

            Util.Log("OnDisconnected", $"({SessionId}): {endPoint}");
		}

		public override void OnSend(int numOfBytes)
		{
			//Console.WriteLine($"Transferred bytes: {numOfBytes}");
		}
	}
}
