using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using DesignData;
using Google.Protobuf;
using Server;
using CommonScripts;

public static class DataManager
{
    static string DESIGN_DB_PATH = $"{Program.DATA_ROOT}/DesignData/db";
    static Dictionary<string, Dictionary<int, byte[]>> _designDataDic = new Dictionary<string, Dictionary<int, byte[]>>();
    public static void LoadAllDesignDB()
    {
        SQLitePCL.Batteries.Init();
        var connectionString = $"Data Source={DESIGN_DB_PATH}/DesignData.db";
        using var connection = new SqliteConnection(connectionString);
        connection.Open();

        var command = connection.CreateCommand();
        command.CommandText = "SELECT name FROM sqlite_master WHERE type='table'";

        List<string> tableNames = new List<string>();
        using (var reader = command.ExecuteReader())
        {
            while (reader.Read())
            {
                tableNames.Add(reader.GetString(0));
            }
        }

        foreach(string tableName in tableNames)
        {
            command.CommandText = $"SELECT id, protobuf FROM {tableName}";
            using var tableReader = command.ExecuteReader();
            Dictionary<int, byte[]> tableData = new Dictionary<int, byte[]>();
            while (tableReader.Read())
            {
                int id = tableReader.GetInt32(0);
                byte[] protobuf = (byte[])tableReader["protobuf"];

                tableData.Add(id, protobuf);
            }
            Util.Log("DataManager", $"{tableName} Load Complete");
            _designDataDic.Add(tableName, tableData);
        }
        Common.GameDefaultInfo = GetDesignData<GameDefaultInfo>(1);
    }

    public static T GetDesignData<T>(int id) where T : IMessage<T>, new()
    {
        string tableName = typeof(T).Name;
        if (_designDataDic.TryGetValue(tableName, out var designData) == false)
            throw new Exception($"존재하지 않는 테이블 -> {tableName}");

        if (designData.TryGetValue(id, out byte[] data) == false)
            throw new Exception($"존재하지 않는 테이블 Id -> {tableName}.{id}");

        
        T t = new T();
        t.MergeFrom(data);
        return t;
    }

    #region Json

    public static T LoadJson<T>(string filePath)
    {
        if (!File.Exists(filePath))
        {
            throw new FileNotFoundException($"파일을 찾을 수 없습니다: {filePath}");
        }

        string json = File.ReadAllText(filePath, Encoding.UTF8);

        // 만약 DataContractJsonSerializer를 사용하려면:
        using (MemoryStream jsonStream = new MemoryStream(Encoding.UTF8.GetBytes(json)))
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T), new DataContractJsonSerializerSettings
            {
                UseSimpleDictionaryFormat = true
            });

            return (T)serializer.ReadObject(jsonStream);
        }
    }


    #endregion
}
