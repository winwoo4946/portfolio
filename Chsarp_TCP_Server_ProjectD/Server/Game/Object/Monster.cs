﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapDefine;
using System.ComponentModel;
using DesignData;
using System.Threading;

namespace Server.Game
{
    public class Monster : Unit
    {
        public bool IsLive { get; private set; }
        Queue<Vector2Int> _path;
        public Monster()
        {
        }

        public override void Init(ClientSession session, GameRoom room, Player player, UnitInfo unitInfo, Vector2Int initPos)
        {
            base.Init(session, room, player, unitInfo, initPos);
            
            if(_unitInfo != null)
            {
                Info.Name = $"Player{player.PlayerInfo.PlayerId}_Monster{_unitInfo.NameId}";
                
            }
            CurrentPosVec = _path.Peek();
            Room.Map.AddObjectInCell(CurrentPosVec, Info);
            IsLive = true;
        }

        public override void Update()
        {
            if (Environment.TickCount64 - MoveTick < MOVE_INTERVAL)
                return;

            Room.Map.RemoveObjectInCell(CurrentPosVec, Info);
            if (_path.Count <= 0)
            {
                IsLive = false;
                return;
            }
        }
    }
}
