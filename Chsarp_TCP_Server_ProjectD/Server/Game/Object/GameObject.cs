﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MapDefine;
using CommonScripts;

namespace Server.Game
{
    public class GameObject
    {
        public GameObjectInfo Info { get; set; } = new GameObjectInfo();

        public long Uid
        {
            get => Info.Uid;
            set => Info.Uid = value;
        }

        public long PoolId { get; set;} = 0;

        public int Tid
        {
            get => Info.Tid;
            set => Info.Tid = value;
        }

        public PlayerInfo Player
        {
            get => Info.PlayerInfo; 
            set => Info.PlayerInfo = value;
        }

        public GameObjectType ObjectType
        {
            get => Info.Type;
            set => Info.Type = value;
        }

        public PositionInfo CurrentPos
        {
            get
            {
                if (Info.CurrentPos == null)
                    Info.CurrentPos = new PositionInfo();
                return Info.CurrentPos;
            }
            set => Info.CurrentPos = value;
        }

        public PositionInfo DestPos
        {
            get
            {
                if (Info.DestPos == null)
                    Info.DestPos = new PositionInfo();
                return Info.DestPos;
            }
            set => Info.DestPos = value;
        }

        public Vector2Int CurrentPosVec
        {
            get => new Vector2Int(CurrentPos.PosX, CurrentPos.PosY);
            set
            {
                CurrentPos.PosX = value.x;
                CurrentPos.PosY = value.y;
            }
        }

        public Vector2Int DestPosVec
        {
            get => new Vector2Int(DestPos.PosX, DestPos.PosY);
            set
            {
                DestPos.PosX = value.x;
                DestPos.PosY = value.y;
            }
        }

        public TeamType TeamType => Player.TeamType;

        public TeamType EnemyTeamType
        {
            get
            {
                if (TeamType == TeamType.Left)
                    return TeamType.Right;

                if (TeamType == TeamType.Right)
                    return TeamType.Left;

                return TeamType.Neutral;
            }
        }

        protected UnitState State
        {
            get => Info.State;
            set 
            {
                BeforeState = Info.State;
                Info.State = value;
            }
        }

        protected UnitState BeforeState { get; set; }

        protected UnitAttackState AttackState
        {
            get => Info.AttackState;
            set => Info.AttackState = value;
        }

        public virtual int Hp
        {
            get => Info.Hp;
            set
            {
                Info.Hp = Math.Max(0, Math.Min(value, MaxHp));
                if (IsDestory == false && Info.Hp == 0)
                {
                    Destory();
                }
            }
        }

        public int MaxHp { get; protected set; }

        public virtual int Level
        {
            get => Info.Level;
            set => Info.Level = Math.Max(1, value);
        }

        public bool IsDestory { get; set; } = false;

        public bool IsAttackable { get; protected set; }

        public Player OwnerPlayer { get; private set; }
        public GameRoom Room { get; protected set; }

        public ClientSession Session { get; protected set; }
        public GameObject Target { get; protected set; }

        public virtual void Init(ClientSession session, GameRoom room, Player player)
        {
            Session = session;
            Room = room;
            OwnerPlayer = player;
            Player = player.PlayerInfo;
            Level = 1;
            IsDestory = false;
            Target = null;
        }

        public virtual void Update() { }

        /// <summary>
        /// 주변 오브젝트 탐색
        /// </summary>
        /// <returns>오브젝트 발견 여부</returns>
        public virtual bool Seach(int searchRange, TeamType teamType)
        {
            var foundList = Room.Map.FindObjectByRange(CurrentPosVec, searchRange, teamType);
            if (foundList.Count == 0)
                return false;

            // 타겟을 찾았으면 추적상태로 변경
            int closestDist = int.MaxValue;
            for (int i = 0; i < foundList.Count; ++i)
            {
                GameObjectInfo info = foundList[i];
                Vector2Int targetPos = new Vector2Int(info.CurrentPos.PosX, info.CurrentPos.PosY);
                int dist = Map.SqrDistance(targetPos, CurrentPosVec);
                if (dist < closestDist)
                {
                    // 가장 가까운 유닛 찾음
                    closestDist = dist;
                    Target = Room.GetGameObject(foundList[i]);
                }
            }

            return Target != null;
        }

        public override string ToString()
        {
            return $"{ObjectType}({Uid})";
        }

        public virtual void Destory()
        {
            IsDestory = true;
            OwnerPlayer.RemoveGameObject(this);
            Util.ObjectLog("Destory", $"", LogColor.DarkRed, Info);
        }
    }
}
