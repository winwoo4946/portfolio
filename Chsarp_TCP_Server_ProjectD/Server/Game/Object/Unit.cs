﻿using CommonScripts;
using DesignData;
using Google.Protobuf.Protocol;
using MapDefine;
using System.Text;

namespace Server.Game
{
    public class Unit : GameObject
    {
        public int AttackRange => _unitInfo.AttackRange;
        public int AttackDamage => _unitInfo.AttackDamage;
        public AttackType AttackType => (AttackType)_unitInfo.AttackType;
        public int ResourceCost => _unitInfo.ResouceCost;

        protected UnitInfo _unitInfo = null;

        protected float MOVE_INTERVAL = 0;
        protected float MoveTick { get; set; } = 0f;
        protected float MoveRate => MoveTick / MOVE_INTERVAL;

        protected float ATTACK_INTERVAL = 0;
        protected float _attackTick = 0;

        protected int _seachRange = 3;
        protected int _sqrSearchRange = 0;
        protected int _sqrAttackRange = 0;

        Queue<Vector2Int> _path = new Queue<Vector2Int>();
        Vector2Int _beforeTargetPos;
        Vector2Int _nextPos;
        public Unit()
        {
            ObjectType = GameObjectType.Unit;
            IsAttackable = true;
        }

        public virtual void Init(ClientSession session, GameRoom room, Player player, UnitInfo unitInfo, Vector2Int initPos)
        {
            base.Init(session, room, player);
            Tid = unitInfo.Id;
            _unitInfo = unitInfo;
            _path.Clear();
            MaxHp = _unitInfo.Hp;
            Hp = _unitInfo.Hp;
            MOVE_INTERVAL = Common.GetUpdateInterval(_unitInfo.Speed);
            MoveTick = 0f;
            ATTACK_INTERVAL = Common.GetUpdateInterval(_unitInfo.AttackSpeed);
            _attackTick = 0f;
            _seachRange = unitInfo.SearchRange;
            _sqrSearchRange = (_seachRange * _seachRange) + (_seachRange * _seachRange);
            _sqrAttackRange = (AttackRange * AttackRange) + (AttackRange * AttackRange);
            ChangeState(UnitState.Idle);

            Map map = Room.Map;
            if (map.GetEnemyInCell(initPos, TeamType) != null)
            {
                // 유닛 생성한 위치에 적 유닛이 있으면 근처로 위치
                map.FindPlaceCell(initPos, 1, TeamType, ref initPos);
            }
            CurrentPosVec = initPos;
            DestPosVec = initPos;
            _nextPos = initPos;
            _beforeTargetPos = new Vector2Int(-1000, -1000);
            Room.Map.AddObjectInCell(CurrentPosVec, Info);

            Util.ObjectLog("InitUnit", $"{Info}", LogColor.Green, Info);
        }

        public override void Update()
        {
            switch (State)
            {
                case UnitState.Idle:
                    // 최초에 한번 주변탐색
                    ResearchTarget();
                    break;
                case UnitState.Move:
                    if (Move() == true)
                    {
                        // 이동이 완료되면 탐색
                        NextPath();
                        if (Seach() == true)
                        {
                            ChangeState(UnitState.Tracking);
                        }
                    }
                    break;
                case UnitState.Tracking:
                    if (Move() == true)
                    {
                        // 이동이 완료되면 다음 추적
                        NextPath();
                        Tracking();
                    }
                    break;
                case UnitState.Attack:
                    AttackUpdate();
                    break;
            }
        }

        /// <summary>
        /// MoveTick 기반 다음 좌표까지 이동
        /// </summary>
        /// <returns>다음좌표까지 이동 완료 여부</returns>
        public bool Move()
        {
            MoveTick += Room.DeltaTime;
            //Util.ObjectLog("MoveTick", $"{MoveTick}", LogColor.Cyan, Info);
            if (MoveTick < MOVE_INTERVAL)
                return false;

            // deltaTime이 밀릴 경우 밀린 만큼 다음 프레임에 더해줌
            MoveTick = Math.Max(0, MoveTick - MOVE_INTERVAL);
            return true;
        }

        public bool Seach()
        {
            TeamType searchTeamType = AttackType == AttackType.Heal ? TeamType : EnemyTeamType;

            if(AttackType == AttackType.Heal)
            {
                // 힐 유닛 탐색 로직
                var foundList = Room.Map.FindObjectByRange(CurrentPosVec, _seachRange, TeamType);
                if (foundList.Count == 0)
                    return false;

                // 타겟을 찾았으면 추적상태로 변경
                int closestDist = int.MaxValue;
                for (int i = 0; i < foundList.Count; ++i)
                {
                    GameObjectInfo info = foundList[i];
                    Vector2Int targetPos = new Vector2Int(info.CurrentPos.PosX, info.CurrentPos.PosY);
                    int dist = Map.SqrDistance(targetPos, CurrentPosVec);
                    GameObject obj = Room.GetGameObject(foundList[i]);
                    if (obj.Hp < obj.MaxHp && dist < closestDist)
                    {
                        // Hp가 깎인 유닛 중 가장 가까운 유닛 찾음
                        closestDist = dist;
                        Target = obj;
                    }
                }

                return Target != null;
            }
            else
            {
                // 공격형 유닛 탐색로직
                return base.Seach(_seachRange, EnemyTeamType);
            }
        }

        public void ResearchTarget()
        {
            Target = null;
            if (Seach() == false)
            {
                // 주변 탐색후 타겟이 없으면 이동
                ChangeState(UnitState.Move);
            }
            else
            {
                State = UnitState.Tracking;
                Tracking();
            }
        }

        /// <summary>
        /// 타겟 추적
        /// </summary>
        /// <returns>신규 추적 위치 갱신 여부</returns>
        public bool Tracking()
        {
            if (Target == null || Target.IsDestory == true)
            {
                ChangeState(UnitState.Move);
                return false;
            }
            // 추적상태일땐 타겟과의 거리를 체크후 계속 추적할지 체크
            int sqrDist = Map.SqrDistance(new Vector2Int(Target.CurrentPos.PosX, Target.CurrentPos.PosY), CurrentPosVec);
            int sqrSearchRange = _sqrSearchRange;
            int sqrAttackRange = _sqrAttackRange;

            Util.ObjectLog("TrackingRange", $"sqrDist : {sqrDist} | sqrSearchRange : {sqrSearchRange} | sqrAttackRange : {sqrAttackRange} | CurrentPosVec : {CurrentPosVec} | TargetUID : {Target.Uid} | TargetPos : {Target.CurrentPos}", LogColor.Cyan, Info);

            if (sqrDist > sqrSearchRange)
            {
                // 타겟과의 거리가 _searchRange 보다 멀어지면 다시 이동상태로 변경
                ChangeState(UnitState.Move);
                return false;
            }

            // 공격 사거리 안에 들어왔는지 체크
            if (sqrDist <= sqrAttackRange)
            {
                // 공격 사거리에 들어왔으면 Attack상태로 변경
                ChangeState(UnitState.Attack);
                return false;
            }

            // 타겟의 이전 좌표와 현재좌표가 같으면 추가 길찾기 안함
            Vector2Int targetPos = new Vector2Int(Target.CurrentPos.PosX, Target.CurrentPos.PosY);
            Vector2Int targetAroundPos = Room.Map.FindClosestTargetAroundPosition(targetPos, CurrentPosVec);
            Util.ObjectLog("Target Pos", $"Current : {CurrentPosVec} | targetPos : {targetPos} | targetAround : {targetAroundPos} | _beforeTargetPos : {_beforeTargetPos}", LogColor.Gray, Info);
            if (_beforeTargetPos == targetAroundPos)
                return false;

            if (State != UnitState.Tracking)
            {
                // State가 Tracking이 아닌상태에서 Tracking이 호출된 경우
                // Attack을 할지 Tracking을 할지 판단하기 위해 호출한 것 이므로
                // 위에서 State가 Attack으로 바뀌지 않았다면 State를 Tracking으로 바꿔준다
                State = UnitState.Tracking;
            }
            SetDest(targetAroundPos);
            _beforeTargetPos = targetAroundPos;
            return true;
        }

        public void AttackUpdate()
        {
            switch (AttackState)
            {
                case UnitAttackState.Move:
                    if (Move() == true)
                    {
                        AttackState = UnitAttackState.Attack;
                    }
                    break;
                case UnitAttackState.Attack:
                    Attack();
                    break;
                case UnitAttackState.End:
                    _attackTick += Room.DeltaTime;
                    if (_attackTick < 200)
                        return;

                    _attackTick = 0f;
                    ResearchTarget();
                    break;
            }
        }

        public void Attack()
        {
            // 추적 대상이 공격범위내로 들어오면 공격
            int sqrDist = Map.SqrDistance(new Vector2Int(Target.CurrentPos.PosX, Target.CurrentPos.PosY), CurrentPosVec);
            if (sqrDist > _sqrAttackRange)
            {
                // 공격 범위를 벗어나면 다른 타겟 검색
                Target = null;
                Seach();
                if (Target == null)
                {
                    // 검색 결과 타겟이 없으면 move로 변경
                    ChangeState(UnitState.Move);
                }
                return;
            }

            _attackTick += Room.DeltaTime;
            if (_attackTick < ATTACK_INTERVAL)
                return;

            _attackTick = Math.Max(0, _attackTick - ATTACK_INTERVAL);

            List<GameObjectInfo> targetList = new List<GameObjectInfo> { Target.Info };
            bool isAttackEnd;
            switch (AttackType)
            {
                case AttackType.Heal:
                    if (Target.IsDestory == false)
                    {
                        // 살아있을때만 힐줄수있음
                        Target.Hp += AttackDamage;
                    }
                    // 힐은 체력이 다 차면 end처리
                    isAttackEnd = Target.Hp == Target.MaxHp;
                    break;
                case AttackType.PhysMultiAtk:
                case AttackType.MagicMultiAtk:
                    // 타겟의 타일에 있는 유닛 다 데미지 입힘
                    Target.Hp -= AttackDamage;
                    var inCellObjects = Room.Map.Find(Target.CurrentPosVec);
                    for(int i = 0; i < inCellObjects.Count; i++)
                    {
                        var obj = inCellObjects[i];
                        if (obj == null || obj.Uid == Target.Uid)
                            continue;

                        GameObject go = Room.GetGameObject(obj);
                        go.Hp -= AttackDamage;
                        targetList.Add(go.Info);
                    }
                    isAttackEnd = Target.Hp == 0;
                    break;
                default:
                    Target.Hp -= AttackDamage;
                    isAttackEnd = Target.Hp == 0;
                    break;
            }
            
            Room.Broadcast(PacketSender.S_AttackGameObject(Info, targetList));
            Util.ObjectLog("AttackPacket", $"TargetCount : {targetList.Count} | Current : {CurrentPosVec} | Next : {_nextPos} | Dest : {DestPosVec} | State : {State} | moveRate : {MoveRate} | moveTick : {MoveTick}", LogColor.Red, Info);

            if (isAttackEnd == true)
            {
                _attackTick = 0f;
                AttackState = UnitAttackState.End;
            }
        }

        StringBuilder _pathSb = new StringBuilder();
        private void FindPath(Vector2Int dest)
        {
            _path.Clear();
            List<Vector2Int> pathList = Room.Map.FindPath(CurrentPosVec, dest);
            _pathSb.Clear();
            for (int i = 0; i < pathList.Count; i++)
            {
                _path.Enqueue(pathList[i]);
                _pathSb.Append($"{pathList[i]}, ");
            }
            GameObjectInfo targetInfo = Target != null ? Target.Info : null;

            if (BeforeState == UnitState.Attack && CurrentPosVec == _nextPos) // 직전 상태가 attack 이면서 next가 current랑 같으면  다음위치로 이동
            {
                NextPath();
            }

            Room.Broadcast(PacketSender.S_MoveSender(Info, targetInfo));
            Util.ObjectLog("MovePacket", $"Current : {CurrentPosVec} | Next : {_nextPos} | Dest : {DestPosVec} | State : {State} | moveRate : {MoveRate} | moveTick : {MoveTick}", LogColor.Red, Info);

        }

        public Vector2Int FindMovePosition()
        {
            Vector2Int enemyTowerVec;
            if (TeamType == TeamType.Left)
            {
                enemyTowerVec = Room.Map.RightTowerPosition[0];
            }
            else
            {
                enemyTowerVec = Room.Map.LeftTowerPosition[0];
            }
            Vector2Int waypoint = Room.Map.FindClosestWayPoint(CurrentPosVec, TeamType);
            int distWay = Map.SqrDistance(waypoint, CurrentPosVec);
            int distTower = Map.SqrDistance(enemyTowerVec, CurrentPosVec);
            if (distWay == 0 || distTower < distWay)
            {
                // 타워가 더 가까우면 타워로
                // 경유지 2개 이상 지났으면 타워로
                return enemyTowerVec;
            }

            for (int r = -1; r <= 1; ++r)
            {
                if (r == 0)
                    continue;

                // waypoint 기준 위아래 중 가까운곳으로
                var v = new Vector2Int(waypoint.x, waypoint.y + r);
                int dist = Map.SqrDistance(v, CurrentPosVec);
                if (dist < distWay)
                {
                    distWay = dist;
                    waypoint = v;
                }
            }

            return waypoint;
        }

        public void ChangeState(UnitState state)
        {
            if (State == state)
                return;

            State = state;
            switch (state)
            {
                case UnitState.Move:
                    Target = null;
                    FindMoveDest();
                    break;
                case UnitState.Tracking:
                    Vector2Int targetPos = new Vector2Int(Target.CurrentPos.PosX, Target.CurrentPos.PosY);
                    _beforeTargetPos = targetPos;
                    SetDest(targetPos);
                    break;
                case UnitState.Attack:
                    AttackState = UnitAttackState.Move;
                    SetDest(CurrentPosVec);
                    _attackTick = ATTACK_INTERVAL;
                    Attack();
                    break;
            }
        }

        void FindMoveDest()
        {
            Vector2Int destPos = FindMovePosition();
            SetDest(destPos);
        }

        void SetDest(Vector2Int destPos)
        {
            if (DestPosVec == destPos)
                return;

            DestPosVec = destPos;
            FindPath(DestPosVec);
        }

        void NextPath()
        {
            SetNextPos();
            if (_path.Count == 0)
            {
                // 이동 상태일 경우
                // 경로가 없으면 다음경로 미리 찾음
                switch (State)
                {
                    case UnitState.Move:
                        FindMoveDest();
                        break;
                }
            }
            //Util.Log($"\n{map.PrintMap()}", "MapPrint");
        }

        void SetNextPos()
        {
            Map map = Room.Map;
            if (map == null)
                return;

            _nextPos = map.NextPosition(_path, TeamType, Info, out var enemy);
            CurrentPosVec = _nextPos;
            if (enemy != null)
            {
                Target = Room.GetGameObject(enemy);
                Tracking();
            }
            Util.ObjectLog("NextPos", $"Current : {CurrentPosVec} | Next : {_nextPos} | Dest : {DestPosVec} | State : {State} | moveRate : {MoveRate} | moveTick : {MoveTick}", LogColor.White, Info);
        }
    }
}
