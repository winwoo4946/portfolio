﻿using CommonScripts;
using DesignData;
using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Game.Object
{
    public class Tower : GameObject
    {
        public override int Level 
        { 
            get => base.Level;
            set 
            {
                base.Level = value;
                MaxResource = TowerCommon.MaxResource(_buildingInfo, Level);
                ResourceProduction = (int)TowerCommon.ResourceProduction(_buildingInfo, Level);
                LevelUpCost = TowerCommon.LevelUpCost(MaxResource, Level);
            }
        }
        public override int Hp 
        { 
            get => base.Hp;
            set
            {
                base.Hp = value;
                CheckNuclearAddCount();
            }
        }
        public int MaxResource { get; private set; }
        public int ResourceProduction { get; private set; }
        public int LevelUpCost { get; private set; }
        public int AttackRange => _buildingInfo.AttackRange;
        public int AttackDamage => _buildingInfo.AttackDamage;

        protected float ATTACK_INTERVAL = 0;
        protected float _attackTick = 0;

        float RESOURCE_INTERVAL = 0;
        float _resourceTick = 0;
        BuildingInfo _buildingInfo;
        int _lastCheckedHpPercent = 100;
        public Tower() 
        {
            ObjectType = GameObjectType.Tower;
            IsAttackable = true;
        }
        public override void Init(ClientSession session, GameRoom room, Player player)
        {
            // Level Property override해서 먼저셋팅해줌
            _buildingInfo = DataManager.GetDesignData<BuildingInfo>(1);
            base.Init(session, room, player);
            Tid = _buildingInfo.Id;
            MaxHp = _buildingInfo.Hp;
            Hp = _buildingInfo.Hp;

            ATTACK_INTERVAL = Common.GetUpdateInterval(_buildingInfo.AttackSpeed);
            _attackTick = 0f;
            RESOURCE_INTERVAL = Common.GetUpdateInterval(_buildingInfo.ResourceInterval);
        }

        public override void Update()
        {
            if(Target == null)
            {
                Seach();
            }
            else
            {
                UpdateAttack();
            }
            UpdateResource();
        }

        private void UpdateAttack()
        {
            _attackTick += Room.DeltaTime;
            if (_attackTick < ATTACK_INTERVAL)
                return;

            _attackTick = Math.Max(0, _attackTick - ATTACK_INTERVAL);
            if (Target == null)
                return;

            Target.Hp -= AttackDamage;
            Room.Broadcast(PacketSender.S_AttackGameObject(Info, new List<GameObjectInfo>() { Target.Info }));
            Util.ObjectLog("AttackPacket", $"Tower({Uid}) | Target : {Target.Uid} | TargetHp : {Target.Hp}", LogColor.Red, Info);
            if (Target.Hp == 0)
            {
                Target = null;
            }
        }

        public bool Seach()
        {
            return base.Seach(AttackRange, EnemyTeamType);
        }

        private void UpdateResource()
        {
            _resourceTick += Room.DeltaTime;
            //Util.ObjectLog("MoveTick", $"{MoveTick}", LogColor.Cyan, Info);
            if (_resourceTick < RESOURCE_INTERVAL)
                return;

            OwnerPlayer.Resource += ResourceProduction;
            // deltaTime이 밀릴 경우 밀린 만큼 다음 프레임에 더해줌
            _resourceTick = Math.Max(0, _resourceTick - RESOURCE_INTERVAL);
        }

        private void CheckNuclearAddCount()
        {
            int[] percentArr = Common.NuclearAddCountPercent;
            int currentHpPercent = (int)((float)Hp / MaxHp * 100);
            foreach (int percent in percentArr)
            {
                if(_lastCheckedHpPercent > percent && currentHpPercent <= percent)
                {
                    ++OwnerPlayer.NuclearCount;
                }
            }
            _lastCheckedHpPercent = currentHpPercent;
        }
    }
}
