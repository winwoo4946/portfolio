﻿using CommonScripts;
using DesignData;
using Google.Protobuf.Protocol;
using Google.Protobuf.WellKnownTypes;
using MapDefine;
using Server.Game.Object;
using System;

namespace Server.Game
{
    public class Player
    {
        ClientSession _session;
        public ClientSession Session
        {
            get { return _session; }
            set
            {
                _session = value;
                if (value != null)
                {
                    PlayerInfo = new PlayerInfo
                    {
                        PlayerId = value.SessionId
                    };
                }
            }
        }
        public GameRoom Room { get; set; }

        public PlayerInfo PlayerInfo { get; private set; }

        public int Id => PlayerInfo.PlayerId;
        public TeamType TeamType
        {
            get => PlayerInfo.TeamType;
            private set => PlayerInfo.TeamType = value;
        }

        public string Name
        {
            get => PlayerInfo.Name;
            set => PlayerInfo.Name = value;
        }
        public Tower Tower { get; private set; }
        public bool IsDestoryTower => Tower.IsDestory;

        int _resource = 0;
        int _maxResource = 0;
        public int Resource
        {
            get => _resource;
            set
            {
                //Util.Log("Resource", $"Player({Id}) : {_resource} | value : {value}");
                _resource = Math.Min(_maxResource, value);
            }
        }

        int _nuclearCount = 0;
        public int NuclearCount
        {
            get => _nuclearCount;
            set
            {
                _nuclearCount = value;
                Session.Send(PacketSender.S_NuclearCount(_nuclearCount));
            }
        }

        public int Population { get; private set; }

        Dictionary<long, GameObject> _objects = new Dictionary<long, GameObject>();

        public void Init(GameRoom room, TeamType teamType)
        {
            Room = room;
            TeamType = teamType;
            CreateTower();
        }

        public void Update()
        {
            _objects.Values.ToList().ForEach(unit =>
            {
                unit.Update();
            });
        }

        public void CreateTower()
        {
            Vector2Int towerPos = new Vector2Int();
            if (TeamType == TeamType.Left)
            {
                towerPos = Room.Map.LeftTowerPosition[0];
            }
            else if (TeamType == TeamType.Right)
            {
                towerPos = Room.Map.RightTowerPosition[0];
            }

            Tower = ObjectManager.Instance.Add<Tower>();
            Tower.Init(_session, Room, this);
            Tower.CurrentPosVec = towerPos;
            Room.Map.AddObjectInCell(towerPos, Tower.Info);
            _objects.Add(Tower.Uid, Tower);
            RefreshMaxResource();
        }

        public void LevelUpTower(long uid)
        {
            Tower tower = GetGameObject(uid) as Tower;
            if (tower == null)
                return;

            if (Resource < tower.LevelUpCost)
                return;

            Resource -= tower.LevelUpCost;
            tower.Level += 1;
            GameRoom room = Room;
            Session.Send(PacketSender.S_LevelUpTower(tower.Info));
            room.UpdateSync(true);
            RefreshMaxResource();
        }

        public void RefreshMaxResource()
        {
            _maxResource = Tower.MaxResource;
        }

        public bool AddUnit(int unitTid, Vector2Int pos, out Unit unit)
        {
            unit = null;
            UnitInfo unitInfo = DataManager.GetDesignData<UnitInfo>(unitTid);
            Util.Log("AddUnit", $"player{Id} - 보유중 : {Resource} | 유닛비용 : {unitInfo.ResouceCost}");
            if (Resource < unitInfo.ResouceCost)
                return false;

            unit = ObjectManager.Instance.Add<Unit>();
            unit.Init(_session, Room, this, unitInfo, pos);
            _objects.Add(unit.Uid, unit);
            ++Population;
            return true;
        }

        public void RemoveGameObject(GameObject gameObject)
        {
            if (gameObject == null)
                return;

            GameRoom room = Room;
            if (room == null)
                return;

            room.Map.RemoveObjectInCell(gameObject.CurrentPosVec, gameObject.Info);
            switch (gameObject.ObjectType)
            {
                case GameObjectType.Unit:
                    _objects.Remove(gameObject.Uid);
                    --Population;
                    break;
                case GameObjectType.Tower:
                    Room.Push(Room.EndGame, this);
                    break;
            }
            ObjectManager.Instance.Release(gameObject);
        }

        public GameObject GetGameObject(GameObjectInfo objectInfo)
        {
            return GetGameObject(objectInfo.Uid);
        }
        public GameObject GetGameObject(long uid)
        {
            if (_objects.TryGetValue(uid, out GameObject go) == false)
                return null;

            return go;
        }

        public List<Unit> AllUnit
        {
            get
            {
                List<Unit> units = new List<Unit>(_objects.Count);
                foreach (var obj in _objects.Values)
                {
                    if(obj is Unit unit)
                    {
                        units.Add(unit);
                    }
                }
                return units;
            }
        }

    }
}
