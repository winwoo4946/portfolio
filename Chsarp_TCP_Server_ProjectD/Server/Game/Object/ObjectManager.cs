﻿using Google.Protobuf.Protocol;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Game
{
    public class ObjectManager
    {
        public static ObjectManager Instance { get; } = new ObjectManager();
        int _uid = 0;
        int _poolId = 0;

        // 각 오브젝트 타입에 대한 풀을 저장
        private Dictionary<Type, Queue<GameObject>> _objectPools = new Dictionary<Type, Queue<GameObject>>();
        public T Add<T>() where T : GameObject, new()
        {
            T gameObject;

            // 해당 타입의 오브젝트 풀에서 사용 가능한 오브젝트가 있는지 확인
            if(_objectPools.TryGetValue(typeof(T), out Queue<GameObject> pool) && pool.Count > 0)
            {
                gameObject = _objectPools[typeof(T)].Dequeue() as T;
            }
            else
            {
                // 사용 가능한 오브젝트가 없으면 새로 생성
                gameObject = new T();
            }
            gameObject.Uid = _uid++;
            return gameObject;
        }

        public void Release<T>(T gameObject) where T : GameObject
        {
            if (gameObject == null) 
                return;

            // 오브젝트를 다시 풀에 넣음
            if (!_objectPools.ContainsKey(typeof(T)))
            {
                _objectPools[typeof(T)] = new Queue<GameObject>();
            }

            _objectPools[typeof(T)].Enqueue(gameObject);
        }
    }
}
