﻿using CommonScripts;
using System.Timers;

namespace Server.Game
{
    public class RoomManager
    {
        public static RoomManager Instance { get; } = new RoomManager();
        object _lock = new object();
        Dictionary<int, GameRoom> _rooms = new Dictionary<int, GameRoom>();
        Queue<Player> _matchingPool = new Queue<Player>();
        HashSet<int> _matchingSet = new HashSet<int>();
        int _roomId = 1;

        Dictionary<int, System.Timers.Timer> roomTimer = new Dictionary<int, System.Timers.Timer>();
        Dictionary<int, ElapsedEventHandler> roomTimerHandler = new Dictionary<int, ElapsedEventHandler>();

        public void SetRoomUpdateTick(GameRoom room, int tick = 66)
        {
            var timer = new System.Timers.Timer();
            timer.Interval = tick;
            ElapsedEventHandler elapsedEventHandler = (s, e) => room.Update();
            timer.Elapsed += elapsedEventHandler;
            timer.AutoReset = true;
            timer.Enabled = true;
            roomTimer.Add(room.RoomId, timer);
            roomTimerHandler.Add(room.RoomId, elapsedEventHandler);
            Util.Log("SetRoomUpdateTick", $"RoomId - {room.RoomId}");
        }

        public void StopRoomUpdateTick(int roomId)
        {
            if (roomTimer.TryGetValue(roomId, out System.Timers.Timer timer))
            {
                timer.Stop();

                if (roomTimerHandler.TryGetValue(roomId, out ElapsedEventHandler elapsedEventHandler))
                {
                    timer.Elapsed -= elapsedEventHandler;
                    roomTimerHandler.Remove(roomId);
                }
                roomTimer.Remove(roomId);
                timer.Dispose();
            }
            
            Util.Log("StopRoomUpdateTick", $"RoomId - {roomId}");
        }

        public GameRoom Add()
        {
            lock (_lock)
            {
                GameRoom room = new GameRoom();
                room.Init();
                room.RoomId = _roomId;
                _rooms.Add(_roomId, room);
                SetRoomUpdateTick(room);
                _roomId++;
                return room;
            }
        }

        public bool Remove(int roomId)
        {
            lock (_lock)
            {
                StopRoomUpdateTick(roomId);
                return _rooms.Remove(roomId);
            }
        }

        public GameRoom? Find(int roomId)
        {
            lock (_lock)
            {
                _rooms.TryGetValue(roomId, out GameRoom room);
                return room;
            }
        }

        public void AddMatchingPool(Player player)
        {
            lock(_lock)
            {
                //GameRoom r = Add();
                //r.Push(r.EnterGame, player);
                //return;

                if (_matchingPool.Count > 0)
                {
                    Player matchingPlayer = DequeuePlayer();
                    if(matchingPlayer != null)
                    {
                        GameRoom room = Add();
                        room.Push(room.EnterGame, matchingPlayer);
                        room.Push(room.EnterGame, player);
                        Util.Log("Matching!", $"player1 - {matchingPlayer.Id} | player2 - {player.Id}");
                    }
                    else
                    {
                        EnqueuePlayer(player);
                    }
                }
                else
                {
                    EnqueuePlayer(player);
                }
            }
        }

        public void RemoveMachingPool(int playerId)
        {
            lock (_lock)
            {
                if (_matchingSet.Contains(playerId))
                {
                    _matchingSet.Remove(playerId);
                    Util.Log("RemoveMachiningPool", $"player - {playerId}");
                }
            }
        }

        public void EnqueuePlayer(Player player)
        {
            lock (_lock)
            {
                _matchingPool.Enqueue(player);
                _matchingSet.Add(player.Id);
                Util.Log("AddMachiningPool", $"player - {player.Id}");
            }
        }

        public Player DequeuePlayer()
        {
            lock (_lock)
            {
                while (_matchingPool.Count > 0)
                {
                    Player player = _matchingPool.Dequeue();
                    if (_matchingSet.Contains(player.Id))
                    {
                        _matchingSet.Remove(player.Id);
                        return player;
                    }
                }
                return null;
            }
        }
    }
}
