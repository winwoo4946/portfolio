﻿using CommonScripts;
using DesignData;
using Google.Protobuf;
using Google.Protobuf.Protocol;
using MapDefine;
using System;
using System.Net.Sockets;

namespace Server.Game
{
    public class GameRoom : JobQueue
    {
        public float DeltaTime { get; private set; }
        long _prevTime = 0;
        public int RoomId { get; set; }
        public Map Map { get; private set; } = new Map();
        public StageState State => _state;

        Dictionary<int, Player> _players = new Dictionary<int, Player>();
        StageState _state = StageState.None;

        // 10초
        readonly int SYNC_INTERVAL = 10000;
        float _syncTick = 0;

        int _readyCount = 0;
        public void Init()
        {
            // 맵 로드
            // TODO mapId 하드코딩된것
            int mapId = 2;
            string MAP_DATA_PATH = $"{Program.DATA_ROOT}/MapData/Json";
            string formatMapId = mapId.ToString("000");
            string mapDefinePath = $"{MAP_DATA_PATH}/Map_{formatMapId}_define.json";
            var mapDefine = DataManager.LoadJson<MapDefine.MapDefine>(mapDefinePath);
            string mapDataPath = $"{MAP_DATA_PATH}/Map_{formatMapId}.json";
            var list = DataManager.LoadJson<List<TileMapInfo>>(mapDataPath);
            Map.LoadMap(mapDefine, list);
            _readyCount = 0;
            _prevTime = Environment.TickCount64;
        }

        public void Update()
        {
            long currentTick = Environment.TickCount64;
            DeltaTime = currentTick - _prevTime;
            switch (_state)
            {
                case StageState.Play:
                    Push(UpdatePlayer);
                    break;
            }

            Flush();
            _prevTime = currentTick;
        }

        private void UpdatePlayer()
        {
            foreach (Player player in _players.Values)
            {
                player.Update();
            }
            UpdateSync();
        }

        public void UpdateSync(bool isForce = false)
        {
            _syncTick += DeltaTime;
            if (isForce == false && _syncTick < SYNC_INTERVAL)
                return;

            _syncTick = 0f;
            Sync();
        }

        void SetStageState(StageState state)
        {
            switch (state)
            {
                case StageState.Enter:
                    _readyCount = 0;
                    Broadcast(PacketSender.S_EnterGame(_players.Values.ToList()));
                    break;
            }
            _state = state;
        }

        public void EnterGame(Player player)
        {
            if (player == null)
                return;

            TeamType team = (TeamType)((_players.Count % 2) + 1);
            player.Init(this, team);
            _players.Add(player.Id, player);

            if (_players.Count == 2)
            {
                SetStageState(StageState.Enter);
            }
        }

        public void ReadyGame(Player player)
        {
            if(player == null) 
                return;

            _readyCount++;
            if (_readyCount == _players.Count)
            {
                Broadcast(PacketSender.S_ReadyGame(StageState.Play));
                SetStageState(StageState.Play);
            }
        }

        public void LeaveGame(int objectId)
        {
            // 본인에게 전달
            if (_players.TryGetValue(objectId, out Player leavePlayer) == false)
                return;

            Broadcast(PacketSender.S_LeaveGame(leavePlayer));

            _players.Remove(objectId);
            leavePlayer.Session = null;
            leavePlayer.Room = null;

            if(_players.Count == 0)
            {
                RoomManager.Instance.Remove(RoomId);
            }
        }

        public void EndGame(Player losePlayer)
        {
            SetStageState(StageState.End);
            Broadcast(PacketSender.S_EndGame(losePlayer));
        }

        public void PlaceUnit(ClientSession session, int unitTid, PositionInfo posInfo)
        {
            if(session == null) 
                return;

            Player player = session.MyPlayer;
            if (player == null)
                return;

            // 인구수 체크
            if (player.Population >= Common.GameDefaultInfo.MaxPopulaion)
                return;

            // 배치 가능 여부 체크
            Vector2Int pos = new Vector2Int(posInfo.PosX, posInfo.PosY);
            if (Map.FindPlaceCell(pos, 1, player.PlayerInfo.TeamType, ref pos) == false)
            {
                // 배치 불가 타일
                return;
            }

            // 배치
            if(player.AddUnit(unitTid, pos, out Unit unit) == false)
            {
                // 코스트 부족
                return;
            }

            // 브로드캐스팅
            Broadcast(PacketSender.S_PlaceUnit(unit));
            Util.ObjectLog("PlaceUnit", "", LogColor.Green, unit.Info);
            player.Resource -= unit.ResourceCost;
            UpdateSync(true);
            unit.Update();
        }

        public void LevelUpTower(ClientSession session, long uid)
        {
            if (session == null)
                return;

            Player player = session.MyPlayer;
            if (player == null)
                return;

            player.LevelUpTower(uid);
        }

        public void Nuclear(ClientSession session)
        {
            if (session == null)
                return;

            Player player = session.MyPlayer;
            if (player == null)
                return;

            if (player.NuclearCount <= 0)
                return;

            List<GameObjectInfo> destoryObjects = new List<GameObjectInfo>();
            foreach(var otherPlayer in _players.Values)
            {
                if (otherPlayer.Id == player.Id)
                    continue;


                var units = otherPlayer.AllUnit;
                foreach (Unit unit in units)
                {
                    unit.Hp = 0;
                    destoryObjects.Add(unit.Info);
                }
            }
            Broadcast(PacketSender.S_Nuclear(destoryObjects));
            player.NuclearCount--;
        }

        public void Sync()
        {
            foreach (Player player in _players.Values)
            {
                player.Session.Send(new S_Sync
                {
                    Player = player.PlayerInfo,
                    Resource = player.Resource
                });
            }
        }

        public Player GetPlayer(int playerId)
        {
            _players.TryGetValue(playerId, out Player player);
            return player;
        }

        public GameObject GetGameObject(GameObjectInfo objectInfo)
        {
            Player player = GetPlayer(objectInfo.PlayerInfo.PlayerId);
            if (player == null)
                return null;

            return player.GetGameObject(objectInfo);
        }

        public void Broadcast(IMessage packet, Player passPlayer = null)
        {
            foreach (Player player in _players.Values)
            {
                if (passPlayer == player)
                    continue;

                player.Session.Send(packet);
            }
        }
    }
}
