﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ServerCore;

namespace Server
{
    struct JobTimerElem : IComparable<JobTimerElem>
    {
        public long execTick; // 실행시간
        public IJob job;

        public int CompareTo(JobTimerElem other)
        {
            return (int)(execTick - other.execTick);
        }
    }
    class JobTimer
    {
        PriorityQueue<JobTimerElem> _pq = new PriorityQueue<JobTimerElem>();
        object _lock = new object();

        public void Push(IJob job, long tickAfter = 0)
        {
            JobTimerElem jobElem;
            jobElem.execTick = (Environment.TickCount64 + tickAfter);
            jobElem.job = job;

            lock (_lock)
            {
                _pq.Push(jobElem);
            }
        }

        public void Flush()
        {
            while (true)
            {
                long now = Environment.TickCount64;

                JobTimerElem jobElem;
                lock (_lock)
                {
                    if (_pq.Count == 0)
                        break;

                    jobElem = _pq.Peek();
                    if (jobElem.execTick > now)
                        break;

                    _pq.Pop();
                }

                jobElem.job.Execute();
            }
        }
    }
}
