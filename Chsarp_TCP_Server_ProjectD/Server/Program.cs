﻿using System.Net;
using CommonScripts;
using ServerCore;

namespace Server
{
    class Program
    {
        static Listener _listener = new Listener();
        public static string DATA_ROOT = "";

        static async Task Main(string[] args)
        {
            if(args.Length == 0)
            {
                DATA_ROOT = "../../../Data";
            } 
            else if(args[0] == "EC2")
            {
                DATA_ROOT = "./Data";
            }
            DataManager.LoadAllDesignDB();

            // DNS (Domain Name System)
            // 172.1.2.3 => 이러면 ip 주소가 바뀌면 하드코딩된 ip주소를 바꿔줘야됨
            // www.ooo.com 이렇게 도메인 주소로 돼 있으면 도메인에 연결된 ip만 바꿔주면됨 
            string host = Dns.GetHostName();
            IPHostEntry ipHost = Dns.GetHostEntry(host);
            IPAddress ipAddr = ipHost.AddressList[0]; // 트레픽이 많은 경우 여러 ip로 분산처리 하기때문에 List를 반환함
            IPEndPoint endPoint = new IPEndPoint(ipAddr, 7777);

            _listener.Init(endPoint, () => SessionManager.Instance.Generate());
            Util.Log("Main", "Listening...");

            await Task.Delay(-1);
        }
    }
}