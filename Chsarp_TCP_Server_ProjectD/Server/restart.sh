#!/bin/bash

# .NET Core TCP 서버 프로세스 ID를 찾아서 종료
SERVER_PORT=7777
pkill -f "dotnet run"
rm server_output.log

# 잠시 대기 (옵션)
sleep 2

# .NET Core TCP 서버 실행 (백그라운드)
nohup dotnet run -c Release -- EC2 > server_output.log 2>&1 &

echo "TCP server restarted on port $SERVER_PORT"