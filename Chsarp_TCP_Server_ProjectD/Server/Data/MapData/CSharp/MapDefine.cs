using System;
using System.Collections.Generic;
namespace MapDefine
{
    public enum TILE_TYPE 
    {
        NONE,
        MOVE,
        PLACE,
        TOWER,
        WAYPOINT,
    };
    [System.Serializable]
    public struct MapDefine
    {
        public Vector2Int Size;
        public List<Vector2Int> LeftTowerPosition;
        public List<Vector2Int> RightTowerPosition;
        public List<Vector2Int> WayPoint;
    }
    [System.Serializable]
    public struct TileMapInfo
    {
        public Vector2Int Position;      //셀좌표index
        public byte TileType;      //타일상태 1 - true(Ground) 0 - false(이동불가상태상태)
    }

    [System.Serializable]
    public struct Vector2Int
    {
        public int x;
        public int y;
        public Vector2Int(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        private static readonly Vector2Int _up = new Vector2Int(0, 1);
        private static readonly Vector2Int _down = new Vector2Int(0, -1);
        private static readonly Vector2Int _left = new Vector2Int(-1, 0);
        private static readonly Vector2Int _right = new Vector2Int(1, 0);

        public static Vector2Int up => _up;
        public static Vector2Int down => _down;
        public static Vector2Int left => _left;
        public static Vector2Int right => _right;
        public static Vector2Int operator +(Vector2Int a, Vector2Int b)
        {
            return new Vector2Int(a.x + b.x, a.y + b.y);
        }

        public static bool operator ==(Vector2Int a, Vector2Int b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(Vector2Int a, Vector2Int b)
        {
            return a.x != b.x || a.y != b.y;
        }

        public override bool Equals(object? obj)
        {
            return obj is Vector2Int other && x == other.x && y == other.y;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(x, y);
        }

        public override string ToString()
        {
            return $"({x}, {y})";
        }
    }

    [System.Serializable]
    public struct Vector2
    {
        public float x;
        public float y;
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
public enum Brush_Mode { None, Brush, Eraser, Refresh };
public enum Layer_Mode { None, Tile, Object, Deco, Place };
