using Google.Protobuf.Protocol;
using System;

#if UNITY_EDITOR
using UnityEngine;
#endif

namespace CommonScripts
{
    public enum LogColor
    {
        Black,
        DarkBlue,
        DarkGreen,
        DarkCyan,
        DarkRed,
        DarkMagenta,
        DarkYellow,
        Gray,
        DarkGray,
        Blue,
        Green,
        Cyan,
        Red,
        Magenta,
        Yellow,
        White
    }

    public static class Util
    {
        public static void Log(object msg)
        {
            Log(msg, LogColor.White);
        }
        public static void Log(object msg, LogColor titleColor)
        {
            Log("Common", msg, titleColor);
        }
        public static void Log(object title, object msg)
        {
            Log(title, msg, LogColor.White);
        }


        public static void Log(object title, object msg, LogColor titleColor)
        {
#if UNITY_EDITOR
            Debug.Log($":{DateTime.Now.ToString("fff")}\n[<color={titleColor}>{title}</color>] {msg}");
#else
            Console.Write($"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss:fff")}]");
            Console.ForegroundColor = (ConsoleColor)titleColor;
            Console.Write($"[{title}]");
            Console.ResetColor();
            Console.WriteLine(msg);
#endif
        }

        public static void ObjectLog(object title, object msg, LogColor titleColor, GameObjectInfo objectInfo)
        {
            string log = $"UID({objectInfo.Uid})|State({objectInfo.State})|Team({objectInfo.PlayerInfo.TeamType})| : {msg}";
            Log(title, log, titleColor);
        }
    }
}
