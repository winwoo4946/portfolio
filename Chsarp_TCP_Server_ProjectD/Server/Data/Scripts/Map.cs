using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;
using Google.Protobuf;
using Google.Protobuf.Protocol;
using MapDefine;
using ServerCore;

namespace CommonScripts
{
    public struct Pos
    {
        public Pos(int y, int x) { Y = y; X = x; }
        public int Y;
        public int X;
    }

    public struct PQNode : IComparable<PQNode>
    {
        public int F;
        public int G;
        public int Y;
        public int X;

        public int CompareTo(PQNode other)
        {
            if (F == other.F)
                return 0;
            return F < other.F ? 1 : -1;
        }
    }

    public class Map
    {
        public int MinX { get; set; }
        public int MaxX { get; set; }
        public int MinY { get; set; }
        public int MaxY { get; set; }

        public int SizeX => _mapDefine.Size.x;
        public int SizeY => _mapDefine.Size.y;

        public List<Vector2Int> LeftTowerPosition => _mapDefine.LeftTowerPosition;
        public List<Vector2Int> RightTowerPosition => _mapDefine.RightTowerPosition;
        public List<Vector2Int> WayPoint => _mapDefine.WayPoint;                        //경유지

        MapDefine.MapDefine _mapDefine;
        // [x,y]
        TileMapInfo[,] _tileMapInfos;

        // [y, x]
        TILE_TYPE[,] _tileType;
        List<GameObjectInfo>[,] _objects;

        public void LoadMap(MapDefine.MapDefine define, List<TileMapInfo> list)
        {
            _mapDefine = define;
            _tileMapInfos = new TileMapInfo[_mapDefine.Size.x, _mapDefine.Size.y];
            foreach (TileMapInfo tilemapInfo in list)
            {
                _tileMapInfos[tilemapInfo.Position.x, tilemapInfo.Position.y] = tilemapInfo;
            }

            Init(list);
        }

        public void Init(List<TileMapInfo> mapInfoList)
        {
            MinX = 0;
            MinY = 0;
            MaxX = SizeX - 1;
            MaxY = SizeY - 1;

            _tileType = new TILE_TYPE[SizeY, SizeX];
            _objects = new List<GameObjectInfo>[SizeY, SizeX];

            for (int y = 0; y < SizeY; y++)
            {
                for (int x = 0; x < SizeX; x++)
                {
                    TileMapInfo info = mapInfoList[y * SizeX + x];
                    _tileType[y, x] = (TILE_TYPE)info.TileType;
                    _objects[y, x] = new List<GameObjectInfo>();
                }
            }
        }

        public TILE_TYPE GetTileType(Vector2Int vec)
        {
            Pos pos = Vec2Pos(vec);
            return _tileType[pos.Y, pos.X];
        }

        public bool CanGo(Vector2Int vec, Vector2Int? prevVec = null)
        {
            if (IsMoveableTile(vec) == false)
                return false;

            // 대각선 이동인 경우 두 인접한 셀을 검사
            if (prevVec.HasValue)
            {
                int deltaY = vec.y - prevVec.Value.y;
                int deltaX = vec.x - prevVec.Value.x;

                if (Math.Abs(deltaY) == 1 && Math.Abs(deltaX) == 1)
                {
                    Vector2Int adjacent1 = new Vector2Int(vec.x, prevVec.Value.y);
                    Vector2Int adjacent2 = new Vector2Int(prevVec.Value.x, vec.y);
                    if (!IsMoveableTile(adjacent1) || !IsMoveableTile(adjacent2))
                        return false;
                }
            }

            return true;
        }

        public bool CanPlace(Vector2Int vec, TeamType teamType)
        {
            if (IsOutOfBound(vec) == true)
                return false;

            if (GetTileType(vec) != TILE_TYPE.PLACE)
                return false;

            // 진형 타입별로 배치가능위치 체크
            int half = SizeX / 2;
            if (teamType == TeamType.Left && vec.x >= half)
                return false;

            if (teamType == TeamType.Right && vec.x <= half)
                return false;

            return true;
        }

        private bool IsMoveableTile(Vector2Int vec)
        {
            if (IsOutOfBound(vec) == true)
                return false;

            TILE_TYPE tileType = GetTileType(vec);
            // 이동 불가타일
            return tileType != TILE_TYPE.NONE;
        }

        /// <summary>
        /// pos을 중심으로 주변에 배치 가능한 Cell을 찾는다
        /// </summary>
        /// <param name="pos">중심 Cell</param>
        /// <param name="foundVec">배치 가능한 타일</param>
        public bool FindPlaceCell(Vector2Int pos, int range, TeamType teamType, ref Vector2Int foundVec)
        {
            foundVec = pos;
            if (CanPlace(pos, teamType))
                return true;

            for (int x = -range; x <= range; x++)
            {
                for (int y = -range; y <= range; y++)
                {
                    // Skip the center unit itself
                    if (IsOutOfBound(pos))
                        continue;

                    int checkX = pos.x + x;
                    int checkY = pos.y + y;

                    Vector2Int checkVec = new Vector2Int(checkX, checkY);
                    if (CanPlace(checkVec, teamType) == false)
                    {
                        // 배치 불가 타일
                        continue;
                    }

                    foundVec = checkVec;
                    return true;
                }
            }

            return false;
        }


        public List<GameObjectInfo> Find(Vector2Int vec)
        {
            if (IsOutOfBound(vec) == true)
                return null;

            Pos pos = Vec2Pos(vec);
            return _objects[pos.Y, pos.X];
        }

        public Vector2Int FindAroundEmptyCell(Vector2Int vec)
        {
            if (IsOutOfBound(vec) == true)
                return vec;

            const int RANGE = 1;
            for(int x = 0; x <= RANGE; ++x) // 뒤는 체크 안함
            {
                for(int y = -RANGE; y <= RANGE; ++y) // 상, 하는 다체크함
                {
                    int checkX = vec.x + x;
                    int checkY = vec.y + y;
                    if (checkX == 0 && checkY == 0)
                        continue;

                    Vector2Int checkPos = new Vector2Int(checkX, checkY);
                    if (IsMoveableTile(checkPos) == false)
                    {
                        // 이동 불가 타일
                        continue;
                    }

                    Pos v2p = Vec2Pos(checkPos);
                    List<GameObjectInfo> inCellObjects = _objects[v2p.Y, v2p.X];
                    if (inCellObjects.Count == 0)
                        return checkPos;
                }
            }

            return vec;
        }

        public void AddObjectInCell(Vector2Int vec, GameObjectInfo cellObject)
        {
            if (IsOutOfBound(vec) == true)
                return;

            Pos pos = Vec2Pos(vec);
            _objects[pos.Y, pos.X].Add(cellObject);
        }

        public void RemoveObjectInCell(Vector2Int vec, GameObjectInfo cellObject)
        {
            if (IsOutOfBound(vec) == true)
                return;

            Pos pos = Vec2Pos(vec);
            _objects[pos.Y, pos.X].Remove(cellObject);
        }

        public string PrintMap()
        {
            string print = "";
            for (int i = _objects.GetLength(0) - 1; i >= 0; --i)
            {
                for (int j = 0; j < _objects.GetLength(1); ++j)
                {
                    if (_objects[i, j].Count > 0)
                    {
                        print += $"  {_objects[i, j].Count}  |";
                        continue;
                    }

                    string type = "";
                    switch(_tileType[i, j])
                    {
                        case TILE_TYPE.MOVE:
                            type = "M";
                            break;
                        case TILE_TYPE.NONE:
                            type = "N";
                            break;
                        case TILE_TYPE.PLACE:
                            type = "P";
                            break;
                        case TILE_TYPE.TOWER:
                            type = "T";
                            break;
                        case TILE_TYPE.WAYPOINT:
                            type = "W";
                            break;
                    }
                    print += $"  {type}  |";
                }
                print += "\n";
            }
            return print;
        }

        public bool IsOutOfBound(Vector2Int vec)
        {
            if (vec.x < 0 || vec.y < 0)
                return true;

            if (vec.x > MaxX || vec.y > MaxY)
                return true;

            return false;
        }

        public GameObjectInfo GetEnemyInCell(Vector2Int pos, TeamType team)
        {
            List<GameObjectInfo> list = Find(pos);
            return list.Find(f => f.PlayerInfo.TeamType != team);
        }

        public Vector2Int NextPosition(Queue<Vector2Int> pathQueue, TeamType team, GameObjectInfo info, out GameObjectInfo enemyInfo)
        {
            Vector2Int currentPos = new Vector2Int(info.CurrentPos.PosX, info.CurrentPos.PosY);
            Vector2Int destPos = new Vector2Int(info.DestPos.PosX, info.DestPos.PosY);
            UnitState state = info.State;
            enemyInfo = null;

            Vector2Int nextPos = currentPos;
            while (pathQueue.Count > 0)
            {
                Vector2Int next = pathQueue.Dequeue();
                if (SqrDistance(next, currentPos) == 0)
                {
                    // 동일한 위치는 패스
                    continue;
                }

                enemyInfo = GetEnemyInCell(next, team);
                if (enemyInfo != null)
                {
                    // 다음 이동타일에 적이있으면 패스 
                    pathQueue.Clear();
                    Util.ObjectLog("NextPosPass", $"Current : {currentPos} | Next : {next} | Dest : {destPos} | State : {state}", LogColor.Gray, info);
                    return nextPos;
                }

                nextPos = next;
                break;
            }

            RemoveObjectInCell(currentPos, info); // 이전 cell 삭제
            AddObjectInCell(nextPos, info); // 이동한 cell 추가
            return nextPos;
        }

        /// <summary>
        /// Pos 중심으로 외곽부터 탐색 후 해당 셀에 있는 오브젝트 List 반환
        /// </summary>
        /// <param name="pos">탐색 중심</param>
        /// <param name="range">탐색 범위 ex) 2 -> 8방향 2칸씩</param>
        /// <returns></returns>
        public List<GameObjectInfo> FindObjectByRange(Vector2Int pos, int range, TeamType teamType)
        {
            List<GameObjectInfo> foundObjs = new List<GameObjectInfo>();
            for (int r = range; r >= 1; --r)
            {
                for (int x = -r; x <= r; x++)
                {
                    for (int y = -r; y <= r; y++)
                    {
                        if (Math.Abs(x) != r && Math.Abs(y) != r) // range를 확장하면서 가장자리만 검사
                            continue;

                        int checkX = pos.x + x;
                        int checkY = pos.y + y;
                        Vector2Int checkPos = new Vector2Int(checkX, checkY);
                        if (IsMoveableTile(checkPos) == false)
                        {
                            // 이동 불가 타일
                            continue;
                        }

                        Pos v2p = Vec2Pos(checkPos);
                        List<GameObjectInfo> inCellObjects = _objects[v2p.Y, v2p.X];
                        if (inCellObjects.Count == 0)
                            continue;

                        foreach (var obj in inCellObjects)
                        {
                            if (obj.PlayerInfo.TeamType != teamType)
                                continue;

                            foundObjs.Add(obj);
                        }

                        if (foundObjs.Count > 0)
                            break;
                    }
                }
            }

            return foundObjs;
        }

        /// <summary>
        /// 타겟 주변 1칸중 가장 가까운 위치
        /// </summary>
        public Vector2Int FindClosestTargetAroundPosition(Vector2Int target, Vector2Int current)
        {
            if (IsOutOfBound(target) == true)
                return target;

            Vector2Int found = target;
            const int RANGE = 1;
            int closestDist = int.MaxValue;
            for (int x = -RANGE; x <= RANGE; ++x)
            {
                for (int y = -RANGE; y <= RANGE; ++y)
                {
                    int checkX = target.x + x;
                    int checkY = target.y + y;
                    if (checkX == 0 && checkY == 0)
                        continue;

                    Vector2Int checkPos = new Vector2Int(checkX, checkY);
                    if (IsMoveableTile(checkPos) == false)
                    {
                        // 이동 불가 타일
                        continue;
                    }

                    // 현재위치와 타겟의 주변좌표 거리 비교
                    int dist = SqrDistance(checkPos, current);
                    if(dist < closestDist)
                    {
                        closestDist = dist;
                        found = checkPos;
                    }
                }
            }
            return found;
        }

        public Vector2Int FindClosestWayPoint(Vector2Int pos, TeamType teamType)
        {
            int minDistanceSquared = int.MaxValue;
            Vector2Int closestWaypoint = pos;

            foreach (var waypoint in WayPoint)
            {
                int xDir = pos.x - waypoint.x;
                if ((teamType == TeamType.Left && xDir >= 0) ||
                    (teamType == TeamType.Right && xDir <= 0))
                {
                    continue;
                }

                int distanceSquared = SqrDistance(pos, waypoint);

                if (distanceSquared < minDistanceSquared)
                {
                    minDistanceSquared = distanceSquared;
                    closestWaypoint = waypoint;
                }
            }

            return closestWaypoint;
        }

        public static int SqrDistance(Vector2Int pos1, Vector2Int pos2)
        {
            int distX = pos2.x - pos1.x;
            int distY = pos2.y - pos1.y;
            return (distX * distX) + (distY * distY);
        }

        #region A* PathFinding

        // U D L R LU LD RU RD (LU = Left-Up, LD = Left-Down, RU = Right-Up, RD = Right-Down)
        int[] _deltaY = new int[] { 1, -1, 0, 0, -1, 1, -1, 1 };
        int[] _deltaX = new int[] { 0, 0, -1, 1, -1, -1, 1, 1 };
        int[] _cost = new int[] { 10, 10, 10, 10, 12, 12, 12, 12 };

        public List<Vector2Int> FindPath(Vector2Int startVec, Vector2Int destVec)
        {
            // 점수 매기기
            // F = G + H
            // F = 최종 점수 (작을 수록 좋음, 경로에 따라 달라짐)
            // G = 시작점에서 해당 좌표까지 이동하는데 드는 비용 (작을 수록 좋음, 경로에 따라 달라짐)
            // H = 목적지에서 얼마나 가까운지 (작을 수록 좋음, 고정)

            // (y, x) 이미 방문했는지 여부 (방문 = closed 상태)
            bool[,] closed = new bool[SizeY, SizeX]; // CloseList

            // (y, x) 가는 길을 한 번이라도 발견했는지
            // 발견X => MaxValue
            // 발견O => F = G + H
            int[,] open = new int[SizeY, SizeX]; // OpenList
            for (int y = 0; y < SizeY; y++)
                for (int x = 0; x < SizeX; x++)
                    open[y, x] = int.MaxValue;

            Pos[,] parent = new Pos[SizeY, SizeX];

            // 오픈리스트에 있는 정보들 중에서, 가장 좋은 후보를 빠르게 뽑아오기 위한 도구
            PriorityQueue<PQNode> pq = new PriorityQueue<PQNode>();

            Pos pos = Vec2Pos(startVec);
            Pos dest = Vec2Pos(destVec);

            // 시작점 발견 (예약 진행)
            int f = 10 * (Math.Abs(dest.Y - pos.Y) + Math.Abs(dest.X - pos.X));
            open[pos.Y, pos.X] = f;
            pq.Push(new PQNode() { F = f, G = 0, Y = pos.Y, X = pos.X });
            parent[pos.Y, pos.X] = new Pos(pos.Y, pos.X);

            while (pq.Count > 0)
            {
                // 제일 좋은 후보를 찾는다
                PQNode node = pq.Pop();
                // 동일한 좌표를 여러 경로로 찾아서, 더 빠른 경로로 인해서 이미 방문(closed)된 경우 스킵
                if (closed[node.Y, node.X])
                    continue;

                // 방문한다
                closed[node.Y, node.X] = true;
                // 목적지 도착했으면 바로 종료
                if (node.Y == dest.Y && node.X == dest.X)
                    break;

                // 상하좌우 등 이동할 수 있는 좌표인지 확인해서 예약(open)한다
                Vector2Int curVec = new Vector2Int(node.X, node.Y);
                for (int i = 0; i < _deltaY.Length; i++)
                {
                    Pos next = new Pos(node.Y + _deltaY[i], node.X + _deltaX[i]);
                    Vector2Int nextVec = Pos2Vec(next);

                    // 유효 범위를 벗어났으면 스킵
                    // 벽으로 막혀서 갈 수 없으면 스킵
                    if (next.Y != dest.Y || next.X != dest.X)
                    {
                        if (CanGo(nextVec, curVec) == false)
                            continue;
                    }

                    // 이미 방문한 곳이면 스킵
                    if (closed[next.Y, next.X])
                        continue;

                    // 비용 계산
                    int g = node.G + _cost[i];
                    int h = _cost[i] * ((dest.Y - next.Y) * (dest.Y - next.Y) + (dest.X - next.X) * (dest.X - next.X));
                    // 다른 경로에서 더 빠른 길 이미 찾았으면 스킵
                    if (open[next.Y, next.X] < g + h)
                        continue;

                    // 예약 진행
                    open[dest.Y, dest.X] = g + h;
                    pq.Push(new PQNode() { F = g + h, G = g, Y = next.Y, X = next.X });
                    parent[next.Y, next.X] = new Pos(node.Y, node.X);
                }
            }

            return CalcCellPathFromParent(parent, dest);
        }

        List<Vector2Int> CalcCellPathFromParent(Pos[,] parent, Pos dest)
        {
            List<Vector2Int> vectors = new List<Vector2Int>();

            int y = dest.Y;
            int x = dest.X;
            while (parent[y, x].Y != y || parent[y, x].X != x)
            {
                vectors.Add(Pos2Vec(new Pos(y, x)));
                Pos pos = parent[y, x];
                y = pos.Y;
                x = pos.X;
            }
            vectors.Add(Pos2Vec(new Pos(y, x)));
            vectors.Reverse();

            return vectors;
        }

        Pos Vec2Pos(Vector2Int vec)
        {
            return new Pos(vec.y, vec.x);
        }

        Vector2Int Pos2Vec(Pos pos)
        {
            return new Vector2Int(pos.X, pos.Y);
        }

        #endregion
    }
}
