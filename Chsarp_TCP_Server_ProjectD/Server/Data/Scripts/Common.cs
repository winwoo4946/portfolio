using DesignData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonScripts
{
    public static class Common
    {
        public static GameDefaultInfo GameDefaultInfo;
        public static float GetUpdateInterval(float value)
        {
            return GameDefaultInfo.UpdatePerMs / value;
        }

        public static int LevelPerValue(int level, int value)
        {
            return (level - 1) * value;
        }
    }

    public static class TowerCommon
    {
        public static int MaxResource(BuildingInfo info, int level)
        {
            return info.ResourceDefaultMax + Common.LevelPerValue(level, info.LevelPerResource);
        }

        public static float ResourceProduction(BuildingInfo info, int level, int maxResource = 0)
        {
            if(maxResource == 0)
            {
                maxResource = MaxResource(info, level);
            }
            return maxResource * ResourceProductionRate(level, info.ResourceRate, info.LevelPerResourceRate);
        }

        private static float ResourceProductionRate(int level, int resourceRate, int levelPerResourceRate)
        {
            return (float)(resourceRate + Common.LevelPerValue(level, levelPerResourceRate)) / 1000;
        }

        public static int LevelUpCost(int maxResource)
        {
            return (int)((float)maxResource / 2);
        }
    }
}
