﻿using Google.Protobuf;
using Google.Protobuf.Protocol;
using Server;
using Server.Game;
using ServerCore;
using System;
using System.Collections.Generic;
using System.Numerics;
using System.Text;

class PacketHandler
{
    public static void C_MatchGameHandler(PacketSession session, IMessage packet)
    {
        ClientSession clientSession = session as ClientSession;
        if (clientSession == null)
            return;

        Player player = clientSession.MyPlayer;
        if (player == null)
            return;

        C_MatchGame matchPacket = packet as C_MatchGame;
        string name = matchPacket.Name;
        player.Name = string.IsNullOrEmpty(name) ? $"player-{player.Id}" : name;
        clientSession.Send(PacketSender.S_MatchGame(player));
        // TODO 매칭풀 등록
        // 매칭 후 방 생성
        // 매칭 대상자들에게 플레이어 정보 전달 -> S_EnterGame
        RoomManager.Instance.AddMatchingPool(player);
    }
    public static void C_EnterGameHandler(PacketSession session, IMessage packet)
    {
        ClientSession clientSession = session as ClientSession;
    }

    public static void C_ReadyGameHandler(PacketSession session, IMessage packet)
    {
        ClientSession clientSession = session as ClientSession;
        Player player = clientSession.MyPlayer;
        if (player == null)
            return;

        GameRoom room = player.Room;
        if (room == null)
            return;

        room.Push(room.ReadyGame, player);
    }

    public static void C_LeaveGameHandler(PacketSession session, IMessage packet)
    {
        ClientSession clientSession = session as ClientSession;
        Player player = clientSession.MyPlayer;
        if (player != null)
        {
            RoomManager.Instance.RemoveMachingPool(player.Id);
            GameRoom room = player.Room;
            if (room != null)
            {
                room.Push(room.LeaveGame, player.Id);
            }
        }
        clientSession.Disconnect();
    }

    public static void C_PlaceUnitHandler(PacketSession session, IMessage packet)
    {
        C_PlaceUnit unitDeployPacket = packet as C_PlaceUnit;
        ClientSession clientSession = session as ClientSession;

        Player player = clientSession.MyPlayer;
        if (player == null)
            return;

        GameRoom room = player.Room;
        if (room == null)
            return;

        room.Push(room.PlaceUnit, clientSession, unitDeployPacket.UnitTid, unitDeployPacket.PosInfo);
    }

    public static void C_CheatHandler(PacketSession session, IMessage packet)
    {
        C_Cheat cheatPacket = packet as C_Cheat;
        ClientSession clientSession = session as ClientSession;
        Player player = clientSession.MyPlayer;
        if (player == null)
            return;

        GameRoom room = player.Room;
        if (room == null)
            return;

        // TODO 라이브에선 예외처리 해야됨
        if (cheatPacket.Cheat == "SMTM")
        {
            // 자원량 증가
            room.Push((p) =>
            {
                p.Resource = 10000;
                p.Session.Send(new S_Cheat());
                room.UpdateSync(true);
            }, player);
        }
    }

    public static void C_LevelUpTowerHandler(PacketSession session, IMessage packet)
    {
        C_LevelUpTower levelUpTowerPacket = packet as C_LevelUpTower;
        ClientSession clientSession = session as ClientSession;
        Player player = clientSession.MyPlayer;
        if (player == null)
            return;

        GameRoom room = player.Room;
        if (room == null)
            return;

        room.Push(room.LevelUpTower, clientSession, levelUpTowerPacket.Uid);
    }

    public static void C_NuclearHandler(PacketSession session, IMessage packet)
    {
        C_Nuclear nuclearPacket = packet as C_Nuclear;
        ClientSession clientSession = session as ClientSession;
        Player player = clientSession.MyPlayer;
        if (player == null)
            return;

        GameRoom room = player.Room;
        if (room == null)
            return;

        room.Push(room.Nuclear, clientSession);
    }
}
