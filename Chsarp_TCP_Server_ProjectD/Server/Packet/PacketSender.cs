﻿using CommonScripts;
using Google.Protobuf;
using Google.Protobuf.Protocol;
using Server.Game;

class PacketSender
{
    public static IMessage S_MatchGame(Player player)
    {
        S_MatchGame packet = new S_MatchGame
        {
            Player = player.PlayerInfo
        };
        return packet;
    }

    public static IMessage S_EnterGame(List<Player> players)
    {
        S_EnterGame packet = new S_EnterGame();
        foreach(Player player in players)
        {
            packet.Players.Add(player.PlayerInfo);
            packet.Towers.Add(player.Tower.Info);
        }
        return packet;
    }

    public static IMessage S_ReadyGame(StageState state)
    {
        S_ReadyGame packet = new S_ReadyGame
        {
            StageState = state
        };
        return packet;
    }

    public static IMessage S_LeaveGame(Player leavePlayer)
    {
        S_LeaveGame packet = new S_LeaveGame
        {
            LeavePlayer = leavePlayer.PlayerInfo
        };
        return packet;
    }

    public static IMessage S_MoveSender(GameObjectInfo objectInfo, GameObjectInfo targetUnit)
    {
        S_Move packet = new S_Move
        {
            ObjectInfo = objectInfo,
            TargetObject = targetUnit
        };
        return packet;
    }

    public static IMessage S_PlaceUnit(GameObject unit)
    {
        S_PlaceUnit packet = new S_PlaceUnit
        {
            Unit = unit.Info
        };
        return packet;
    }

    public static IMessage S_AttackGameObject(GameObjectInfo attackObject, List<GameObjectInfo> targetObject)
    {
        S_AttackGameObject packet = new S_AttackGameObject
        {
            AttackObject = attackObject
        };
        packet.TargetObject.AddRange(targetObject);
        return packet;
    }

    public static IMessage S_LevelUpTower(GameObjectInfo towerInfo)
    {
        S_LevelUpTower packet = new S_LevelUpTower
        {
            TowerInfo = towerInfo
        };
        return packet;
    }

    public static IMessage S_Nuclear(List<GameObjectInfo> destoryObjects)
    {
        S_Nuclear packet = new S_Nuclear();
        packet.DestoryObjects.AddRange(destoryObjects);
        return packet;
    }

    public static IMessage S_NuclearCount(int nuclearCount)
    {
        S_NuclearCount packet = new S_NuclearCount
        {
            NuclearCount = nuclearCount
        };
        return packet;
    }

    public static IMessage S_EndGame(Player losePlayer)
    {
        S_EndGame packet = new S_EndGame
        {
            LosePlayer = losePlayer.PlayerInfo
        };
        return packet;
    }
}