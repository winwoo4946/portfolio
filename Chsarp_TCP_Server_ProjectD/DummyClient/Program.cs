﻿using DummyClinet;
using ServerCore;
using System.Net;

namespace DummyClient
{
    class Program
    {
        static void Main(string[] args)
        {
            // DNS (Domain Name System)
            // 172.1.2.3 => 이러면 ip 주소가 바뀌면 하드코딩된 ip주소를 바꿔줘야됨
            // www.ooo.com 이렇게 도메인 주소로 돼 있으면 도메인에 연결된 ip만 바꿔주면됨 
            string host = Dns.GetHostName();
            IPHostEntry ipHost = Dns.GetHostEntry(host);
            IPAddress ipAddr = ipHost.AddressList[0]; // 트레픽이 많은 경우 여러 ip로 분산처리 하기때문에 List를 반환함
            IPEndPoint endPoint = new IPEndPoint(ipAddr, 7777);

            Connector connector = new Connector();
            connector.Connect(endPoint, () => SessionManager.Instance.Generate(), 100);

            while (true)
            {
                try
                {
                    SessionManager.Instance.SendForEach();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                Thread.Sleep(250);
            }
        }
    }
}