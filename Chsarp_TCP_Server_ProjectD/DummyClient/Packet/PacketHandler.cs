﻿using DummyClient;
using ServerCore;
using System;
using System.Collections.Generic;
using System.Text;

class PacketHandler
{
	public static void S_BroadcastEnterHandler(PacketSession session, IPacket packet)
	{
        S_BroadcastEnter p = packet as S_BroadcastEnter;
		ServerSession serverSession = session as ServerSession;
    }

    public static void S_PlayerListHandler(PacketSession session, IPacket packet)
	{
        S_PlayerList p = packet as S_PlayerList;
		ServerSession serverSession = session as ServerSession;
    }

    public static void S_BroadcastLeaveHandler(PacketSession session, IPacket packet)
    {
        S_BroadcastLeave p = packet as S_BroadcastLeave;
        ServerSession serverSession = session as ServerSession;
    }

    public static void S_BroadcastMoveHandler(PacketSession session, IPacket packet)
    {
        S_BroadcastMove p = packet as S_BroadcastMove;
        ServerSession serverSession = session as ServerSession;
    }
}
