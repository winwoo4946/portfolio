﻿using DummyClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DummyClinet
{
    class SessionManager
    {
        static SessionManager _session = new SessionManager();
        public static SessionManager Instance => _session;

        List<ServerSession> _sessions = new List<ServerSession>();
        object _lock = new object();
        Random rand = new Random();
        public void SendForEach()
        {
            lock (_lock)
            {
                foreach (ServerSession session in _sessions)
                {
                    C_Move p = new()
                    {
                        posX = rand.Next(-50, 50),
                        posZ = rand.Next(-50, 50)
                    };
                    session.Send(p.Write());
                }
            }
        }

        public ServerSession Generate()
        {
            lock (_lock)
            {
                ServerSession session = new ServerSession();
                _sessions.Add(session);
                return session;
            }
        }
    }
}
