#pragma once

#include <memory>

namespace lab11
{
	template<typename T>
	class Storage
	{
	public:
		Storage(unsigned int length);
		Storage(unsigned int length, const T& initialValue);
		Storage(const Storage<T>& other);
		Storage<T>& operator=(const Storage<T>& other);
		Storage(Storage<T>&& other);
		Storage<T>& operator=(Storage<T>&& other);

		bool Update(unsigned int index, const T& data);
		const std::unique_ptr<T[]>& GetData() const;
		unsigned int GetSize() const;
	private:
		std::unique_ptr<T[]> mItems;
		unsigned int mSize;
	};

	template<typename T>Storage<T>::Storage(unsigned int length)
		: mSize(length)
	{
		mItems = std::make_unique<T[]>(length);
		memset(mItems.get(), 0, sizeof(T) * length);
	}

	template<typename T>Storage<T>::Storage(unsigned int length, const T& initialValue)
		: mSize(length)
	{
		mItems = std::make_unique<T[]>(length);
		for (size_t i = 0; i < length; i++)
		{
			mItems[i] = initialValue;
		}
	}

	template<typename T>Storage<T>::Storage(const Storage<T>& other)
		: mSize(other.mSize)
	{
		mItems = std::make_unique<T[]>(mSize);
		for (size_t i = 0; i < mSize; i++)
		{
			mItems[i] = other.mItems[i];
		}
	}

	template<typename T>
	Storage<T>& Storage<T>::operator=(const Storage<T>& other)
	{
		if (this == &other)
			return *this;

		mSize = other.mSize;
		mItems.reset(new T[mSize]);
		for (size_t i = 0; i < mSize; i++)
		{
			mItems[i] = other.mItems[i];
		}

		return *this;
	}

	template<typename T>Storage<T>::Storage(Storage<T>&& other)
		: mSize(other.mSize)
		, mItems(std::move(other.mItems))
	{
		other.mItems = nullptr;
		other.mSize = 0;
	}

	template<typename T>
	Storage<T>& Storage<T>::operator=(Storage<T>&& other)
	{
		if (this == &other)
			return *this;

		mItems = nullptr;
		mItems = std::move(other.mItems);
		mSize = other.mSize;

		other.mItems = nullptr;
		other.mSize = 0;

		return *this;
	}

	template<typename T>
	bool Storage<T>::Update(unsigned int index, const T& data)
	{
		if (index >= mSize)
			return false;

		mItems[index] = data;

		return true;
	}

	template<typename T>
	const std::unique_ptr<T[]>& Storage<T>::GetData() const
	{
		return std::move(mItems);
	}

	template<typename T>
	unsigned int Storage<T>::GetSize() const
	{
		return mSize;
	}
}