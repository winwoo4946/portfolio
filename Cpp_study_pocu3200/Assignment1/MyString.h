#pragma once
namespace assignment1
{
	class MyString
	{
	public:
		MyString(const char* s);
		MyString(const MyString& other);
		~MyString();

		unsigned int GetLength() const;
		const char* GetCString() const;
		void Append(const char* s);
		MyString operator+(const MyString& other) const;
		int IndexOf(const char* s);
		int LastIndexOf(const char* s);
		void Interleave(const char* s);
		bool RemoveAt(unsigned int i);
		void PadLeft(unsigned int totalLength);
		void PadLeft(unsigned int totalLength, const char c);
		void PadRight(unsigned int totalLength);
		void PadRight(unsigned int totalLength, const char c);
		void Reverse();
		bool operator==(const MyString& rhs) const;
		MyString& operator=(const MyString& rhs);
		void ToLower();
		void ToUpper();
	private:
		const int PAD_LEFT = 0;
		const int PAD_RIGHT = 1;
		char* mString;
		int mStringLength;

		void pad(unsigned int totalLength, const char c, int padDir);
		void swap(char& a, char& b);
		int getStrLen(const char* s) const;
		void copyMemory(char* dst, const char* src, int size) const;
		void strConcat(char* dst, const char* src) const;
	};
}