#include <cstring>
#include <cmath>
#include "PolyLine.h"

namespace lab4
{
	PolyLine::PolyLine()
		: mCurrentIndex(0)
	{
		memset(mPoints, 0, sizeof(mPoints));
	}

	PolyLine::PolyLine(const PolyLine& other)
		: mCurrentIndex(other.mCurrentIndex)
	{
		memset(mPoints, 0, sizeof(mPoints));
		for (int i = 0; i < other.mCurrentIndex; i++)
		{
			mPoints[i] = new Point(*other.mPoints[i]);
		}
	}

	PolyLine::~PolyLine()
	{
		for (int i = 0; i < MAX_POINT_SIZE; i++)
		{
			delete mPoints[i];
			mPoints[i] = nullptr;
		}
	}

	PolyLine& PolyLine::operator=(const PolyLine& other)
	{
		if (*this == other)
			return *this;

		mCurrentIndex = other.mCurrentIndex;
		for (int i = 0; i < MAX_POINT_SIZE; i++)
		{
			delete mPoints[i];
			mPoints[i] = nullptr;

			if (i < mCurrentIndex)
			{
				mPoints[i] = new Point(*other.mPoints[i]);
			}
		}

		return *this;
	}

	bool PolyLine::operator==(const PolyLine& other) const
	{
		return &mPoints == &other.mPoints && &mCurrentIndex == &other.mCurrentIndex;
	}

	bool PolyLine::AddPoint(float x, float y)
	{
		if (mCurrentIndex >= MAX_POINT_SIZE)
			return false;

		return AddPoint(new Point(x, y));
	}

	bool PolyLine::AddPoint(const Point* point)
	{
		if (mCurrentIndex >= MAX_POINT_SIZE)
			return false;

		mPoints[mCurrentIndex] = point;
		mCurrentIndex++;
		return true;
	}

	bool PolyLine::RemovePoint(unsigned int i)
	{
		if ((signed)i < 0 || (signed)i >= mCurrentIndex)
			return false;

		delete mPoints[i];
		mPoints[i] = nullptr;

		for (int j = i; j < MAX_POINT_SIZE - 1; j++)
		{
			mPoints[j] = mPoints[j + 1];
		}

		mCurrentIndex--;
		return true;
	}

	bool PolyLine::TryGetMinBoundingRectangle(Point* outMin, Point* outMax) const
	{
		if (mCurrentIndex <= 0)
			return false;

		Point min = Point();
		Point max = Point();
		min = *mPoints[0];
		max = *mPoints[0];

		for (int i = 0; i < mCurrentIndex; i++)
		{
			float x = (*mPoints[i]).GetX();
			float y = (*mPoints[i]).GetY();
			if (x < min.GetX())
			{
				min.SetX(x);
			}
			else if (x > max.GetX())
			{
				max.SetX(x);
			}

			if (y < min.GetY())
			{
				min.SetY(y);
			}
			else if (y > max.GetY())
			{
				max.SetY(y);
			}
		}

		*outMin = min;
		*outMax = max;

		return true;
	}

	const Point* PolyLine::operator[](unsigned int i) const
	{
		if ((signed)i < 0 || (signed)i >= mCurrentIndex)
			return NULL;

		return mPoints[i];
	}
}