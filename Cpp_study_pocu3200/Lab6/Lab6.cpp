#include "Lab6.h"

using namespace std;

namespace lab6
{
	int Sum(const std::vector<int>& v)
	{
		if (v.size() == 0)
			return 0;

		int result = v[0];
		for (size_t i = 1; i < v.size(); i++)
		{
			result += v[i];
		}
		return result;
	}

	int Min(const std::vector<int>& v)
	{
		int result = INT32_MAX;
		for (size_t i = 0; i < v.size(); i++)
		{
			if (result > v[i])
				result = v[i];
		}
		return result;
	}

	int Max(const std::vector<int>& v)
	{
		int result = INT32_MIN;
		for (size_t i = 0; i < v.size(); i++)
		{
			if (result < v[i])
				result = v[i];
		}
		return result;
	}

	float Average(const std::vector<int>& v)
	{
		if (v.size() == 0)
			return 0.f;

		return (float)Sum(v) / v.size();
	}

	int NumberWithMaxOccurrence(const std::vector<int>& v)
	{
		if (v.size() == 0)
			return 0;

		if (v.size() == 1)
			return v[0];

		map<int, int> maxOccurrenceMap;
		map<int, int>::iterator it;

		for (size_t i = 0; i < v.size(); i++)
		{
			int key = v[i];
			it = maxOccurrenceMap.find(key);
			if (it == maxOccurrenceMap.end())
			{
				maxOccurrenceMap.insert(make_pair(key, 1));
			}
			else
			{
				maxOccurrenceMap[key]++;
			}
		}

		map<int, int>::iterator result = maxOccurrenceMap.begin();
		for (it = ++maxOccurrenceMap.begin(); it != maxOccurrenceMap.end(); it++)
		{
			if (it->second > result -> second)
			{
				result = it;
			}
		}

		return result->first;
	}

	void SortDescending(std::vector<int>& v)
	{
		if (v.size() == 0)
			return;

		for (size_t i = 0; i < v.size() - 1; i++)
		{
			for (size_t j = i + 1; j < v.size(); j++)
			{
				if (v[i] < v[j])
				{
					v[i] = v[i] ^ v[j];
					v[j] = v[i] ^ v[j];
					v[i] = v[i] ^ v[j];
				}
			}
		}
	}

}