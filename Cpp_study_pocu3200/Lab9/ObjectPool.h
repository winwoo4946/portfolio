#pragma once

#include <unordered_map>
#include <string>
#include <queue>
namespace lab9
{
	template<class T>
	class ObjectPool
	{
	public:
		ObjectPool() = delete;
		ObjectPool(const ObjectPool<T>& other) = delete;
		ObjectPool<T>& operator=(const ObjectPool<T>& other) = delete;
		ObjectPool(size_t maxPoolSize);
		~ObjectPool();

		T* Get();
		void Return(T* obj);
		const size_t GetFreeObjectCount() const;
		const size_t GetMaxFreeObjectCount() const;

	private:
		std::queue<T*> mPool;
		const size_t mMaxPoolSize;
	};

	template<class T>ObjectPool<T>::ObjectPool(size_t maxPoolSize)
		: mMaxPoolSize(maxPoolSize)
	{
	}
	
	template<class T>ObjectPool<T>::~ObjectPool()
	{
		while (mPool.empty() == false)
		{
			delete mPool.front();
			mPool.pop();
		}
	}

	template<class T>
	T* ObjectPool<T>::Get()
	{
		if (mPool.empty() == true)
		{
			return new T();
		}

		T* obj = mPool.front();
		mPool.pop();
		return obj;
	}

	template<class T>
	void ObjectPool<T>::Return(T* obj)
	{
		if (GetFreeObjectCount() >= mMaxPoolSize)
		{
			delete obj;
			return;
		}

		mPool.push(obj);
	}

	template<class T>
	const size_t ObjectPool<T>::GetFreeObjectCount() const
	{
		return mPool.size();
	}

	template<class T>
	const size_t ObjectPool<T>::GetMaxFreeObjectCount() const
	{
		return mMaxPoolSize;
	}
}