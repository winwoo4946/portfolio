#include "Boatplane.h"

namespace assignment2
{
	Boatplane::Boatplane(const Boatplane& other)
		: Vehicle(other)
	{
	}

	Boatplane::Boatplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
	}

	Boatplane::~Boatplane()
	{
	}

	Boatplane& Boatplane::operator=(const Boatplane& other)
	{
		if (other == *this)
			return *this;

		Vehicle::operator=(other);
		return *this;
	}

	bool Boatplane::operator==(const Boatplane& other) const
	{
		return Vehicle::operator==(other);
	}

	bool Boatplane::Travel()
	{
		if (mTravelCount > 0)
		{
			if (mRestCount == 0 || mRestCount % 3 > 0)
			{
				mRestCount++;
				return false;
			}
			else
			{
				mRestCount = 0;
			}
		}

		mTravelCount++;
		return true;
	}

	unsigned int Boatplane::GetMaxSpeed() const
	{
		unsigned int fly = GetFlySpeed();
		unsigned int seil = GetSailSpeed();
		return fly > seil ? fly : seil;
	}

	unsigned int Boatplane::GetFlySpeed() const
	{
		unsigned int totalWeight = GetTotalPassengersWeight();

		int a = 500 - totalWeight;
		double b = a / 300.0;
		double e = pow(EULUR, b);
		double c = 150.0 * e;

		int iSpeed = mymath::Rounds(c);
		return static_cast<unsigned int>(iSpeed);
	}

	unsigned int Boatplane::GetSailSpeed() const
	{
		unsigned int totalPassenger = GetTotalPassengersWeight();
		double a = 1.7 * totalPassenger;
		double speed = 800.0 - a;
		speed = mymath::Max(speed, 20.0);

		int iSpeed = mymath::Rounds(speed);
		return static_cast<unsigned int>(iSpeed);
	}
}