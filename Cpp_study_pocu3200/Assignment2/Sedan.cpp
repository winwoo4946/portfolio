#include "Sedan.h"

namespace assignment2
{
	Sedan::Sedan(const Sedan& other)
		: Vehicle(other)
	{
		mTrailer = new Trailer(*other.mTrailer);
	}

	Sedan::Sedan()
		: Vehicle(4)
	{
	}

	Sedan::~Sedan()
	{
		delete mTrailer;
	}

	Sedan& Sedan::operator=(const Sedan& other)
	{
		if (other == *this)
			return *this;

		Vehicle::operator=(other);
		if (mTrailer != NULL)
		{
			delete mTrailer;
		}

		mTrailer = other.mTrailer;
		return *this;
	}

	bool Sedan::operator==(const Sedan& other) const
	{
		return Vehicle::operator==(other);
	}

	bool Sedan::Travel()
	{
		if (mTrailer == NULL)
		{
			if (mTravelCount > 0 && mTravelCount % 5 == 0)
			{
				if (mRestCount == 0)
				{
					mRestCount++;
					return false;
				}
				else
				{
					mRestCount = 0;
				}
			}

			mTravelCount++;
			return true;
		}
		else
		{
			if (mTravelCount > 0 && mTravelCount % 5 == 0)
			{
				if (mRestCount == 0 || mRestCount % 2 > 0)
				{
					mRestCount++;
					return false;
				}
				else
				{
					mRestCount = 0;
				}
			}

			mTravelCount++;
			return true;
		}
	}

	bool Sedan::AddTrailer(const Trailer* trailer)
	{
		if (mTrailer != NULL)
			return false;

		mTrailer = trailer;
		return true;
	}

	bool Sedan::RemoveTrailer()
	{
		if (mTrailer == NULL)
			return false;

		delete mTrailer;
		mTrailer = NULL;
		return true;
	}

	unsigned int Sedan::GetTotalPassengersWeight() const
	{
		if (mTrailer != NULL)
		{
			return mTrailer->GetWeight() + Vehicle::GetTotalPassengersWeight();
		}
		else
		{
			return Vehicle::GetTotalPassengersWeight();
		}
	}

	unsigned int Sedan::GetMaxSpeed() const
	{
		return GetDriveSpeed();
	}

	unsigned int Sedan::GetDriveSpeed() const
	{
		unsigned int totalWeight = GetTotalPassengersWeight();

		if (totalWeight <= 80)
		{
			return 480;
		}

		if (totalWeight > 80 && totalWeight <= 160)
		{
			return 458;
		}

		if (totalWeight > 160 && totalWeight <= 260)
		{
			return 400;
		}

		if (totalWeight > 260 && totalWeight <= 350)
		{
			return 380;
		}

		return 300;
	}
}