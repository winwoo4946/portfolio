#pragma once

#include "Boatplane.h"
#include "Vehicle.h"
#include "Airplane.h"
#include "ISailable.h"

namespace assignment2
{
	class Airplane;

	class Boat : public Vehicle, public ISailable
	{
	public:
		Boat(unsigned int maxPassengersCount);
		Boat(const Boat& other);
		~Boat();

		Boat& operator=(const Boat& other);
		bool operator==(const Boat& other) const;

		Boatplane operator+(Airplane& plane);

		bool Travel();
		unsigned int GetMaxSpeed() const;
		unsigned int GetSailSpeed() const;
	};
}