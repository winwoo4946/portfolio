#include "Motorcycle.h"

namespace assignment2
{
	Motorcycle::Motorcycle(const Motorcycle& other)
		: Vehicle(other)
	{
	}

	Motorcycle::Motorcycle()
		: Vehicle(2)
	{
	}

	Motorcycle::~Motorcycle()
	{
	}

	Motorcycle& Motorcycle::operator=(const Motorcycle& other)
	{
		if (other == *this)
			return *this;

		Vehicle::operator=(other);
		return *this;
	}

	bool Motorcycle::operator==(const Motorcycle& other) const
	{
		return Vehicle::operator==(other);
	}

	bool Motorcycle::Travel()
	{
		if (mTravelCount > 0 && mTravelCount % 5 == 0)
		{
			if (mRestCount == 0)
			{
				mRestCount++;
				return false;
			}
			else
			{
				mRestCount = 0;
			}
		}

		mTravelCount++;
		return true;
	}

	unsigned int Motorcycle::GetMaxSpeed() const
	{
		return GetDriveSpeed();
	}

	unsigned int Motorcycle::GetDriveSpeed() const
	{
		unsigned int totalWeight = GetTotalPassengersWeight();

		double a = -(totalWeight / 15.0);
		double b = pow(a, 3);
		double c = 2 * static_cast<double>(totalWeight);

		double speed = b + c + 400;
		speed = mymath::Max(speed, 0.f);

		int roundSpeed = mymath::Rounds(speed);
		return roundSpeed;
	}
}