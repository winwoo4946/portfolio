#include "MyMath.h"

namespace mymath
{
	int Floor(double value)
	{
		return static_cast<int>(value);
	}

	int Ceil(double value)
	{
		int intValue = static_cast<int>(value);
		if (value == intValue)
			return intValue;

		return intValue + 1;
	}

	int Rounds(double value)
	{
		return static_cast<int>(value + 0.5);
	}

	int Squre(int value)
	{
		return value * value;
	}

	double Sqrt(double value)
	{
		double x = 2;

		for (int i = 0; i < 10; i++)
		{
			x = (x + (value / x)) / 2;
		}

		return x;
	}

	double Pow(double base, int exp)
	{
		if (exp == 0)
		{
			return 1;
		}

		double n = Pow(base, exp / 2);
		double temp = n * n;

		if (exp % 2 == 0)
		{
			return temp;
		}
		else
		{
			return base * temp;
		}
	}

	double Max(double a, double b)
	{
		return a > b ? a : b;
	}

	double Min(double a, double b)
	{
		return a < b ? a : b;
	}

	int Max(int a, int b)
	{
		return a > b ? a : b;
	}

	int Min(int a, int b)
	{
		return a < b ? a : b;
	}
}