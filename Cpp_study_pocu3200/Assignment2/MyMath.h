#pragma once

namespace mymath
{
	int Floor(double value);
	int Ceil(double value);
	int Rounds(double value);
	int Squre(int value);
	double Sqrt(double value);
	double Pow(double base, int exp);
	double Max(double a, double b);
	double Min(double a, double b);
	int Max(int a, int b);
	int Min(int a, int b);
}