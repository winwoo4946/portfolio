#pragma once

#include "Boatplane.h"
#include "Vehicle.h"
#include "Boat.h"
#include "IFlyable.h"
#include "IDrivable.h"

namespace assignment2
{
	class Boat;

	class Airplane : public Vehicle, public IDrivable, public IFlyable
	{
	public:
		Airplane(const Airplane& other);
		Airplane(unsigned int maxPassengersCount);
		~Airplane();

		Airplane& operator=(const Airplane& other);
		bool operator==(const Airplane& other) const;

		Boatplane operator+(Boat& boat);

		unsigned int GetMaxSpeed() const;
		bool Travel();

		unsigned int GetDriveSpeed() const;
		unsigned int GetFlySpeed() const;
	};
}