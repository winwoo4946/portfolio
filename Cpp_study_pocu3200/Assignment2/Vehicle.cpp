#include "Vehicle.h"

namespace assignment2
{
	Vehicle::Vehicle(const Vehicle& other)
		: mMaxPassengersCount(other.mMaxPassengersCount)
		, mCurrentPassengerCount(other.mCurrentPassengerCount)
		, mTravelCount(other.mTravelCount)
		, mRestCount(other.mRestCount)
	{
		memset(mPassengers, 0, sizeof(mPassengers));
		for (unsigned int i = 0; i < other.mCurrentPassengerCount; i++)
		{
			mPassengers[i] = new Person(*other.mPassengers[i]);
		}
	}

	Vehicle::Vehicle(unsigned int maxPassengersCount)
		: mMaxPassengersCount(maxPassengersCount)
		, mCurrentPassengerCount(0)
		, mTravelCount(0)
		, mRestCount(0)
	{
		memset(mPassengers, 0, sizeof(mPassengers));
	}

	Vehicle::~Vehicle()
	{
		for (unsigned int i = 0; i < MAX_PASSENTERS_COUNT; i++)
		{
			delete mPassengers[i];
			mPassengers[i] = nullptr;
		}
	}

	Vehicle& Vehicle::operator=(const Vehicle& other)
	{
		if (other == *this)
			return *this;

		mMaxPassengersCount = other.mMaxPassengersCount;
		mCurrentPassengerCount = other.mCurrentPassengerCount;
		mTravelCount = other.mTravelCount;
		mRestCount = other.mRestCount;

		for (unsigned int i = 0; i < MAX_PASSENTERS_COUNT; i++)
		{
			delete mPassengers[i];
			mPassengers[i] = nullptr;

			if (i < mCurrentPassengerCount)
			{
				mPassengers[i] = new Person(*other.mPassengers[i]);
			}
		}

		return *this;
	}

	bool Vehicle::operator==(const Vehicle& other) const
	{
		return &mPassengers == &other.mPassengers &&
			&mMaxPassengersCount == &other.mMaxPassengersCount &&
			& mCurrentPassengerCount == &other.mCurrentPassengerCount &&
			& mTravelCount == &other.mTravelCount &&
			& mRestCount == &other.mRestCount;
	}

	bool Vehicle::AddPassenger(const Person* person)
	{
		if (person == NULL)
			return false;

		if (mCurrentPassengerCount >= mMaxPassengersCount)
			return false;

		for (size_t i = 0; i < mCurrentPassengerCount; i++)
		{
			if (mPassengers[i] == person)
				return false;
		}

		mPassengers[mCurrentPassengerCount] = person;
		mCurrentPassengerCount++;
		return true;
	}

	bool Vehicle::RemovePassenger(unsigned int i)
	{
		if (i >= mCurrentPassengerCount)
			return false;

		delete mPassengers[i];
		mCurrentPassengerCount--;

		for (unsigned int j = i; j < mCurrentPassengerCount; j++)
		{
			mPassengers[j] = mPassengers[j + 1];
		}

		mPassengers[mCurrentPassengerCount] = NULL;
		return true;
	}

	unsigned int Vehicle::GetPassengersCount() const
	{
		return mCurrentPassengerCount;
	}

	unsigned int Vehicle::GetMaxPassengersCount() const
	{
		return mMaxPassengersCount;
	}

	unsigned int Vehicle::GetTotalPassengersWeight() const
	{
		unsigned int totalWeight = 0;
		for (unsigned int i = 0; i < mCurrentPassengerCount; i++)
		{
			totalWeight += mPassengers[i]->GetWeight();
		}

		return totalWeight;
	}

	unsigned int Vehicle::GetTravelCount() const
	{
		return mTravelCount;
	}

	unsigned int Vehicle::GetTravelDistance() const
	{
		return mTravelCount * GetMaxSpeed();
	}

	void Vehicle::ResetTravelCount()
	{
		mTravelCount = 0;
		mRestCount = 0;
	}

	const Person* Vehicle::GetPassenger(unsigned int i) const
	{
		if (i >= mCurrentPassengerCount)
			return NULL;

		return mPassengers[i];
	}
}