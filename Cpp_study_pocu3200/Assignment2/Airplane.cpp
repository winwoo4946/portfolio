
#include "Airplane.h"

namespace assignment2
{
	Airplane::Airplane(const Airplane& other)
		: Vehicle(other)
	{
	}

	Airplane::Airplane(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
	}

	Airplane::~Airplane()
	{
	}

	Airplane& Airplane::operator=(const Airplane& other)
	{
		if (other == *this)
			return *this;

		Vehicle::operator=(other);
		return *this;
	}

	bool Airplane::operator==(const Airplane& other) const
	{
		return Vehicle::operator==(other);
	}

	Boatplane Airplane::operator+(Boat& boat)
	{
		Boatplane bp(GetMaxPassengersCount() + boat.GetMaxPassengersCount());

		while (GetPassengersCount() > 0)
		{
			const Person* p = GetPassenger(0);
			bp.AddPassenger(new Person(*p));
			RemovePassenger(0);
		}

		while (boat.GetPassengersCount() > 0)
		{
			const Person* p = boat.GetPassenger(0);
			bp.AddPassenger(new Person(*p));
			boat.RemovePassenger(0);
		}

		return bp;
	}

	bool Airplane::Travel()
	{
		if (mTravelCount > 0)
		{
			if (mRestCount == 0 || mRestCount % 3 > 0)
			{
				mRestCount++;
				return false;
			}
			else
			{
				mRestCount = 0;
			}
		}

		mTravelCount++;
		return true;
	}

	unsigned int Airplane::GetMaxSpeed() const
	{
		unsigned int fly = GetFlySpeed();
		unsigned int drive = GetDriveSpeed();
		return fly > drive ? fly : drive;
	}
	
	unsigned int Airplane::GetFlySpeed() const
	{
		unsigned int totalWeight = GetTotalPassengersWeight();
		
		int a = 800 - totalWeight;
		double b = a / 500.0;
		double e = pow(EULUR, b);
		double c = 200.0 * e;

		return mymath::Rounds(c);
	}

	unsigned int Airplane::GetDriveSpeed() const
	{
		unsigned int totalWeight = GetTotalPassengersWeight();

		int a = 400 - totalWeight;
		double b = a / 70.0;
		double e = pow(EULUR, b);
		double c = 4.0 * e;

		return mymath::Rounds(c);
	}
}