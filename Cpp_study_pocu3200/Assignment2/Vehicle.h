#pragma once
#include <string>
#include <cmath>
#include "Person.h"
#include "MyMath.h"

using namespace std;

namespace assignment2
{
	class Vehicle
	{
	public:
		Vehicle(const Vehicle& other);
		Vehicle(unsigned int maxPassengersCount);
		virtual ~Vehicle();

		virtual Vehicle& operator=(const Vehicle& other);
		virtual bool operator==(const Vehicle& other) const;

		virtual bool Travel() = 0;
		virtual unsigned int GetMaxSpeed() const = 0;
		virtual unsigned int GetTotalPassengersWeight() const;

		bool AddPassenger(const Person* person);
		bool RemovePassenger(unsigned int i);
		const Person* GetPassenger(unsigned int i) const;
		unsigned int GetPassengersCount() const;
		unsigned int GetMaxPassengersCount() const;
		unsigned int GetTravelCount() const;
		unsigned int GetTravelDistance() const;
		void ResetTravelCount();

		const float EULUR = 2.71828182845904523536f;
	protected:
		unsigned int mTravelCount;
		unsigned int mRestCount;
	private:
		enum
		{
			MAX_PASSENTERS_COUNT = 100
		};
		unsigned int mMaxPassengersCount;
		const Person* mPassengers[MAX_PASSENTERS_COUNT];
		unsigned int mCurrentPassengerCount;
	};
}