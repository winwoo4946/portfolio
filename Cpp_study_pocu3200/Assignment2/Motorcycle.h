#pragma once

#include "Vehicle.h"
#include "IDrivable.h"

namespace assignment2
{
	class Motorcycle : public Vehicle, public IDrivable
	{
	public:
		Motorcycle();
		Motorcycle(const Motorcycle& other);
		~Motorcycle();

		Motorcycle& operator=(const Motorcycle& other);
		bool operator==(const Motorcycle& other) const;

		bool Travel();
		unsigned int GetMaxSpeed() const;
		unsigned int GetDriveSpeed() const;
	};
}