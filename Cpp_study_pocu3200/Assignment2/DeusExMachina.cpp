#include "DeusExMachina.h"

//#include <iomanip>
//#include <iostream>
//using namespace std;

namespace assignment2
{
	DeusExMachina::DeusExMachina()
		: mVehicleCount(0)
	{
		memset(mVehicles, NULL, sizeof(mVehicles));
	}

	DeusExMachina::DeusExMachina(const DeusExMachina& other)
		: mVehicleCount(0)
	{
		memset(mVehicles, NULL, sizeof(mVehicles));
	}

	DeusExMachina::~DeusExMachina()
	{
		for (int i = 0; i < mVehicleCount; i++)
		{
			delete mVehicles[i];
		}
	}

	DeusExMachina* DeusExMachina::mInstance = NULL;
	DeusExMachina* DeusExMachina::GetInstance()
	{
		if (mInstance == NULL)
		{
			mInstance = new DeusExMachina();
			atexit(destory);
		}
		return mInstance;
	}

	void DeusExMachina::destory()
	{
		delete mInstance;
	}

	void DeusExMachina::PrintVehicleName() const
	{
		for (int i = 0; i < mVehicleCount; i++)
		{
		} 
		//cout << endl;
	}

	void DeusExMachina::PrintVehicleTravelCount() const
	{
		for (int i = 0; i < mVehicleCount; i++)
		{
			PrintLog(to_string(mVehicles[i]->GetTravelDistance()));
		}
		//cout << endl;
	}

	void DeusExMachina::Travel() const
	{
		for (int i = 0; i < mVehicleCount; i++)
		{
			bool b = mVehicles[i]->Travel();
			if (b == true)
			{
				PrintLog("Y");
			}
			else
			{
				PrintLog("X");
			}
		}
		//cout << endl;
	}

	void DeusExMachina::PrintLog(string str) const
	{
		//cout << setw(5) << left << str;
	}

	bool DeusExMachina::AddVehicle(Vehicle* vehicle)
	{
		if (mVehicleCount >= MAX_VEHICLE)
			return false;

		for (int i = 0; i < mVehicleCount; i++)
		{
			if (mVehicles[i] == vehicle)
				return false;
		}

		mVehicles[mVehicleCount] = vehicle;
		mVehicles[mVehicleCount]->ResetTravelCount();
		mVehicleCount++;

		return true;
	}

	bool DeusExMachina::RemoveVehicle(unsigned int i)
	{
		if (i >= static_cast<unsigned int>(mVehicleCount))
			return false;

		delete mVehicles[i];

		mVehicleCount--;

		for (int j = i; j < mVehicleCount; j++)
		{
			mVehicles[j] = mVehicles[j + 1];
		}

		mVehicles[mVehicleCount] = NULL;
		return true;
	}

	const Vehicle* DeusExMachina::GetFurthestTravelled() const
	{
		if (mVehicleCount <= 0)
			return NULL;

		Vehicle* result = NULL;
		for (int i = 0; i < mVehicleCount; i++)
		{
			if (result == NULL)
			{
				result = mVehicles[i];
				continue;
			}
			
			if (result->GetTravelDistance() < mVehicles[i]->GetTravelDistance())
			{
				result = mVehicles[i];
			}
		}

		return result;
	}

	Vehicle* DeusExMachina::GetVehicle(unsigned int i) const
	{
		if (i >= static_cast<unsigned int>(mVehicleCount))
			return NULL;

		return mVehicles[i];
	}
}