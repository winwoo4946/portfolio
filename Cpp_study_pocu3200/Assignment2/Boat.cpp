#include "Boat.h"

namespace assignment2
{
	Boat::Boat(const Boat& other)
		: Vehicle(other)
	{
	}

	Boat::Boat(unsigned int maxPassengersCount)
		: Vehicle(maxPassengersCount)
	{
	}

	Boat::~Boat()
	{
	}

	Boat& Boat::operator=(const Boat& other)
	{
		if (other == *this)
			return *this;

		Vehicle::operator=(other);
		return *this;
	}

	bool Boat::operator==(const Boat& other) const
	{
		return Vehicle::operator==(other);
	}

	Boatplane Boat::operator+(Airplane& plane)
	{
		Boatplane bp(GetMaxPassengersCount() + plane.GetMaxPassengersCount());

		while (plane.GetPassengersCount() > 0)
		{
			const Person* p = plane.GetPassenger(0);
			bp.AddPassenger(new Person(*p));
			plane.RemovePassenger(0);
		}

		while (GetPassengersCount() > 0)
		{
			const Person* p = GetPassenger(0);
			bp.AddPassenger(new Person(*p));
			RemovePassenger(0);
		}

		return bp;
	}

	bool Boat::Travel()
	{
		if (mTravelCount > 0 && mTravelCount % 2 == 0)
		{
			if (mRestCount == 0)
			{
				mRestCount++;
				return false;
			}
			else
			{
				mRestCount = 0;
			}
		}

		mTravelCount++;
		return true;
	}

	unsigned int Boat::GetMaxSpeed() const
	{
		return GetSailSpeed();
	}

	unsigned int Boat::GetSailSpeed() const
	{
		unsigned int totalPassenger = GetTotalPassengersWeight();
		unsigned int speed = 800 - (10 * totalPassenger);
		speed = mymath::Max(speed, 20);

		int iSpeed = mymath::Rounds(speed);
		return static_cast<unsigned int>(iSpeed);
	}
}