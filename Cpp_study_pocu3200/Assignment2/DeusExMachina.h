#pragma once

#include "Vehicle.h"

namespace assignment2
{
	class DeusExMachina
	{
	public:
		static DeusExMachina* GetInstance();
		void Travel() const;
		bool AddVehicle(Vehicle* vehicle);
		bool RemoveVehicle(unsigned int i);
		const Vehicle* GetFurthestTravelled() const;
		Vehicle* GetVehicle(unsigned int i) const;

		void PrintVehicleName() const;
		void PrintVehicleTravelCount() const;
		void PrintLog(string str) const;
	private:
		DeusExMachina();
		DeusExMachina(const DeusExMachina& other);
		~DeusExMachina();

		static DeusExMachina* mInstance;
		static void destory();
		int mVehicleCount;
		enum
		{
			MAX_VEHICLE = 10
		};

		Vehicle* mVehicles[MAX_VEHICLE];
	};
}