#include "UBoat.h"

namespace assignment2
{
	UBoat::UBoat(const UBoat& other)
		: Vehicle(other)
	{
	}

	UBoat::UBoat()
		: Vehicle(50)
	{
	}

	UBoat::~UBoat()
	{
	}

	UBoat& UBoat::operator=(const UBoat& other)
	{
		if (other == *this)
			return *this;

		Vehicle::operator=(other);
		return *this;
	}

	bool UBoat::operator==(const UBoat& other) const
	{
		return Vehicle::operator==(other);
	}

	bool UBoat::Travel()
	{
		if (mTravelCount > 0 && mTravelCount % 2 == 0)
		{
			if (mRestCount == 0 || mRestCount % 4 > 0)
			{
				mRestCount++;
				return false;
			}
			else
			{
				mRestCount = 0;
			}
		}

		mTravelCount++;
		return true;
	}

	unsigned int UBoat::GetMaxSpeed() const
	{
		unsigned int sail = GetSailSpeed();
		unsigned int dive = GetDiveSpeed();

		return sail > dive ? sail : dive;
	}

	unsigned int UBoat::GetSailSpeed() const
	{
		unsigned int totalWeight = GetTotalPassengersWeight();

		double a = 550 - ((double)totalWeight / 10);
		double speed = mymath::Max(a, 200.f);

		int roundSpeed = mymath::Rounds(speed);
		return static_cast<unsigned int>(roundSpeed);
	}

	unsigned int UBoat::GetDiveSpeed() const
	{
		unsigned int totalWeight = GetTotalPassengersWeight();

		double a = ((double)totalWeight + 150) / 150;
		double b = log(a);
		double c = 500 * b;
		double speed = c + 30.f;

		int roundSpeed = mymath::Rounds(speed);
		return static_cast<unsigned int>(roundSpeed);
	}
}