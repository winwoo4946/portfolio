#pragma once

#include "Vehicle.h"
#include "IDrivable.h"
#include "Trailer.h"
namespace assignment2
{
	class Trailer;

	class Sedan : public Vehicle, public IDrivable
	{
	public:
		Sedan();
		Sedan(const Sedan& other);
		~Sedan();

		Sedan& operator=(const Sedan& other);
		bool operator==(const Sedan& other) const;

		unsigned int GetTotalPassengersWeight() const;
		bool Travel();

		bool AddTrailer(const Trailer* trailer);
		bool RemoveTrailer();

		unsigned int GetMaxSpeed() const;
		unsigned int GetDriveSpeed() const;
	private:
		const Trailer* mTrailer = NULL;
	};
}