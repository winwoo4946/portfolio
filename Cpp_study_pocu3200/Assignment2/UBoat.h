#pragma once
#include "Vehicle.h"
#include "ISailable.h"
#include "IDivable.h"

namespace assignment2
{
	class UBoat : public Vehicle, public ISailable, public IDivable
	{
	public:
		UBoat();
		UBoat(const UBoat& other);
		~UBoat();

		UBoat& operator=(const UBoat& other);
		bool operator==(const UBoat& other) const;

		bool Travel();
		unsigned int GetMaxSpeed() const;
		unsigned int GetSailSpeed() const;
		unsigned int GetDiveSpeed() const;
	};
}