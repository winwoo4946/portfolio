#include "Person.h"

namespace assignment2
{
	Person::Person()
		: mName("")
		, mWeight(0)
	{
	}

	Person::Person(const char* name, unsigned int weight)
		: mName(std::string(name))
		, mWeight(weight)
	{
	}

	Person::Person(const Person& other)
		: mName(other.mName)
		, mWeight(other.mWeight)
	{
	}

	Person::~Person()
	{
	}

	Person& Person::operator=(const Person& other)
	{
		if (*this == other)
			return *this;

		mName = other.mName;
		mWeight = other.mWeight;

		return *this;
	}

	bool Person::operator==(const Person& other) const
	{
		return &mName == &other.mName && &mWeight == &other.mWeight;
	}

	const std::string& Person::GetName() const
	{
		return mName;
	}

	unsigned int Person::GetWeight() const
	{
		return mWeight;
	}
}