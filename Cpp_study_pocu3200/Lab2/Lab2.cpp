﻿#include "Lab2.h"
#include <iomanip>

using namespace std;

namespace lab2
{
	void PrintIntegers(std::istream& in, std::ostream& out)
	{
		int input = 0;
		string trash;

		out << setw(12) << "oct" << setw(11) << "dec" << setw(9) << "hex" << endl;
		out << setfill('-') << setw(13) << " " << setw(11) << " " << setw(8) << "" << endl << setfill(' ');
		while (!in.eof())
		{
			in >> input;

			if (in.fail())
			{
				in.clear();
				in >> trash;
				continue;
			}

			out << setw(12) << oct << input << setw(11) << dec << input << setw(9) << uppercase << hex << input << endl;
		}
	}

	void PrintMaxFloat(std::istream& in, std::ostream& out)
	{
		float input = 0.0f;
		float iMax = -FLT_MAX;
		string trash;

		out.precision(3);
		out << fixed << internal << showpos << showpoint << showpos;
		while (!in.eof())
		{
			in >> input;

			if (in.fail())
			{
				in.clear();
				in >> trash;
				continue;
			}

			if (input > iMax)
			{
				iMax = input;
			}
			
			out << setw(5) << "" << setw(15) << input << endl;
		}

		out << setw(5) << left << "max:" << right << setw(15) << internal << iMax << endl;
	}
}