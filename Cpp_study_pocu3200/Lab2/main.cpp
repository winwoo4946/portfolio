#include "Lab2.h"

#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	ifstream in;
	ofstream out;

	lab2::PrintIntegers(in, out);
	lab2::PrintMaxFloat(in, out);

	//in.open("test.txt");
	//lab2::PrintIntegers(in, cout);
	//lab2::PrintMaxFloat(in, cout);

	return 0;
}