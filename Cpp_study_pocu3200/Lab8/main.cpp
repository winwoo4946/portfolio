#include <cassert>
#include <vector>
#include <iostream>
#include <bitset>
#include <cstdint>
#include "FixedVector.h"

using namespace lab8;
using namespace std;

void Test1()
{
	int i1 = 23;
	int i2 = 25;
	int i3 = 10;
	int i4 = -4;
	int i5 = 70;

	FixedVector<int, 33> iv;

	assert(iv.GetCapacity() == 33);

	iv.Add(i1);
	iv.Add(i2);
	iv.Add(i3);
	iv.Add(i4);

	assert(iv.GetSize() == 4);
	assert(iv.GetIndex(i2) == 1);
	assert(iv.Get(1) == i2);
	assert(iv[1] == i2);

	bool bRemoved = iv.Remove(i5);
	assert(!bRemoved);
	assert(iv.GetSize() == 4);

	bRemoved = iv.Remove(i2);
	assert(bRemoved);
	assert(iv.GetSize() == 3);
	assert(iv.GetIndex(i2) == -1);
}

int main()
{
	uint32_t a = 16;
	cout << bitset<8>(a) << endl;
	a = a >> 4;
	cout << bitset<8>(a) << endl;

	char b = 1;

	for (int i = 0; i < 8; i++)
	{
		cout << bitset<8>(a) << endl;

		uint32_t flag = (a & b);
		cout << flag << endl;
		if (flag > 0)
		{
			cout << "true / " << endl;
			cout << bitset<8>((a >> i)) << endl;
		}
		else
		{
			cout << "false / " << endl;
		}

		b <<= 1;
		cout << "----------------" << endl;
	}
	
//	cout << bitset<4>(bR) << endl;
	cout << endl;


	cout << "=================================" << endl;

	FixedVector<int, 5> v;

	v.Add(1);
	v.Add(2);
	v.Add(3);
	v.Add(4);

	cout << "Size : " << v.GetSize() << endl;
	cout << "Capacity : " << v.GetCapacity() << endl;
	for (size_t i = 0; i < v.GetSize(); i++)
	{
		cout << "Get " << i << " : " << v.Get(i) << endl;
		cout << "before [" << i << "] : " << v[i] << endl;
		//int& a = v[i];
		//a = 2;
		cout << "after [" << i << "] : " << v[i] << endl;
	}
}

