#pragma once
#include <cstdint>
using namespace std;
namespace lab8
{
	template <size_t N>
	class FixedVector<bool, N>
	{
	public:
		FixedVector();
		FixedVector(const FixedVector<bool, N>& other);
		~FixedVector();
		FixedVector<bool, N>& operator=(const FixedVector<bool, N>& other);

		bool Add(const bool& b);
		bool Remove(const bool& b);
		bool Get(unsigned int index);
		bool operator[](unsigned int index);
		int GetIndex(const bool b) const;
		size_t GetSize() const;
		size_t GetCapacity() const;

		void Print() const;

	private:
		enum
		{
			MAX_BIT = 32
		};
		size_t mSize;
		uint32_t mArray[((N - 1) / MAX_BIT) + 1];
	};

	template<size_t N>FixedVector<bool, N>::FixedVector()
		: mSize(0)
	{
		memset(mArray, 0, sizeof(mArray));
	}

	template<size_t N>FixedVector<bool, N>::FixedVector(const FixedVector<bool, N>& bOther)
		: mSize(bOther.mSize)
		, mArray(bOther.mArray)
	{
		memset(mArray, 0, sizeof(mArray));
	}

	template<size_t N>FixedVector<bool, N>::~FixedVector()
	{
	}

	template<size_t N>FixedVector<bool, N>& FixedVector<bool, N>::operator=(const FixedVector<bool, N>& bOther)
	{
		mSize = bOther.mSize;
		mArray = bOther.mArray;
	}

	template<size_t N>
	bool FixedVector<bool, N>::Add(const bool& b)
	{
		if (mSize >= N)
			return false;

		const unsigned int index = mSize / MAX_BIT;
		unsigned int size = mSize % MAX_BIT;

		if (b == true)
		{
			mArray[index] |= (1 << size);
		}
		else
		{
			mArray[index] &= ~(1 << size);
		}
		mSize++;
		return true;
	}

	template<size_t N>
	bool FixedVector<bool, N>::Remove(const bool& b)
	{
		bool bRemove = false;
		const unsigned int maxIndex = ((mSize - 1) / MAX_BIT) + 1;

		for (size_t i = 0; i < maxIndex; i++)
		{
			if (bRemove == true)
			{
				uint32_t n = (mArray[i] & 1); // 이전으로 보낼 첫 비트
				uint32_t n2 = (n << (MAX_BIT - 1)); // 이전으로 보내기 위해 맨뒤로 민다.

				mArray[i - 1] |= n2; // 이전의 맨뒤로 합친다.
				mArray[i] >>= 1; // 한개 빠지만큼 앞으로 민다.
				continue;
			}

			uint32_t frontBit = 0;
			for (size_t j = 0; j < mSize && j < MAX_BIT; j++)
			{
				if ((mArray[i] & (1 << j)) > 0) // 1
				{
					if (b == true)
					{
						uint32_t n = mArray[i] >> (j + 1); // 지워질 비트까지 민다.
						uint32_t n2 = n << j;  // 지워진 비트 앞 비트수 만큼 당긴다.
						mArray[i] = n2 | frontBit; // 지워진 비트 앞의 비트들을 OR시킨다.
						
						bRemove = true;
						break;
					}
					frontBit += (1 << j);
				}
				else
				{
					if (b == false)
					{
						uint32_t n = mArray[i] >> (j + 1); // 지워질 비트까지 민다.
						uint32_t n2 = n << j;  // 지워진 비트 앞 비트수 만큼 당긴다.
						mArray[i] = n2 | frontBit; // 지워진 비트 앞의 비트들을 OR시킨다.

						bRemove = true;
						break;
					}
				}
			}
		}

		if (bRemove == true)
		{
			mSize--;
		}

		return bRemove;
	}

	template<size_t N>
	bool FixedVector<bool, N>::Get(unsigned int index)
	{
		unsigned int intIndex = index / MAX_BIT;
		unsigned int bitIndex = index % MAX_BIT;

		bool b = mArray[intIndex] & (1 << bitIndex);
		return b;
	}

	template<size_t N>
	bool FixedVector<bool, N>::operator[](unsigned int index)
	{
		unsigned int intIndex = index / MAX_BIT;
		unsigned int bitIndex = index % MAX_BIT;
		bool b = mArray[intIndex] & (1 << bitIndex);
		return b;
	}

	template<size_t N>
	int FixedVector<bool, N>::GetIndex(bool b) const
	{
		const unsigned int maxIndex = ((mSize - 1) / MAX_BIT) + 1;
		for (size_t i = 0; i < maxIndex; i++)
		{
			for (size_t j = 0; j < mSize && j < MAX_BIT; j++)
			{
				if ((mArray[i] & (1 << j)) > 0) // 1
				{
					if (b == true)
					{
						return (i * MAX_BIT) + j;
					}
				}
				else
				{
					if (b == false)
					{
						return (i * MAX_BIT) + j;
					}
				}
			}
		}
		return -1;
	}

	template<size_t N>
	size_t FixedVector<bool, N>::GetSize() const
	{
		return mSize;
	}

	template<size_t N>
	size_t FixedVector<bool, N>::GetCapacity() const
	{
		return N;
	}

	template<size_t N>
	void FixedVector<bool, N>::Print() const
	{
		const unsigned int maxIndex = ((mSize - 1) / MAX_BIT) + 1;
		for (size_t i = 0; i < maxIndex; i++)
		{
			cout << bitset<MAX_BIT>(mArray[i]) << endl;
		}

		cout << "==================================" << endl;
	}
}