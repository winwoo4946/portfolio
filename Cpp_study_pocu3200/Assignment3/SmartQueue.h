#pragma once

#include <queue>
#include <limits>

using namespace std;

namespace assignment3
{

	template <typename T>
	class SmartQueue
	{
	public:
		SmartQueue();
		~SmartQueue();

		void Enqueue(const T number);
		T Peek() const;
		T Dequeue();
		T GetMax();
		T GetMin();
		double GetAverage() const;
		T GetSum() const;
		double GetVariance() const;
		double GetStandardDeviation() const;
		unsigned int GetCount() const;
	private:
		T mTotal;
		double mSqureTotal;
		queue<T> mQueue;
	};

	template <typename T>
	inline SmartQueue<T>::SmartQueue()
		: mTotal(0)
		, mSqureTotal(0)
	{
	}

	template <typename T>
	inline SmartQueue<T>::~SmartQueue()
	{
	}

	template <typename T>
	inline void SmartQueue<T>::Enqueue(T number)
	{
		mQueue.push(number);
		mTotal += number;
		mSqureTotal += pow(number, 2);
	}

	template <typename T>
	inline T SmartQueue<T>::Dequeue()
	{
		T number = mQueue.front();

		mTotal -= number;
		mSqureTotal -= pow(number, 2);

		mQueue.pop();
		return number;
	}

	template <typename T>
	inline T SmartQueue<T>::Peek() const
	{
		return mQueue.front();
	}

	template <typename T>
	inline unsigned int SmartQueue<T>::GetCount() const
	{
		return mQueue.size();
	}

	template <typename T>
	inline T SmartQueue<T>::GetMax()
	{
		if (mQueue.empty() == true)
		{
			return numeric_limits<T>().lowest();
		}

		T tMax = mQueue.front();
		unsigned int size = mQueue.size();
		while (size > 0)
		{
			T t = mQueue.front();
			mQueue.pop();
			mQueue.push(t);
			if (t > tMax)
				tMax = t;

			size--;
		}

		return tMax;
	}

	template <typename T>
	inline T SmartQueue<T>::GetMin()
	{
		if (mQueue.empty() == true)
		{
			return numeric_limits<T>().max();
		}

		T tMin = mQueue.front();
		unsigned int size = mQueue.size();
		while (size > 0)
		{
			T t = mQueue.front();
			mQueue.pop();
			mQueue.push(t);
			if (t < tMin)
				tMin = t;

			size--;
		}

		return tMin;
	}

	template <typename T>
	inline double SmartQueue<T>::GetAverage() const
	{
		T sum = GetSum();
		unsigned int count = GetCount();
		double d = static_cast<double>(sum) / count;
		d = round(d * 1000) / 1000;
		return d;
	}

	template <typename T>
	inline T SmartQueue<T>::GetSum() const
	{
		return mTotal;
	}

	template <typename T>
	inline double SmartQueue<T>::GetVariance() const
	{
		double count = GetCount();
		double dSqureTotalAverage = mSqureTotal / count;
		double dAverage = mTotal / count;
		double dResult = dSqureTotalAverage - pow(dAverage, 2);
		return round(dResult * 1000) / 1000;
	}

	template <typename T>
	inline double SmartQueue<T>::GetStandardDeviation() const
	{
		double count = GetCount();
		double dSqureTotalAverage = mSqureTotal / count;
		double dAverage = mTotal / count;
		double dResult = dSqureTotalAverage - pow(dAverage, 2);
		return round(sqrt(dResult) * 1000) / 1000;
	}
}