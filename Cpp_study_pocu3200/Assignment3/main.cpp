#include <cassert>
#include "SmartStack.h"
#include "SmartQueue.h"
#include "QueueStack.h"
#include <iostream>
using namespace assignment3;

int main()
{
	SmartStack<double> ss;
	QueueStack<double> dd(3);

	ss.Push(3.4);
	ss.Push(1.2);
	ss.Push(4.6);
	ss.Push(3.32);
	ss.Push(10.2);
	ss.Push(1.1);
	ss.Push(-5.9);
	ss.Push(1.1);
	ss.Push(-12.4);
	ss.Push(9.2);

	dd.Enqueue(3.4);
	dd.Enqueue(1.2);
	dd.Enqueue(4.6);
	dd.Enqueue(3.32);
	dd.Enqueue(10.2);
	dd.Enqueue(1.1);
	dd.Enqueue(-5.9);
	dd.Enqueue(1.1);
	dd.Enqueue(-12.4);
	dd.Enqueue(9.2);
	//assert(ss.GetCount() == 10U);
	
	

	cout << ss.Peek() << endl;
	cout << ss.GetMax() << endl;
	cout << ss.GetMin() << endl;
	cout << ss.GetSum() << endl;
	cout << ss.GetAverage() << endl;
	cout << ss.GetVariance() << endl;
	cout << ss.GetStandardDeviation() << endl;
	cout << ss.GetCount() << endl;
	cout << ss.Empty() << endl;
	double popped1 = ss.Pop();
	double popped2 = ss.Pop();
	cout << popped1 << endl;
	cout << popped2 << endl;
	cout << "======================================" << endl;
	
	cout << dd.Peek() << endl;
	cout << dd.GetMax() << endl;
	cout << dd.GetMin() << endl;
	cout << dd.GetSum() << endl;
	cout << dd.GetAverage() << endl;
	cout << dd.GetVariance() << endl;
	cout << dd.GetStandardDeviation() << endl;
	cout << dd.GetCount() << endl;
	cout << dd.Empty() << endl;
	double deq1 = dd.Dequeue();
	double deq2 = dd.Dequeue();
	cout << deq1 << endl;
	cout << deq2 << endl;


	/*assert(ss.GetMax() == 10.2);
	assert(ss.GetMin() == -12.4);
	assert(ss.GetSum() == 15.82);
	assert(ss.GetAverage() == 1.582);
	assert(ss.GetVariance() == 39.983);
	assert(ss.GetStandardDeviation() == 6.323);*/

	/*assert(popped1 == 3.4);
	assert(popped2 == 1.2);*/

	return 0;
}
