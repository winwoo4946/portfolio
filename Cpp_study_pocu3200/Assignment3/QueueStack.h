#pragma once

#include <queue>
#include <stack>
#include <limits>
#include <cmath>
#include "SmartStack.h"

using namespace std;

namespace assignment3
{
	template <typename T>
	class QueueStack
	{
	public:
		QueueStack() = delete;
		QueueStack(const unsigned int maxStackSize);
		QueueStack(const QueueStack<T>& rhs);
		~QueueStack();
		QueueStack<T>& operator=(const QueueStack<T>& rhs);

		void Enqueue(const T number);
		T Peek() const;
		T Dequeue();
		T GetMax() const;
		T GetMin() const;
		double GetAverage() const;
		T GetSum() const;
		unsigned int GetCount() const;
		unsigned int GetStackCount() const;
	private:
		T mTotal;

		queue<SmartStack<T>* >* mQueue;
		const unsigned int mMaxStackSize;
	};

	template <typename T>
	inline QueueStack<T>::QueueStack(const unsigned int maxStackSize)
		: mTotal(0)
		, mMaxStackSize(maxStackSize)
	{
		mQueue = new queue<SmartStack<T>*>();
	}

	template <typename T>
	inline QueueStack<T>::QueueStack(const QueueStack<T>& rhs)
		: mTotal(rhs.mTotal)
		, mMaxStackSize(rhs.maxStackSize)
	{
		mQueue = new queue<SmartStack<T>*>();
	}

	template <typename T>
	inline QueueStack<T>::~QueueStack()
	{
		while (mQueue->empty() == false)
		{
			SmartStack<T>* s = mQueue->front();
			mQueue->pop();
			delete s;
		}
		delete mQueue;
	}

	template <typename T>
	inline QueueStack<T>& QueueStack<T>::operator=(const QueueStack& rhs)
	{
		if (this == &rhs)
			return *this;

		mTotal = rhs.mTotal;

		while (mQueue->empty() == false)
		{
			SmartStack<T>* s = mQueue->front();
			mQueue->pop();
			delete s;
		}
		delete mQueue;

		mQueue = new queue<SmartStack<T>*>();
		
		unsigned int size = rhs.GetStackCount();
		while (size > 0)
		{
			SmartStack<T>* s = rhs.mQueue->front();
			rhs.mQueue->pop();
			rhs.mQueue->push(s);
			mQueue->push(new SmartStack<T>(*s));

			size--;
		}

		return *this;
	}

	template <typename T>
	inline void QueueStack<T>::Enqueue(const T number)
	{
		if (mQueue->empty() == true)
		{
			SmartStack<T>* s = new SmartStack<T>();
			mQueue->push(s);
		}

		SmartStack<T>* s = mQueue->back();
		
		if (s->GetCount() >= mMaxStackSize)
		{
			SmartStack<T>* s = new SmartStack<T>();
			s->Push(number);
			mQueue->push(s);
		}
		else
		{
			s->Push(number);
		}

		mTotal += number;
	}

	template <typename T>
	inline T QueueStack<T>::Peek() const
	{
		SmartStack<T>* s =  mQueue->front();
		return s->Peek();
	}

	template <typename T>
	inline T QueueStack<T>::Dequeue()
	{
		SmartStack<T>* s = mQueue->front();
		T number = s->Peek();
		s->Pop();

		if (s->GetCount() == 0)
		{
			mQueue->pop();
			delete s;
		}

		mTotal -= number;

		return number;
	}

	template <typename T>
	inline unsigned int QueueStack<T>::GetCount() const
	{
		unsigned int size = mQueue->size();

		unsigned int totalSize = 0;
		while (size > 0)
		{
			SmartStack<T>* s = mQueue->front();
			mQueue->pop();
			mQueue->push(s);

			totalSize += s->GetCount();

			size--;
		}

		return totalSize;
	}

	template <typename T>
	inline unsigned int QueueStack<T>::GetStackCount() const
	{
		return mQueue->size();
	}

	template <typename T>
	inline T QueueStack<T>::GetMax() const
	{
		if (mQueue->empty() == true)
		{
			return numeric_limits<T>().lowest();
		}

		SmartStack<T>* s = mQueue->front();
		T tMax = s->GetMax();

		unsigned int size = mQueue->size();
		while (size > 0)
		{
			s = mQueue->front();
			T t = s->GetMax();
			mQueue->pop();
			mQueue->push(s);

			if (t > tMax)
				tMax = t;

			size--;
		}

		return tMax;
	}

	template <typename T>
	inline T QueueStack<T>::GetMin() const
	{
		if (mQueue->empty() == true)
		{
			return numeric_limits<T>().max();
		}

		SmartStack<T>* s = mQueue->front();
		T tMin = s->GetMin();

		unsigned int size = mQueue->size();
		while (size > 0)
		{
			s = mQueue->front();
			T t = s->GetMin();
			mQueue->pop();
			mQueue->push(s);

			if (t < tMin)
				tMin = t;

			size--;
		}

		return tMin;
	}

	template <typename T>
	inline double QueueStack<T>::GetAverage() const
	{
		T sum = GetSum();
		unsigned int count = GetCount();
		double d = sum / static_cast<double>(count);
		d = round(d * 1000) / 1000;
		return d;
	}

	template <typename T>
	inline T QueueStack<T>::GetSum() const
	{
		return mTotal;
	}
}