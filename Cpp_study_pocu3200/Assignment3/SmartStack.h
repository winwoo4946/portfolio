#pragma once

#include <stack>
#include <limits>

using namespace std;

namespace assignment3
{
	template <typename T>
	class SmartStack
	{
	public:
		SmartStack();
		SmartStack(const SmartStack& rhs);
		~SmartStack();
		SmartStack<T>& operator=(const SmartStack& rhs);

		T GetMax() const;
		T GetMin() const;
		double GetAverage() const;
		T GetSum() const;
		double GetVariance() const;
		double GetStandardDeviation() const;

		void Push(const T number);
		T Pop();
		T Peek() const;
		unsigned int GetCount() const;
		bool Empty() const;
	private:
		stack<T>* mMinStack;
		stack<T>* mMaxStack;
		T mTotal;
		double mSqureTotal;

		stack<T>* mStack;

	};

	template <typename T>
	inline SmartStack<T>::SmartStack()
		: mTotal(0)
		, mSqureTotal(0)
	{
		mMinStack = new stack<T>();
		mMaxStack = new stack<T>();

		mStack = new stack<T>();
	}

	template <typename T>
	inline SmartStack<T>::SmartStack(const SmartStack<T>& rhs)
		: mTotal(rhs.mTotal)
		, mSqureTotal(rhs.mSqureTotal)
	{
		mMinStack = new stack<T>(*rhs.mMinStack);
		mMaxStack = new stack<T>(*rhs.mMaxStack);
		mStack = new stack<T>(*rhs.mStack);
	}

	template <typename T>
	inline SmartStack<T>::~SmartStack()
	{
		delete mMinStack;
		delete mMaxStack;
		delete mStack;
	}

	template <typename T>
	inline SmartStack<T>& SmartStack<T>::operator=(const SmartStack<T>& rhs)
	{
		if (this == &rhs)
			return *this;

		delete mMinStack;
		delete mMaxStack;

		mMinStack = new stack<T>(*rhs.mMinStack);
		mMaxStack = new stack<T>(*rhs.mMaxStack);
		mTotal = rhs.mTotal;
		mSqureTotal = rhs.mSqureTotal;

		delete mStack;
		mStack = new stack<T>(*rhs.mStack);

		return *this;
	}

	template <typename T>
	inline void SmartStack<T>::Push(T number)
	{
		mStack->push(number);

		if (mMinStack->empty() == true || mMinStack->top() >= number)
		{
			mMinStack->push(number);
		}

		if (mMaxStack->empty() == true || mMaxStack->top() <= number)
		{
			mMaxStack->push(number);
		}

		mTotal += number;
		mSqureTotal += pow(number, 2);
	}

	template <typename T>
	inline T SmartStack<T>::Pop()
	{
		T number = mStack->top();
		
		if (mMinStack->empty() == false && mMinStack->top() == number)
		{
			mMinStack->pop();
		}

		if (mMaxStack->empty() == false && mMaxStack->top() == number)
		{
			mMaxStack->pop();
		}

		mTotal -= number;
		mSqureTotal -= pow(number, 2);

		mStack->pop();
		return number;
	}

	template <typename T>
	inline T SmartStack<T>::Peek() const
	{
		return mStack->top();
	}

	template <typename T>
	inline unsigned int SmartStack<T>::GetCount() const
	{
		return mStack->size();
	}

	template <typename T>
	inline bool SmartStack<T>::Empty() const
	{
		return mStack->empty();
	}

	template <typename T>
	inline T SmartStack<T>::GetMax() const
	{
		if (Empty() == true)
		{
			return numeric_limits<T>().lowest();
		}

		return mMaxStack->top();
	}

	template <typename T>
	inline T SmartStack<T>::GetMin() const
	{
		if (Empty() == true)
		{
			return numeric_limits<T>().max();
		}

		return mMinStack->top();
	}

	template <typename T>
	inline double SmartStack<T>::GetAverage() const
	{
		T sum = GetSum();
		unsigned int count = GetCount();
		double d = static_cast<double>(sum) / count;
		d = round(d * 1000) / 1000;
		return d;
	}

	template <typename T>
	inline T SmartStack<T>::GetSum() const
	{
		return mTotal;
	}

	template <typename T>
	inline double SmartStack<T>::GetVariance() const
	{
		double count = static_cast<double>(GetCount());
		double dSqureTotalAverage = mSqureTotal / count;
		double dAverage = mTotal / count;
		double dResult = dSqureTotalAverage - pow(dAverage, 2);
		return round(dResult * 1000) / 1000;
	}

	template <typename T>
	inline double SmartStack<T>::GetStandardDeviation() const
	{
		double count = static_cast<double>(GetCount());
		double dSqureTotalAverage = mSqureTotal / count;
		double dAverage = mTotal / count;
		double dResult = dSqureTotalAverage - pow(dAverage, 2);
		return round(sqrt(dResult) * 1000) / 1000;
	}
}