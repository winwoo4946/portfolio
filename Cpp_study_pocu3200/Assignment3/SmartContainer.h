#pragma once

#include <stack>
#include <limits>
#include "MyMath.h"

using namespace std;

namespace assignment3
{
	template <typename T>
	class SmartContainer
	{
	public:
		SmartContainer();
		SmartContainer(const SmartContainer<T>& rhs);
		virtual ~SmartContainer();
		virtual SmartContainer<T>& operator=(const SmartContainer<T>& rhs);
		virtual T Peek() const = 0;
		virtual T GetMax() const;
		virtual T GetMin() const;
		virtual double GetAverage() const;
		virtual T GetSum() const;
		virtual double GetVariance() const;
		virtual double GetStandardDeviation() const;
		virtual unsigned int GetCount() const = 0;
		virtual bool Empty() const = 0;
	protected:
		virtual void Add(const T number);
		virtual void Remove(const T number);

		stack<T>* mMinStack;
		stack<T>* mMaxStack;
		T mTotal;
		T mSqureTotal;
	};

	template <typename T>
	inline SmartContainer<T>::SmartContainer()
		: mTotal(0)
		, mSqureTotal(0)
	{
		mMinStack = new stack<T>();
		mMaxStack = new stack<T>();
	}

	template <typename T>
	inline SmartContainer<T>::SmartContainer(const SmartContainer<T>& rhs)
		: mTotal(rhs.mTotal)
		, mSqureTotal(rhs.mSqureTotal)
	{
		mMinStack = new stack<T>();
		mMaxStack = new stack<T>();
	}

	template <typename T>
	inline SmartContainer<T>::~SmartContainer()
	{
		delete mMinStack;
		delete mMaxStack;
	}

	template <typename T>
	inline SmartContainer<T>& SmartContainer<T>::operator=(const SmartContainer<T>& rhs)
	{
		if (this == &rhs)
			return *this;

		delete mMinStack;
		delete mMaxStack;

		mMinStack = new stack<T>();
		mMaxStack = new stack<T>();
		mTotal = rhs.mTotal;
		mSqureTotal = rhs.mSqureTotal;

		return *this;
	}

	template <typename T>
	inline void SmartContainer<T>::Add(const T number)
	{
		if (mMinStack->empty() == true || mMinStack->top() >= number)
		{
			mMinStack->push(number);
		}

		if (mMaxStack->empty() == true || mMaxStack->top() <= number)
		{
			mMaxStack->push(number);
		}

		mTotal += number;
		mSqureTotal += (number * number);
	}

	template <typename T>
	inline void SmartContainer<T>::Remove(const T number)
	{
		if (mMinStack->top() == number)
		{
			mMinStack->pop();
		}

		if (mMaxStack->top() == number)
		{
			mMaxStack->pop();
		}

		mTotal -= number;
		mSqureTotal -= (number * number);
	}

	template <typename T>
	inline T SmartContainer<T>::GetMax() const
	{
		if (Empty() == true)
		{
			return numeric_limits<T>().max();
		}

		return mMaxStack->top();
	}

	template <typename T>
	inline T SmartContainer<T>::GetMin() const
	{
		if (Empty() == true)
		{
			return numeric_limits<T>().min();
		}

		return mMinStack->top();
	}

	template <typename T>
	inline double SmartContainer<T>::GetAverage() const
	{
		T sum = GetSum();
		unsigned int count = GetCount();
		double d = static_cast<double>(sum) / count;
		d = round(d * 1000) / 1000;
		return d;
	}

	template <typename T>
	inline T SmartContainer<T>::GetSum() const
	{
		return mTotal;
	}

	template <typename T>
	inline double SmartContainer<T>::GetVariance() const
	{
		double dSqureTotalAverage = mSqureTotal / GetCount();
		double dAverage = mTotal / GetCount();
		double dResult = dSqureTotalAverage - pow(dAverage, 2);
		return round(dResult * 1000) / 1000;
	}

	template <typename T>
	inline double SmartContainer<T>::GetStandardDeviation() const
	{
		double dSqureTotalAverage = mSqureTotal / GetCount();
		double dAverage = mTotal / GetCount();
		double dResult = dSqureTotalAverage - pow(dAverage, 2);
		return round(sqrt(dResult) * 1000) / 1000;
	}
}