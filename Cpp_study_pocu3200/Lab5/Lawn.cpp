#include "Lawn.h"
#include "MyMath.h"

namespace lab5
{
	Lawn::Lawn()
	{
	}

	Lawn::~Lawn()
	{
	}

	unsigned int Lawn::GetGrassPrice(eGrassType grassType) const
	{
		const double DALLAS_RATE = 100;
		int pricePerSqureMeter = static_cast<int>(grassType);
		unsigned int squreMeter = GetArea();

		return MyMath::Ceil(squreMeter * (pricePerSqureMeter / DALLAS_RATE));
	}

	unsigned int Lawn::GetMinimumSodRollsCount() const
	{
		return MyMath::Ceil(GetArea() / ONE_ROLL_GRASS_SIZE);
	}
}