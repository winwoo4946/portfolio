#pragma once

namespace lab5
{
	class MyMath
	{
	public:
		static int Floor(double value);
		static int Ceil(double value);
		static int Rounds(double value);
		static int Squre(int value);
		static double Sqrt(double value);
	};
}
