#pragma once

#include "Lawn.h"

namespace lab5
{
	class CircleLawn : public Lawn
	{
	public:
		CircleLawn(unsigned int r);
		~CircleLawn();

		unsigned int GetArea() const;
	private:
		const double PI = 3.14;
		const unsigned int mRadius;
	};
}