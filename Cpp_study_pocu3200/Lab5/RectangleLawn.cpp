#include "RectangleLawn.h"
#include "MyMath.h"
namespace lab5
{
	RectangleLawn::RectangleLawn(unsigned int width, unsigned int height)
		: mWidth(width)
		, mHeight(height)
	{
	}

	RectangleLawn::~RectangleLawn()
	{
	}

	unsigned int RectangleLawn::GetArea() const
	{
		return mWidth * mHeight;
	}

	unsigned int RectangleLawn::GetEdgeSize() const
	{
		int size = (mWidth * 2) + (mHeight * 2);
		return size;
	}

	unsigned int RectangleLawn::GetMinimumFencesCount() const
	{
		int cmSize = GetEdgeSize() * 100;
		return MyMath::Ceil(cmSize / ONE_FENCE_WIDTH);
	}

	unsigned int RectangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		unsigned int dallar = static_cast<unsigned int>(fenceType);
		return dallar * MyMath::Ceil((GetMinimumFencesCount() * ONE_FENCE_WIDTH) / 100);
	}
}