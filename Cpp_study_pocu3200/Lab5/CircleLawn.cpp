#include "CircleLawn.h"
#include "MyMath.h"

namespace lab5
{
	CircleLawn::CircleLawn(unsigned int r)
		: mRadius(r)
	{
	}

	CircleLawn::~CircleLawn()
	{

	}

	unsigned int CircleLawn::GetArea() const
	{
		return MyMath::Rounds(PI * MyMath::Squre(mRadius));
	}
}