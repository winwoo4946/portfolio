#include "EquilateralTriangleLawn.h"
#include "MyMath.h"
namespace lab5
{
	EquilateralTriangleLawn::EquilateralTriangleLawn(unsigned int sideSize)
		: mSideSize(sideSize)
	{
	}

	EquilateralTriangleLawn::~EquilateralTriangleLawn()
	{
	}

	unsigned int EquilateralTriangleLawn::GetArea() const
	{
		double squreMeter = MyMath::Squre(mSideSize) * (MyMath::Sqrt(3) / 4);
		squreMeter = MyMath::Rounds(squreMeter);
		return static_cast<unsigned int>(squreMeter);
	}

	unsigned int EquilateralTriangleLawn::GetEdgeSize() const
	{
		int size = mSideSize * 3;
		return size;
	}

	unsigned int EquilateralTriangleLawn::GetMinimumFencesCount() const
	{
		int cmSize = GetEdgeSize() * 100;
		return MyMath::Ceil(cmSize / ONE_FENCE_WIDTH);
	}

	unsigned int EquilateralTriangleLawn::GetFencePrice(eFenceType fenceType) const
	{
		int dallar = static_cast<int>(fenceType);
		return dallar * ((GetMinimumFencesCount() * ONE_FENCE_WIDTH) / 100);
	}
}