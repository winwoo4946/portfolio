#pragma once

#include <iostream>
#include <vector>
#include <map>

namespace lab7
{
	template <typename K, class V>
	std::map<K, V> ConvertVectorsToMap(const std::vector<K>& keys, const std::vector<V>& values)
	{
		std::map<K, V> m;
		
		size_t minSize = keys.size() > values.size() ? values.size() : keys.size();

		for (size_t i = 0; i < minSize; i++)
		{
			typename std::map<K, V>::iterator it = m.find(keys[i]);
			if (it != m.end())
				continue;

			m.insert(std::pair<K, V>(keys[i], values[i]));
		}

		return m;
	}

	template <typename K, class V>
	std::vector<K> GetKeys(const std::map<K, V>& m)
	{
		std::vector<K> v;
		
		for (typename std::map<K, V>::const_iterator it = m.begin(); it != m.end(); it++)
		{
			v.push_back(it->first);
		}

		return v;
	}

	template <typename K, class V>
	std::vector<V> GetValues(const std::map<K, V>& m)
	{
		std::vector<V> v;

		for (typename std::map<K, V>::const_iterator it = m.begin(); it != m.end(); it++)
		{
			v.push_back(it->second);
		}

		return v;
	}

	template <typename T>
	std::vector<T> Reverse(const std::vector<T>& v)
	{
		std::vector<T> rv;

		if (v.size() == 0)
			return rv;

		for (int i = v.size() - 1; i >= 0; i--)
		{
			rv.push_back(v[i]);
		}

		return rv;
	}
}

template <typename T>
std::vector<T> operator+(const std::vector<T>& v1, const std::vector<T>& v2)
{
	std::vector<T> combined;

	for (size_t i = 0; i < v1.size(); i++)
	{
		bool bContain = false;
		for (size_t j = 0; j < combined.size(); j++)
		{
			if (combined[j] == v1[i])
			{
				bContain = true;
				break;
			}
		}

		if (bContain == true)
			continue;

		combined.push_back(v1[i]);
	}

	for (size_t i = 0; i < v2.size(); i++)
	{
		bool bContain = false;
		for (size_t j = 0; j < combined.size(); j++)
		{
			if (combined[j] == v2[i])
			{
				bContain = true;
				break;
			}
		}

		if (bContain == true)
			continue;

		combined.push_back(v2[i]);
	}
	return combined;
}

template <typename K, class V>
std::map<K, V> operator+(const std::map<K, V>& m1, const std::map<K, V>& m2)
{
	std::map<K, V> combined;

	typename std::map<K, V>::const_iterator it;
	for (it = m1.begin(); it != m1.end(); it++)
	{
		combined.insert(typename std::pair<K, V>(it->first, it->second));
	}

	for (it = m2.begin(); it != m2.end(); it++)
	{
		if (combined.find(it->first) != combined.end())
			continue;

		combined.insert(typename std::pair<K, V>(it->first, it->second));
	}

	return combined;
}

template<typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
	for (size_t i = 0; i < v.size(); i++)
	{
		if (i == v.size() - 1)
		{
			os << v[i];
			break;
		}

		os << v[i] << ", ";
	}

	return os;
}

template <typename K, class V>
std::ostream& operator<<(std::ostream& os, const std::map<K, V>& m)
{
	for (typename std::map<K, V>::const_iterator it = m.begin(); it != m.end(); it++)
	{
		os << "{ " << it->first << ", " << it->second << " }" << std::endl;
	}
	return os;
}