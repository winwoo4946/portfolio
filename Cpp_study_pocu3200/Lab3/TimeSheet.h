#pragma once
#include <string>

namespace lab3
{
	class TimeSheet
	{
	public:
		TimeSheet(const char* name, unsigned int maxEntries);
		TimeSheet(const TimeSheet& other);
		~TimeSheet();

		void AddTime(int timeInHours);
		int GetTimeEntry(unsigned int index) const;
		int GetTotalTime() const;
		float GetAverageTime() const;
		float GetStandardDeviation() const;
		const std::string& GetName() const;
		TimeSheet& operator=(const TimeSheet& ohter);

	private:
		// 필요에 따라 private 변수와 메서드를 추가하세요.
		float calcSqrt(const float num) const;
		bool operator==(const TimeSheet& other) const;

		const int MIN_TIME = 1;
		const int MAX_TIME = 10;

		std::string mName;
		int* mEntries;
		unsigned int mMaxEntryCounts;
		int mCurrentEntryIndex = 0;
	};
}