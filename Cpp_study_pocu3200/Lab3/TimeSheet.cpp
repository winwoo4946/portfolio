#include "Timesheet.h"

namespace lab3
{
	TimeSheet::TimeSheet(const char* name, unsigned int maxEntries)
		: mName(name)
		, mMaxEntryCounts(maxEntries)
		, mCurrentEntryIndex(0)
	{
		mEntries = new int[mMaxEntryCounts];
	}

	TimeSheet::TimeSheet(const TimeSheet& other)
		: mName(other.mName)
		, mMaxEntryCounts(other.mMaxEntryCounts)
		, mCurrentEntryIndex(other.mCurrentEntryIndex)
	{
		mEntries = new int[mMaxEntryCounts];
		memcpy(mEntries, other.mEntries, (sizeof(int) * mCurrentEntryIndex));
	}

	TimeSheet::~TimeSheet()
	{
		delete[] mEntries;
	}

	void TimeSheet::AddTime(int timeInHours)
	{
		if (timeInHours < MIN_TIME || timeInHours > MAX_TIME)
			return;

		if (mCurrentEntryIndex >= (signed)mMaxEntryCounts)
			return;

		mEntries[mCurrentEntryIndex] = timeInHours;
		mCurrentEntryIndex++;
	}

	int TimeSheet::GetTimeEntry(unsigned int index) const
	{
		if ((signed)index >= mCurrentEntryIndex)
			return -1;

		return (signed)index < mCurrentEntryIndex ? mEntries[index] : -1;
	}

	int TimeSheet::GetTotalTime() const
	{
		int totalTime = 0;
		for (int i = 0; i < mCurrentEntryIndex; i++)
		{
			totalTime += mEntries[i];
		}
		return totalTime;
	}

	float TimeSheet::GetAverageTime() const
	{
		float totalTime = (float)GetTotalTime();
		if (totalTime <= 0.0f)
			return 0.0f;

		return totalTime / (float)mCurrentEntryIndex;
	}

	float TimeSheet::GetStandardDeviation() const
	{
		float averageTime = GetAverageTime();
		float totalSquareDeviation = 0.0f;
		for (int i = 0; i < mCurrentEntryIndex; i++)
		{
			float deviation = mEntries[i] - averageTime;
			totalSquareDeviation += (deviation * deviation);
		}

		if (totalSquareDeviation == 0)
			return 0.0f;

		return calcSqrt(totalSquareDeviation / (float)mCurrentEntryIndex);
	}

	TimeSheet& TimeSheet::operator=(const TimeSheet& other)
	{
		if (*this == other)
			return *this;

		delete[] mEntries;

		mName = other.mName;
		mMaxEntryCounts = other.mMaxEntryCounts;
		mCurrentEntryIndex = other.mCurrentEntryIndex;
		mEntries = new int[mMaxEntryCounts];
		memcpy(mEntries, other.mEntries, (sizeof(int) * mCurrentEntryIndex));
		return *this;
	}

	bool TimeSheet::operator==(const TimeSheet& other) const
	{
		return this->mEntries == other.mEntries
			&& this->mName == other.mName
			&& this->mCurrentEntryIndex == other.mCurrentEntryIndex;
	}

	float TimeSheet::calcSqrt(const float num) const
	{
		float x = 2;

		for (int i = 0; i < 10; i++)
		{
			x = (x + (num / x)) / 2;
		}

		return x;
	}

	const std::string& TimeSheet::GetName() const
	{
		return mName;
	}
}