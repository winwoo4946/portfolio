#pragma once

#include<iostream>
namespace lab1
{
	class Base
	{
		virtual void method()
		{
			std::cout << "form Base" << std::endl;
		}
	public:
		virtual ~Base()
		{
			method();
		}

		void baseMethod()
		{
			method();
		}
	};
}