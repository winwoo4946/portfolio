#include "MyMath.h"

namespace MyMath
{
	int MyMath::Floor(double value)
	{
		return static_cast<int>(value);
	}

	int MyMath::Ceil(double value)
	{
		int intValue = static_cast<int>(value);
		if (value == intValue)
			return intValue;

		return intValue + 1;
	}

	int MyMath::Rounds(double value)
	{
		return static_cast<int>(value + 0.5);
	}

	int MyMath::Squre(int value)
	{
		return value * value;
	}

	double MyMath::Sqrt(double value)
	{
		double x = 2;

		for (int i = 0; i < 10; i++)
		{
			x = (x + (value / x)) / 2;
		}

		return x;
	}
}