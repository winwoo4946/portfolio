"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const core_1 = require("@nestjs/core");
const app_module_1 = require("./app.module");
require("reflect-metadata");
const cookieParser = require("cookie-parser");
const nest_winston_1 = require("nest-winston");
const winston_config_1 = require("./configs/winston.config");
async function bootstrap() {
    const app = await core_1.NestFactory.create(app_module_1.AppModule, {
        logger: nest_winston_1.WinstonModule.createLogger(winston_config_1.default.getConfig(null))
    });
    app.use(cookieParser());
    app.enableCors({
        origin: process.env.ORIGIN,
        credentials: true
    });
    app.useGlobalPipes(new common_1.ValidationPipe({
        whitelist: true,
        forbidNonWhitelisted: true,
        transform: true
    }));
    await app.listen(9000);
}
bootstrap();
//# sourceMappingURL=main.js.map