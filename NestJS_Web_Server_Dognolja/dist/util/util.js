"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.jsonToQueryString = void 0;
function jsonToQueryString(json) {
    return Object.keys(json).map(k => `${encodeURIComponent(k)}=${encodeURI(json[k])}`).join('&');
}
exports.jsonToQueryString = jsonToQueryString;
//# sourceMappingURL=util.js.map