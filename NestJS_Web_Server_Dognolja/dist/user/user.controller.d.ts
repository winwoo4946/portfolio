import { AuthUserInfo } from "src/auth/login";
import { CreateUserDTO } from "./dto/createUser.dto";
import { UserService } from "./user.service";
export declare class UserController {
    private readonly userService;
    constructor(userService: UserService);
    createUser(createUserDTO: CreateUserDTO): Promise<import("../auth/user.entity").UserEntity>;
    getUserByUI(authInfo: AuthUserInfo): Promise<MyUserInfo>;
    addHotelWish(userId: number, hotelId: number): Promise<void>;
    getHotelWish(userId: number): Promise<import("../hotel/hotel.entity").HotelEntity[]>;
    addReservationHotel(userId: number, hotelId: number): Promise<void>;
    getReservedHotel(userId: number): Promise<import("../auth/user.entity").UserEntity[]>;
}
