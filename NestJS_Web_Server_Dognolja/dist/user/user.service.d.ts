import { CreateUserDTO } from "./dto/createUser.dto";
import { UserEntity } from "../auth/user.entity";
import { DataSource, Repository } from "typeorm";
import { HotelEntity } from "src/hotel/hotel.entity";
import { UserHotelWishEntity } from "./userHotelWish.entity";
export declare class UserService {
    private readonly dataSource;
    private readonly userRepo;
    private readonly hotelRepo;
    private readonly hotelWishRepo;
    constructor(dataSource: DataSource, userRepo: Repository<UserEntity>, hotelRepo: Repository<HotelEntity>, hotelWishRepo: Repository<UserHotelWishEntity>);
    createUser(userDTO: CreateUserDTO): Promise<UserEntity>;
    getUserByUID(uid: string): Promise<MyUserInfo>;
    getUserById(id: number): Promise<UserEntity>;
    createHotelWish(userId: number, hotelId: number): Promise<void>;
    getHotelWish(userId: number): Promise<HotelEntity[]>;
    deleteHotelWish(userId: number, hotelId: number): Promise<void>;
    createHotelReservaton(userId: number, hotelId: number): Promise<void>;
    getReservedHotel(userID: number): Promise<UserEntity[]>;
}
