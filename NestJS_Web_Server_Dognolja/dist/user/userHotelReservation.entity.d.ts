import { BaseEntity } from "typeorm";
export declare class UserHoteReservationEntity extends BaseEntity {
    user_id: number;
    hotel_id: number;
}
