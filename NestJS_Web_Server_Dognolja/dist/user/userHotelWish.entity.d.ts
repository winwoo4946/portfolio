import { BaseEntity } from "typeorm";
export declare class UserHotelWishEntity extends BaseEntity {
    user_id: number;
    hotel_id: number;
}
