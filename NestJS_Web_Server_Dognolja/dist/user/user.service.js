"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const user_entity_1 = require("../auth/user.entity");
const typeorm_2 = require("typeorm");
const hotel_entity_1 = require("../hotel/hotel.entity");
const userHotelWish_entity_1 = require("./userHotelWish.entity");
let UserService = class UserService {
    constructor(dataSource, userRepo, hotelRepo, hotelWishRepo) {
        this.dataSource = dataSource;
        this.userRepo = userRepo;
        this.hotelRepo = hotelRepo;
        this.hotelWishRepo = hotelWishRepo;
    }
    async createUser(userDTO) {
        const user = await this.userRepo.create(Object.assign({}, userDTO));
        await this.userRepo.save(user);
        return user;
    }
    async getUserByUID(uid) {
        console.log(uid);
        const user = await this.userRepo.findOneBy({ uid });
        if (!user) {
            throw new common_1.NotFoundException(`Not Found User`);
        }
        console.log("get user by uid : ", user);
        return user;
    }
    async getUserById(id) {
        const user = await this.userRepo.findOneBy({ id });
        if (!user) {
            throw new common_1.NotFoundException(`Not Found User`);
        }
        return user;
    }
    async createHotelWish(userId, hotelId) {
        const hotel = await this.hotelRepo.findOneBy({ id: hotelId });
        console.log("create hotel : ", hotel);
        if (!hotel) {
            throw new common_1.NotFoundException("존재하지 않는 호텔입니다.");
        }
        const user = await this.userRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.wishHotels", "wishHotel")
            .where("user.id = :id", { id: userId })
            .getOne();
        if (!user) {
            throw new common_1.NotFoundException("존재하지 않는 유저입니다.");
        }
        if (user.wishHotels.find(f => f.id === hotelId)) {
            throw new common_1.BadRequestException("이미 찜한 호텔입니다.");
        }
        user.wishHotels.push(hotel);
        await this.userRepo.save(user);
        return;
    }
    async getHotelWish(userId) {
        const user = await this.userRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.wishHotels", "wishHotel")
            .leftJoinAndSelect("wishHotel.photos", "wishHotelPhoto")
            .where("user.id = :userId", { userId })
            .getOne();
        return user.wishHotels;
    }
    async deleteHotelWish(userId, hotelId) {
        console.log("userId : ", userId, " / hotelId : ", hotelId);
        let a = this.dataSource.createQueryBuilder()
            .delete()
            .from('user_table_wish_hotels_hotel_table')
            .where('userTableId= :userId', { userId })
            .andWhere('hotelTableId= :hotelId', { hotelId })
            .execute();
        console.log("Aa : ", a);
        return;
    }
    async createHotelReservaton(userId, hotelId) {
        const hotel = await this.hotelRepo.findOneBy({ id: hotelId });
        if (!hotel) {
            throw new common_1.NotFoundException("존재하지 않는 호텔입니다.");
        }
        const user = await this.userRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.reservationHotels", "reservationHotel")
            .where("user.id= : id ", { id: userId })
            .getOne();
        console.log("user : ", user);
        if (!user) {
            throw new common_1.NotFoundException("존재하지 않는 유저입니다.");
        }
        if (user.reservateionHotels.find(f => f.id === hotelId)) {
            throw new common_1.BadRequestException("이미 찜한 호텔입니다.");
        }
        user.reservateionHotels.push(hotel);
        await this.userRepo.save(user);
        return;
    }
    async getReservedHotel(userID) {
        const hotel = await this.hotelRepo.createQueryBuilder("hotel")
            .leftJoinAndSelect("hotel.reservateionHotels", "reservateionHotels")
            .where("hotel.id = :hotelId", { hotelId: 1 })
            .getOne();
        return hotel.reservedUsers;
    }
};
UserService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(user_entity_1.UserEntity)),
    __param(2, (0, typeorm_1.InjectRepository)(hotel_entity_1.HotelEntity)),
    __param(3, (0, typeorm_1.InjectRepository)(userHotelWish_entity_1.UserHotelWishEntity)),
    __metadata("design:paramtypes", [typeorm_2.DataSource,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map