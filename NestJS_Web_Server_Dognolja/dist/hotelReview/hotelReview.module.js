"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HotelReviewModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const auth_module_1 = require("../auth/auth.module");
const user_entity_1 = require("../auth/user.entity");
const hotel_entity_1 = require("../hotel/hotel.entity");
const hotel_module_1 = require("../hotel/hotel.module");
const hotelKakao_entity_1 = require("../hotel/hotelKakao.entity");
const hotelNaver_entity_1 = require("../hotel/hotelNaver.entity");
const hotelPhoto_entity_1 = require("../hotel/hotelPhoto.entity");
const uploads_service_1 = require("../uploads/uploads.service");
const user_module_1 = require("../user/user.module");
const hotelReview_controller_1 = require("./hotelReview.controller");
const hotelReview_entity_1 = require("./hotelReview.entity");
const hotelReview_service_1 = require("./hotelReview.service");
const hotelReviewPhoto_entity_1 = require("./hotelReviewPhoto.entity");
let HotelReviewModule = class HotelReviewModule {
};
HotelReviewModule = __decorate([
    (0, common_1.Module)({
        imports: [
            auth_module_1.AuthModule,
            typeorm_1.TypeOrmModule.forFeature([
                hotel_entity_1.HotelEntity,
                hotelPhoto_entity_1.HotelPhotoEntity,
                hotelReview_entity_1.HotelReviewEntity,
                hotelReviewPhoto_entity_1.HotelReviewPhotoEntity,
                user_entity_1.UserEntity,
                hotelKakao_entity_1.HotelKakaoEntity,
                hotelNaver_entity_1.HotelNaverEntity
            ]),
            user_module_1.UserModule,
            hotel_module_1.HotelModule
        ],
        exports: [hotelReview_service_1.HotelReviewService],
        controllers: [hotelReview_controller_1.HotelReviewController],
        providers: [hotelReview_service_1.HotelReviewService, uploads_service_1.UploadsService]
    })
], HotelReviewModule);
exports.HotelReviewModule = HotelReviewModule;
//# sourceMappingURL=hotelReview.module.js.map