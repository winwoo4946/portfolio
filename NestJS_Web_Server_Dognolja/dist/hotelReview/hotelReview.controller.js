"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HotelReviewController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const auth_decorator_1 = require("../auth/auth.decorator");
const beautyReview_dto_1 = require("../commonDto/beautyReview.dto");
const hotelReview_service_1 = require("./hotelReview.service");
let HotelReviewController = class HotelReviewController {
    constructor(hotelReivewService) {
        this.hotelReivewService = hotelReivewService;
    }
    getHotelReviews(id) {
        return this.hotelReivewService.getHotelReviews(id);
    }
    addReview(authInfo, reviewDTO, files) {
        return this.hotelReivewService.createReview(authInfo, reviewDTO, files);
    }
    getReviews(id) {
        return this.hotelReivewService.getHotelReviewsByUser(id);
    }
    getWrittenHotel(id) {
        return this.hotelReivewService.getWrittenShopReviewList(id);
    }
};
__decorate([
    (0, common_1.Get)('/:id'),
    __param(0, (0, common_1.Param)('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], HotelReviewController.prototype, "getHotelReviews", null);
__decorate([
    (0, common_1.Post)('createReview'),
    (0, common_1.UseGuards)(auth_decorator_1.JwtAuthGuard),
    (0, common_1.UseInterceptors)((0, platform_express_1.FilesInterceptor)('files', 5)),
    __param(0, (0, auth_decorator_1.GetAuthUserInfo)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.UploadedFiles)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, beautyReview_dto_1.ShopReviewDTO,
        Array]),
    __metadata("design:returntype", void 0)
], HotelReviewController.prototype, "addReview", null);
__decorate([
    (0, common_1.Get)('getReviews/:id'),
    __param(0, (0, common_1.Param)('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], HotelReviewController.prototype, "getReviews", null);
__decorate([
    (0, common_1.Get)('getWrittenHotel/:id'),
    __param(0, (0, common_1.Param)('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], HotelReviewController.prototype, "getWrittenHotel", null);
HotelReviewController = __decorate([
    (0, common_1.Controller)('hotelReview'),
    __metadata("design:paramtypes", [hotelReview_service_1.HotelReviewService])
], HotelReviewController);
exports.HotelReviewController = HotelReviewController;
//# sourceMappingURL=hotelReview.controller.js.map