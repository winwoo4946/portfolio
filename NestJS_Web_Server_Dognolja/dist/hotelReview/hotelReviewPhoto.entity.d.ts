import { BaseEntity } from "typeorm";
import { HotelReviewEntity } from "./hotelReview.entity";
export declare class HotelReviewPhotoEntity extends BaseEntity {
    id: number;
    url: string;
    review: HotelReviewEntity;
}
