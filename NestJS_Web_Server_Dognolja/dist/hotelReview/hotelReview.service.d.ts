/// <reference types="multer" />
import { HotelEntity } from "src/hotel/hotel.entity";
import { HotelService } from "src/hotel/hotel.service";
import { UserService } from "src/user/user.service";
import { HotelReviewEntity } from "./hotelReview.entity";
import { Repository } from "typeorm";
import { AuthUserInfo } from "src/auth/login";
import { UploadsService } from "src/uploads/uploads.service";
import { HotelReviewPhotoEntity } from "./hotelReviewPhoto.entity";
import { ShopReviewDTO } from "src/commonDto/beautyReview.dto";
export declare class HotelReviewService {
    private readonly hotelService;
    private readonly userService;
    private readonly uploadsService;
    private readonly hotelReviewRepo;
    private readonly hotelReviewPhotoRepo;
    private readonly hotelRepo;
    constructor(hotelService: HotelService, userService: UserService, uploadsService: UploadsService, hotelReviewRepo: Repository<HotelReviewEntity>, hotelReviewPhotoRepo: Repository<HotelReviewPhotoEntity>, hotelRepo: Repository<HotelEntity>);
    createReview(authInfo: AuthUserInfo, hotelReviewDTO: ShopReviewDTO, files: Array<Express.Multer.File>): Promise<HotelReviewEntity>;
    getHotelReviews(hotelId: number): Promise<ReviewBase[]>;
    getHotelReviewCountByContent(content: string): Promise<number>;
    getHotelReviewsByUser(ownerId: number): Promise<ReviewBase[]>;
    getHotelReviewsByHotelId(hotelId: number): Promise<ReviewBase[]>;
    getWrittenShopReviewList(userId: number): Promise<HotelReviewEntity[]>;
    getWritableHotel(userId: number): Promise<HotelEntity[]>;
}
