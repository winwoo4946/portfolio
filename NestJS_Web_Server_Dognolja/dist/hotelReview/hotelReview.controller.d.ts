/// <reference types="multer" />
import { AuthUserInfo } from "src/auth/login";
import { ShopReviewDTO } from "src/commonDto/beautyReview.dto";
import { HotelReviewService } from "./hotelReview.service";
export declare class HotelReviewController {
    private readonly hotelReivewService;
    constructor(hotelReivewService: HotelReviewService);
    getHotelReviews(id: number): Promise<ReviewBase[]>;
    addReview(authInfo: AuthUserInfo, reviewDTO: ShopReviewDTO, files: Array<Express.Multer.File>): Promise<import("./hotelReview.entity").HotelReviewEntity>;
    getReviews(id: number): Promise<ReviewBase[]>;
    getWrittenHotel(id: number): Promise<import("./hotelReview.entity").HotelReviewEntity[]>;
}
