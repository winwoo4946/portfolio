"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HotelReviewEntity = void 0;
const hotel_entity_1 = require("../hotel/hotel.entity");
const user_entity_1 = require("../auth/user.entity");
const typeorm_1 = require("typeorm");
const hotelReviewPhoto_entity_1 = require("./hotelReviewPhoto.entity");
let HotelReviewEntity = class HotelReviewEntity extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("uuid"),
    __metadata("design:type", Number)
], HotelReviewEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)("text"),
    __metadata("design:type", String)
], HotelReviewEntity.prototype, "content", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], HotelReviewEntity.prototype, "score", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Date)
], HotelReviewEntity.prototype, "date", void 0);
__decorate([
    (0, typeorm_1.Index)(),
    (0, typeorm_1.ManyToOne)(type => hotel_entity_1.HotelEntity, hotel => hotel.reviews),
    __metadata("design:type", hotel_entity_1.HotelEntity)
], HotelReviewEntity.prototype, "hotel", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => user_entity_1.UserEntity, user => user.hotelReviews),
    __metadata("design:type", user_entity_1.UserEntity)
], HotelReviewEntity.prototype, "owner", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => hotelReviewPhoto_entity_1.HotelReviewPhotoEntity, photo => photo.review),
    __metadata("design:type", Array)
], HotelReviewEntity.prototype, "photos", void 0);
HotelReviewEntity = __decorate([
    (0, typeorm_1.Entity)('hotel_review_table')
], HotelReviewEntity);
exports.HotelReviewEntity = HotelReviewEntity;
//# sourceMappingURL=hotelReview.entity.js.map