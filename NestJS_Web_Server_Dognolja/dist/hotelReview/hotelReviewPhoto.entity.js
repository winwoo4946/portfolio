"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HotelReviewPhotoEntity = void 0;
const typeorm_1 = require("typeorm");
const hotelReview_entity_1 = require("./hotelReview.entity");
let HotelReviewPhotoEntity = class HotelReviewPhotoEntity extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], HotelReviewPhotoEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)("text"),
    __metadata("design:type", String)
], HotelReviewPhotoEntity.prototype, "url", void 0);
__decorate([
    (0, typeorm_1.Index)(),
    (0, typeorm_1.ManyToOne)(type => hotelReview_entity_1.HotelReviewEntity, review => review.photos),
    __metadata("design:type", hotelReview_entity_1.HotelReviewEntity)
], HotelReviewPhotoEntity.prototype, "review", void 0);
HotelReviewPhotoEntity = __decorate([
    (0, typeorm_1.Entity)('hotel_review_photo_table')
], HotelReviewPhotoEntity);
exports.HotelReviewPhotoEntity = HotelReviewPhotoEntity;
//# sourceMappingURL=hotelReviewPhoto.entity.js.map