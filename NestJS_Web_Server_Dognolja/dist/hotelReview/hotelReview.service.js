"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HotelReviewService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const hotel_entity_1 = require("../hotel/hotel.entity");
const hotel_service_1 = require("../hotel/hotel.service");
const user_service_1 = require("../user/user.service");
const hotelReview_entity_1 = require("./hotelReview.entity");
const typeorm_2 = require("typeorm");
const uploads_service_1 = require("../uploads/uploads.service");
const hotelReviewPhoto_entity_1 = require("./hotelReviewPhoto.entity");
let HotelReviewService = class HotelReviewService {
    constructor(hotelService, userService, uploadsService, hotelReviewRepo, hotelReviewPhotoRepo, hotelRepo) {
        this.hotelService = hotelService;
        this.userService = userService;
        this.uploadsService = uploadsService;
        this.hotelReviewRepo = hotelReviewRepo;
        this.hotelReviewPhotoRepo = hotelReviewPhotoRepo;
        this.hotelRepo = hotelRepo;
    }
    async createReview(authInfo, hotelReviewDTO, files) {
        const count = await this.getHotelReviewCountByContent(hotelReviewDTO.content);
        if (count > 0)
            return;
        const hotel = await this.hotelService.getHotelById(hotelReviewDTO.shopId);
        const user = await this.userService.getUserByUID(authInfo.uid);
        const photoUrls = await this.uploadsService.uploadReviewPhoto("hotel", hotelReviewDTO.shopId, files);
        const photos = [];
        for (let url of photoUrls) {
            const photo = this.hotelReviewPhotoRepo.create({
                url
            });
            await this.hotelReviewPhotoRepo.save(photo);
            photos.push(photo);
        }
        const review = this.hotelReviewRepo.create(Object.assign(Object.assign({}, hotelReviewDTO), { date: new Date(), hotel: hotel, owner: user, photos }));
        await this.hotelReviewRepo.save(review);
        return review;
    }
    async getHotelReviews(hotelId) {
        const hotelReviewEntity = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .leftJoin('hotelReview.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("hotelReview.hotelId = :hotelId", { hotelId })
            .getMany();
        return hotelReviewEntity;
    }
    async getHotelReviewCountByContent(content) {
        const found = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .where("hotelReview.content = :content", { content })
            .getCount();
        return found;
    }
    async getHotelReviewsByUser(ownerId) {
        const reviews = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .where("hotelReview.ownerId = :ownerId", { ownerId })
            .getMany();
        console.log("reviews : ", reviews);
        return reviews;
    }
    async getHotelReviewsByHotelId(hotelId) {
        const reviews = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .where("hotelReview.hotelId= :hotelId", { hotelId })
            .getMany();
        return reviews;
    }
    async getWrittenShopReviewList(userId) {
        const revieList = await this.hotelReviewRepo.createQueryBuilder("review")
            .leftJoin("review.hotel", "hotel")
            .addSelect(["hotel.id", "hotel.title", "hotel.phone", "hotel.url",
            "hotel.address", "hotel.roadAddress", "hotel.thumbnailPhotoId"])
            .leftJoinAndSelect('hotel.photos', 'photo')
            .where("review.ownerId= :id", { id: userId })
            .getMany();
        console.log("revieList : ", revieList);
        return revieList;
    }
    async getWritableHotel(userId) {
        return;
    }
};
HotelReviewService = __decorate([
    (0, common_1.Injectable)(),
    __param(3, (0, typeorm_1.InjectRepository)(hotelReview_entity_1.HotelReviewEntity)),
    __param(4, (0, typeorm_1.InjectRepository)(hotelReviewPhoto_entity_1.HotelReviewPhotoEntity)),
    __param(5, (0, typeorm_1.InjectRepository)(hotel_entity_1.HotelEntity)),
    __metadata("design:paramtypes", [hotel_service_1.HotelService,
        user_service_1.UserService,
        uploads_service_1.UploadsService,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], HotelReviewService);
exports.HotelReviewService = HotelReviewService;
//# sourceMappingURL=hotelReview.service.js.map