import { HotelEntity } from "src/hotel/hotel.entity";
import { UserEntity } from "src/auth/user.entity";
import { BaseEntity } from "typeorm";
import { HotelReviewPhotoEntity } from "./hotelReviewPhoto.entity";
export declare class HotelReviewEntity extends BaseEntity {
    id: number;
    content: string;
    score: number;
    date: Date;
    hotel: HotelEntity;
    owner: UserEntity;
    photos: HotelReviewPhotoEntity[];
}
