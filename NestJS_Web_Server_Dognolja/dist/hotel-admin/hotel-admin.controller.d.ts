export declare class HotelAdminController {
    reservations(): Promise<void>;
    uploadPhoto(): Promise<void>;
    updateHotelInfo(): Promise<void>;
    addHotel(): Promise<void>;
}
