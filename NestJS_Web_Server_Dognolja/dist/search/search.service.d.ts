import { ShopType } from 'src/dognolja_common/commonEnum';
import { BeautyService } from 'src/beauty/beauty.service';
import { HotelService } from 'src/hotel/hotel.service';
import { RedisService } from 'src/redis/redis.service';
export declare class SearchService {
    private readonly redisService;
    private readonly hotelService;
    private readonly beautyService;
    constructor(redisService: RedisService, hotelService: HotelService, beautyService: BeautyService);
    autoCompleteList(type: ShopType, keyword: string): Promise<string[]>;
    searchHotel(address: string): Promise<ShopBaseInfo[]>;
    searchBeauty(address: string): Promise<ShopBaseInfo[]>;
}
