"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SearchService = void 0;
const common_1 = require("@nestjs/common");
const korean_regexp_1 = require("korean-regexp");
const beauty_service_1 = require("../beauty/beauty.service");
const hotel_service_1 = require("../hotel/hotel.service");
const redis_service_1 = require("../redis/redis.service");
let SearchService = class SearchService {
    constructor(redisService, hotelService, beautyService) {
        this.redisService = redisService;
        this.hotelService = hotelService;
        this.beautyService = beautyService;
    }
    async autoCompleteList(type, keyword) {
        const keywordArr = keyword.split(' ');
        const searchKeyword = keywordArr[keywordArr.length - 1];
        const arr = this.redisService.getAddressArr(type);
        const keywordResults = [];
        for (let address of arr) {
            if (keywordResults.length >= this.redisService.ADDRESS_RESULT_COUNT)
                break;
            const match = address.search((0, korean_regexp_1.getRegExp)(searchKeyword));
            if (match === -1) {
                continue;
            }
            keywordResults.push(address);
        }
        const redisResults = await this.redisService.getAddressByKeyward(type, keywordResults);
        let results = [];
        for (let redisResult of redisResults) {
            if (keywordArr.length > 1) {
                for (let address of redisResult) {
                    if (address.indexOf(keyword) === -1)
                        continue;
                    results.push(address);
                }
            }
            else {
                results = [...results, ...redisResult];
            }
        }
        return results;
    }
    async searchHotel(address) {
        return await this.hotelService.getHotelsByAddress(address);
    }
    async searchBeauty(address) {
        return await this.beautyService.getBeautyByAddress(address);
    }
};
SearchService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [redis_service_1.RedisService,
        hotel_service_1.HotelService,
        beauty_service_1.BeautyService])
], SearchService);
exports.SearchService = SearchService;
//# sourceMappingURL=search.service.js.map