import { ShopType } from 'src/dognolja_common/commonEnum';
import { SearchService } from './search.service';
export declare class SearchController {
    private readonly searchService;
    constructor(searchService: SearchService);
    autoCompleteList(type: ShopType, keyword: string): Promise<string[]>;
    hotel(address: string): Promise<ShopBaseInfo[]>;
    beauty(address: string): Promise<ShopBaseInfo[]>;
}
