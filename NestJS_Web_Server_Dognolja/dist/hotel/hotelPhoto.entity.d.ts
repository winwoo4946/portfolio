import { BaseEntity } from "typeorm";
import { HotelEntity } from "./hotel.entity";
export declare class HotelPhotoEntity extends BaseEntity {
    id: number;
    url: string;
    hotel: HotelEntity;
}
