"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HotelEntity = void 0;
const user_entity_1 = require("../auth/user.entity");
const hotelReview_entity_1 = require("../hotelReview/hotelReview.entity");
const typeorm_1 = require("typeorm");
const hotelPhoto_entity_1 = require("./hotelPhoto.entity");
let HotelEntity = class HotelEntity extends typeorm_1.BaseEntity {
    constructor() {
        super(...arguments);
        this.price1 = 0;
        this.price2 = 0;
        this.price3 = 0;
        this.price4 = 0;
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], HotelEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], HotelEntity.prototype, "title", void 0);
__decorate([
    (0, typeorm_1.Column)("text"),
    __metadata("design:type", String)
], HotelEntity.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)("varchar", { length: 45, default: true }),
    __metadata("design:type", String)
], HotelEntity.prototype, "phone", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], HotelEntity.prototype, "categoryName", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], HotelEntity.prototype, "url", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], HotelEntity.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], HotelEntity.prototype, "roadAddress", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], HotelEntity.prototype, "mapx", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], HotelEntity.prototype, "mapy", void 0);
__decorate([
    (0, typeorm_1.Column)("float"),
    __metadata("design:type", Number)
], HotelEntity.prototype, "mapLat", void 0);
__decorate([
    (0, typeorm_1.Column)("float"),
    __metadata("design:type", Number)
], HotelEntity.prototype, "mapLng", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], HotelEntity.prototype, "thumbnailPhotoId", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], HotelEntity.prototype, "price1", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], HotelEntity.prototype, "price2", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], HotelEntity.prototype, "price3", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], HotelEntity.prototype, "price4", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => hotelReview_entity_1.HotelReviewEntity, review => review.hotel),
    __metadata("design:type", Array)
], HotelEntity.prototype, "reviews", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => hotelPhoto_entity_1.HotelPhotoEntity, photo => photo.hotel),
    __metadata("design:type", Array)
], HotelEntity.prototype, "photos", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => user_entity_1.UserEntity, (user) => user.wishHotels),
    __metadata("design:type", Array)
], HotelEntity.prototype, "wishUsers", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => user_entity_1.UserEntity, (user) => user.reservateionHotels),
    __metadata("design:type", Array)
], HotelEntity.prototype, "reservedUsers", void 0);
HotelEntity = __decorate([
    (0, typeorm_1.Entity)('hotel_table')
], HotelEntity);
exports.HotelEntity = HotelEntity;
//# sourceMappingURL=hotel.entity.js.map