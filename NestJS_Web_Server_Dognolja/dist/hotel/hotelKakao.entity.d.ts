import { BaseEntity } from "typeorm";
export declare class HotelKakaoEntity extends BaseEntity {
    id: number;
    kakaoMapId: number;
    title: string;
    description: string;
    phone: string;
    categoryName: string;
    kakaoPlaceUrl: string;
    address: string;
    roadAddress: string;
    x: number;
    y: number;
}
