export declare class HotelDTO {
    title: string;
    description: string;
    phone: string;
    photoUrls: string[];
    categoryName: string;
    url: string;
    address: string;
    roadAddress: string;
    mapx: number;
    mapy: number;
    mapLat: number;
    mapLng: number;
}
