import { HotelEntity } from './hotel.entity';
import { HotelService } from './hotel.service';
import { HotelDTO } from './dto/hotel.dto';
export declare class HotelController {
    private readonly hotelService;
    constructor(hotelService: HotelService);
    createHotel(hotelDTO: HotelDTO): Promise<HotelEntity>;
    getHotelById(id: number): Promise<ShopInfo>;
}
