import { UserEntity } from "src/auth/user.entity";
import { HotelReviewEntity } from "src/hotelReview/hotelReview.entity";
import { BaseEntity } from "typeorm";
import { HotelPhotoEntity } from "./hotelPhoto.entity";
export declare class HotelEntity extends BaseEntity {
    id: number;
    title: string;
    description: string;
    phone: string;
    categoryName: string;
    url: string;
    address: string;
    roadAddress: string;
    mapx: number;
    mapy: number;
    mapLat: number;
    mapLng: number;
    thumbnailPhotoId: number;
    price1: number;
    price2: number;
    price3: number;
    price4: number;
    reviews: HotelReviewEntity[];
    photos: HotelPhotoEntity[];
    wishUsers: UserEntity[];
    reservedUsers: UserEntity[];
}
