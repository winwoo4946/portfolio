"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HotelService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const hotel_entity_1 = require("./hotel.entity");
const hotelKakao_entity_1 = require("./hotelKakao.entity");
const hotelNaver_entity_1 = require("./hotelNaver.entity");
const typeorm_2 = require("typeorm");
const hotelPhoto_entity_1 = require("./hotelPhoto.entity");
let HotelService = class HotelService {
    constructor(hotelPhotoRepository, hotelKakaoRepository, hotelNaverRepository, hotelRepository) {
        this.hotelPhotoRepository = hotelPhotoRepository;
        this.hotelKakaoRepository = hotelKakaoRepository;
        this.hotelNaverRepository = hotelNaverRepository;
        this.hotelRepository = hotelRepository;
    }
    async getHotelById(id) {
        const found = await this.hotelRepository.findOneBy({ id });
        if (!found) {
            throw new common_1.NotFoundException(`Not found Hotel Info - id(${id})`);
        }
        return found;
    }
    async createHotel(hotelDTO) {
        const hotel = this.hotelRepository.create(hotelDTO);
        await this.hotelRepository.save(hotel);
        return hotel;
    }
    async getHotelsByAddress(address) {
        const hotelEntities = await this.hotelRepository.createQueryBuilder('hotel')
            .leftJoinAndSelect('hotel.photos', 'photo')
            .leftJoinAndSelect('hotel.reviews', 'review')
            .where("hotel.address like :address", { address: `%${address}%` })
            .getMany();
        const hotelInfos = [];
        for (let entity of hotelEntities) {
            hotelInfos.push(this.getShopBaseInfo(entity));
        }
        return hotelInfos;
    }
    async getHotelByAddressListData(address) {
        const result = await this.hotelRepository.createQueryBuilder('hotel')
            .select()
            .addSelect((subQuery) => {
            return subQuery.select("url").from(hotelPhoto_entity_1.HotelPhotoEntity, "photo").where("photo.id = hotel.thumbnailPhotoId");
        }, "photoUrl")
            .where("hotel.address like :address", { address: `%${address}%` })
            .getRawMany();
        return result;
    }
    async getHotelDetail(id) {
        const hotelEntity = await this.hotelRepository.createQueryBuilder('hotel')
            .leftJoin('hotel.photos', 'photo').addSelect(['photo.id', 'photo.url'])
            .leftJoinAndSelect('hotel.reviews', 'review')
            .leftJoin('review.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("hotel.id = :id", { id })
            .getOne();
        const shopInfo = Object.assign(Object.assign({}, this.getShopBaseInfo(hotelEntity)), { description: hotelEntity.description, phone: hotelEntity.phone, reviews: hotelEntity.reviews, photos: hotelEntity.photos, address: hotelEntity.address, roadAddress: hotelEntity.roadAddress, operatingTime: [], subInfos: ['~4.9kg', '호텔링', '(1일)', '25,000원'] });
        return shopInfo;
    }
    getShopBaseInfo(entity) {
        let score = 0;
        let reviewCount = entity.reviews.length;
        for (let review of entity.reviews) {
            score += review.score;
        }
        const shopBaseInfo = {
            id: entity.id,
            title: entity.title,
            mapLat: entity.mapLat,
            mapLng: entity.mapLng,
            price1: entity.price1,
            mapx: entity.mapx,
            mapy: entity.mapy,
            thumbnailUrl: entity.photos.length > 0 ? entity.photos[0].url : "",
            hashTags: [],
            score: Math.floor((score / reviewCount) * 10) / 10,
            reviewCount
        };
        return shopBaseInfo;
    }
    async getHotelCountByKakaoId(kakaoId) {
        const found = await this.hotelRepository
            .createQueryBuilder("hotel")
            .where("hotel.kakaoMapId = :kakaoId", { kakaoId })
            .getCount();
        return found;
    }
    async getHotelKakaoCountByKakaoId(kakaoId) {
        const found = await this.hotelKakaoRepository
            .createQueryBuilder("hotelKakao")
            .where("hotelKakao.kakaoMapId = :kakaoId", { kakaoId })
            .getCount();
        return found;
    }
    async getHotelKakaoCountByAddress(address) {
        const found = await this.hotelKakaoRepository
            .createQueryBuilder("hotelKakao")
            .where("hotelKakao.address = :address", { address })
            .getCount();
        return found;
    }
    async getHotelNaverCountByAddress(address) {
        const found = await this.hotelNaverRepository
            .createQueryBuilder("hotelNaver")
            .where("hotelNaver.address like :address", { address: `%${address}%` })
            .getCount();
        return found;
    }
    async getHotelPhotoCountByUrl(url) {
        const found = await this.hotelPhotoRepository
            .createQueryBuilder("hotelPhoto")
            .where("hotelPhoto.url = :url", { url })
            .getCount();
        return found;
    }
    async getAllHotelKakao() {
        return await this.hotelKakaoRepository
            .createQueryBuilder("hotelKakao")
            .getMany();
    }
    async getAllHotelNaver() {
        return await this.hotelNaverRepository
            .createQueryBuilder("hotelNaver")
            .getMany();
    }
    async getAllHotel() {
        return await this.hotelRepository.find();
    }
    async createHotelKakao(kakaoMapDTO) {
        const count = await this.getHotelKakaoCountByKakaoId(kakaoMapDTO.kakaoMapId);
        if (count > 0) {
            return;
        }
        const hotel = this.hotelKakaoRepository.create(kakaoMapDTO);
        await this.hotelKakaoRepository.save(hotel);
        return hotel;
    }
    async createHotelNaver(naverMapDTO) {
        const count = await this.getHotelNaverCountByAddress(naverMapDTO.address);
        if (count > 0) {
            return;
        }
        const hotel = this.hotelNaverRepository.create(naverMapDTO);
        await this.hotelNaverRepository.save(hotel);
        return hotel;
    }
    async createHotelGoogle(hotelDTO) {
        const hotel = this.hotelRepository.create(hotelDTO);
        await this.hotelRepository.save(hotelDTO);
        let promises = [];
        for (let photoUrl of hotelDTO.photoUrls) {
            const count = await this.getHotelPhotoCountByUrl(photoUrl);
            if (count > 0)
                continue;
            const photo = this.hotelPhotoRepository.create({
                url: photoUrl,
                hotel: hotel
            });
            promises.push(this.hotelPhotoRepository.save(photo));
        }
        await Promise.all(promises);
        return hotel;
    }
    async deleteHotelNaver(id) {
        const found = await this.hotelNaverRepository.findOneBy({ id });
        if (!found)
            return;
        await this.hotelNaverRepository
            .createQueryBuilder()
            .delete()
            .from(hotelNaver_entity_1.HotelNaverEntity)
            .where("id = :id", { id: id })
            .execute();
        console.log(`delete naver - ${id}`);
    }
    async updateHotelNaverZeroResult(id, isZero) {
        const found = await this.hotelNaverRepository.findOneBy({ id });
        if (!found)
            return;
        await this.hotelNaverRepository
            .createQueryBuilder()
            .update(hotelNaver_entity_1.HotelNaverEntity)
            .set({ zeroResult: isZero })
            .where("id = :id", { id: id })
            .execute();
        console.log(`update naver result - ${id} - ${isZero}`);
    }
};
HotelService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(hotelPhoto_entity_1.HotelPhotoEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(hotelKakao_entity_1.HotelKakaoEntity)),
    __param(2, (0, typeorm_1.InjectRepository)(hotelNaver_entity_1.HotelNaverEntity)),
    __param(3, (0, typeorm_1.InjectRepository)(hotel_entity_1.HotelEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], HotelService);
exports.HotelService = HotelService;
//# sourceMappingURL=hotel.service.js.map