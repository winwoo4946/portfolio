/// <reference types="multer" />
import { ConfigService } from '@nestjs/config';
import { ShopType } from 'src/dognolja_common/commonEnum';
export declare class UploadsService {
    private readonly configService;
    private readonly AWS_ACCESS_KEY;
    private readonly AWS_SECRET_KEY;
    private readonly BUCKET_NAME;
    private readonly REGION;
    private readonly OBJECT_URL;
    private readonly S3;
    constructor(configService: ConfigService);
    uploadReviewPhoto(type: ShopType, id: number, files: Array<Express.Multer.File>): Promise<string[]>;
}
