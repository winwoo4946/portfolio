"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadsService = void 0;
const client_s3_1 = require("@aws-sdk/client-s3");
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
let UploadsService = class UploadsService {
    constructor(configService) {
        this.configService = configService;
        this.BUCKET_NAME = 'dog-nolja-bucket';
        this.REGION = 'ap-northeast-2';
        this.OBJECT_URL = `https://${this.BUCKET_NAME}.s3.${this.REGION}.amazonaws.com/`;
        this.AWS_ACCESS_KEY = this.configService.get('AWS_ACCESS_KEY');
        this.AWS_SECRET_KEY = this.configService.get('AWS_SECRET_KEY');
        this.S3 = new client_s3_1.S3Client({
            region: this.REGION,
            credentials: {
                accessKeyId: this.AWS_ACCESS_KEY,
                secretAccessKey: this.AWS_SECRET_KEY
            }
        });
    }
    async uploadReviewPhoto(type, id, files) {
        const FOLDER_NAME = `${type}ReviewPhoto`;
        const objectUrls = [];
        const promises = [];
        for (let file of files) {
            const photoUrl = `${FOLDER_NAME}/${id}/${file.originalname}`;
            const promise = this.S3.send(new client_s3_1.PutObjectCommand({
                Bucket: this.BUCKET_NAME,
                Key: photoUrl,
                Body: file.buffer
            }));
            promises.push(promise);
            objectUrls.push(`${this.OBJECT_URL}${photoUrl}`);
        }
        await Promise.all(promises);
        return objectUrls;
    }
};
UploadsService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService])
], UploadsService);
exports.UploadsService = UploadsService;
//# sourceMappingURL=uploads.service.js.map