"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeautyReviewEntity = void 0;
const user_entity_1 = require("../auth/user.entity");
const beauty_entity_1 = require("../beauty/beauty.entity");
const typeorm_1 = require("typeorm");
const beautyReviewPhoto_entity_1 = require("./beautyReviewPhoto.entity");
let BeautyReviewEntity = class BeautyReviewEntity extends typeorm_1.BaseEntity {
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("uuid"),
    __metadata("design:type", Number)
], BeautyReviewEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)("text"),
    __metadata("design:type", String)
], BeautyReviewEntity.prototype, "content", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], BeautyReviewEntity.prototype, "score", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Date)
], BeautyReviewEntity.prototype, "date", void 0);
__decorate([
    (0, typeorm_1.Index)(),
    (0, typeorm_1.ManyToOne)(type => beauty_entity_1.BeautyEntity, beauty => beauty.reviews),
    __metadata("design:type", beauty_entity_1.BeautyEntity)
], BeautyReviewEntity.prototype, "beauty", void 0);
__decorate([
    (0, typeorm_1.ManyToOne)(type => user_entity_1.UserEntity, user => user.hotelReviews),
    __metadata("design:type", user_entity_1.UserEntity)
], BeautyReviewEntity.prototype, "owner", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => beautyReviewPhoto_entity_1.BeautyReviewPhotoEntity, photo => photo.review),
    __metadata("design:type", Array)
], BeautyReviewEntity.prototype, "photos", void 0);
BeautyReviewEntity = __decorate([
    (0, typeorm_1.Entity)('beauty_review_table')
], BeautyReviewEntity);
exports.BeautyReviewEntity = BeautyReviewEntity;
//# sourceMappingURL=beautyReview.entity.js.map