"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeautyReviewModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const beauty_module_1 = require("../beauty/beauty.module");
const uploads_service_1 = require("../uploads/uploads.service");
const user_module_1 = require("../user/user.module");
const beauty_review_controller_1 = require("./beauty-review.controller");
const beauty_review_service_1 = require("./beauty-review.service");
const beautyReview_entity_1 = require("./beautyReview.entity");
const beautyReviewPhoto_entity_1 = require("./beautyReviewPhoto.entity");
let BeautyReviewModule = class BeautyReviewModule {
};
BeautyReviewModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                beautyReview_entity_1.BeautyReviewEntity,
                beautyReviewPhoto_entity_1.BeautyReviewPhotoEntity,
            ]),
            user_module_1.UserModule,
            beauty_module_1.BeautyModule
        ],
        exports: [beauty_review_service_1.BeautyReviewService],
        controllers: [beauty_review_controller_1.BeautyReviewController],
        providers: [beauty_review_service_1.BeautyReviewService, uploads_service_1.UploadsService]
    })
], BeautyReviewModule);
exports.BeautyReviewModule = BeautyReviewModule;
//# sourceMappingURL=beauty-review.module.js.map