import { BaseEntity } from "typeorm";
import { BeautyReviewEntity } from "./beautyReview.entity";
export declare class BeautyReviewPhotoEntity extends BaseEntity {
    id: number;
    url: string;
    review: BeautyReviewEntity;
}
