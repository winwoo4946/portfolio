/// <reference types="multer" />
import { AuthUserInfo } from 'src/auth/login';
import { BeautyEntity } from 'src/beauty/beauty.entity';
import { BeautyService } from 'src/beauty/beauty.service';
import { ShopReviewDTO } from 'src/commonDto/beautyReview.dto';
import { UploadsService } from 'src/uploads/uploads.service';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';
import { BeautyReviewEntity } from './beautyReview.entity';
import { BeautyReviewPhotoEntity } from './beautyReviewPhoto.entity';
export declare class BeautyReviewService {
    private readonly beautyService;
    private readonly userService;
    private readonly uploadsService;
    private readonly beautyReviewRepo;
    private readonly beautyReviewPhotoRepo;
    constructor(beautyService: BeautyService, userService: UserService, uploadsService: UploadsService, beautyReviewRepo: Repository<BeautyReviewEntity>, beautyReviewPhotoRepo: Repository<BeautyReviewPhotoEntity>);
    createReview(authInfo: AuthUserInfo, reviewDTO: ShopReviewDTO, files: Array<Express.Multer.File>): Promise<ReviewBase>;
    createReviewByMap(beauty: BeautyEntity, content: string, score: number, date: Date): Promise<BeautyReviewEntity>;
    getBeautyReviews(beautyId: number): Promise<ReviewBase[]>;
    getBeautyReviewCountByContent(content: string): Promise<number>;
}
