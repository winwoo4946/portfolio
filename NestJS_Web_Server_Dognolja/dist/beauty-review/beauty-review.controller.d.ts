/// <reference types="multer" />
import { AuthUserInfo } from 'src/auth/login';
import { ShopReviewDTO } from 'src/commonDto/beautyReview.dto';
import { BeautyReviewService } from './beauty-review.service';
export declare class BeautyReviewController {
    private readonly beautyReivewService;
    constructor(beautyReivewService: BeautyReviewService);
    getBeautyReviews(id: number): Promise<ReviewBase[]>;
    addReview(authInfo: AuthUserInfo, reviewDTO: ShopReviewDTO, files: Array<Express.Multer.File>): Promise<ReviewBase>;
}
