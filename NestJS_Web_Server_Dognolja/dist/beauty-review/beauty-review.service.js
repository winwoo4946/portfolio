"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeautyReviewService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const beauty_service_1 = require("../beauty/beauty.service");
const uploads_service_1 = require("../uploads/uploads.service");
const user_service_1 = require("../user/user.service");
const typeorm_2 = require("typeorm");
const beautyReview_entity_1 = require("./beautyReview.entity");
const beautyReviewPhoto_entity_1 = require("./beautyReviewPhoto.entity");
let BeautyReviewService = class BeautyReviewService {
    constructor(beautyService, userService, uploadsService, beautyReviewRepo, beautyReviewPhotoRepo) {
        this.beautyService = beautyService;
        this.userService = userService;
        this.uploadsService = uploadsService;
        this.beautyReviewRepo = beautyReviewRepo;
        this.beautyReviewPhotoRepo = beautyReviewPhotoRepo;
    }
    async createReview(authInfo, reviewDTO, files) {
        const count = await this.getBeautyReviewCountByContent(reviewDTO.content);
        if (count > 0)
            return;
        const beauty = await this.beautyService.getBeautyById(reviewDTO.shopId);
        const user = await this.userService.getUserByUID(authInfo.uid);
        const photoUrls = await this.uploadsService.uploadReviewPhoto('beauty', reviewDTO.shopId, files);
        const photos = [];
        for (let url of photoUrls) {
            const photo = this.beautyReviewPhotoRepo.create({
                url
            });
            await this.beautyReviewPhotoRepo.save(photo);
            photos.push(photo);
        }
        const review = this.beautyReviewRepo.create(Object.assign(Object.assign({}, reviewDTO), { date: new Date(), beauty: beauty, owner: user, photos }));
        await this.beautyReviewRepo.save(review);
        return review;
    }
    async createReviewByMap(beauty, content, score, date) {
        const count = await this.getBeautyReviewCountByContent(content);
        if (count > 0)
            return;
        const owner = await this.userService.getUserById(1);
        const review = this.beautyReviewRepo.create({
            content,
            score,
            date,
            beauty,
            owner
        });
        await this.beautyReviewRepo.save(review);
        return review;
    }
    async getBeautyReviews(beautyId) {
        const reviews = await this.beautyReviewRepo
            .createQueryBuilder("beautyReview")
            .leftJoin('beautyReview.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("beautyReview.beautyId = :beautyId", { beautyId: beautyId })
            .getMany();
        return reviews;
    }
    async getBeautyReviewCountByContent(content) {
        const found = await this.beautyReviewRepo
            .createQueryBuilder("beautyReview")
            .where("beautyReview.content = :content", { content })
            .getCount();
        return found;
    }
};
BeautyReviewService = __decorate([
    (0, common_1.Injectable)(),
    __param(3, (0, typeorm_1.InjectRepository)(beautyReview_entity_1.BeautyReviewEntity)),
    __param(4, (0, typeorm_1.InjectRepository)(beautyReviewPhoto_entity_1.BeautyReviewPhotoEntity)),
    __metadata("design:paramtypes", [beauty_service_1.BeautyService,
        user_service_1.UserService,
        uploads_service_1.UploadsService,
        typeorm_2.Repository,
        typeorm_2.Repository])
], BeautyReviewService);
exports.BeautyReviewService = BeautyReviewService;
//# sourceMappingURL=beauty-review.service.js.map