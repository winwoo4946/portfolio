"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeautyReviewController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const auth_decorator_1 = require("../auth/auth.decorator");
const beautyReview_dto_1 = require("../commonDto/beautyReview.dto");
const beauty_review_service_1 = require("./beauty-review.service");
let BeautyReviewController = class BeautyReviewController {
    constructor(beautyReivewService) {
        this.beautyReivewService = beautyReivewService;
    }
    getBeautyReviews(id) {
        return this.beautyReivewService.getBeautyReviews(id);
    }
    addReview(authInfo, reviewDTO, files) {
        return this.beautyReivewService.createReview(authInfo, reviewDTO, files);
    }
};
__decorate([
    (0, common_1.Get)('/:id'),
    __param(0, (0, common_1.Param)('id', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], BeautyReviewController.prototype, "getBeautyReviews", null);
__decorate([
    (0, common_1.Post)('createReview'),
    (0, common_1.UseGuards)(auth_decorator_1.JwtAuthGuard),
    (0, common_1.UseInterceptors)((0, platform_express_1.FilesInterceptor)('files', 5)),
    __param(0, (0, auth_decorator_1.GetAuthUserInfo)()),
    __param(1, (0, common_1.Body)()),
    __param(2, (0, common_1.UploadedFiles)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, beautyReview_dto_1.ShopReviewDTO,
        Array]),
    __metadata("design:returntype", void 0)
], BeautyReviewController.prototype, "addReview", null);
BeautyReviewController = __decorate([
    (0, common_1.Controller)('beautyReview'),
    __metadata("design:paramtypes", [beauty_review_service_1.BeautyReviewService])
], BeautyReviewController);
exports.BeautyReviewController = BeautyReviewController;
//# sourceMappingURL=beauty-review.controller.js.map