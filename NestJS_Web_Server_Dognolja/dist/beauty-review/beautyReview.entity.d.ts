import { UserEntity } from "src/auth/user.entity";
import { BeautyEntity } from "src/beauty/beauty.entity";
import { BaseEntity } from "typeorm";
import { BeautyReviewPhotoEntity } from "./beautyReviewPhoto.entity";
export declare class BeautyReviewEntity extends BaseEntity {
    id: number;
    content: string;
    score: number;
    date: Date;
    beauty: BeautyEntity;
    owner: UserEntity;
    photos: BeautyReviewPhotoEntity[];
}
