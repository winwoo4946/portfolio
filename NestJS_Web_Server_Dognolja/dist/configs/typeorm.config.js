"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.typeORMConfigAsync = void 0;
const config_1 = require("@nestjs/config");
class TypeOrmConfig {
    static getOrmConfig(configService) {
        const option = {
            type: 'mysql',
            host: configService.get('DB_HOST'),
            port: configService.get('DB_PORT'),
            username: configService.get('DB_USERNAME'),
            password: configService.get('DB_PASSWORD'),
            database: configService.get('DB_NAME'),
            entities: [__dirname + '/../**/*.entity.{js,ts}'],
            synchronize: true,
            logging: true,
            charset: 'utf8mb4_general_ci'
        };
        console.log(`typeORMoption : ${JSON.stringify(option)}`);
        return option;
    }
}
exports.default = TypeOrmConfig;
exports.typeORMConfigAsync = {
    imports: [config_1.ConfigModule],
    inject: [config_1.ConfigService],
    useFactory: async (configService) => TypeOrmConfig.getOrmConfig(configService)
};
//# sourceMappingURL=typeorm.config.js.map