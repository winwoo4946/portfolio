"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.redisConfigAsync = void 0;
const config_1 = require("@nestjs/config");
const redisStore = require("cache-manager-ioredis");
class RedisConfig {
    static getConfig(configService) {
        const option = {
            store: redisStore,
            host: configService.get('REDIS_HOST'),
            port: configService.get('REDIS_PORT'),
            ttl: 5
        };
        console.log(`redisOption : ${JSON.stringify(option)}`);
        return option;
    }
}
exports.default = RedisConfig;
exports.redisConfigAsync = {
    imports: [config_1.ConfigModule],
    inject: [config_1.ConfigService],
    useFactory: async (configService) => RedisConfig.getConfig(configService)
};
//# sourceMappingURL=redis.config.js.map