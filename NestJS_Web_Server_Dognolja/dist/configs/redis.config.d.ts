import { CacheModuleAsyncOptions, CacheModuleOptions } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cache, Store, StoreConfig } from 'cache-manager';
export interface RedisCache extends Cache {
    store: RedisStore;
}
export interface RedisStore extends Store {
    name: 'redis';
    getClient: () => any;
    isCacheableValue: (value: any) => boolean;
}
export interface RedisStoreConfig extends StoreConfig {
    store: 'memory' | 'none' | RedisStore | {
        create(...args: any[]): Store;
    };
}
export default class RedisConfig {
    static getConfig(configService: ConfigService): CacheModuleOptions<RedisStoreConfig>;
}
export declare const redisConfigAsync: CacheModuleAsyncOptions;
