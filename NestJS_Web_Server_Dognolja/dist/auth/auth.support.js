"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthSupport = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const redis_service_1 = require("../redis/redis.service");
const jose = require("jose");
const axios_1 = require("axios");
const util_1 = require("../util/util");
const commonEnum_1 = require("../dognolja_common/commonEnum");
let AuthSupport = class AuthSupport {
    constructor(configService, redisService) {
        this.configService = configService;
        this.redisService = redisService;
    }
    getClientInfo(loginPlatform) {
        let client_id = '';
        let client_secret = '';
        let admin_key = '';
        let redirect_url = '';
        switch (loginPlatform) {
            case commonEnum_1.LoginPlatformType.Google:
                break;
            case commonEnum_1.LoginPlatformType.Naver:
                client_id = this.configService.get('NAVER_CLIENT_ID');
                client_secret = this.configService.get('NAVER_CLIENT_SECRET');
                redirect_url = this.configService.get('NAVER_LOGIN_REDRIECT_URL');
                break;
            case commonEnum_1.LoginPlatformType.Kakao:
                client_id = this.configService.get('KAKAO_REST_API_KEY');
                client_secret = this.configService.get('KAKAO_CLIENT_SECRET');
                admin_key = this.configService.get('KAKAO_ADMIN_KEY');
                redirect_url = this.configService.get('KAKAO_LOGIN_REDRIECT_URL');
                break;
        }
        return {
            client_id,
            client_secret,
            admin_key,
            redirect_url
        };
    }
    setAuthCookie(res, loginCookie) {
        const cookieOption = {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        };
        const nowTime = new Date().getTime();
        res.cookie(commonEnum_1.AuthCookieKeys.LOGIN_PLATFORM, loginCookie.platform);
        res.cookie(commonEnum_1.AuthCookieKeys.ID_TOKEN, loginCookie.idToken.token, cookieOption);
        res.cookie(commonEnum_1.AuthCookieKeys.ID_TOKEN_EXPIRES_IN, nowTime + (loginCookie.idToken.expiresIn * 1000));
        res.cookie(commonEnum_1.AuthCookieKeys.REFRESH_TOKEN, loginCookie.refreshToken.token, cookieOption);
        res.cookie(commonEnum_1.AuthCookieKeys.REFRESH_TOKEN_EXPIRES_IN, nowTime + (loginCookie.refreshToken.expiresIn * 1000));
    }
    clearAuthCookie(res) {
        res.clearCookie(commonEnum_1.AuthCookieKeys.LOGIN_PLATFORM);
        res.clearCookie(commonEnum_1.AuthCookieKeys.ID_TOKEN);
        res.clearCookie(commonEnum_1.AuthCookieKeys.ID_TOKEN_EXPIRES_IN);
        res.clearCookie(commonEnum_1.AuthCookieKeys.REFRESH_TOKEN);
        res.clearCookie(commonEnum_1.AuthCookieKeys.REFRESH_TOKEN_EXPIRES_IN);
    }
    async validateIdTokenByCookie(cookies) {
        const idToken = cookies[commonEnum_1.AuthCookieKeys.ID_TOKEN];
        if (!idToken) {
            throw new common_1.UnauthorizedException('Not found id Token');
        }
        await this.checkIdTokenExpiresIn(cookies);
        const loginPlatform = cookies[commonEnum_1.AuthCookieKeys.LOGIN_PLATFORM];
        const verifyToken = await this.validateIdToken(loginPlatform, idToken);
        return parseInt(verifyToken.payload.sub);
    }
    async validateIdToken(platform, token) {
        const clientInfo = this.getClientInfo(platform);
        switch (platform) {
            case commonEnum_1.LoginPlatformType.Google:
                return null;
            case commonEnum_1.LoginPlatformType.Naver:
                return null;
            case commonEnum_1.LoginPlatformType.Kakao:
                return await this.validateForKakao(token, clientInfo.client_id);
        }
    }
    async validateForKakao(token, appKey) {
        common_1.Logger.debug(token, 'id_token');
        const tokenArr = token.split('.');
        const header = this.decodeBase64(tokenArr[0]);
        common_1.Logger.debug(header, 'Token header');
        const payload = this.decodeBase64(tokenArr[1]);
        common_1.Logger.debug(payload, 'Token payload');
        if (payload.iss !== 'https://kauth.kakao.com') {
            throw new common_1.UnauthorizedException('login fail (iss)');
        }
        if (payload.aud !== appKey) {
            throw new common_1.UnauthorizedException('login fail (aud)');
        }
        if ((payload.exp * 1000) <= new Date().getTime()) {
            throw new common_1.UnauthorizedException('login fail (exp)');
        }
        const publicKeys = await this.getAuthOIDCPulbicKeys();
        const foundKey = publicKeys.find(s => s.kid === header.kid);
        if (!foundKey) {
            throw new common_1.UnauthorizedException('login fail (kid)');
        }
        common_1.Logger.debug(foundKey, 'Token Sign');
        const rsaPublicKey = await jose.importJWK({
            kty: foundKey.kty,
            e: foundKey.e,
            n: foundKey.n
        }, foundKey.alg);
        try {
            return await jose.jwtVerify(token, rsaPublicKey);
        }
        catch (e) {
            throw new common_1.UnauthorizedException(e.code, e.message);
        }
    }
    async checkIdTokenExpiresIn(cookies) {
        const idTokenExpiresIn = cookies[commonEnum_1.AuthCookieKeys.ID_TOKEN_EXPIRES_IN];
        if (new Date().getTime() > parseInt(idTokenExpiresIn)) {
            const platformType = cookies[commonEnum_1.AuthCookieKeys.LOGIN_PLATFORM];
            const refreshToken = cookies[commonEnum_1.AuthCookieKeys.REFRESH_TOKEN];
            const { client_id } = this.getClientInfo(platformType);
            const tokenBody = {
                'grant_type': 'refresh_token',
                'client_id': client_id,
                'refresh_token': refreshToken
            };
            const refresh = await axios_1.default.post(`https://kauth.kakao.com/oauth/token`, (0, util_1.jsonToQueryString)(tokenBody), {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            });
            refresh.data;
        }
    }
    async getAuthOIDCPulbicKeys() {
        let publicKeys = await this.redisService.get(this.redisService.AUTH_OIDC_PUBLIC_KEY);
        if (publicKeys) {
            return JSON.parse(publicKeys);
        }
        const get = await axios_1.default.get('https://kauth.kakao.com/.well-known/jwks.json');
        publicKeys = get.data.keys;
        const ttl = 60 * 60 * 2;
        await this.redisService.setEx(this.redisService.AUTH_OIDC_PUBLIC_KEY, JSON.stringify(publicKeys), ttl);
        return publicKeys;
    }
    decodeBase64(token) {
        const buffer = Buffer.from(token, 'base64');
        return JSON.parse(buffer.toString());
    }
};
AuthSupport = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService,
        redis_service_1.RedisService])
], AuthSupport);
exports.AuthSupport = AuthSupport;
//# sourceMappingURL=auth.support.js.map