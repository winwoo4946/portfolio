"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IdTokenGuard = void 0;
const common_1 = require("@nestjs/common");
const commonEnum_1 = require("../dognolja_common/commonEnum");
const auth_support_1 = require("./auth.support");
let IdTokenGuard = class IdTokenGuard {
    constructor(authSupport) {
        this.authSupport = authSupport;
    }
    async canActivate(context) {
        const request = context.switchToHttp().getRequest();
        const platformType = request.cookies[commonEnum_1.AuthCookieKeys.LOGIN_PLATFORM];
        const platformId = await this.authSupport.validateIdTokenByCookie(request.cookies);
        request.authInfo = {
            platformType,
            platformId
        };
        return true;
    }
};
IdTokenGuard = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [auth_support_1.AuthSupport])
], IdTokenGuard);
exports.IdTokenGuard = IdTokenGuard;
//# sourceMappingURL=idToken.guard.js.map