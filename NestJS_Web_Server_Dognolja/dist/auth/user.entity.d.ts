import { BeautyReviewEntity } from "src/beauty-review/beautyReview.entity";
import { HotelEntity } from "src/hotel/hotel.entity";
import { HotelReviewEntity } from "src/hotelReview/hotelReview.entity";
import { BaseEntity } from "typeorm";
export declare class UserEntity extends BaseEntity {
    id: number;
    uid: string;
    platformId: string;
    nickName: string;
    email: string;
    gender: string;
    age: string;
    thumbnailImgUrl: string;
    profileImgUrl: string;
    loginTime: Date;
    beautyReviews: BeautyReviewEntity[];
    hotelReviews: HotelReviewEntity[];
    wishHotels: HotelEntity[];
    reservateionHotels: HotelEntity[];
}
