import { CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthSupport } from './auth.support';
export declare class IdTokenGuard implements CanActivate {
    private readonly authSupport;
    constructor(authSupport: AuthSupport);
    canActivate(context: ExecutionContext): Promise<boolean>;
}
