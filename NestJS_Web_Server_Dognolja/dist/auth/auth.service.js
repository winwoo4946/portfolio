"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const user_entity_1 = require("./user.entity");
const jwt_1 = require("@nestjs/jwt");
const config_1 = require("@nestjs/config");
let AuthService = class AuthService {
    constructor(jwtService, configService, userRepo) {
        this.jwtService = jwtService;
        this.configService = configService;
        this.userRepo = userRepo;
    }
    async passportLogin(strategyInfo, res) {
        const token = await this.login(strategyInfo.loginInfoDTO);
        const maxAge = this.configService.get('JWT_EXPIRES_IN') * 1000;
        const expiresIn = new Date().getTime() + maxAge;
        res.cookie('DN_AT', token, { httpOnly: true, maxAge });
        res.cookie('DN_AT_EX', (expiresIn), { httpOnly: false, maxAge });
        return "<script>window.close();</script >";
    }
    async passportLogout(res) {
        res.clearCookie('DN_AT');
        res.clearCookie('DN_AT_EX');
    }
    async login(loginInfoDto) {
        let user = await this.userRepo.findOne({
            where: {
                uid: loginInfoDto.uid
            }
        });
        if (!user) {
            user = this.userRepo.create(loginInfoDto);
            await this.userRepo.save(user);
            common_1.Logger.debug(`${JSON.stringify(user)}`, 'Created');
        }
        else {
            await this.userRepo.update(user.id, { loginTime: new Date() });
        }
        const expiresIn = new Date().getTime() + (this.configService.get("JWT_EXPIRES_IN") * 1000);
        const payload = {
            uid: loginInfoDto.uid,
            loginTime: loginInfoDto.loginTime,
            expiresIn
        };
        const token = this.jwtService.sign(payload);
        return token;
    }
};
AuthService = __decorate([
    (0, common_1.Injectable)(),
    __param(2, (0, typeorm_1.InjectRepository)(user_entity_1.UserEntity)),
    __metadata("design:paramtypes", [jwt_1.JwtService,
        config_1.ConfigService,
        typeorm_2.Repository])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map