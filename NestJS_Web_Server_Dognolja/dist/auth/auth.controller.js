"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const common_1 = require("@nestjs/common");
const auth_service_1 = require("./auth.service");
const auth_decorator_1 = require("./auth.decorator");
let AuthController = class AuthController {
    constructor(authService) {
        this.authService = authService;
    }
    async platformLogout(res) {
        this.authService.passportLogout(res);
    }
    async kakaoLogin() {
        return common_1.HttpStatus.OK;
    }
    async kakaoLoginRedirect(strategyInfo, res) {
        return await this.authService.passportLogin(strategyInfo, res);
    }
    async naverLogin() {
        return common_1.HttpStatus.OK;
    }
    async naverLoginRedirect(strategyInfo, res) {
        return await this.authService.passportLogin(strategyInfo, res);
    }
    async googleLogin() {
        return common_1.HttpStatus.OK;
    }
    async googleLoginRedirect(strategyInfo, res) {
        return await this.authService.passportLogin(strategyInfo, res);
    }
    async authTest(userInfo) {
        console.log(userInfo);
    }
};
__decorate([
    (0, common_1.Post)('/platformLogout'),
    (0, common_1.HttpCode)(200),
    __param(0, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "platformLogout", null);
__decorate([
    (0, common_1.Get)('/kakaoLogin'),
    (0, common_1.HttpCode)(200),
    (0, common_1.UseGuards)(auth_decorator_1.KakaoAuthGuard),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "kakaoLogin", null);
__decorate([
    (0, common_1.Get)('/kakaoLoginRedirect'),
    (0, common_1.UseGuards)(auth_decorator_1.KakaoAuthGuard),
    __param(0, (0, auth_decorator_1.GetPlatformLoginInfo)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "kakaoLoginRedirect", null);
__decorate([
    (0, common_1.Get)('/naverLogin'),
    (0, common_1.HttpCode)(200),
    (0, common_1.UseGuards)(auth_decorator_1.NaverAuthGuard),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "naverLogin", null);
__decorate([
    (0, common_1.Get)('/naverLoginRedirect'),
    (0, common_1.UseGuards)(auth_decorator_1.NaverAuthGuard),
    __param(0, (0, auth_decorator_1.GetPlatformLoginInfo)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "naverLoginRedirect", null);
__decorate([
    (0, common_1.Get)('/googleLogin'),
    (0, common_1.HttpCode)(200),
    (0, common_1.UseGuards)(auth_decorator_1.GoogleAuthGuard),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "googleLogin", null);
__decorate([
    (0, common_1.Get)('/googleLoginRedirect'),
    (0, common_1.UseGuards)(auth_decorator_1.GoogleAuthGuard),
    __param(0, (0, auth_decorator_1.GetPlatformLoginInfo)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "googleLoginRedirect", null);
__decorate([
    (0, common_1.Get)("/authTest"),
    (0, common_1.UseGuards)(auth_decorator_1.JwtAuthGuard),
    __param(0, (0, auth_decorator_1.GetAuthUserInfo)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AuthController.prototype, "authTest", null);
AuthController = __decorate([
    (0, common_1.Controller)('auth'),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AuthController);
exports.AuthController = AuthController;
//# sourceMappingURL=auth.controller.js.map