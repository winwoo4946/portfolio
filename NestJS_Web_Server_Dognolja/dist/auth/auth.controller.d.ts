import { HttpStatus } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { AuthUserInfo, StrategyInfo } from './login';
export declare class AuthController {
    private readonly authService;
    constructor(authService: AuthService);
    platformLogout(res: Response): Promise<void>;
    kakaoLogin(): Promise<HttpStatus>;
    kakaoLoginRedirect(strategyInfo: StrategyInfo, res: Response): Promise<string>;
    naverLogin(): Promise<HttpStatus>;
    naverLoginRedirect(strategyInfo: StrategyInfo, res: Response): Promise<string>;
    googleLogin(): Promise<HttpStatus>;
    googleLoginRedirect(strategyInfo: StrategyInfo, res: Response): Promise<string>;
    authTest(userInfo: AuthUserInfo): Promise<void>;
}
