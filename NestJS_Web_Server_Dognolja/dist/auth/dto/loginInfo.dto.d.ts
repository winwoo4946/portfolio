export declare class LoginInfoDTO {
    uid: string;
    platformId: string;
    nickName: string;
    email: string;
    gender: string;
    age: string;
    thumbnailImgUrl: string;
    profileImgUrl: string;
    loginTime: Date;
}
