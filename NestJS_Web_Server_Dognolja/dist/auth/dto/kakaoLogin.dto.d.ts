import { LoginPlatformType } from "src/dognolja_common/commonEnum";
export declare class PlatformLoginDTO {
    platformType: LoginPlatformType;
    refresh_token: string;
}
