"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlatformLoginDTO = void 0;
const class_validator_1 = require("class-validator");
const commonEnum_1 = require("../../dognolja_common/commonEnum");
class PlatformLoginDTO {
}
__decorate([
    (0, class_validator_1.IsIn)([commonEnum_1.LoginPlatformType.Google, commonEnum_1.LoginPlatformType.Naver, commonEnum_1.LoginPlatformType.Kakao]),
    __metadata("design:type", String)
], PlatformLoginDTO.prototype, "platformType", void 0);
__decorate([
    (0, class_validator_1.IsString)(),
    __metadata("design:type", String)
], PlatformLoginDTO.prototype, "refresh_token", void 0);
exports.PlatformLoginDTO = PlatformLoginDTO;
//# sourceMappingURL=kakaoLogin.dto.js.map