import { Repository } from 'typeorm';
import { LoginInfoDTO } from './dto/loginInfo.dto';
import { AuthToken, StrategyInfo } from './login';
import { UserEntity } from './user.entity';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { Response } from 'express';
export declare class AuthService {
    private readonly jwtService;
    private readonly configService;
    private readonly userRepo;
    constructor(jwtService: JwtService, configService: ConfigService, userRepo: Repository<UserEntity>);
    passportLogin(strategyInfo: StrategyInfo, res: Response): Promise<string>;
    passportLogout(res: Response): Promise<void>;
    login(loginInfoDto: LoginInfoDTO): Promise<AuthToken>;
}
