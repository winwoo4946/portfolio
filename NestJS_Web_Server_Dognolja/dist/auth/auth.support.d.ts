import { ConfigService } from "@nestjs/config";
import { RedisService } from "src/redis/redis.service";
import { LoginClientInfo, LoginCookie } from "./login";
import { Response } from "express";
import { LoginPlatformType } from "src/dognolja_common/commonEnum";
export declare class AuthSupport {
    private readonly configService;
    private readonly redisService;
    constructor(configService: ConfigService, redisService: RedisService);
    getClientInfo(loginPlatform: LoginPlatformType): LoginClientInfo;
    setAuthCookie(res: Response, loginCookie: LoginCookie): void;
    clearAuthCookie(res: Response): void;
    validateIdTokenByCookie(cookies: any): Promise<number>;
    private validateIdToken;
    private validateForKakao;
    private checkIdTokenExpiresIn;
    private getAuthOIDCPulbicKeys;
    private decodeBase64;
}
