"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.JwtAuthGuard = exports.GoogleAuthGuard = exports.NaverAuthGuard = exports.KakaoAuthGuard = exports.GetAuthUserInfo = exports.GetPlatformLoginInfo = void 0;
const common_1 = require("@nestjs/common");
const passport_1 = require("@nestjs/passport");
exports.GetPlatformLoginInfo = (0, common_1.createParamDecorator)((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
});
exports.GetAuthUserInfo = (0, common_1.createParamDecorator)((data, ctx) => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
});
class KakaoAuthGuard extends (0, passport_1.AuthGuard)('kakao') {
}
exports.KakaoAuthGuard = KakaoAuthGuard;
;
class NaverAuthGuard extends (0, passport_1.AuthGuard)('naver') {
}
exports.NaverAuthGuard = NaverAuthGuard;
;
class GoogleAuthGuard extends (0, passport_1.AuthGuard)('google') {
}
exports.GoogleAuthGuard = GoogleAuthGuard;
;
class JwtAuthGuard extends (0, passport_1.AuthGuard)('jwt') {
}
exports.JwtAuthGuard = JwtAuthGuard;
;
//# sourceMappingURL=auth.decorator.js.map