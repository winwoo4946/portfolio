import { ConfigService } from "@nestjs/config";
import { Strategy } from "passport-kakao";
import { StrategyInfo } from "./login";
declare const KakaoStrategy_base: new (...args: any[]) => Strategy;
export declare class KakaoStrategy extends KakaoStrategy_base {
    constructor(configService: ConfigService);
    validate(accessToken: string, refreshToken: string, profile: any, done: (error: any, loginInfo?: StrategyInfo) => void): Promise<void>;
}
export {};
