import { ConfigService } from "@nestjs/config";
import { Strategy } from "passport-google-oauth20";
import { StrategyInfo } from "./login";
declare const GoogleStrategy_base: new (...args: any[]) => Strategy;
export declare class GoogleStrategy extends GoogleStrategy_base {
    constructor(configService: ConfigService);
    validate(accessToken: string, refreshToken: string, profile: any, done: (error: any, loginInfo?: StrategyInfo) => void): Promise<void>;
}
export {};
