import { ConfigService } from "@nestjs/config";
import { StrategyInfo } from "./login";
declare const NaverStrategy_base: new (...args: any[]) => any;
export declare class NaverStrategy extends NaverStrategy_base {
    constructor(configService: ConfigService);
    validate(accessToken: string, refreshToken: string, profile: any, done: (error: any, loginInfo?: StrategyInfo) => void): Promise<void>;
}
export {};
