"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RedisService = void 0;
const common_1 = require("@nestjs/common");
const commonEnum_1 = require("../dognolja_common/commonEnum");
const beauty_service_1 = require("../beauty/beauty.service");
const hotel_service_1 = require("../hotel/hotel.service");
let RedisService = class RedisService {
    constructor(cacheManager, hotelService, beautyService) {
        this.cacheManager = cacheManager;
        this.hotelService = hotelService;
        this.beautyService = beautyService;
        this.ADDRESS_TYPE_SET = [
            new Set(['시', '도']),
            new Set(['구', '군', '면', '동', '시', '읍']),
            new Set(['동', '가', '읍', '면', '리', '구']),
            new Set(['리', '동', '가', '면', '읍']),
        ];
        this.addressSetMap = new Map([
            [commonEnum_1.ShopType.HOTEL, new Set()],
            [commonEnum_1.ShopType.BEAUTY, new Set()]
        ]);
        this.addressArrayMap = new Map([
            [commonEnum_1.ShopType.HOTEL, []],
            [commonEnum_1.ShopType.BEAUTY, []]
        ]);
        this.ADDRESS_RESULT_COUNT = 5;
        this.ADDRESS_REDIS_KEY = 'address';
        this.AUTH_OIDC_PUBLIC_KEY = 'auth_OIDC_public_key';
        this.redisClient = this.cacheManager.store.getClient();
    }
    async onModuleInit() {
        await this.setAddressCache();
        console.log('redis init');
    }
    getAddressArr(type) {
        return this.addressArrayMap.get(type);
    }
    async getSample() {
        let redis = this.cacheManager.store.getClient();
        const numbers = [1, 3, 5, 7, 9];
        let add = await redis.sadd("user-set", numbers);
        console.log(`add0 : ${add}`);
        const elementCount = await redis.scard("user-set");
        console.log(elementCount);
        add = await redis.sadd("user-set", "1");
        console.log(`add1 : ${add}`);
        const newElementCount = await redis.scard("user-set");
        console.log(newElementCount);
        add = await redis.sadd("user-set", "2");
        console.log(`add2 : ${add}`);
        const newElementCount2 = await redis.scard("user-set");
        console.log(newElementCount2);
        const memebers = await redis.smembers("user-set");
        console.log(memebers);
        const isMember = await redis.sismember("user-set", 3);
        console.log(isMember);
        return "set";
    }
    async setAddressCache() {
        const hotels = await this.hotelService.getAllHotel();
        let promises = [];
        for (let hotel of hotels) {
            promises.push(this.saveAddress('hotel', hotel.address));
        }
        const beautys = await this.beautyService.getAllBeauty();
        for (let beauty of beautys) {
            promises.push(this.saveAddress('beauty', beauty.address));
        }
        await Promise.all(promises);
        this.addressArrayMap.set('hotel', Array.from(this.addressSetMap.get('hotel')));
        this.addressArrayMap.set('beauty', Array.from(this.addressSetMap.get('beauty')));
        this.addressSetMap.forEach((value) => {
            value.clear();
        });
        this.addressSetMap = null;
    }
    async saveAddress(type, address) {
        if (type === commonEnum_1.ShopType.ALL) {
            throw new Error("[all] Type can't save address");
        }
        const addressComponents = address.split(' ');
        const promises = [];
        for (let i = 1; i < this.ADDRESS_TYPE_SET.length; ++i) {
            if (i >= addressComponents.length)
                break;
            const addressKey = addressComponents[i];
            const fullAddress = addressComponents.slice(0, i + 1).join(' ');
            const lastChar = addressKey.at(addressKey.length - 1);
            if (!this.ADDRESS_TYPE_SET[i].has(lastChar))
                continue;
            promises.push(this.sadd(`address:${type}:${addressKey}`, fullAddress));
            this.addressSetMap.get(type).add(addressKey);
        }
        return promises;
    }
    async getAddressByKeyward(type, keywords) {
        let promises = [];
        for (let keywrodResult of keywords) {
            promises.push(this.smembers(`address:${type}:${keywrodResult}`));
        }
        return Promise.all(promises);
    }
    async get(key) {
        return this.redisClient.get(key);
    }
    async set(key, value) {
        return this.redisClient.set(key, value);
    }
    async setEx(key, value, ttl) {
        return this.redisClient.set(key, value, 'EX', ttl);
    }
    async sadd(key, value) {
        return this.redisClient.sadd(key, value);
    }
    async smembers(key) {
        return this.redisClient.smembers(key);
    }
};
RedisService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, common_1.Inject)(common_1.CACHE_MANAGER)),
    __metadata("design:paramtypes", [Object, hotel_service_1.HotelService,
        beauty_service_1.BeautyService])
], RedisService);
exports.RedisService = RedisService;
//# sourceMappingURL=redis.service.js.map