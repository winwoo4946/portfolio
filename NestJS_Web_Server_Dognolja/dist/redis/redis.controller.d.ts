import { RedisService } from "./redis.service";
export declare class RedisController {
    private readonly redisService;
    constructor(redisService: RedisService);
    getCache(): Promise<string>;
}
