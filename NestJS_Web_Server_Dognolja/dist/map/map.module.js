"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MapModule = void 0;
const axios_1 = require("@nestjs/axios");
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const beauty_review_module_1 = require("../beauty-review/beauty-review.module");
const beauty_module_1 = require("../beauty/beauty.module");
const beautyNaver_entity_1 = require("../beauty/beautyNaver.entity");
const hotel_entity_1 = require("../hotel/hotel.entity");
const hotel_module_1 = require("../hotel/hotel.module");
const hotelKakao_entity_1 = require("../hotel/hotelKakao.entity");
const hotelNaver_entity_1 = require("../hotel/hotelNaver.entity");
const hotelPhoto_entity_1 = require("../hotel/hotelPhoto.entity");
const hotelReview_module_1 = require("../hotelReview/hotelReview.module");
const map_controller_1 = require("./map.controller");
const map_service_1 = require("./map.service");
let MapModule = class MapModule {
};
MapModule = __decorate([
    (0, common_1.Module)({
        imports: [
            axios_1.HttpModule.register({
                timeout: 5000,
                maxRedirects: 5
            }),
            typeorm_1.TypeOrmModule.forFeature([
                hotel_entity_1.HotelEntity,
                hotelPhoto_entity_1.HotelPhotoEntity,
                hotelKakao_entity_1.HotelKakaoEntity,
                hotelNaver_entity_1.HotelNaverEntity,
                beautyNaver_entity_1.BeautyNaverEntity
            ]),
            hotel_module_1.HotelModule,
            hotelReview_module_1.HotelReviewModule,
            beauty_module_1.BeautyModule,
            beauty_review_module_1.BeautyReviewModule
        ],
        controllers: [map_controller_1.MapController],
        providers: [map_service_1.MapService]
    })
], MapModule);
exports.MapModule = MapModule;
//# sourceMappingURL=map.module.js.map