"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MapController = void 0;
const common_1 = require("@nestjs/common");
const map_service_1 = require("./map.service");
let MapController = class MapController {
    constructor(mapService) {
        this.mapService = mapService;
    }
    async getNaverMap() {
        return await this.mapService.naverMap();
    }
    async getKakaoMap() {
        return await this.mapService.kakaoMap();
    }
    async getGoogleMap() {
        return await this.mapService.googleMap();
    }
    async checkKakaoToNaver() {
        return await this.mapService.checkKakaoToNaver();
    }
    async checkNaverToKakao() {
        return await this.mapService.checkNaverToKakao();
    }
    async addressTest() {
        return await this.mapService.addressTest();
    }
    async googleMapPosition() {
        return await this.mapService.googleMapPosition();
    }
};
__decorate([
    (0, common_1.Get)('naverMap'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MapController.prototype, "getNaverMap", null);
__decorate([
    (0, common_1.Get)('kakaoMap'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MapController.prototype, "getKakaoMap", null);
__decorate([
    (0, common_1.Get)('googleMap'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MapController.prototype, "getGoogleMap", null);
__decorate([
    (0, common_1.Get)('checkKakaoToNaver'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MapController.prototype, "checkKakaoToNaver", null);
__decorate([
    (0, common_1.Get)('checkNaverToKakao'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MapController.prototype, "checkNaverToKakao", null);
__decorate([
    (0, common_1.Get)('addressTest'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MapController.prototype, "addressTest", null);
__decorate([
    (0, common_1.Get)('googleMapPosition'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], MapController.prototype, "googleMapPosition", null);
MapController = __decorate([
    (0, common_1.Controller)('map'),
    __metadata("design:paramtypes", [map_service_1.MapService])
], MapController);
exports.MapController = MapController;
//# sourceMappingURL=map.controller.js.map