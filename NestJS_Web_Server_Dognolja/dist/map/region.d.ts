export declare const sigRegion: {
    SIG_CD: number;
    SIG_ENG_NM: string;
    SIG_KOR_NM: string;
}[];
export declare const emdReigon: {
    EMD_CD: number;
    EMD_ENG_NM: string;
    EMD_KOR_NM: string;
}[];
