"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MapService = void 0;
const common_1 = require("@nestjs/common");
const config_1 = require("@nestjs/config");
const axios_1 = require("axios");
const beauty_review_service_1 = require("../beauty-review/beauty-review.service");
const beauty_service_1 = require("../beauty/beauty.service");
const hotel_service_1 = require("../hotel/hotel.service");
const hotelReview_service_1 = require("../hotelReview/hotelReview.service");
const promises_1 = require("timers/promises");
const region_1 = require("./region");
let MapService = class MapService {
    constructor(configService, hotelService, hotelReviewService, beautyService, beautyReviewService) {
        this.configService = configService;
        this.hotelService = hotelService;
        this.hotelReviewService = hotelReviewService;
        this.beautyService = beautyService;
        this.beautyReviewService = beautyReviewService;
        this.CLIENT_ID = this.configService.get('NAVER_CLIENT_ID');
        this.CLIENT_SECRET = this.configService.get('NAVER_CLIENT_SECRET');
        this.KAKAO_API_KEY = this.configService.get('KAKAO_REST_API_KEY');
    }
    async naverMap() {
        let map = new Map();
        let apiCount = 0;
        const regionList = region_1.emdReigon;
        for (let i = 0; i < regionList.length; ++i) {
            const regtionName = regionList[i].EMD_KOR_NM;
            const result = await axios_1.default.get('https://openapi.naver.com/v1/search/local.json', {
                headers: {
                    'X-Naver-Client-Id': this.CLIENT_ID,
                    'X-Naver-Client-Secret': this.CLIENT_SECRET
                },
                params: {
                    query: `${regtionName} 애견미용`,
                    display: 5,
                }
            });
            ++apiCount;
            await (0, promises_1.setTimeout)(100);
            for (let item of result.data.items) {
                if (map.has(item.address))
                    continue;
                const dto = {
                    title: item.title,
                    description: item.description,
                    categoryName: item.category,
                    naverUrl: item.link,
                    address: item.address,
                    roadAddress: item.roadAddress,
                    x: item.mapx,
                    y: item.mapy
                };
                map.set(item.address, dto);
            }
        }
        let addHotelCount = 0;
        for (let [key, value] of map) {
            this.beautyService.createBeautyNaver(value);
            addHotelCount++;
        }
        return `apiCount : ${apiCount} / addCount : ${addHotelCount}`;
    }
    async kakaoMap() {
        let map = new Map();
        const regionList = region_1.sigRegion;
        let apiCount = 0;
        for (let i = 0; i < regionList.length; ++i) {
            const regtionName = regionList[i].SIG_KOR_NM;
            const result = await axios_1.default.get(`https://dapi.kakao.com/v2/local/search/keyword.json`, {
                headers: {
                    'Authorization': `KakaoAK ${this.KAKAO_API_KEY}`,
                },
                params: {
                    query: `${regtionName} 애견미용`,
                    size: 15
                }
            });
            return;
            ++apiCount;
            for (let item of result.data.documents) {
                if (map.has(item.address_name))
                    continue;
                const dto = {
                    kakaoMapId: item.id,
                    title: item.place_name,
                    description: "",
                    phone: item.phone,
                    categoryName: item.category_name,
                    kakaoPlaceUrl: item.place_url,
                    address: item.address_name,
                    roadAddress: item.road_address_name,
                    x: item.x,
                    y: item.y
                };
                map.set(item.address_name, dto);
            }
        }
        let addHotelCount = 0;
        for (let [key, value] of map) {
            this.hotelService.createHotelKakao(value);
            ++addHotelCount;
        }
        return `apiCount : ${apiCount} / addHotelCount : ${addHotelCount}`;
    }
    async googleMap() {
        const key = this.configService.get('GOOGLE_MAP_API_KEY');
        const navers = await this.beautyService.getAllBeautyNaver();
        const mapDataList = [];
        const naverCount = navers.length;
        console.log(`naver count : ${naverCount}`);
        let count = 0;
        for (let naver of navers) {
            count++;
            if (naver.zeroResult) {
                continue;
            }
            const beautyCount = await this.beautyService.getBeautyCountByTitle(naver.address);
            if (beautyCount > 0) {
                continue;
            }
            const encoding = encodeURIComponent(naver.title);
            let placeInfo = await axios_1.default.get(`https://maps.googleapis.com/maps/api/place/textsearch/json?query=${encoding}&language=ko&key=${key}`);
            if (placeInfo.data.results.length <= 0) {
                await this.beautyService.updateBeautyNaverZeroResult(naver.id, true);
                continue;
            }
            const placeId = placeInfo.data.results[0].place_id;
            let placeDetail = await axios_1.default.get(`https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeId}&language=ko&key=${key}`);
            const placeDetailResult = placeDetail.data.result;
            let promises = [];
            const photoUrls = [];
            if (placeDetailResult.photos) {
                for (let photo of placeDetailResult.photos) {
                    promises.push(axios_1.default.get(`https://maps.googleapis.com/maps/api/place/photo?photo_reference=${photo.photo_reference}&maxheight=${1600}&maxwidth=${1600}&key=${key}`));
                }
                try {
                    const photoRefResult = await Promise.all(promises);
                    for (let photoRef of photoRefResult) {
                        photoUrls.push(photoRef.request.res.responseUrl);
                    }
                }
                catch (e) {
                    console.log(e.message);
                }
            }
            const beautyDTO = Object.assign({ phone: placeDetailResult.formatted_phone_number, url: naver.naverUrl, photoUrls: photoUrls, mapx: naver.x, mapy: naver.y, mapLat: placeDetailResult.geometry.location.lat, mapLng: placeDetailResult.geometry.location.lng }, naver);
            const beauty = await this.beautyService.createBeautyGoogle(beautyDTO);
            if (placeDetailResult.reviews) {
                for (let review of placeDetailResult.reviews) {
                    if (!review.text)
                        continue;
                    let date = new Date(review.time * 1000);
                    await this.beautyReviewService.createReviewByMap(beauty, review.text, review.rating, date);
                }
            }
            console.log(`count : ${count}/${naverCount}`);
        }
        return 'complete';
    }
    async googleMapPosition() {
        const key = this.configService.get('GOOGLE_MAP_API_KEY');
        const beautys = await this.beautyService.getAllBeauty();
        const promises = [];
        for (let beauty of beautys) {
            if (beauty.mapLat > 0 && beauty.mapLng > 0)
                continue;
            const s = beauty.address.split(' ');
            for (let i = s.length - 1; i >= 0; --i) {
                const lastWord = s[i];
                const lastChar = lastWord.at(lastWord.length - 1);
                if (isNaN(Number(lastChar))) {
                    s[i] = '';
                    continue;
                }
                break;
            }
            const address = s.join(' ');
            const result = await axios_1.default.get(`https://dapi.kakao.com/v2/local/search/keyword.json`, {
                headers: {
                    'Authorization': `KakaoAK ${this.KAKAO_API_KEY}`,
                },
                params: {
                    query: address,
                    size: 1
                }
            });
            if (result.data.documents.length > 0) {
                const data = result.data.documents[0];
                promises.push(this.beautyService.updateBeautyPosition(beauty.id, data.x, data.y));
            }
        }
        let count = 0;
        for (let p of promises) {
            p.then(() => {
                ++count;
                console.log(`${count} / ${promises.length} = ${Math.floor((count / promises.length) * 100)}%`);
            });
        }
        await Promise.all(promises);
        return "완료";
    }
    async checkKakaoToNaver() {
        const results = [];
        const kakakos = await this.hotelService.getAllHotelKakao();
        for (let kakao of kakakos) {
            const addressList = kakao.address.split(' ');
            const address = `${addressList[addressList.length - 2]} ${addressList[addressList.length - 1]}`;
            let count = await this.hotelService.getHotelNaverCountByAddress(address);
            results.push(`kakao(${kakao.id}) - ${address} match naver count - ${count}`);
        }
        return results.join('<br>');
    }
    async checkNaverToKakao() {
        const results = [];
        const navers = await this.hotelService.getAllHotelNaver();
        for (let naver of navers) {
            let count = await this.hotelService.getHotelKakaoCountByAddress(naver.address);
            if (count > 0) {
                results.push(`naver(${naver.id}) - ${naver.address} match kakao count - ${count}`);
            }
        }
        return results.join('<br>');
    }
    async addressTest() {
        const hotelList = await this.hotelService.getAllHotel();
        const map = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];
        for (let hotel of hotelList) {
            const addressComponents = hotel.address.split(' ');
            for (let i = 0; i < addressComponents.length; ++i) {
                const address = addressComponents[i];
                const lastChar = address.at(address.length - 1);
                if (!map[i][lastChar]) {
                    map[i][lastChar] = lastChar;
                }
            }
        }
        let result = '';
        for (let item of map) {
            result += `${Object.keys(item).join('/')}<br>`;
        }
        return result;
    }
};
MapService = __decorate([
    (0, common_1.Injectable)(),
    __metadata("design:paramtypes", [config_1.ConfigService,
        hotel_service_1.HotelService,
        hotelReview_service_1.HotelReviewService,
        beauty_service_1.BeautyService,
        beauty_review_service_1.BeautyReviewService])
], MapService);
exports.MapService = MapService;
//# sourceMappingURL=map.service.js.map