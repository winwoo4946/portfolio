import { MapService } from './map.service';
export declare class MapController {
    private readonly mapService;
    constructor(mapService: MapService);
    getNaverMap(): Promise<string>;
    getKakaoMap(): Promise<string>;
    getGoogleMap(): Promise<string>;
    checkKakaoToNaver(): Promise<string>;
    checkNaverToKakao(): Promise<string>;
    addressTest(): Promise<string>;
    googleMapPosition(): Promise<string>;
}
