import { ConfigService } from '@nestjs/config';
import { BeautyReviewService } from 'src/beauty-review/beauty-review.service';
import { BeautyService } from 'src/beauty/beauty.service';
import { HotelService } from 'src/hotel/hotel.service';
import { HotelReviewService } from 'src/hotelReview/hotelReview.service';
export declare class MapService {
    private readonly configService;
    private readonly hotelService;
    private readonly hotelReviewService;
    private readonly beautyService;
    private readonly beautyReviewService;
    private readonly CLIENT_ID;
    private readonly CLIENT_SECRET;
    private readonly KAKAO_API_KEY;
    constructor(configService: ConfigService, hotelService: HotelService, hotelReviewService: HotelReviewService, beautyService: BeautyService, beautyReviewService: BeautyReviewService);
    naverMap(): Promise<string>;
    kakaoMap(): Promise<string>;
    googleMap(): Promise<string>;
    googleMapPosition(): Promise<string>;
    checkKakaoToNaver(): Promise<string>;
    checkNaverToKakao(): Promise<string>;
    addressTest(): Promise<string>;
}
