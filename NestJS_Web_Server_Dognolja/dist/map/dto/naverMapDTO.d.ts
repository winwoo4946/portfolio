export declare class NaverMapDTO {
    title: string;
    description: string;
    categoryName: string;
    naverUrl: string;
    address: string;
    roadAddress: string;
    x: number;
    y: number;
}
