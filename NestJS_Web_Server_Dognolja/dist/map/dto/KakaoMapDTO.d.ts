export declare class KakaoMapDTO {
    kakaoMapId: number;
    title: string;
    description: string;
    phone: string;
    categoryName: string;
    kakaoPlaceUrl: string;
    address: string;
    roadAddress: string;
    x: number;
    y: number;
}
