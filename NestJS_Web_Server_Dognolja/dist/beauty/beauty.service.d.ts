import { NaverMapDTO } from 'src/map/dto/naverMapDTO';
import { Repository } from 'typeorm';
import { BeautyEntity } from './beauty.entity';
import { BeautyNaverEntity } from './beautyNaver.entity';
import { BeautyPhotoEntity } from './beautyPhoto.entity';
import { BeautyDTO } from './dto/beauty.dto';
export declare class BeautyService {
    private readonly beautyRepository;
    private readonly beautyPhotoRepository;
    private readonly beautyNaverRepository;
    constructor(beautyRepository: Repository<BeautyEntity>, beautyPhotoRepository: Repository<BeautyPhotoEntity>, beautyNaverRepository: Repository<BeautyNaverEntity>);
    getAllBeauty(): Promise<BeautyEntity[]>;
    getBeautyById(id: number): Promise<BeautyEntity>;
    getBeautyByAddress(address: string): Promise<ShopBaseInfo[]>;
    getBeautyDetail(id: number): Promise<ShopInfo>;
    private getShopBaseInfo;
    getBeautyCountByTitle(address: string): Promise<number>;
    updateBeautyPosition(id: number, x: number, y: number): Promise<import("typeorm").UpdateResult>;
    createBeauty(beautyDTO: BeautyDTO): Promise<BeautyEntity>;
    getAllBeautyNaver(): Promise<BeautyNaverEntity[]>;
    getBeautyNaverCountByAddress(address: string): Promise<number>;
    createBeautyNaver(naverMapDTO: NaverMapDTO): Promise<BeautyNaverEntity>;
    getBeautyPhotoCountByUrl(url: string): Promise<number>;
    updateBeautyNaverZeroResult(id: number, isZero: boolean): Promise<void>;
    createBeautyGoogle(beatuyDTO: BeautyDTO): Promise<BeautyEntity>;
}
