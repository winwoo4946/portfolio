import { BaseEntity } from "typeorm";
import { BeautyEntity } from "./beauty.entity";
export declare class BeautyPhotoEntity extends BaseEntity {
    id: number;
    url: string;
    beauty: BeautyEntity;
}
