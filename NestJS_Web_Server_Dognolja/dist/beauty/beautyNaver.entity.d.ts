import { BaseEntity } from "typeorm";
export declare class BeautyNaverEntity extends BaseEntity {
    id: number;
    title: string;
    description: string;
    categoryName: string;
    naverUrl: string;
    address: string;
    roadAddress: string;
    x: number;
    y: number;
    zeroResult: boolean;
}
