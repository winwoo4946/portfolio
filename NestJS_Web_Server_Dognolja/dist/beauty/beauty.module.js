"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeautyModule = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const beauty_controller_1 = require("./beauty.controller");
const beauty_entity_1 = require("./beauty.entity");
const beauty_service_1 = require("./beauty.service");
const beautyNaver_entity_1 = require("./beautyNaver.entity");
const beautyPhoto_entity_1 = require("./beautyPhoto.entity");
let BeautyModule = class BeautyModule {
};
BeautyModule = __decorate([
    (0, common_1.Module)({
        imports: [
            typeorm_1.TypeOrmModule.forFeature([
                beauty_entity_1.BeautyEntity,
                beautyPhoto_entity_1.BeautyPhotoEntity,
                beautyNaver_entity_1.BeautyNaverEntity
            ])
        ],
        exports: [beauty_service_1.BeautyService],
        controllers: [beauty_controller_1.BeautyController],
        providers: [beauty_service_1.BeautyService]
    })
], BeautyModule);
exports.BeautyModule = BeautyModule;
//# sourceMappingURL=beauty.module.js.map