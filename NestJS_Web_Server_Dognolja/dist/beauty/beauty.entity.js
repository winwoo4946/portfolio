"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeautyEntity = void 0;
const beautyReview_entity_1 = require("../beauty-review/beautyReview.entity");
const typeorm_1 = require("typeorm");
const beautyPhoto_entity_1 = require("./beautyPhoto.entity");
let BeautyEntity = class BeautyEntity extends typeorm_1.BaseEntity {
    constructor() {
        super(...arguments);
        this.price1 = 0;
        this.price2 = 0;
        this.price3 = 0;
        this.price4 = 0;
    }
};
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)(),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], BeautyEntity.prototype, "title", void 0);
__decorate([
    (0, typeorm_1.Column)("text"),
    __metadata("design:type", String)
], BeautyEntity.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)("varchar", { length: 45, default: true }),
    __metadata("design:type", String)
], BeautyEntity.prototype, "phone", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], BeautyEntity.prototype, "categoryName", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], BeautyEntity.prototype, "url", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], BeautyEntity.prototype, "address", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", String)
], BeautyEntity.prototype, "roadAddress", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "mapx", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "mapy", void 0);
__decorate([
    (0, typeorm_1.Column)("double"),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "mapLat", void 0);
__decorate([
    (0, typeorm_1.Column)("double"),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "mapLng", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: 0 }),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "thumbnailPhotoId", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "price1", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "price2", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "price3", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: true }),
    __metadata("design:type", Number)
], BeautyEntity.prototype, "price4", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => beautyReview_entity_1.BeautyReviewEntity, review => review.beauty),
    __metadata("design:type", Array)
], BeautyEntity.prototype, "reviews", void 0);
__decorate([
    (0, typeorm_1.OneToMany)(type => beautyPhoto_entity_1.BeautyPhotoEntity, photo => photo.beauty),
    __metadata("design:type", Array)
], BeautyEntity.prototype, "photos", void 0);
BeautyEntity = __decorate([
    (0, typeorm_1.Entity)('beauty_table')
], BeautyEntity);
exports.BeautyEntity = BeautyEntity;
//# sourceMappingURL=beauty.entity.js.map