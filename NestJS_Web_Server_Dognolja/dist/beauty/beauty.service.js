"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BeautyService = void 0;
const common_1 = require("@nestjs/common");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
const beauty_entity_1 = require("./beauty.entity");
const beautyNaver_entity_1 = require("./beautyNaver.entity");
const beautyPhoto_entity_1 = require("./beautyPhoto.entity");
let BeautyService = class BeautyService {
    constructor(beautyRepository, beautyPhotoRepository, beautyNaverRepository) {
        this.beautyRepository = beautyRepository;
        this.beautyPhotoRepository = beautyPhotoRepository;
        this.beautyNaverRepository = beautyNaverRepository;
    }
    async getAllBeauty() {
        return this.beautyRepository.find();
    }
    async getBeautyById(id) {
        const found = await this.beautyRepository.findOneBy({ id });
        if (!found) {
            throw new common_1.NotFoundException(`Not found Beauty Info - id(${id})`);
        }
        return found;
    }
    async getBeautyByAddress(address) {
        const beautyEntities = await this.beautyRepository.createQueryBuilder('beauty')
            .leftJoinAndSelect('beauty.photos', 'photo')
            .leftJoinAndSelect('beauty.reviews', 'review')
            .where("beauty.address like :address", { address: `%${address}%` })
            .take(50)
            .getMany();
        const beautyInfos = [];
        for (let entity of beautyEntities) {
            let score = 0;
            let reviewCount = entity.reviews.length;
            for (let review of entity.reviews) {
                score += review.score;
            }
            beautyInfos.push({
                id: entity.id,
                title: entity.title,
                mapLat: entity.mapLat,
                mapLng: entity.mapLng,
                price1: entity.price1,
                mapx: entity.mapx,
                mapy: entity.mapy,
                thumbnailUrl: entity.photos.length > 0 ? entity.photos[0].url : "",
                hashTags: [],
                score: reviewCount === 0 ? 0 : Math.floor((score / reviewCount) * 10) / 10,
                reviewCount
            });
        }
        return beautyInfos;
    }
    async getBeautyDetail(id) {
        const beautyEntity = await this.beautyRepository.createQueryBuilder('beauty')
            .leftJoin('beauty.photos', 'photo').addSelect(['photo.id', 'photo.url'])
            .leftJoinAndSelect('beauty.reviews', 'review')
            .leftJoin('review.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("beauty.id = :id", { id })
            .getOne();
        const shopInfo = Object.assign(Object.assign({}, this.getShopBaseInfo(beautyEntity)), { description: beautyEntity.description, phone: beautyEntity.phone, reviews: beautyEntity.reviews, photos: beautyEntity.photos, address: beautyEntity.address, roadAddress: beautyEntity.roadAddress, operatingTime: [], subInfos: ['~4.9kg', '호텔링', '(1일)', '25,000원'] });
        return shopInfo;
    }
    getShopBaseInfo(entity) {
        let score = 0;
        let reviewCount = entity.reviews.length;
        for (let review of entity.reviews) {
            score += review.score;
        }
        const shopBaseInfo = {
            id: entity.id,
            title: entity.title,
            mapLat: entity.mapLat,
            mapLng: entity.mapLng,
            price1: entity.price1,
            mapx: entity.mapx,
            mapy: entity.mapy,
            thumbnailUrl: entity.photos.length > 0 ? entity.photos[0].url : "",
            hashTags: [],
            score: Math.floor((score / reviewCount) * 10) / 10,
            reviewCount
        };
        return shopBaseInfo;
    }
    async getBeautyCountByTitle(address) {
        return this.beautyRepository.createQueryBuilder("beauty")
            .where("beauty.address = :address", { address })
            .getCount();
    }
    async updateBeautyPosition(id, x, y) {
        return this.beautyRepository.createQueryBuilder()
            .update()
            .set({
            mapLat: y,
            mapLng: x,
        })
            .where("id = :id", { id })
            .execute();
    }
    async createBeauty(beautyDTO) {
        const beauty = this.beautyRepository.create(beautyDTO);
        await this.beautyRepository.save(beauty);
        return beauty;
    }
    async getAllBeautyNaver() {
        return await this.beautyNaverRepository
            .createQueryBuilder("beautyNaver")
            .getMany();
    }
    async getBeautyNaverCountByAddress(address) {
        const found = await this.beautyNaverRepository
            .createQueryBuilder("beautyNaver")
            .where("beautyNaver.address like :address", { address: `%${address}%` })
            .getCount();
        return found;
    }
    async createBeautyNaver(naverMapDTO) {
        const count = await this.getBeautyNaverCountByAddress(naverMapDTO.address);
        if (count > 0) {
            return;
        }
        const beauty = this.beautyNaverRepository.create(naverMapDTO);
        await this.beautyNaverRepository.save(beauty);
        return beauty;
    }
    async getBeautyPhotoCountByUrl(url) {
        const found = await this.beautyPhotoRepository
            .createQueryBuilder("beautyPhoto")
            .where("beautyPhoto.url = :url", { url })
            .getCount();
        return found;
    }
    async updateBeautyNaverZeroResult(id, isZero) {
        const found = await this.beautyNaverRepository.findOneBy({ id });
        if (!found)
            return;
        await this.beautyNaverRepository
            .createQueryBuilder()
            .update(beautyNaver_entity_1.BeautyNaverEntity)
            .set({ zeroResult: isZero })
            .where("id = :id", { id: id })
            .execute();
        console.log(`update naver result - ${id} - ${isZero}`);
    }
    async createBeautyGoogle(beatuyDTO) {
        const beauty = this.beautyRepository.create(beatuyDTO);
        const beautyEntity = await this.beautyRepository.save(beatuyDTO);
        let promises = [];
        for (let photoUrl of beatuyDTO.photoUrls) {
            const count = await this.getBeautyPhotoCountByUrl(photoUrl);
            if (count > 0)
                continue;
            const photo = this.beautyPhotoRepository.create({
                url: photoUrl,
                beauty: beauty
            });
            promises.push(this.beautyPhotoRepository.save(photo));
        }
        const photos = await Promise.all(promises);
        if (photos.length > 0) {
            await this.beautyRepository.createQueryBuilder()
                .update(beauty_entity_1.BeautyEntity)
                .set({ thumbnailPhotoId: photos[photos.length - 1].id })
                .where("id = :id", { id: beautyEntity.id })
                .execute();
        }
        return beauty;
    }
};
BeautyService = __decorate([
    (0, common_1.Injectable)(),
    __param(0, (0, typeorm_1.InjectRepository)(beauty_entity_1.BeautyEntity)),
    __param(1, (0, typeorm_1.InjectRepository)(beautyPhoto_entity_1.BeautyPhotoEntity)),
    __param(2, (0, typeorm_1.InjectRepository)(beautyNaver_entity_1.BeautyNaverEntity)),
    __metadata("design:paramtypes", [typeorm_2.Repository,
        typeorm_2.Repository,
        typeorm_2.Repository])
], BeautyService);
exports.BeautyService = BeautyService;
//# sourceMappingURL=beauty.service.js.map