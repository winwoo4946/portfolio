import { BeautyReviewEntity } from "src/beauty-review/beautyReview.entity";
import { BaseEntity } from "typeorm";
import { BeautyPhotoEntity } from "./beautyPhoto.entity";
export declare class BeautyEntity extends BaseEntity {
    id: number;
    title: string;
    description: string;
    phone: string;
    categoryName: string;
    url: string;
    address: string;
    roadAddress: string;
    mapx: number;
    mapy: number;
    mapLat: number;
    mapLng: number;
    thumbnailPhotoId: number;
    price1: number;
    price2: number;
    price3: number;
    price4: number;
    reviews: BeautyReviewEntity[];
    photos: BeautyPhotoEntity[];
}
