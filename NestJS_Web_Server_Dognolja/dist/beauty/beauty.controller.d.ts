import { BeautyService } from './beauty.service';
export declare class BeautyController {
    private readonly beautyService;
    constructor(beautyService: BeautyService);
    getBeautyById(id: number): Promise<ShopInfo>;
}
