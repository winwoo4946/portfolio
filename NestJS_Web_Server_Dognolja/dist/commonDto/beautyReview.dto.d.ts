export declare class ShopReviewDTO {
    shopId: number;
    content: string;
    score: number;
}
