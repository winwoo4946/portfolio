"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var SampleController_1;
Object.defineProperty(exports, "__esModule", { value: true });
exports.SampleController = void 0;
const sample_dto_1 = require("./dto/sample.dto");
const sample_service_1 = require("./sample.service");
const common_1 = require("@nestjs/common");
const korean_regexp_1 = require("korean-regexp");
const logger_1 = require("../log/logger");
function DecoTest(test) {
    return (target, propertyKey, descriptor) => {
        console.log('test : ', test);
        console.log('target : ', target);
        console.log('propertyKey : ', propertyKey);
        console.log('descriptor : ', descriptor);
    };
}
let SampleController = SampleController_1 = class SampleController {
    constructor(sampleService) {
        this.sampleService = sampleService;
    }
    async mTomAdd() {
        return await this.sampleService.mTomAdd();
    }
    async mTomSelect() {
        return await this.sampleService.mTomSelect();
    }
    getSample() {
        return this.sampleService.getSample();
    }
    getLogTest() {
        logger_1.Logger.log(SampleController_1, "LogTest");
        const obj = { a: 1, b: 2 };
        logger_1.Logger.log(SampleController_1, "LogTest obj : ", obj);
        logger_1.Logger.debug(SampleController_1, "Log Debug");
        logger_1.Logger.error(SampleController_1, "Log Error");
        logger_1.Logger.verbose(SampleController_1, "Log Verbose");
        logger_1.Logger.warn(SampleController_1, "Log warn");
    }
    getCookieTest(req) {
        console.log(req);
        console.log(req.cookies);
    }
    cookieTest(dto, res) {
        const cookieOption = {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000
        };
        res.cookie('idToken', 'token.id_token', cookieOption);
        res.cookie('idTokenExpiresIn', 'token.expires_in', cookieOption);
        res.cookie('refreshToken', 'token.refresh_token', cookieOption);
        res.cookie('refreshTokenExpiresIn', 'token.refresh_token_expires_in', cookieOption);
    }
    getQuerySample(name, age) {
        return this.sampleService.getQuerySample(name, age);
    }
    getSampleIdWithQuery(index, name, age) {
        return this.sampleService.getSampleIdWithQuery(index, name, age);
    }
    getSampleId(index) {
        return this.sampleService.getSampleIndex(index);
    }
    postSample(sampleData) {
        return this.sampleService.addSample(sampleData);
    }
    deleteSampleIndex(index) {
        return this.sampleService.deleteSampleByIndex(index);
    }
    patchSample(index) {
        return this.sampleService.patchSample(index);
    }
    searchTest(text) {
        const txt = '서울특별시 종로구 필운동 290 101호';
        const match = txt.search((0, korean_regexp_1.getRegExp)(text));
        return match > -1 ? match : '실패';
    }
};
__decorate([
    (0, common_1.Get)('mTomAdd'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SampleController.prototype, "mTomAdd", null);
__decorate([
    (0, common_1.Get)('mTomSelect'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], SampleController.prototype, "mTomSelect", null);
__decorate([
    (0, common_1.Get)(),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "getSample", null);
__decorate([
    (0, common_1.Get)('logTest'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "getLogTest", null);
__decorate([
    (0, common_1.Get)('getCookieTest'),
    __param(0, (0, common_1.Req)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "getCookieTest", null);
__decorate([
    (0, common_1.Post)('/CookieTest'),
    __param(0, (0, common_1.Body)()),
    __param(1, (0, common_1.Res)({ passthrough: true })),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "cookieTest", null);
__decorate([
    (0, common_1.Get)('querySample'),
    __param(0, (0, common_1.Query)('name')),
    __param(1, (0, common_1.Query)('age')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Number]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "getQuerySample", null);
__decorate([
    (0, common_1.Get)('/:index/query'),
    __param(0, (0, common_1.Param)('index')),
    __param(1, (0, common_1.Query)('name')),
    __param(2, (0, common_1.Query)('age')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number, String, Number]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "getSampleIdWithQuery", null);
__decorate([
    (0, common_1.Get)('/:index'),
    __param(0, (0, common_1.Param)('index', common_1.ParseIntPipe)),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "getSampleId", null);
__decorate([
    (0, common_1.Post)(),
    __param(0, (0, common_1.Body)()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [sample_dto_1.SampleDTO]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "postSample", null);
__decorate([
    (0, common_1.Delete)('/:index'),
    __param(0, (0, common_1.Param)('index')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "deleteSampleIndex", null);
__decorate([
    (0, common_1.Patch)('/:index'),
    __param(0, (0, common_1.Param)('index')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Number]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "patchSample", null);
__decorate([
    (0, common_1.Get)('SearchTest/:text'),
    __param(0, (0, common_1.Param)('text')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", void 0)
], SampleController.prototype, "searchTest", null);
SampleController = SampleController_1 = __decorate([
    (0, common_1.Controller)('sample'),
    __metadata("design:paramtypes", [sample_service_1.SampleService])
], SampleController);
exports.SampleController = SampleController;
//# sourceMappingURL=sample.controller.js.map