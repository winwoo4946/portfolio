import { SampleDTO } from './dto/sample.dto';
import { SampleService } from './sample.service';
import { Request, Response } from 'express';
export declare class SampleController {
    private readonly sampleService;
    constructor(sampleService: SampleService);
    mTomAdd(): Promise<string>;
    mTomSelect(): Promise<import("./category.entity").CategoryEntity[]>;
    getSample(): string;
    getLogTest(): void;
    getCookieTest(req: Request): void;
    cookieTest(dto: any, res: Response): void;
    getQuerySample(name: string, age: number): string;
    getSampleIdWithQuery(index: number, name: string, age: number): string;
    getSampleId(index: number): SampleDTO;
    postSample(sampleData: SampleDTO): string;
    deleteSampleIndex(index: number): string;
    patchSample(index: number): string;
    searchTest(text: string): number | "실패";
}
