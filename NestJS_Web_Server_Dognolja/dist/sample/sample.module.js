"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SampleModule = void 0;
const sample_service_1 = require("./sample.service");
const common_1 = require("@nestjs/common");
const sample_controller_1 = require("./sample.controller");
const auth_module_1 = require("../auth/auth.module");
const redis_module_1 = require("../redis/redis.module");
const config_1 = require("@nestjs/config");
const typeorm_1 = require("@nestjs/typeorm");
const category_entity_1 = require("./category.entity");
const question_entity_1 = require("./question.entity");
let SampleModule = class SampleModule {
};
SampleModule = __decorate([
    (0, common_1.Module)({
        imports: [
            auth_module_1.AuthModule,
            redis_module_1.RedisModule,
            config_1.ConfigModule,
            typeorm_1.TypeOrmModule.forFeature([
                category_entity_1.CategoryEntity,
                question_entity_1.QuestionEntity
            ])
        ],
        controllers: [sample_controller_1.SampleController],
        providers: [sample_service_1.SampleService]
    })
], SampleModule);
exports.SampleModule = SampleModule;
//# sourceMappingURL=sample.module.js.map