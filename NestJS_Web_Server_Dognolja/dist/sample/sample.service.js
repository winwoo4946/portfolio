"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SampleService = void 0;
const common_1 = require("@nestjs/common");
const category_entity_1 = require("./category.entity");
const question_entity_1 = require("./question.entity");
const typeorm_1 = require("@nestjs/typeorm");
const typeorm_2 = require("typeorm");
let SampleService = class SampleService {
    constructor(dataSource, categoryRepo, questionRepo) {
        this.dataSource = dataSource;
        this.categoryRepo = categoryRepo;
        this.questionRepo = questionRepo;
        this.sampleDTOs = [];
    }
    getSample() {
        console.log("getSample");
        return "Get Sample Success!";
    }
    getSampleIndex(index) {
        if (index >= this.sampleDTOs.length) {
            throw new common_1.NotFoundException(`overflow index : ${index}`);
        }
        if (index < 0) {
            throw new common_1.NotFoundException(`underflow index : ${index}`);
        }
        return this.sampleDTOs[index];
    }
    getQuerySample(name, age) {
        return `query param name : ${name} / age ${age}`;
    }
    getSampleIdWithQuery(index, name, age) {
        return `index : ${index} / name : ${name} / age : ${age}`;
    }
    addSample(sampleDTO) {
        this.sampleDTOs.push(sampleDTO);
        return `addSample index : ${this.sampleDTOs.length} \n
                samplePost ${JSON.stringify(sampleDTO)}`;
    }
    deleteSampleByIndex(index) {
        const dto = this.getSampleIndex(index);
        if (!dto)
            return;
        const arr = this.sampleDTOs.slice(0, index);
        arr.concat(this.sampleDTOs.slice(index + 1, this.sampleDTOs.length));
        this.sampleDTOs = arr;
        return `sampleDelete index : ${index}`;
    }
    patchSample(index) {
        return `samplePatch index : ${index}`;
    }
    async mTomAdd() {
        await this.dataSource.transaction(async (manager) => {
            const category1 = new category_entity_1.CategoryEntity();
            category1.name = "animals";
            const category2 = new category_entity_1.CategoryEntity();
            category2.name = "zoo";
            const question = new question_entity_1.QuestionEntity();
            question.title = "dogs";
            question.text = "who let the dogs out?";
            question.categories = [category1, category2];
            await manager.save(question);
            throw new Error("test");
            category1.questions = [question];
            category2.questions = [question];
            await manager.save(category1);
            await manager.save(category2);
        });
        return 'added';
    }
    async mTomSelect() {
        const cateogory = await this.categoryRepo
            .createQueryBuilder('category')
            .leftJoinAndSelect('category.questions', 'qeustion')
            .getMany();
        return cateogory;
    }
};
SampleService = __decorate([
    (0, common_1.Injectable)(),
    __param(1, (0, typeorm_1.InjectRepository)(category_entity_1.CategoryEntity)),
    __param(2, (0, typeorm_1.InjectRepository)(question_entity_1.QuestionEntity)),
    __metadata("design:paramtypes", [typeorm_2.DataSource,
        typeorm_2.Repository,
        typeorm_2.Repository])
], SampleService);
exports.SampleService = SampleService;
//# sourceMappingURL=sample.service.js.map