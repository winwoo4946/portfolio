import { BaseEntity } from "typeorm";
import { QuestionEntity } from "./question.entity";
export declare class CategoryEntity extends BaseEntity {
    id: number;
    name: string;
    questions: QuestionEntity[];
}
