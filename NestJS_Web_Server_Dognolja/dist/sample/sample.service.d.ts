import { SampleDTO } from './dto/sample.dto';
import { CategoryEntity } from './category.entity';
import { QuestionEntity } from './question.entity';
import { DataSource, Repository } from 'typeorm';
export declare class SampleService {
    private readonly dataSource;
    private readonly categoryRepo;
    private readonly questionRepo;
    constructor(dataSource: DataSource, categoryRepo: Repository<CategoryEntity>, questionRepo: Repository<QuestionEntity>);
    private sampleDTOs;
    getSample(): string;
    getSampleIndex(index: number): SampleDTO;
    getQuerySample(name: string, age: number): string;
    getSampleIdWithQuery(index: number, name: string, age: number): string;
    addSample(sampleDTO: SampleDTO): string;
    deleteSampleByIndex(index: number): string;
    patchSample(index: number): string;
    mTomAdd(): Promise<string>;
    mTomSelect(): Promise<CategoryEntity[]>;
}
