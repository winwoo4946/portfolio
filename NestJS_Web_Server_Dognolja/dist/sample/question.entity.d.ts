import { BaseEntity } from "typeorm";
import { CategoryEntity } from "./category.entity";
export declare class QuestionEntity extends BaseEntity {
    id: number;
    title: string;
    text: string;
    categories: CategoryEntity[];
}
