export declare const LoginPlatformType: {
    readonly Google: "Google";
    readonly Naver: "Naver";
    readonly Kakao: "Kakao";
};
export declare type LoginPlatformType = DEnum<typeof LoginPlatformType>;
export declare const ShopType: {
    readonly HOTEL: "hotel";
    readonly BEAUTY: "beauty";
    readonly ALL: "all";
};
export declare type ShopType = DEnum<typeof ShopType>;
export declare const AuthCookieKeys: {
    readonly LOGIN_PLATFORM: "loginPlatform";
    readonly ID_TOKEN: "idToken";
    readonly ID_TOKEN_EXPIRES_IN: "idTokenExpiresIn";
    readonly REFRESH_TOKEN: "refreshToken";
    readonly REFRESH_TOKEN_EXPIRES_IN: "refreshTokenExpiresIn";
};
export declare type AuthCookieKeys = DEnum<typeof AuthCookieKeys>;
