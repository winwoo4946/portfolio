"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthCookieKeys = exports.ShopType = exports.LoginPlatformType = void 0;
exports.LoginPlatformType = {
    Google: 'Google',
    Naver: 'Naver',
    Kakao: 'Kakao'
};
exports.ShopType = {
    HOTEL: 'hotel',
    BEAUTY: 'beauty',
    ALL: 'all'
};
exports.AuthCookieKeys = {
    LOGIN_PLATFORM: 'loginPlatform',
    ID_TOKEN: 'idToken',
    ID_TOKEN_EXPIRES_IN: 'idTokenExpiresIn',
    REFRESH_TOKEN: 'refreshToken',
    REFRESH_TOKEN_EXPIRES_IN: 'refreshTokenExpiresIn',
};
//# sourceMappingURL=commonEnum.js.map