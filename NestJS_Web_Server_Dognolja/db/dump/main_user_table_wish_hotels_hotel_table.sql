-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: main
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_table_wish_hotels_hotel_table`
--

DROP TABLE IF EXISTS `user_table_wish_hotels_hotel_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_table_wish_hotels_hotel_table` (
  `userTableId` int NOT NULL,
  `hotelTableId` int NOT NULL,
  PRIMARY KEY (`userTableId`,`hotelTableId`),
  KEY `IDX_72005f077f2996679792bcffa4` (`userTableId`),
  KEY `IDX_e4af5381314aeb2bb6bc3e7e8f` (`hotelTableId`),
  CONSTRAINT `FK_72005f077f2996679792bcffa49` FOREIGN KEY (`userTableId`) REFERENCES `user_table` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_e4af5381314aeb2bb6bc3e7e8f0` FOREIGN KEY (`hotelTableId`) REFERENCES `hotel_table` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_table_wish_hotels_hotel_table`
--

LOCK TABLES `user_table_wish_hotels_hotel_table` WRITE;
/*!40000 ALTER TABLE `user_table_wish_hotels_hotel_table` DISABLE KEYS */;
INSERT INTO `user_table_wish_hotels_hotel_table` VALUES (3,1),(3,2),(3,3),(4,1),(4,2),(4,3),(4,6);
/*!40000 ALTER TABLE `user_table_wish_hotels_hotel_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-24 16:22:30
