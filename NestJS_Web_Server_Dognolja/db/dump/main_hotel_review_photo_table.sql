-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: main
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hotel_review_photo_table`
--

DROP TABLE IF EXISTS `hotel_review_photo_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hotel_review_photo_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` text NOT NULL,
  `reviewId` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_86596d5bc6f16c00b0ea95e8eb` (`reviewId`),
  CONSTRAINT `FK_86596d5bc6f16c00b0ea95e8ebe` FOREIGN KEY (`reviewId`) REFERENCES `hotel_review_table` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel_review_photo_table`
--

LOCK TABLES `hotel_review_photo_table` WRITE;
/*!40000 ALTER TABLE `hotel_review_photo_table` DISABLE KEYS */;
INSERT INTO `hotel_review_photo_table` VALUES (6,'https://dog-nolja-bucket.s3.ap-northeast-2.amazonaws.com/hotelReviewPhoto/9999/indexDog_change.png','fe06c435-3843-4e76-b5a6-82302bb8c1e6'),(7,'https://dog-nolja-bucket.s3.ap-northeast-2.amazonaws.com/hotelReviewPhoto/9999/kakao_login_large_narrow.png','fe06c435-3843-4e76-b5a6-82302bb8c1e6'),(8,'https://dog-nolja-bucket.s3.ap-northeast-2.amazonaws.com/hotelReviewPhoto/9999/kakao_login_large_wide.png','fe06c435-3843-4e76-b5a6-82302bb8c1e6'),(9,'https://dog-nolja-bucket.s3.ap-northeast-2.amazonaws.com/hotelReviewPhoto/9999/thumb.png','2fe8e748-b1f8-4fe5-b3b2-412200fcfbbb'),(10,'https://dog-nolja-bucket.s3.ap-northeast-2.amazonaws.com/hotelReviewPhoto/9999/thumb.png','d95e17c0-70a1-4a13-897b-12b9a31fb139');
/*!40000 ALTER TABLE `hotel_review_photo_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-11-24 16:22:31
