-- MySQL dump 10.13  Distrib 8.0.28, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: main
-- ------------------------------------------------------
-- Server version	8.0.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `user_table`
--

DROP TABLE IF EXISTS `user_table`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_table` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nickName` varchar(255) NOT NULL DEFAULT '',
  `uid` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL DEFAULT '',
  `gender` varchar(255) NOT NULL DEFAULT '',
  `age` varchar(255) NOT NULL DEFAULT '',
  `thumbnailImgUrl` varchar(255) NOT NULL DEFAULT '',
  `profileImgUrl` varchar(255) NOT NULL DEFAULT '',
  `loginTime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `platformId` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_b6ce58d46f7b61d703543c9a70` (`uid`),
  KEY `IDX_21ef887d862f8db758308cb5c3` (`platformId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_table`
--

LOCK TABLES `user_table` WRITE;
/*!40000 ALTER TABLE `user_table` DISABLE KEYS */;
INSERT INTO `user_table` VALUES (1,'anonymous','','','','','','','2022-06-19 12:59:31',''),(3,'승우','kakao_2292444418','winwoo4946@naver.com','male','30~39','http://k.kakaocdn.net/dn/4HE2U/btrsrT69JPs/NedYYJuxLbKLLzWJe0gia1/img_110x110.jpg','http://k.kakaocdn.net/dn/4HE2U/btrsrT69JPs/NedYYJuxLbKLLzWJe0gia1/img_640x640.jpg','2022-08-30 21:43:38',''),(4,'302Develop','kakao_2292407898','302develop@kakao.com','','','http://k.kakaocdn.net/dn/dpk9l1/btqmGhA2lKL/Oz0wDuJn1YV2DIn92f6DVK/img_110x110.jpg','http://k.kakaocdn.net/dn/dpk9l1/btqmGhA2lKL/Oz0wDuJn1YV2DIn92f6DVK/img_640x640.jpg','2022-06-19 17:03:37',''),(7,'윈우','naver_VfQVFQvuorh8k_nc0iDWX3BSty1cO2Pmjtd8yLqVS4c','winwoo4946@naver.com','none','30-39','https://ssl.pstatic.net/static/pwe/address/img_profile.png','https://ssl.pstatic.net/static/pwe/address/img_profile.png','2022-08-30 17:25:14','VfQVFQvuorh8k_nc0iDWX3BSty1cO2Pmjtd8yLqVS4c'),(8,'윈우','google_116783207498236426181','winwoo4946@gmail.com','none','none','https://lh3.googleusercontent.com/a/AItbvmnrYT0KPU6ohMZj0JC4agPNrUem3XfcJBaOzvbH=s96-c','https://lh3.googleusercontent.com/a/AItbvmnrYT0KPU6ohMZj0JC4agPNrUem3XfcJBaOzvbH=s96-c','2022-08-30 17:25:35','116783207498236426181');
/*!40000 ALTER TABLE `user_table` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-30 22:14:13
