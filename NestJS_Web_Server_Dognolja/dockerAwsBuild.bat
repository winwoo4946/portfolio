echo off

echo =================================================
echo ==    aws login                                ==
echo =================================================
aws ecr get-login-password --region ap-northeast-2 | docker login --username AWS --password-stdin 365053879875.dkr.ecr.ap-northeast-2.amazonaws.com

echo =================================================
echo ==    local aws docker image remove            ==
echo =================================================
docker rmi dog-nolja-aws
docker rmi 365053879875.dkr.ecr.ap-northeast-2.amazonaws.com/dog-nolja:latest

echo =================================================
echo ==    new aws image build                      ==
echo =================================================

docker build -t dog-nolja-aws -f ./Dockerfile_AWS .
docker tag dog-nolja-aws:latest 365053879875.dkr.ecr.ap-northeast-2.amazonaws.com/dog-nolja:latest

echo =================================================
echo ==    aws ecr lastest image remove             ==
echo =================================================
aws ecr batch-delete-image --repository-name dog-nolja --image-ids imageTag=latest

echo =================================================
echo ==    aws ecr push                             ==
echo =================================================
docker push 365053879875.dkr.ecr.ap-northeast-2.amazonaws.com/dog-nolja:latest

pause