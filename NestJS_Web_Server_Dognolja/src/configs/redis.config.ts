import { CacheModuleAsyncOptions, CacheModuleOptions } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Cache, Store, StoreConfig } from 'cache-manager';
import * as redisStore from 'cache-manager-ioredis';
import Redis from 'redis';

/**
 * redis 확장관련
 * https://stackoverflow.com/questions/68117902/how-can-i-get-redis-io-client-from-nestjs-cachemanager-module
 * 
 * ioredis 사용법
 * https://github.com/luin/ioredis#basic-usage
 * 
 * redis기본 명령어
 * https://sabarada.tistory.com/104
 */

/**
 * cache-manager Cache 확장
 * store 타입을 RedisStore로 변경
 */
export interface RedisCache extends Cache {
    store: RedisStore;
}

/**
 * cache-manager Store 확장
 * getClinet() 반환 타입을 Redis.RedisClient로 변경
 * Redis.RedisClient는 redis 접근 개체타입으로 보면됨.
 * (근데 이상하게 Redis.RedisClient는 코드접근이 안되는데 타입선언은 문제없음)
 * ===
 * @220723
 * package update 후 Redis.RedisClient를 못찾아서 docker 빌드시 에러 발생하여
 * any타입으로 변경. 추후에 타입관련해서 알아볼 필요 있을듯
 */
export interface RedisStore extends Store {
    name: 'redis';
    getClient: () => any;
    isCacheableValue: (value: any) => boolean;
}

/**
 * StoreConfig 확장
 * store 변수타입에 Store를 RedisStore로 변경
 */
export interface RedisStoreConfig extends StoreConfig {
    store: 'memory' | 'none' | RedisStore | {
        create(...args: any[]): Store;
    };
}

export default class RedisConfig {
    static getConfig(configService: ConfigService): CacheModuleOptions<RedisStoreConfig> {
        const option: CacheModuleOptions<RedisStoreConfig> = {
            store: redisStore,
            host: configService.get('REDIS_HOST'),
            port: configService.get('REDIS_PORT'),
            ttl: 5
        }
        console.log(`redisOption : ${JSON.stringify(option)}`);
        return option;
    }
}

export const redisConfigAsync: CacheModuleAsyncOptions = {
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (configService: ConfigService): Promise<CacheModuleOptions<RedisStoreConfig>> => RedisConfig.getConfig(configService)
};