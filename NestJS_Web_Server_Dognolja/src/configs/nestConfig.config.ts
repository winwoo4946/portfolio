import * as Joi from 'joi';
import { ConfigModuleOptions } from "@nestjs/config";

export const nestConfig: ConfigModuleOptions = {
    isGlobal: true,
    envFilePath: `.env.${process.env.NODE_ENV}`,
    ignoreEnvFile: process.env.NODE_ENV === 'production',
    validationSchema: Joi.object({ 
        // NODE_ENV: Joi.string().valid('dev.local', 'dev.docker', 'prod').required(), 
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(), 
        DB_USERNAME: Joi.string().required(), 
        DB_PASSWORD: Joi.string().required(),
        DB_NAME: Joi.string().required(),
        REDIS_HOST: Joi.string().required(),
        REDIS_PORT: Joi.number().required(),
        NAVER_CLIENT_ID: Joi.string().required(),
        NAVER_CLIENT_SECRET: Joi.string().required(),
        NAVER_LOGIN_REDRIECT_URL: Joi.string().required(),
        KAKAO_REST_API_KEY: Joi.string().required(),
        KAKAO_ADMIN_KEY: Joi.string().required(),
        KAKAO_LOGIN_REDRIECT_URL: Joi.string().required(),
        KAKAO_CLIENT_SECRET: Joi.string().required(),
        GOOGLE_MAP_API_KEY: Joi.string().required(),
        GOOGLE_OAUTH_CLIENT_ID: Joi.string().required(),
        GOOGLE_OAUTH_CLIENT_SECRET: Joi.string().required(),
        ORIGIN: Joi.string().required(),
        AWS_ACCESS_KEY: Joi.string().required(),
        AWS_SECRET_KEY: Joi.string().required(),
        JWT_SECRET: Joi.string().required(),
        JWT_EXPIRES_IN: Joi.number().required()
    }),
}