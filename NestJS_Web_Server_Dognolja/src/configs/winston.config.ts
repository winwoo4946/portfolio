import { ConfigModule, ConfigService } from '@nestjs/config';
import { WinstonModuleAsyncOptions, WinstonModuleOptions } from 'nest-winston';
import * as Winston from 'winston';
import * as winstonDaily from 'winston-daily-rotate-file';
import { utilities as nestWinstonModuleUtilities } from 'nest-winston';
const logDir = __dirname + '/../../logs'; // log 파일을 관리할 폴더

export default class WinstonConfig {
    private static dailyOptions(level: string) {
        return {
            level,
            datePattern: 'YYYY-MM-DD',
            dirname: logDir + `/${level}`,
            filename: `%DATE%.${level}.log`,
            maxFiles: 7, //7일치 로그파일 저장
            zippedArchive: true, // 로그가 쌓이면 압축하여 관리
        };
    };

    static getConfig(configService: ConfigService): WinstonModuleOptions {
        return {
            transports: [
                new Winston.transports.Console({
                    level: process.env.NODE_ENV === 'production' ? 'info' : 'silly',
                    format: Winston.format.combine(
                        Winston.format.timestamp(),
                        nestWinstonModuleUtilities.format.nestLike('DogNolja')
                    )
                }),
                // info, warn, error 로그는 파일로 관리
                // new winstonDaily(this.dailyOptions('info')),
                // new winstonDaily(this.dailyOptions('warn')),
                // new winstonDaily(this.dailyOptions('error')),
            ]
        };
    }
}

export const winstonConfigAsync: WinstonModuleAsyncOptions = {
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: async (configService: ConfigService): Promise<WinstonModuleOptions> => WinstonConfig.getConfig(configService)
};