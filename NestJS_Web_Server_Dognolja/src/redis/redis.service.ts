import { CACHE_MANAGER, Inject, Injectable, NotFoundException, OnModuleInit } from '@nestjs/common';
import { ShopType } from 'src/dognolja_common/commonEnum';
import { getRegExp } from 'korean-regexp';
import { BeautyEntity } from 'src/beauty/beauty.entity';
import { BeautyService } from 'src/beauty/beauty.service';
import { RedisCache } from 'src/configs/redis.config';
import { HotelEntity } from 'src/hotel/hotel.entity';
import { HotelService } from 'src/hotel/hotel.service';

type RedisCommonArg = string | number;
@Injectable()
export class RedisService implements OnModuleInit {
    //#region private
    // 각 주소 구분 별 마지막 글자 모음
    private readonly ADDRESS_TYPE_SET: Set<string>[] = [
        new Set<string>(['시', '도']),
        new Set<string>(['구', '군', '면', '동', '시', '읍']),
        new Set<string>(['동', '가', '읍', '면', '리', '구']),
        new Set<string>(['리', '동', '가', '면', '읍']),
    ];

    private addressSetMap = new Map<ShopType, Set<string>>([
        [ShopType.HOTEL, new Set<string>()],
        [ShopType.BEAUTY, new Set<string>()]
    ]);

    private addressArrayMap = new Map<ShopType, string[]>([
        [ShopType.HOTEL, []],
        [ShopType.BEAUTY, []]
    ]);

    private redisClient: any;
    //#endregion

    //#region public

    // address
    readonly ADDRESS_RESULT_COUNT = 5;
    readonly ADDRESS_REDIS_KEY = 'address';

    // auth
    readonly AUTH_OIDC_PUBLIC_KEY = 'auth_OIDC_public_key';

    //#endregion

    constructor(
        @Inject(CACHE_MANAGER)
        private readonly cacheManager: RedisCache,

        private readonly hotelService: HotelService,
        private readonly beautyService: BeautyService
    ) {
        this.redisClient = this.cacheManager.store.getClient();
    }

    async onModuleInit() {
        await this.setAddressCache();
        console.log('redis init');
    }

    getAddressArr(type: ShopType) {
        return this.addressArrayMap.get(type);
    }

    async getSample() {
        // cacheManager.store.getClient()는 Redis.RedisClient
        let redis = this.cacheManager.store.getClient();

        const numbers = [1, 3, 5, 7, 9];
        let add = await redis.sadd("user-set", numbers);
        console.log(`add0 : ${add}`);

        const elementCount = await redis.scard("user-set");
        console.log(elementCount); // 5

        add = await redis.sadd("user-set", "1");
        console.log(`add1 : ${add}`);
        const newElementCount = await redis.scard("user-set");
        console.log(newElementCount); // 5

        add = await redis.sadd("user-set", "2");
        console.log(`add2 : ${add}`);
        const newElementCount2 = await redis.scard("user-set");
        console.log(newElementCount2); // 6

        const memebers = await redis.smembers("user-set");
        console.log(memebers); //[ '1', '2', '3', '5', '7', '9' ]

        const isMember = await redis.sismember("user-set", 3);
        console.log(isMember); // 1 (means true, and if it's 0, it means false)
        return "set"
    }

    async setAddressCache(): Promise<void> {
        const hotels: HotelEntity[] = await this.hotelService.getAllHotel();
        let promises = [];
        for (let hotel of hotels) {
            promises.push(this.saveAddress('hotel', hotel.address));
        }
        const beautys: BeautyEntity[] = await this.beautyService.getAllBeauty();
        for (let beauty of beautys) {
            promises.push(this.saveAddress('beauty', beauty.address));
        }
        await Promise.all(promises);

        // set을 array로 바꾼뒤 arrayMap에 넣어준다.
        this.addressArrayMap.set('hotel', Array.from(this.addressSetMap.get('hotel')));
        this.addressArrayMap.set('beauty', Array.from(this.addressSetMap.get('beauty')));

        // set은 array추출용이므로 메모리에서 삭제 한다.
        this.addressSetMap.forEach((value: Set<string>) => {
            value.clear();
        });
        this.addressSetMap = null;
    }

    private async saveAddress(type: ShopType, address: string) {
        if (type === ShopType.ALL) {
            throw new Error("[all] Type can't save address");
        }

        const addressComponents: string[] = address.split(' ');
        const promises = [];
        for (let i = 1; i < this.ADDRESS_TYPE_SET.length; ++i) {
            if (i >= addressComponents.length)
                break;

            // addressComponents는 주소 단위 배열
            // ex) '경기도 수원시 팔달구' -> ['경기도', '수원시', '팔달구']
            const addressKey: string = addressComponents[i];

            // 해당 addressComponent 앞에 있는 주소를 하나의 문자열로 만든다
            // ex) addressComponent = '팔달구' -> fullAddress = '경기도 수원시 팔달구'
            const fullAddress: string = addressComponents.slice(0, i + 1).join(' ');

            // 마지막글자 추출(시, 도, 구, 군, 읍 등등)
            const lastChar: string = addressKey.at(addressKey.length - 1);

            // 주소 마지막글자가 ADDRESS_TYPE_SET에 속하지 않는 경우
            // 검색어로 지원하지 않는 단어로 간주하고 redis에 저장하지 않는다.
            if (!this.ADDRESS_TYPE_SET[i].has(lastChar))
                continue;

            promises.push(this.sadd(`address:${type}:${addressKey}`, fullAddress));

            // address set에 주소 추가
            this.addressSetMap.get(type).add(addressKey);
        }
        return promises;
    }

    async getAddressByKeyward(type: ShopType, keywords: string[]): Promise<string[][]> {
        let promises = [];
        for (let keywrodResult of keywords) {
            promises.push(this.smembers(`address:${type}:${keywrodResult}`));
        }
        return Promise.all(promises);
    }

    async get(key: string) {
        return this.redisClient.get(key);
    }

    async set(key: string, value: RedisCommonArg) {
        return this.redisClient.set(key, value);
    }

    async setEx(key: string, value: RedisCommonArg, ttl: number) {
        return this.redisClient.set(key, value, 'EX', ttl);
    }

    async sadd(key: string, value: RedisCommonArg) {
        return this.redisClient.sadd(key, value);
    }

    async smembers(key: string) {
        return this.redisClient.smembers(key);
    }
}
