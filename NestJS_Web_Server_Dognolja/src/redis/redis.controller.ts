import { Controller, Get, Query } from "@nestjs/common"
import { RedisService } from "./redis.service"


@Controller('redis')
export class RedisController {
    constructor(
        private readonly redisService: RedisService
    ) { }

    @Get("/cache")
    async getCache(): Promise<string> {
        return this.redisService.getSample();
    }
}
