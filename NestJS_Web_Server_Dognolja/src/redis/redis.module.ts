import { CacheModule, Module } from '@nestjs/common';
import { RedisService } from './redis.service';
import { RedisController } from './redis.controller';
import { redisConfigAsync } from 'src/configs/redis.config';
import { HotelModule } from 'src/hotel/hotel.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HotelEntity } from 'src/hotel/hotel.entity';
import { BeautyEntity } from 'src/beauty/beauty.entity';
import { BeautyModule } from 'src/beauty/beauty.module';

@Module({
  imports: [
    CacheModule.registerAsync(redisConfigAsync),
    TypeOrmModule.forFeature([
      HotelEntity,
      BeautyEntity
    ]),
    HotelModule,
    BeautyModule
  ],
  exports: [RedisService],
  providers: [RedisService],
  controllers: [RedisController]
})
export class RedisModule { }
