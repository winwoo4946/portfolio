export const LoginPlatformType = {
    Google: 'Google',
    Naver: 'Naver',
    Kakao: 'Kakao'
} as const;
export type LoginPlatformType = DEnum<typeof LoginPlatformType>;

export const ShopType = {
    HOTEL: 'hotel',
    BEAUTY: 'beauty',
    ALL: 'all'
} as const;
export type ShopType = DEnum<typeof ShopType>;

export const AuthCookieKeys = {
    LOGIN_PLATFORM: 'loginPlatform',
    ID_TOKEN: 'idToken',
    ID_TOKEN_EXPIRES_IN: 'idTokenExpiresIn',
    REFRESH_TOKEN: 'refreshToken',
    REFRESH_TOKEN_EXPIRES_IN: 'refreshTokenExpiresIn',
} as const;
export type AuthCookieKeys = DEnum<typeof AuthCookieKeys>;