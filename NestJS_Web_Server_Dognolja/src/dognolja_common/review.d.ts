declare interface ReviewBase {
    id: number;
    content: string;
    score: number;
    date: Date;
    owner: UserBase;
}