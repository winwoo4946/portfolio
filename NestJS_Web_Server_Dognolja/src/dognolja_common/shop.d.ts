declare interface ShopBaseInfo {
    id: number;
    thumbnailUrl: string;
    title: string;
    hashTags: string[];
    score: number;
    reviewCount: number;
    mapx: number;
    mapy: number;
    mapLat: number;
    mapLng: number;
    price1: number;
}

declare interface ShopInfo extends ShopBaseInfo {
    description: string;
    phone: string;
    reviews: ShopReivew[];
    photos: ShopPhoto[];
    address: string;
    roadAddress: string;
    operatingTime: string[];
    subInfos: string[];
}

declare interface ShopPhoto extends PhotoBase { }
declare interface ShopReivew extends ReviewBase { }
declare interface ShopInfoCustom extends ShopInfo { 
    hotel:any;
    reviewId:string | number; 
}