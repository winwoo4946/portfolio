declare interface UserBase {
    id: number,
    nickName: string,
    thumbnailImgUrl: string;
    profileImgUrl: string;
}

declare interface UserInfo extends UserBase {
    email: string;
    gender: string;
    age: string;
    loginTime: Date;
}

declare interface MyUserInfo extends UserInfo {
    
}