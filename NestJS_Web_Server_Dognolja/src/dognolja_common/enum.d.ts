type ValueType = string | number | boolean;

declare type DEnum<T extends { [k: string]: ValueType } | ReadonlyArray<ValueType>> = T extends ReadonlyArray<ValueType>
  ? T[number]
  : T extends { [k: string]: infer U }
  ? U
  : never;