declare interface PhotoBase {
    id: number;
    url: string;
}