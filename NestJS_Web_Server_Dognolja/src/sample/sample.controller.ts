import { SampleDTO } from './dto/sample.dto';
import { SampleService } from './sample.service';
import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, Query, Req, Res, UseGuards } from '@nestjs/common';
import { getRegExp } from 'korean-regexp';
import { CookieOptions, Request, Response } from 'express';
import { IdTokenGuard } from 'src/auth/idToken.guard';
import { Logger } from 'src/log/logger';

function DecoTest(test: string) : MethodDecorator {
    return (target: Object, propertyKey: string | symbol, descriptor: TypedPropertyDescriptor<any>) => {
        console.log('test : ', test);
        console.log('target : ', target);
        console.log('propertyKey : ', propertyKey);
        console.log('descriptor : ', descriptor);
    }
}

@Controller('sample')
export class SampleController {
    constructor(private readonly sampleService: SampleService) { }

    @Get('mTomAdd')
    async mTomAdd() {
        return await this.sampleService.mTomAdd();
    }

    @Get('mTomSelect') 
    async mTomSelect() {
        return await this.sampleService.mTomSelect();
    }

    @Get()
    //@DecoTest('test')
    //@UseGuards(IdTokenGuard)
    getSample() {
        return this.sampleService.getSample();
    }

    @Get('logTest')
    getLogTest() {
        // 로그메세지만 출력할 경우
        Logger.log(SampleController, "LogTest");
        // 오브젝트까지 출력 해야되는 경우
        const obj = {a: 1, b: 2};
        Logger.log(SampleController, "LogTest obj : ", obj);

        // 그 외 로그레벨 별 로그
        Logger.debug(SampleController, "Log Debug");
        Logger.error(SampleController, "Log Error");
        Logger.verbose(SampleController, "Log Verbose");
        Logger.warn(SampleController, "Log warn");
    }

    @Get('getCookieTest')
    getCookieTest(@Req() req: Request) {
        console.log(req);
        console.log(req.cookies);
    }

    @Post('/CookieTest')
    cookieTest(@Body() dto: any, @Res({ passthrough: true }) res: Response) {
        const cookieOption: CookieOptions = {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000 // 1day
        };
        res.cookie('idToken', 'token.id_token', cookieOption);
        res.cookie('idTokenExpiresIn', 'token.expires_in', cookieOption);
        res.cookie('refreshToken', 'token.refresh_token', cookieOption);
        res.cookie('refreshTokenExpiresIn', 'token.refresh_token_expires_in', cookieOption);
    }

    /**
     * get 이용시 쿼리를 날리게 되면 '/:index'와 겹치기 때문에 
     * '/:index' 보다 위에 선언 해줘야함
     */
    @Get('querySample')
    getQuerySample(@Query('name') name: string, @Query('age') age: number) {
        return this.sampleService.getQuerySample(name, age);
    }

    /**
     * index에 대한 쿼리를 받고싶은 경우
     */
    @Get('/:index/query')
    getSampleIdWithQuery(@Param('index') index: number,
        @Query('name') name: string,
        @Query('age') age: number) {

        return this.sampleService.getSampleIdWithQuery(index, name, age);
    }

    /**
     * Get('/:{변수명}')
     * @Param({변수명}) 
     * 
     * Get에서 설정한 변수명 파라미터에 따라 
     * @Param()을 이용하여 해당 변수값을 전달 받을 수 있음 
     */
    @Get('/:index')
    getSampleId(@Param('index', ParseIntPipe) index: number) {
        return this.sampleService.getSampleIndex(index);
    }

    @Post()
    postSample(@Body() sampleData: SampleDTO) {
        return this.sampleService.addSample(sampleData);
    }

    @Delete('/:index')
    deleteSampleIndex(@Param('index') index: number) {
        return this.sampleService.deleteSampleByIndex(index);
    }

    /**
     * Put은 데이터 전체를 갱신하므로 Patch를 주로 사용함
     * 단, 브라우저 버전에따라 Patch를 지원않하는 브라우저가 있을 수 있음
     * Patch를 못쓸경우 Post로 쓰자
     */
    @Patch('/:index')
    patchSample(@Param('index') index: number) {
        return this.sampleService.patchSample(index);
    }

    @Get('SearchTest/:text')
    searchTest(@Param('text') text: string) {
        const txt = '서울특별시 종로구 필운동 290 101호';
        const match = txt.search(getRegExp(text));
        return match > -1 ? match : '실패';
    }
}
