import { IsString, IsNumber } from 'class-validator';

export class SampleDTO {

    /**
     * ValidationPipe에서 whiteList를 사용한 경우
     * 데이터 변환 시 @Is데이터타입 이 붙어있는 변수와 매칭되는지 자동 체크됨
     * @Is데이터타입 을 안붙이면 변수명을 맞춰도 에러남(엄격하게 체크된다고 보면됨)
     */

    @IsString()
    readonly name: string;
    @IsNumber()
    readonly age: number;
}