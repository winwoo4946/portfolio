import { SampleDTO } from './dto/sample.dto';
import { Injectable, NotFoundException, Patch } from '@nestjs/common';
import { CategoryEntity } from './category.entity';
import { QuestionEntity } from './question.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';

@Injectable()
export class SampleService {
    constructor(
        private readonly dataSource: DataSource,
        @InjectRepository(CategoryEntity)
        private readonly categoryRepo: Repository<CategoryEntity>,
        @InjectRepository(QuestionEntity)
        private readonly questionRepo: Repository<QuestionEntity>,
    ) {}
    private sampleDTOs: SampleDTO[] = [];
    getSample(): string {
        console.log("getSample");
        return "Get Sample Success!";
    }

    getSampleIndex(index: number): SampleDTO {
        if (index >= this.sampleDTOs.length) {
            throw new NotFoundException(`overflow index : ${index}`);
        }

        if (index < 0) {
            throw new NotFoundException(`underflow index : ${index}`);
        }

        return this.sampleDTOs[index];
    }

    getQuerySample(name: string, age: number): string {
        return `query param name : ${name} / age ${age}`;
    }

    getSampleIdWithQuery(index: number, name: string, age: number): string {
        return `index : ${index} / name : ${name} / age : ${age}`;
    }

    addSample(sampleDTO: SampleDTO): string {
        this.sampleDTOs.push(sampleDTO);
        return `addSample index : ${this.sampleDTOs.length} \n
                samplePost ${JSON.stringify(sampleDTO)}`;
    }

    deleteSampleByIndex(index: number): string {
        const dto: SampleDTO = this.getSampleIndex(index);
        if(!dto) 
            return;

        const arr: SampleDTO[] = this.sampleDTOs.slice(0, index);
        arr.concat(this.sampleDTOs.slice(index + 1, this.sampleDTOs.length));
        this.sampleDTOs = arr;
        return `sampleDelete index : ${index}`;
    }

    patchSample(index: number): string {
        return `samplePatch index : ${index}`;
    }

    async mTomAdd() {

        await this.dataSource.transaction(async manager => {
            const category1 = new CategoryEntity()
            category1.name = "animals"
    
            const category2 = new CategoryEntity()
            category2.name = "zoo"
    
            const question = new QuestionEntity()
            question.title = "dogs"
            question.text = "who let the dogs out?"
            question.categories = [category1, category2]
            await manager.save(question);

            throw new Error("test");
    
            category1.questions = [question];
            category2.questions = [question];
            await manager.save(category1);
            await manager.save(category2);
        });
        return 'added';
    }

    async mTomSelect() {
        // const questions = await this.questionRepo
        //                     .createQueryBuilder("question")
        //                     .leftJoinAndSelect("question.categories", "category")
        //                     .getMany()
        
        const cateogory = await this.categoryRepo
                            .createQueryBuilder('category')
                            .leftJoinAndSelect('category.questions', 'qeustion')
                            .getMany();
        return cateogory;
    }
}
