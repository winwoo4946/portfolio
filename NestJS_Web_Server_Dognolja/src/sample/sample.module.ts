import { SampleService } from './sample.service';
import { Module } from '@nestjs/common';
import { SampleController } from './sample.controller';
import { AuthModule } from 'src/auth/auth.module';
import { RedisModule } from 'src/redis/redis.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CategoryEntity } from './category.entity';
import { QuestionEntity } from './question.entity';

@Module({
    imports: [
        AuthModule, 
        RedisModule, 
        ConfigModule, 
        TypeOrmModule.forFeature([
            CategoryEntity,
            QuestionEntity
        ])
    ],
    controllers: [SampleController],
    providers: [SampleService]
})
export class SampleModule {}
