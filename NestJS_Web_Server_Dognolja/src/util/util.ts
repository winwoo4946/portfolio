export function jsonToQueryString(json: Object) {
    return Object.keys(json).map(k => `${encodeURIComponent(k)}=${encodeURI(json[k])}`).join('&');
}