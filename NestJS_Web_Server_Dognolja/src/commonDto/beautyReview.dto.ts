import { IsNumberString, IsString } from "class-validator";


export class ShopReviewDTO {
    @IsNumberString()
    shopId: number;

    @IsString()
    content: string;

    @IsNumberString()
    score: number;
}