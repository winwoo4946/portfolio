import { ConfigModule } from '@nestjs/config';
import { MiddlewareConsumer, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { typeORMConfigAsync } from './configs/typeorm.config';
import { SampleModule } from './sample/sample.module';
import { HotelModule } from './hotel/hotel.module';
import { nestConfig } from './configs/nestConfig.config';
import { MapModule } from './map/map.module';
import { HotelReviewModule } from './hotelReview/hotelReview.module';
import { UserModule } from './user/user.module';
import { RedisModule } from './redis/redis.module';
import { SearchModule } from './search/search.module';
import { AuthModule } from './auth/auth.module';
import { HotelAdminModule } from './hotel-admin/hotel-admin.module';
import { BeautyModule } from './beauty/beauty.module';
import { BeautyReviewModule } from './beauty-review/beauty-review.module';
import LogsMiddleware from './log/logs.middleware';

@Module({
    imports: [
        ConfigModule.forRoot(nestConfig),
        TypeOrmModule.forRootAsync(typeORMConfigAsync),
        SampleModule,
        UserModule,
        HotelModule,
        HotelReviewModule,
        MapModule,
        RedisModule,
        SearchModule,
        AuthModule,
        HotelAdminModule,
        BeautyModule,
        BeautyReviewModule
    ],
    controllers: [],
    providers: [],
})
export class AppModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(LogsMiddleware)
            .forRoutes('*');
    }
}
