import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BeautyModule } from 'src/beauty/beauty.module';
import { UploadsService } from 'src/uploads/uploads.service';
import { UserModule } from 'src/user/user.module';
import { BeautyReviewController } from './beauty-review.controller';
import { BeautyReviewService } from './beauty-review.service';
import { BeautyReviewEntity } from './beautyReview.entity';
import { BeautyReviewPhotoEntity } from './beautyReviewPhoto.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BeautyReviewEntity,
      BeautyReviewPhotoEntity,
    ]),
    UserModule,
    BeautyModule
  ],
  exports:[BeautyReviewService],
  controllers: [BeautyReviewController],
  providers: [BeautyReviewService, UploadsService]
})
export class BeautyReviewModule {}
