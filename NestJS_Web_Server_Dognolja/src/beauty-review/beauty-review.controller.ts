import { Body, Controller, Get, Param, ParseIntPipe, Post, UploadedFiles, UseGuards, UseInterceptors } from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { GetAuthUserInfo, JwtAuthGuard } from 'src/auth/auth.decorator';
import { AuthUserInfo } from 'src/auth/login';
import { ShopReviewDTO } from 'src/commonDto/beautyReview.dto';
import { BeautyReviewService } from './beauty-review.service';

@Controller('beautyReview')
export class BeautyReviewController {
    constructor(private readonly beautyReivewService: BeautyReviewService) { }

    @Get('/:id')
    getBeautyReviews(@Param('id', ParseIntPipe) id: number) {
        return this.beautyReivewService.getBeautyReviews(id);
    }

    @Post('createReview')
    @UseGuards(JwtAuthGuard)
    @UseInterceptors(FilesInterceptor('files', 5))
    addReview(
        @GetAuthUserInfo() authInfo: AuthUserInfo,
        @Body() reviewDTO: ShopReviewDTO, 
        @UploadedFiles() files: Array<Express.Multer.File>
    ) {
        return this.beautyReivewService.createReview(authInfo, reviewDTO, files);
    }
}
