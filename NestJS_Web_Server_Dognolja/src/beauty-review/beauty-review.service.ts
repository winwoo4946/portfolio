import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AuthUserInfo } from 'src/auth/login';
import { UserEntity } from 'src/auth/user.entity';
import { BeautyEntity } from 'src/beauty/beauty.entity';
import { BeautyService } from 'src/beauty/beauty.service';
import { ShopReviewDTO } from 'src/commonDto/beautyReview.dto';
import { UploadsService } from 'src/uploads/uploads.service';
import { UserService } from 'src/user/user.service';
import { Repository } from 'typeorm';
import { BeautyReviewEntity } from './beautyReview.entity';
import { BeautyReviewPhotoEntity } from './beautyReviewPhoto.entity';

@Injectable()
export class BeautyReviewService {
    constructor(
        private readonly beautyService: BeautyService,
        private readonly userService: UserService,
        private readonly uploadsService: UploadsService,
        @InjectRepository(BeautyReviewEntity)
        private readonly beautyReviewRepo: Repository<BeautyReviewEntity>,
        @InjectRepository(BeautyReviewPhotoEntity)
        private readonly beautyReviewPhotoRepo: Repository<BeautyReviewPhotoEntity>
    ) { }

    async createReview(
        authInfo: AuthUserInfo,
        reviewDTO: ShopReviewDTO,
        files: Array<Express.Multer.File>
    ): Promise<ReviewBase> {
        const count = await this.getBeautyReviewCountByContent(reviewDTO.content);
        if (count > 0)
            return;

        const beauty: BeautyEntity = await this.beautyService.getBeautyById(reviewDTO.shopId);
        const user: MyUserInfo = await this.userService.getUserByUID(authInfo.uid);

        const photoUrls = await this.uploadsService.uploadReviewPhoto('beauty', reviewDTO.shopId, files);

        const photos: BeautyReviewPhotoEntity[] = [];
        for (let url of photoUrls) {
            const photo = this.beautyReviewPhotoRepo.create({
                url
            });
            await this.beautyReviewPhotoRepo.save(photo);
            photos.push(photo);
        }

        const review = this.beautyReviewRepo.create({
            ...reviewDTO,
            date: new Date(),
            beauty: beauty,
            owner: user,
            photos
        });
        await this.beautyReviewRepo.save(review);

        return review;
    }

    async createReviewByMap(beauty: BeautyEntity, content: string, score: number, date: Date): Promise<BeautyReviewEntity> {
        const count = await this.getBeautyReviewCountByContent(content);
        if (count > 0)
            return;

        const owner: UserEntity = await this.userService.getUserById(1);
        const review = this.beautyReviewRepo.create({
            content,
            score,
            date,
            beauty,
            owner
        });
        await this.beautyReviewRepo.save(review);
        return review;
    }

    async getBeautyReviews(beautyId: number): Promise<ReviewBase[]> {
        const reviews = await this.beautyReviewRepo
            .createQueryBuilder("beautyReview")
            .leftJoin('beautyReview.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("beautyReview.beautyId = :beautyId", { beautyId: beautyId })
            .getMany();

        return reviews;
    }

    async getBeautyReviewCountByContent(content: string): Promise<number> {
        const found = await this.beautyReviewRepo
            .createQueryBuilder("beautyReview")
            .where("beautyReview.content = :content", { content })
            .getCount();

        return found;
    }
}
