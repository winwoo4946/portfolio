
import { UserEntity } from "src/auth/user.entity";
import { BeautyEntity } from "src/beauty/beauty.entity";
import { BaseEntity, Column, Entity, Index, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { BeautyReviewPhotoEntity } from "./beautyReviewPhoto.entity";

@Entity('beauty_review_table')
export class BeautyReviewEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: number;

    @Column("text")
    content: string;

    @Column()
    score: number;

    @Column()
    date: Date;

    @Index()
    @ManyToOne(type => BeautyEntity, beauty => beauty.reviews)
    beauty: BeautyEntity;

    @ManyToOne(type => UserEntity, user => user.hotelReviews)
    owner: UserEntity;

    @OneToMany(type => BeautyReviewPhotoEntity, photo => photo.review)
    photos: BeautyReviewPhotoEntity[];
}