import { BaseEntity, Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BeautyReviewEntity } from "./beautyReview.entity";

@Entity('beauty_review_photo_table')
export class BeautyReviewPhotoEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    url: string;

    @Index()
    @ManyToOne(type => BeautyReviewEntity, review => review.photos)
    review: BeautyReviewEntity;
}