import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import "reflect-metadata"
import * as cookieParser from 'cookie-parser';
import { WinstonModule } from 'nest-winston';
import WinstonConfig from './configs/winston.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: WinstonModule.createLogger(WinstonConfig.getConfig(null))
  });
  app.use(cookieParser());
  app.enableCors({
    origin: process.env.ORIGIN,
    credentials: true
  }); // Cors활성화
  app.useGlobalPipes(
    new ValidationPipe({ // 전송 받은 데이터 무결성 체크 파이프
      whitelist: true,
      forbidNonWhitelisted: true,
      transform: true // 전송 받은 데이터는 string이지만 받는 파라미터의 정의된 타입값으로 자동 변환됨
    })
  );
  await app.listen(9000);
}
bootstrap();
