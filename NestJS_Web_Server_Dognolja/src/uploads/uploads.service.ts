import { PutObjectCommand, S3Client } from '@aws-sdk/client-s3';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ShopType } from 'src/dognolja_common/commonEnum';

@Injectable()
export class UploadsService {
    private readonly AWS_ACCESS_KEY: string;
    private readonly AWS_SECRET_KEY: string;
    private readonly BUCKET_NAME = 'dog-nolja-bucket';
    private readonly REGION = 'ap-northeast-2';
    private readonly OBJECT_URL = `https://${this.BUCKET_NAME}.s3.${this.REGION}.amazonaws.com/`;
    private readonly S3: S3Client;
    constructor(
        private readonly configService: ConfigService
    ) { 
        this.AWS_ACCESS_KEY = this.configService.get('AWS_ACCESS_KEY');
        this.AWS_SECRET_KEY = this.configService.get('AWS_SECRET_KEY');
        this.S3 = new S3Client({
            region: this.REGION,
            credentials: {
                accessKeyId: this.AWS_ACCESS_KEY,
                secretAccessKey: this.AWS_SECRET_KEY
            }
        })
    }

    async uploadReviewPhoto(type: ShopType, id: number, files: Array<Express.Multer.File>): Promise<string[]> {
        const FOLDER_NAME = `${type}ReviewPhoto`;
        const objectUrls: string[] = [];
        const promises = [];
        for(let file of files) {
            // 호텔 별 리뷰사진 경로 생성
            const photoUrl = `${FOLDER_NAME}/${id}/${file.originalname}`;
            const promise = this.S3.send(new PutObjectCommand({
                Bucket: this.BUCKET_NAME,
                Key: photoUrl,
                Body: file.buffer
            }));
            // s3 upload를 비동기 동시 처리로 진행
            promises.push(promise);
            // 리뷰사진 별 생성된 경로를 바탕으로 만들어진 url 추가
            objectUrls.push(`${this.OBJECT_URL}${photoUrl}`);
        }
        await Promise.all(promises);

        return objectUrls;
    }
}
