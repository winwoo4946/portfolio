
import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity('hotal_table_naver')
export class HotelNaverEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column("text")
    description: string;

    @Column()
    categoryName: string;

    @Column()
    naverUrl: string;

    @Column()
    address: string;

    @Column()
    roadAddress: string;

    @Column()
    x: number;

    @Column()
    y: number;

    @Column()
    zeroResult: boolean;
}