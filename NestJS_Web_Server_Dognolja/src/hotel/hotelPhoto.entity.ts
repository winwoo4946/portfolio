import { BaseEntity, Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { HotelEntity } from "./hotel.entity";

@Entity('hotel_photo_table')
export class HotelPhotoEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    url: string;

    @Index()
    @ManyToOne(type => HotelEntity, hotel => hotel.photos)
    hotel: HotelEntity;
}