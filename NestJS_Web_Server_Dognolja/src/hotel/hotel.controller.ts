import { HotelEntity } from './hotel.entity';
import { HotelService } from './hotel.service';
import { Body, Controller, Get, Param, ParseIntPipe, Post, UsePipes, ValidationPipe } from '@nestjs/common';
import { HotelDTO } from './dto/hotel.dto';

@Controller('hotel')
export class HotelController {
    constructor(private readonly hotelService: HotelService) { }
    
    @Post('createHotel')
    @UsePipes(ValidationPipe)
    createHotel(@Body() hotelDTO: HotelDTO): Promise<HotelEntity> {
        return this.hotelService.createHotel(hotelDTO);
    }

    @Get('/:id')
    getHotelById(@Param('id', ParseIntPipe) id: number) {
        return this.hotelService.getHotelDetail(id);
    }
}
