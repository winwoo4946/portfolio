import { UserEntity } from "src/auth/user.entity";
import { HotelReviewEntity } from "src/hotelReview/hotelReview.entity";
import { UserHotelWishEntity } from "src/user/userHotelWish.entity";
import { BaseEntity, Column, Entity, JoinColumn, ManyToMany, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { HotelPhotoEntity } from "./hotelPhoto.entity";

@Entity('hotel_table')
export class HotelEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column("text")
    description: string;

    @Column("varchar", { length: 45, default: true })
    phone: string;

    @Column()
    categoryName: string;

    @Column()
    url: string;

    @Column()
    address: string;

    @Column()
    roadAddress: string;

    @Column()
    mapx: number;

    @Column()
    mapy: number;

    @Column("float")
    mapLat: number;

    @Column("float")
    mapLng: number;

    @Column()
    thumbnailPhotoId: number;

    @Column({default: true})
    price1: number = 0;

    @Column({default: true})
    price2: number = 0;

    @Column({default: true})
    price3: number = 0;

    @Column({default: true})
    price4: number = 0;

    @OneToMany(type => HotelReviewEntity, review => review.hotel)
    reviews: HotelReviewEntity[];

    @OneToMany(type => HotelPhotoEntity, photo => photo.hotel)
    photos: HotelPhotoEntity[];
 
    @ManyToMany(()=> UserEntity, (user) =>user.wishHotels)
    wishUsers : UserEntity[];
    
    @ManyToMany (()=>UserEntity, (user) => user.reservateionHotels)
    reservedUsers : UserEntity[];
}