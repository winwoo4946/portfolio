import { BaseEntity, Column, Entity, Index, PrimaryGeneratedColumn } from "typeorm";

@Entity('hotal_table_kakao')
export class HotelKakaoEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column("bigint")
    kakaoMapId: number;

    @Column()
    title: string;

    @Column("text")
    description: string;

    @Column("varchar", { length: 45 })
    phone: string;

    @Column()
    categoryName: string;

    @Column()
    kakaoPlaceUrl: string;

    @Column()
    address: string;

    @Column()
    roadAddress: string;

    @Column()
    x: number;

    @Column()
    y: number;
}