import { Inject, Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HotelEntity } from './hotel.entity';
import { HotelDTO } from './dto/hotel.dto';
import { KakaoMapDTO } from 'src/map/dto/KakaoMapDTO';
import { HotelKakaoEntity } from './hotelKakao.entity';
import { NaverMapDTO } from 'src/map/dto/naverMapDTO';
import { HotelNaverEntity } from './hotelNaver.entity';
import { Repository } from 'typeorm';
import { HotelPhotoEntity } from './hotelPhoto.entity';
import { title } from 'process';

@Injectable()
export class HotelService {
    constructor(
        @InjectRepository(HotelPhotoEntity)
        private readonly hotelPhotoRepository: Repository<HotelPhotoEntity>,

        @InjectRepository(HotelKakaoEntity)
        private readonly hotelKakaoRepository: Repository<HotelKakaoEntity>,

        @InjectRepository(HotelNaverEntity)
        private readonly hotelNaverRepository: Repository<HotelNaverEntity>,

        @InjectRepository(HotelEntity)
        private readonly hotelRepository: Repository<HotelEntity>
    ) { }

    async getHotelById(id: number): Promise<HotelEntity> {
        const found = await this.hotelRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`Not found Hotel Info - id(${id})`);
        }
        return found;
    }

    async createHotel(hotelDTO: HotelDTO): Promise<HotelEntity> {
        const hotel = this.hotelRepository.create(hotelDTO);
        await this.hotelRepository.save(hotel);
        return hotel;
    }

    async getHotelsByAddress(address: string): Promise<ShopBaseInfo[]> {
        const hotelEntities: HotelEntity[] = await this.hotelRepository.createQueryBuilder('hotel')
            .leftJoinAndSelect('hotel.photos', 'photo')
            .leftJoinAndSelect('hotel.reviews', 'review')
            .where("hotel.address like :address", { address: `%${address}%` })
            .getMany();

        const hotelInfos: ShopBaseInfo[] = []
        for (let entity of hotelEntities) {
            hotelInfos.push(this.getShopBaseInfo(entity));
        }
        return hotelInfos;
    }

    async getHotelByAddressListData(address: string) {
        const result = await this.hotelRepository.createQueryBuilder('hotel')
            .select()
            .addSelect((subQuery) => {
                return subQuery.select("url").from(HotelPhotoEntity, "photo").where("photo.id = hotel.thumbnailPhotoId")
            }, "photoUrl")
            .where("hotel.address like :address", { address: `%${address}%` })
            .getRawMany();
        return result;
    }

    async getHotelDetail(id: number): Promise<ShopInfo> {
        const hotelEntity = await this.hotelRepository.createQueryBuilder('hotel')
            .leftJoin('hotel.photos', 'photo').addSelect(['photo.id', 'photo.url'])
            .leftJoinAndSelect('hotel.reviews', 'review')
            .leftJoin('review.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("hotel.id = :id", { id })
            .getOne();

        const shopInfo: ShopInfo = {
            ...this.getShopBaseInfo(hotelEntity),
            description: hotelEntity.description,
            phone: hotelEntity.phone,
            reviews: hotelEntity.reviews,
            photos: hotelEntity.photos,
            address: hotelEntity.address,
            roadAddress: hotelEntity.roadAddress,
            operatingTime: [],
            subInfos: ['~4.9kg', '호텔링', '(1일)', '25,000원']
        }
        return shopInfo;
    }

    private getShopBaseInfo(entity: HotelEntity): ShopBaseInfo {
        let score: number = 0;
        let reviewCount: number = entity.reviews.length;
        for (let review of entity.reviews) {
            score += review.score;
        }
        const shopBaseInfo: ShopBaseInfo = {
            id: entity.id,
            title: entity.title,
            mapLat: entity.mapLat,
            mapLng: entity.mapLng,
            price1: entity.price1,
            mapx: entity.mapx,
            mapy: entity.mapy,
            thumbnailUrl: entity.photos.length > 0 ? entity.photos[0].url : "",
            hashTags: [],
            score: Math.floor((score / reviewCount) * 10) / 10,
            reviewCount
        }
        return shopBaseInfo;
    }

    //#region 맵관련
    async getHotelCountByKakaoId(kakaoId: number): Promise<number> {
        const found = await this.hotelRepository
            .createQueryBuilder("hotel")
            .where("hotel.kakaoMapId = :kakaoId", { kakaoId })
            .getCount();

        return found;
    }

    async getHotelKakaoCountByKakaoId(kakaoId: number): Promise<number> {
        const found = await this.hotelKakaoRepository
            .createQueryBuilder("hotelKakao")
            .where("hotelKakao.kakaoMapId = :kakaoId", { kakaoId })
            .getCount();

        return found;
    }

    async getHotelKakaoCountByAddress(address: string): Promise<number> {
        const found = await this.hotelKakaoRepository
            .createQueryBuilder("hotelKakao")
            .where("hotelKakao.address = :address", { address })
            .getCount();

        return found;
    }

    async getHotelNaverCountByAddress(address: string): Promise<number> {
        const found = await this.hotelNaverRepository
            .createQueryBuilder("hotelNaver")
            .where("hotelNaver.address like :address", { address: `%${address}%` })
            .getCount();

        return found;
    }

    async getHotelPhotoCountByUrl(url: string): Promise<number> {
        const found = await this.hotelPhotoRepository
            .createQueryBuilder("hotelPhoto")
            .where("hotelPhoto.url = :url", { url })
            .getCount();

        return found;
    }

    async getAllHotelKakao(): Promise<HotelKakaoEntity[]> {
        return await this.hotelKakaoRepository
            .createQueryBuilder("hotelKakao")
            .getMany();
    }

    async getAllHotelNaver(): Promise<HotelNaverEntity[]> {
        return await this.hotelNaverRepository
            .createQueryBuilder("hotelNaver")
            .getMany();
    }

    async getAllHotel(): Promise<HotelEntity[]> {
        return await this.hotelRepository.find();
    }

    async createHotelKakao(kakaoMapDTO: KakaoMapDTO): Promise<HotelKakaoEntity> {
        const count = await this.getHotelKakaoCountByKakaoId(kakaoMapDTO.kakaoMapId);
        if (count > 0) {
            // 이미 존재하는 호텔
            return;
        }

        const hotel = this.hotelKakaoRepository.create(kakaoMapDTO);
        await this.hotelKakaoRepository.save(hotel);
        return hotel;
    }

    async createHotelNaver(naverMapDTO: NaverMapDTO): Promise<HotelNaverEntity> {
        const count = await this.getHotelNaverCountByAddress(naverMapDTO.address);
        if (count > 0) {
            // 이미 존재하는 호텔
            return;
        }

        const hotel = this.hotelNaverRepository.create(naverMapDTO);
        await this.hotelNaverRepository.save(hotel);
        return hotel;
    }

    async createHotelGoogle(hotelDTO: HotelDTO): Promise<HotelEntity> {
        // 호텔 생성
        const hotel = this.hotelRepository.create(hotelDTO);
        await this.hotelRepository.save(hotelDTO);

        // 포토 생성
        // 포토<-호텔 연결
        let promises = [];
        for (let photoUrl of hotelDTO.photoUrls) {
            const count = await this.getHotelPhotoCountByUrl(photoUrl);
            if (count > 0)
                continue;

            const photo = this.hotelPhotoRepository.create({
                url: photoUrl,
                hotel: hotel
            });
            promises.push(this.hotelPhotoRepository.save(photo));
        }
        await Promise.all(promises);

        // 리뷰 생성
        // 리뷰<-호텔 연결
        // for(let reviewDTO of reviews) {
        //     const review = this.hotelReviewService.createReview(reviewDTO, hotel);
        // }
        return hotel;
    }

    async deleteHotelNaver(id: number) {
        const found = await this.hotelNaverRepository.findOneBy({ id });
        if (!found)
            return;

        await this.hotelNaverRepository
            .createQueryBuilder()
            .delete()
            .from(HotelNaverEntity)
            .where("id = :id", { id: id })
            .execute();

        console.log(`delete naver - ${id}`);
    }

    async updateHotelNaverZeroResult(id: number, isZero: boolean) {
        const found = await this.hotelNaverRepository.findOneBy({ id });
        if (!found)
            return;

        await this.hotelNaverRepository
            .createQueryBuilder()
            .update(HotelNaverEntity)
            .set({ zeroResult: isZero })
            .where("id = :id", { id: id })
            .execute();

        console.log(`update naver result - ${id} - ${isZero}`);
    }
    //#endregion
}
