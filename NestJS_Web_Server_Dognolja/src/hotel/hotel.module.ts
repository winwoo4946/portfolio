import { TypeOrmModule } from '@nestjs/typeorm';
import { Module } from '@nestjs/common';
import { HotelController } from './hotel.controller';
import { HotelService } from './hotel.service';
import { HotelEntity } from './hotel.entity';
import { HotelPhotoEntity } from './hotelPhoto.entity';
import { HotelReviewEntity } from 'src/hotelReview/hotelReview.entity';
import { HotelKakaoEntity } from './hotelKakao.entity';
import { HotelNaverEntity } from './hotelNaver.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      HotelEntity,
      HotelPhotoEntity,
      HotelReviewEntity,
      HotelNaverEntity,
      HotelKakaoEntity
    ])
  ],
  exports: [HotelService],
  controllers: [HotelController],
  providers: [
    HotelService
  ]
})
export class HotelModule { }
