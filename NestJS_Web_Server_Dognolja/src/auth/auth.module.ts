import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { UserEntity } from './user.entity';
import { RedisModule } from 'src/redis/redis.module';
import { KakaoStrategy } from './kakao.strategy';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { NaverStrategy } from './naver.strategy';
import { JwtStrategy } from './jwt.strategy';
import { GoogleStrategy } from './google.strategy';

@Module({
    imports: [
        ConfigModule,
        RedisModule,
        TypeOrmModule.forFeature([UserEntity]),
        PassportModule.register({defaultStrategy: ['jwt', 'kakao', 'naver', 'google']}),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                secret: configService.get('JWT_SECRET'),
                signOptions: {
                    expiresIn: configService.get('JWT_EXPIRES_IN')
                }
            })
        })
    ],
    controllers: [AuthController],
    providers: [AuthService, KakaoStrategy, NaverStrategy, GoogleStrategy, JwtStrategy],
    exports: [JwtStrategy, PassportModule]
})
export class AuthModule { }
