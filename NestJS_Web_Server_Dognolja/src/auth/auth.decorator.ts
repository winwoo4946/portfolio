import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthUserInfo, StrategyInfo } from './login';

export const GetPlatformLoginInfo = createParamDecorator<string, ExecutionContext, StrategyInfo>(
    (data: string, ctx: ExecutionContext): StrategyInfo => {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    }
)

export const GetAuthUserInfo = createParamDecorator<string, ExecutionContext, AuthUserInfo>(
    (data: string, ctx: ExecutionContext): AuthUserInfo => {
        const request = ctx.switchToHttp().getRequest();
        return request.user;
    }
)

export class KakaoAuthGuard extends AuthGuard('kakao') {};
export class NaverAuthGuard extends AuthGuard('naver') {};
export class GoogleAuthGuard extends AuthGuard('google') {};
export class JwtAuthGuard extends AuthGuard('jwt') {};