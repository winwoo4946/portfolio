import { Injectable, Logger, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { RedisService } from "src/redis/redis.service";
import * as jose from 'jose';
import { AuthOIDCPulicKey, LoginClientInfo, LoginCookie } from "./login";
import axios from "axios";
import { CookieOptions, Response } from "express";
import { jsonToQueryString } from "src/util/util";
import { deprecate } from "util";
import { AuthCookieKeys, LoginPlatformType } from "src/dognolja_common/commonEnum";

@Injectable()
/** @deprecated 사용하지 않음. 참고용 코드 */
export class AuthSupport {
    constructor(
        private readonly configService: ConfigService,
        private readonly redisService: RedisService,
    ) {}

    getClientInfo(loginPlatform: LoginPlatformType): LoginClientInfo {
        let client_id = '';
        let client_secret = '';
        let admin_key = '';
        let redirect_url = '';

        switch (loginPlatform) {
            case LoginPlatformType.Google:
                break;
            case LoginPlatformType.Naver:
                client_id = this.configService.get('NAVER_CLIENT_ID');
                client_secret = this.configService.get('NAVER_CLIENT_SECRET');
                redirect_url = this.configService.get('NAVER_LOGIN_REDRIECT_URL');
                break;
            case LoginPlatformType.Kakao:
                client_id = this.configService.get('KAKAO_REST_API_KEY');
                client_secret = this.configService.get('KAKAO_CLIENT_SECRET');
                admin_key = this.configService.get('KAKAO_ADMIN_KEY');
                redirect_url = this.configService.get('KAKAO_LOGIN_REDRIECT_URL');
                break;
        }

        return {
            client_id,
            client_secret,
            admin_key,
            redirect_url
        }
    }

    /**
     * 인증 관련 쿠키 등록 (로그인)
     * @param res : Express Response Object
     */
    setAuthCookie(res: Response, loginCookie: LoginCookie): void {
        const cookieOption: CookieOptions = {
            httpOnly: true,
            maxAge: 24 * 60 * 60 * 1000 // 1day
        };

        const nowTime = new Date().getTime();
        res.cookie(AuthCookieKeys.LOGIN_PLATFORM, loginCookie.platform);
        res.cookie(AuthCookieKeys.ID_TOKEN, loginCookie.idToken.token, cookieOption);
        res.cookie(AuthCookieKeys.ID_TOKEN_EXPIRES_IN, nowTime + (loginCookie.idToken.expiresIn * 1000));
        res.cookie(AuthCookieKeys.REFRESH_TOKEN, loginCookie.refreshToken.token, cookieOption);
        res.cookie(AuthCookieKeys.REFRESH_TOKEN_EXPIRES_IN, nowTime + (loginCookie.refreshToken.expiresIn * 1000));
    }

    /**
     * 인증 관련 쿠키 삭제 (로그아웃)
     * @param res : Express Response Object
     */
    clearAuthCookie(res: Response): void {
        res.clearCookie(AuthCookieKeys.LOGIN_PLATFORM);
        res.clearCookie(AuthCookieKeys.ID_TOKEN);
        res.clearCookie(AuthCookieKeys.ID_TOKEN_EXPIRES_IN);
        res.clearCookie(AuthCookieKeys.REFRESH_TOKEN);
        res.clearCookie(AuthCookieKeys.REFRESH_TOKEN_EXPIRES_IN);
    }

    /**
     * 쿠키에 있는 idToken을 이용하여 검증
     * @param cookies 쿠키모음
     * @returns 유저 플랫폼 아이디
     */
    async validateIdTokenByCookie(cookies: any): Promise<number> {
        const idToken: string = cookies[AuthCookieKeys.ID_TOKEN];
        if(!idToken) {
            throw new UnauthorizedException('Not found id Token');
        }

        await this.checkIdTokenExpiresIn(cookies);

        const loginPlatform: LoginPlatformType = cookies[AuthCookieKeys.LOGIN_PLATFORM];
        const verifyToken: jose.JWTVerifyResult = await this.validateIdToken(loginPlatform, idToken);
        return parseInt(verifyToken.payload.sub);
    }

    private async validateIdToken(platform: LoginPlatformType, token: string): Promise<jose.JWTVerifyResult> {
        const clientInfo = this.getClientInfo(platform);
        switch (platform) {
            case LoginPlatformType.Google:
                return null;
            case LoginPlatformType.Naver:
                return null;
            case LoginPlatformType.Kakao:
                return await this.validateForKakao(token, clientInfo.client_id);
        }
    }
    
    private async validateForKakao(token: string, appKey: string): Promise<jose.JWTVerifyResult> {
        Logger.debug(token, 'id_token');
        const tokenArr: string[] = token.split('.');
    
        const header = this.decodeBase64(tokenArr[0]);
        Logger.debug(header, 'Token header');
    
        const payload = this.decodeBase64(tokenArr[1]);
        Logger.debug(payload, 'Token payload');
    
        // iss 는 https://kauth.kakao.com로 고정
        if (payload.iss !== 'https://kauth.kakao.com') {
            throw new UnauthorizedException('login fail (iss)');
        }
    
        // app Key 검증
        if (payload.aud !== appKey) {
            throw new UnauthorizedException('login fail (aud)');
        }
    
        // 토큰 만료 시간 검증
        // exp가 seconds로 오기때문에 mileSeconds로 변경하기위해 1000곱해줌
        if ((payload.exp * 1000) <= new Date().getTime()) {
            throw new UnauthorizedException('login fail (exp)');
        }
    
        // 서명검증
        const publicKeys: AuthOIDCPulicKey[] = await this.getAuthOIDCPulbicKeys();
        const foundKey = publicKeys.find(s => s.kid === header.kid);
        if (!foundKey) {
            throw new UnauthorizedException('login fail (kid)');
        }
        Logger.debug(foundKey, 'Token Sign');
    
        const rsaPublicKey = await jose.importJWK({
            kty: foundKey.kty,
            e: foundKey.e,
            n: foundKey.n
        }, foundKey.alg);
    
        try {
            return await jose.jwtVerify(token, rsaPublicKey);
        } catch (e) {
            throw new UnauthorizedException(e.code, e.message);
        }
    }

    /**
     * idToken 만료 체크 후 refresh_token을 이용해서 재갱신 처리
     * @param cookies 쿠키
     */
    private async checkIdTokenExpiresIn(cookies: any) {
        const idTokenExpiresIn: string = cookies[AuthCookieKeys.ID_TOKEN_EXPIRES_IN];
        if(new Date().getTime() > parseInt(idTokenExpiresIn)) {
            // 토큰 만료
            // idToken 재갱신 처리
            const platformType: LoginPlatformType = cookies[AuthCookieKeys.LOGIN_PLATFORM];
            const refreshToken: string = cookies[AuthCookieKeys.REFRESH_TOKEN];
            const {client_id} = this.getClientInfo(platformType);
            const tokenBody = {
                'grant_type': 'refresh_token',
                'client_id': client_id,
                'refresh_token': refreshToken
            };
            const refresh = await axios.post(`https://kauth.kakao.com/oauth/token`, jsonToQueryString(tokenBody),
                {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            
            refresh.data;
        }
    }
    
    /**
     * 카카오에서 제공하는 jwt 공개키 목록 조회 
     * 일정 기간 캐싱하여 사용해야하기 때문에 redis조회 후 없을경우에만 요청
     * https://developers.kakao.com/docs/latest/ko/kakaologin/rest-api#oidc-find-public-key
     * @returns 카카오 oidc pulic key
     */
    private async getAuthOIDCPulbicKeys(): Promise<AuthOIDCPulicKey[]> {
        let publicKeys = await this.redisService.get(this.redisService.AUTH_OIDC_PUBLIC_KEY);
        if (publicKeys) {
            return JSON.parse(publicKeys);
        }
    
        const get = await axios.get('https://kauth.kakao.com/.well-known/jwks.json');
        publicKeys = get.data.keys;
    
        const ttl = 60 * 60 * 2 // 2시간
        await this.redisService.setEx(this.redisService.AUTH_OIDC_PUBLIC_KEY, JSON.stringify(publicKeys), ttl);
    
        return publicKeys;
    }
    
    private decodeBase64(token: string) {
        const buffer = Buffer.from(token, 'base64');
        return JSON.parse(buffer.toString());
    }
}