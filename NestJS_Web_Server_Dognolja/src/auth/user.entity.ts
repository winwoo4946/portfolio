import { BeautyReviewEntity } from "src/beauty-review/beautyReview.entity";
import { HotelEntity } from "src/hotel/hotel.entity";
import { HotelService } from "src/hotel/hotel.service";
import { HotelReviewEntity } from "src/hotelReview/hotelReview.entity";
import { UserHotelWishEntity } from "src/user/userHotelWish.entity";
import { BaseEntity, Column, Entity, Index, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('user_table')
export class UserEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column()
    uid: string;

    @Index()
    @Column()
    platformId: string;

    @Column({ default: '' })
    nickName: string;

    @Column({ default: '' })
    email: string;

    @Column({ default: '' })
    gender: string;

    @Column({ default: '' })
    age: string;

    @Column({ default: '' })
    thumbnailImgUrl: string;

    @Column({ default: '' })
    profileImgUrl: string;

    @Column({ default: () => "CURRENT_TIMESTAMP" })
    loginTime: Date;

    @OneToMany(() => BeautyReviewEntity, review => review.owner)
    beautyReviews: BeautyReviewEntity[];

    @OneToMany(() => HotelReviewEntity, review => review.owner)
    hotelReviews: HotelReviewEntity[];

    @ManyToMany(() => HotelEntity, (hotel) => hotel.wishUsers)
    @JoinTable()
    wishHotels : HotelEntity[];

    @ManyToMany(() => HotelEntity, (hotel) => hotel.reservedUsers)
    @JoinTable()
    reservateionHotels : HotelEntity[];
    
}