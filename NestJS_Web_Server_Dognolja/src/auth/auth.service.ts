import { Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LoginInfoDTO } from './dto/loginInfo.dto';
import { AuthToken, AuthUserInfo, StrategyInfo } from './login';
import { UserEntity } from './user.entity';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import {  Response } from 'express';

@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,

        @InjectRepository(UserEntity)
        private readonly userRepo: Repository<UserEntity>,
    ) { }

    async passportLogin(strategyInfo: StrategyInfo, res: Response) {
        const token: AuthToken = await this.login(strategyInfo.loginInfoDTO);
        const maxAge = this.configService.get('JWT_EXPIRES_IN') * 1000 // 1day
        const expiresIn = new Date().getTime() + maxAge;
        res.cookie('DN_AT', token, {httpOnly: true, maxAge});
        res.cookie('DN_AT_EX', (expiresIn), {httpOnly: false, maxAge});
        return "<script>window.close();</script >";
    }

    async passportLogout(res: Response) {
        res.clearCookie('DN_AT');
        res.clearCookie('DN_AT_EX');
    }

    async login(loginInfoDto: LoginInfoDTO): Promise<AuthToken> {
        // 회원 가입된 유저인지 db에서 검색
        let user: UserEntity = await this.userRepo.findOne({
            where: {
                uid: loginInfoDto.uid
            }
        });

        if (!user) {
            // 유저가 검색되지 않으면, db에 등록(신규가입)
            user = this.userRepo.create(loginInfoDto);
            await this.userRepo.save(user);
            Logger.debug(`${JSON.stringify(user)}`, 'Created');
        } else {
            // 이미 등록된 유저면 로그인 시간만 update
            await this.userRepo.update(user.id, {loginTime: new Date()});
        }

        // 토큰 만료시간 UNIX Timestemp mileSeconds로 계산
        const expiresIn = new Date().getTime() + (this.configService.get("JWT_EXPIRES_IN") * 1000);
        // 유저 uid, loginTime을 갖고있는 payload 생성
        const payload: AuthUserInfo = {
            uid: loginInfoDto.uid,
            loginTime: loginInfoDto.loginTime,
            expiresIn
        }
        // payload를 이용하여 토큰생성
        const token: AuthToken = this.jwtService.sign(payload);
        return token;
    }
}