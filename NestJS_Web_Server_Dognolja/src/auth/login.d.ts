import { LoginPlatformType } from "src/union/union";
import { LoginInfoDTO } from "./dto/loginInfo.dto";

/** @deprecated 사용하지 않음. 참고용 코드 */
export declare class LoginCookie {
    platform: LoginPlatformType
    idToken: LoginTokenInfo;
    refreshToken: LoginTokenInfo;
}

/** @deprecated 사용하지 않음. 참고용 코드 */
export declare class LoginTokenInfo {
    token: string; // 토큰
    expiresIn: number; // 토큰만료시간
}

/** @deprecated 사용하지 않음. 참고용 코드 */
export declare class AuthOIDCPulicKey {
    kid: string; // 공개키 ID
    kty: string; // 공개키 타입, RSA로 고정
    alg: string; // 암호화 알고리즘
    use: string; // 공개키의 용도, sig(서명)으로 고정
    n: string; // 공개키 모듈(Modulus) 공개키는 n과 e의 쌍으로 구성됨
    e: string; // 공개키 지수(Exponent) 공개키는 n과 e의 쌍으로 구성됨
}

/** @deprecated 사용하지 않음. 참고용 코드 */
export declare interface AuthInfo {
    platformType: LoginPlatformType;
    platformId: number;
}

export declare interface LoginClientInfo {
    client_id: string;
    client_secret: string;
    redirect_url: string;
    admin_key: string;
}

export declare interface StrategyInfo {
    accessToken: string;
    refreshToken: string;
    loginInfoDTO: LoginInfoDTO;
}

export declare interface AuthUserInfo {
    uid: string;
    loginTime: Date;
    expiresIn: number;
}
export declare type AuthToken = string;