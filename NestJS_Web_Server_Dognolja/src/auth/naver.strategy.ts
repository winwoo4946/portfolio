import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-naver";
import { LoginInfoDTO } from "./dto/loginInfo.dto";
import { StrategyInfo } from "./login";

@Injectable()
export class NaverStrategy extends PassportStrategy(Strategy) {
    constructor(
        configService: ConfigService,
    ) {
        super({
            clientID: configService.get('NAVER_CLIENT_ID'),
            clientSecret: configService.get('NAVER_CLIENT_SECRET'),
            callbackURL: configService.get('NAVER_LOGIN_REDRIECT_URL')
        });
    }

    async validate(accessToken: string, refreshToken: string, profile: any, done: (error: any, loginInfo?: StrategyInfo) => void) {
        console.log(profile);
        const profileJson = profile._json;
        const loginInfoDTO: LoginInfoDTO = {
            uid: `naver_${profile.id}`,
            platformId: profile.id,
            nickName: profileJson.nickname,
            email: profileJson.email,
            gender: 'none',
            age: profileJson.age,
            profileImgUrl: profileJson.profile_image,
            thumbnailImgUrl: profileJson.profile_image,
            loginTime: new Date()
        };
        done(null, {loginInfoDTO, accessToken, refreshToken});
    }
}