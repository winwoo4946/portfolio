import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-google-oauth20";
import { LoginInfoDTO } from "./dto/loginInfo.dto";
import { StrategyInfo } from "./login";

@Injectable()
export class GoogleStrategy extends PassportStrategy(Strategy, 'google') {
    constructor(
        configService: ConfigService,
    ) {
        super({
            clientID: configService.get('GOOGLE_OAUTH_CLIENT_ID'),
            clientSecret: configService.get('GOOGLE_OAUTH_CLIENT_SECRET'),
            callbackURL: configService.get('GOOGLE_OAUTH_REDRIECT_URL'),
            scope: ['email', 'profile']
        });
    }

    async validate(accessToken: string, refreshToken: string, profile: any, done: (error: any, loginInfo?: StrategyInfo) => void) {
        console.log(profile);
        const profileJson = profile._json;
        const loginInfoDTO: LoginInfoDTO = {
            uid: `google_${profileJson.sub}`,
            platformId: profileJson.sub,
            nickName: profileJson.name,
            email: profileJson.email,
            gender: "none",
            age: "none",
            profileImgUrl: profileJson.picture,
            thumbnailImgUrl: profileJson.picture,
            loginTime: new Date()
        };
        done(null, {loginInfoDTO, accessToken, refreshToken});
    }
}