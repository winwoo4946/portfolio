import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { AuthCookieKeys } from 'src/dognolja_common/commonEnum';
import { AuthSupport } from './auth.support';
import { AuthInfo } from './login';

@Injectable()
export class IdTokenGuard implements CanActivate {
    constructor(
        private readonly authSupport: AuthSupport
    ) {}
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const platformType = request.cookies[AuthCookieKeys.LOGIN_PLATFORM];
        const platformId = await this.authSupport.validateIdTokenByCookie(request.cookies);
        request.authInfo = {
            platformType,
            platformId
        } as AuthInfo;
        return true;
    }
}