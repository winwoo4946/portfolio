import { IsIn, IsString } from "class-validator";
import { LoginPlatformType } from "src/dognolja_common/commonEnum";

export class PlatformLoginDTO {
    @IsIn([LoginPlatformType.Google, LoginPlatformType.Naver, LoginPlatformType.Kakao])
    platformType: LoginPlatformType;
    @IsString()
    refresh_token: string;
}