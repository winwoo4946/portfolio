import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { Strategy } from "passport-kakao";
import { LoginInfoDTO } from "./dto/loginInfo.dto";
import { StrategyInfo } from "./login";

@Injectable()
export class KakaoStrategy extends PassportStrategy(Strategy) {
    constructor(
        configService: ConfigService,
    ) {
        super({
            clientID: configService.get('KAKAO_REST_API_KEY'),
            clientSecret: configService.get('KAKAO_CLIENT_SECRET'),
            callbackURL: configService.get('KAKAO_LOGIN_REDRIECT_URL')
        });
    }

    async validate(accessToken: string, refreshToken: string, profile: any, done: (error: any, loginInfo?: StrategyInfo) => void) {
        const profileJson = profile._json;
        const account = profileJson.kakao_account;
        const loginInfoDTO: LoginInfoDTO = {
            uid: `kakao_${profileJson.id}`,
            platformId: profileJson.id,
            nickName: account.profile.nickname,
            email: account.email,
            gender: account.gender,
            age: account.age_range,
            profileImgUrl: account.profile.profile_image_url,
            thumbnailImgUrl: account.profile.thumbnail_image_url,
            loginTime: new Date(profileJson.connected_at)
        };
        done(null, {loginInfoDTO, accessToken, refreshToken});
    }
}