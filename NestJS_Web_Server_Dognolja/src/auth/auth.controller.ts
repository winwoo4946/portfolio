import { Controller, Get, HttpCode, HttpStatus, Post, Res, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { Response } from 'express';
import { AuthUserInfo, StrategyInfo } from './login';
import { GetAuthUserInfo, GetPlatformLoginInfo, GoogleAuthGuard, JwtAuthGuard, KakaoAuthGuard, NaverAuthGuard } from './auth.decorator';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ) { }

    @Post('/platformLogout')
    @HttpCode(200)
    async platformLogout(@Res({ passthrough: true }) res: Response) {
        this.authService.passportLogout(res);
    }

    @Get('/kakaoLogin')
    @HttpCode(200)
    @UseGuards(KakaoAuthGuard)
    async kakaoLogin() {
        return HttpStatus.OK;
    }

    @Get('/kakaoLoginRedirect')
    @UseGuards(KakaoAuthGuard)
    async kakaoLoginRedirect(@GetPlatformLoginInfo() strategyInfo: StrategyInfo, @Res({ passthrough: true }) res: Response) {
        return await this.authService.passportLogin(strategyInfo, res);
    }

    @Get('/naverLogin')
    @HttpCode(200)
    @UseGuards(NaverAuthGuard)
    async naverLogin() {
        return HttpStatus.OK;
    }

    @Get('/naverLoginRedirect')
    @UseGuards(NaverAuthGuard)
    async naverLoginRedirect(@GetPlatformLoginInfo() strategyInfo: StrategyInfo, @Res({ passthrough: true }) res: Response) {
        return await this.authService.passportLogin(strategyInfo, res);
    }

    @Get('/googleLogin')
    @HttpCode(200)
    @UseGuards(GoogleAuthGuard)
    async googleLogin() {
        return HttpStatus.OK;
    }

    @Get('/googleLoginRedirect')
    @UseGuards(GoogleAuthGuard)
    async googleLoginRedirect(@GetPlatformLoginInfo() strategyInfo: StrategyInfo, @Res({ passthrough: true }) res: Response) {
        return await this.authService.passportLogin(strategyInfo, res);
    }

    @Get("/authTest")
    @UseGuards(JwtAuthGuard)
    async authTest(@GetAuthUserInfo() userInfo: AuthUserInfo) {
        console.log(userInfo);
    }

}
