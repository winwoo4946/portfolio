import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { PassportStrategy } from "@nestjs/passport";
import { Request } from "express";
import { ExtractJwt, Strategy } from "passport-jwt";
import { LoginInfoDTO } from "./dto/loginInfo.dto";
import { StrategyInfo } from "./login";

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
    constructor(
        configService: ConfigService,
    ) {
        super({
            secretOrKey: configService.get('JWT_SECRET'),
            jwtFromRequest: ExtractJwt.fromExtractors([
                (request: Request) => {
                    const token = request?.cookies?.DN_AT;
                    console.log(token);
                    return token;
                }
            ])
        });
    }

    async validate(payload) {
        console.log(payload);
        return payload;
    }
}