import { Controller, Get, Post } from '@nestjs/common';

@Controller('hotel-admin')
export class HotelAdminController {
    @Get('reservations/:id')
    async reservations() {

    }

    @Post('uploadsPhoto')
    async uploadPhoto() {

    }

    @Post('updateHotelInfo')
    async updateHotelInfo() {

    }

    @Post('addHotel')
    async addHotel() {
        
    }
}
