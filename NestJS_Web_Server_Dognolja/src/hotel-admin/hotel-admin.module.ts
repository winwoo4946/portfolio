import { Module } from '@nestjs/common';
import { HotelAdminController } from './hotel-admin.controller';
import { HotelAdminService } from './hotel-admin.service';

@Module({
  controllers: [HotelAdminController],
  providers: [HotelAdminService]
})
export class HotelAdminModule {}
