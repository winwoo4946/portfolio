import { Injectable } from '@nestjs/common';
import { ShopType } from 'src/dognolja_common/commonEnum';
import { getRegExp } from 'korean-regexp';
import { BeautyEntity } from 'src/beauty/beauty.entity';
import { BeautyService } from 'src/beauty/beauty.service';
import { HotelEntity } from 'src/hotel/hotel.entity';
import { HotelService } from 'src/hotel/hotel.service';
import { RedisService } from 'src/redis/redis.service';

@Injectable()
export class SearchService {

    constructor(
        private readonly redisService: RedisService,
        private readonly hotelService: HotelService,
        private readonly beautyService: BeautyService
    ) {}

    async autoCompleteList(type: ShopType, keyword: string): Promise<string[]> {
        const keywordArr = keyword.split(' ');
        // 검색 키워드를 띄워쓰기 기준으로 나눈뒤
        // 실제 검색은 맨 마지막 단어 기준으로 검색
        const searchKeyword = keywordArr[keywordArr.length - 1];

        const arr: string[] = this.redisService.getAddressArr(type);
        const keywordResults: string[] = [];

        for (let address of arr) {
            // 최대 노출 개수 체크
            if (keywordResults.length >= this.redisService.ADDRESS_RESULT_COUNT)
                break;

            const match = address.search(getRegExp(searchKeyword));
            if (match === -1) {
                // 검색 안됨
                continue;
            }

            // 검색 됨
            keywordResults.push(address);
        }
        
        const redisResults: string[][] = await this.redisService.getAddressByKeyward(type, keywordResults);
        let results: string[] = [];
        // redis에서 뽑아온 주소 결과에서
        // keyword와 매칭 되는 주소만 반환
        for (let redisResult of redisResults) {
            if (keywordArr.length > 1) {
                // keywordArr.length가 1보다 큰 경우는 정밀검색(ex. 서울시 구로구 오류동)했다고 가정하여
                // keyword와 일치하는 내용만 걸러냄
                // redis에 set형태로 저장되기때문에 set 안을 한번더 검색
                for (let address of redisResult) {
                    if (address.indexOf(keyword) === -1)
                        continue;

                    results.push(address);
                }
            } else {
                results = [...results, ...redisResult];
            }
        }

        return results;
    }

    async searchHotel(address: string): Promise<ShopBaseInfo[]> {
        return await this.hotelService.getHotelsByAddress(address);
    }
    async searchBeauty(address: string): Promise<ShopBaseInfo[]> {
        return await this.beautyService.getBeautyByAddress(address);
    }
    
}
