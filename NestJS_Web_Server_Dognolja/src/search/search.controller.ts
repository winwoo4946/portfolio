import { Controller, Get, Query } from '@nestjs/common';
import { ShopType } from 'src/dognolja_common/commonEnum';
import { SearchService } from './search.service';

@Controller('search')
export class SearchController {
    constructor(
        private readonly searchService: SearchService
    ) { }

    @Get('autoCompleteList')
    async autoCompleteList(@Query('type') type: ShopType, @Query('keyword') keyword: string) {
        return this.searchService.autoCompleteList(type, keyword);
    }

    @Get('hotel')
    async hotel(@Query('address') address: string) {
        return this.searchService.searchHotel(address);
    }

    @Get('beauty')
    async beauty(@Query('address') address: string) {
        return this.searchService.searchBeauty(address);
    }
}
