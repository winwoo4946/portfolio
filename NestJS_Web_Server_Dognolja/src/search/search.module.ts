import { Module } from '@nestjs/common';
import { AuthModule } from 'src/auth/auth.module';
import { BeautyModule } from 'src/beauty/beauty.module';
import { HotelModule } from 'src/hotel/hotel.module';
import { RedisModule } from 'src/redis/redis.module';
import { SearchController } from './search.controller';
import { SearchService } from './search.service';

@Module({
  imports:[
    RedisModule,
    HotelModule,
    BeautyModule,
    AuthModule
  ],
  controllers: [SearchController],
  providers: [SearchService]
})
export class SearchModule {}
