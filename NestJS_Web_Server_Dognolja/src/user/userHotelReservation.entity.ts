 import { BaseEntity, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn } from "typeorm";

@Entity('user_hotel_reservation')
export class UserHoteReservationEntity extends BaseEntity {
    // 호텔 예약 
    @PrimaryColumn()
    user_id: number;

    @PrimaryColumn()
    hotel_id: number;
 
}