import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { UserEntity } from "src/auth/user.entity";
import { HotelEntity } from "src/hotel/hotel.entity";
import { UserController } from "./user.controller";
import { UserService } from "./user.service";
import { UserHotelWishEntity } from "./userHotelWish.entity";

@Module({
  imports: [
    TypeOrmModule.forFeature([UserEntity, HotelEntity, UserHotelWishEntity])
  ],
  exports: [UserService],
  controllers: [UserController],
  providers: [UserService]
})
export class UserModule { }
