import { BadRequestException, Injectable, NotFoundException } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CreateUserDTO } from "./dto/createUser.dto";
import { UserEntity } from "../auth/user.entity";
import { DataSource, Repository } from "typeorm";
import { HotelEntity } from "src/hotel/hotel.entity";
import { UserHotelWishEntity } from "./userHotelWish.entity";
import { ColdObservable } from "rxjs/internal/testing/ColdObservable";

@Injectable()
export class UserService {
    constructor(
        private readonly dataSource: DataSource, 
        @InjectRepository(UserEntity)
        private readonly userRepo: Repository<UserEntity>,
        @InjectRepository(HotelEntity)
        private readonly hotelRepo: Repository<HotelEntity>,
        @InjectRepository(UserHotelWishEntity)
        private readonly hotelWishRepo: Repository<UserHotelWishEntity>,
    ) { }

    async createUser(userDTO: CreateUserDTO): Promise<UserEntity> {
        const user = await this.userRepo.create({
            ...userDTO
        });
        await this.userRepo.save(user);
        return user;
    }

    async getUserByUID(uid: string): Promise<MyUserInfo> {
        console.log(uid);
        const user = await this.userRepo.findOneBy({ uid });
        if (!user) {
            throw new NotFoundException(`Not Found User`);
        }
        console.log("get user by uid : ",user);
        return user;
    }

    async getUserById(id: number): Promise<UserEntity> {
        const user = await this.userRepo.findOneBy({ id });
        if (!user) {
            throw new NotFoundException(`Not Found User`);
        }
        return user;
    }


    //좋아요 관련
    async createHotelWish(userId: number, hotelId: number): Promise<void> {
        const hotel = await this.hotelRepo.findOneBy({ id: hotelId });
        console.log("create hotel : ", hotel);

        if (!hotel) {
            throw new NotFoundException("존재하지 않는 호텔입니다.");
        }

        const user = await this.userRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.wishHotels", "wishHotel")
            .where("user.id = :id", { id: userId })
            .getOne();

        if (!user) {
            throw new NotFoundException("존재하지 않는 유저입니다.");
        }

        if (user.wishHotels.find(f => f.id === hotelId)) {
            throw new BadRequestException("이미 찜한 호텔입니다.");
        }

      
        user.wishHotels.push(hotel);
        await this.userRepo.save(user);
        return;
    }

    async getHotelWish(userId: number): Promise<HotelEntity[]> {
        const user = await this.userRepo.createQueryBuilder("user")
            .leftJoinAndSelect("user.wishHotels", "wishHotel")
            .leftJoinAndSelect("wishHotel.photos","wishHotelPhoto")
            //.select(['user.id','wishHotel.*','wishHotelPhoto.url'])
            .where("user.id = :userId", { userId })
            .getOne();
            //.getSql();

          
        return user.wishHotels;
    }

    async deleteHotelWish(userId: number, hotelId:number): Promise<void>{
        // 호텔 좋아요 취소
        console.log("userId : ", userId," / hotelId : ", hotelId);
        //  let a = await this.dataSource.manager.query(` delete from user_table_wish_hotels_hotel_table where userTableId =${userId} and hotelTableId=${hotelId}`);
        // delet 

        let a = this.dataSource.createQueryBuilder()
        .delete()
        .from('user_table_wish_hotels_hotel_table')
        .where('userTableId= :userId',{userId})
        .andWhere('hotelTableId= :hotelId',{hotelId})
        .execute();

        console.log("Aa : ", a);


        // const hotel = await this.hotelRepo.findOneBy({ id: hotelId });
        // if (!hotel) {
        //     throw new NotFoundException("존재하지 않는 호텔입니다.");
        // }

        // const user = await this.userRepo.createQueryBuilder("user")
        // .leftJoinAndSelect("user.wishHotels", "wishHotel")
        // .where("user_wishHotel.userTableId = :id", {id:userId})
        // //.andWhere("user_wishHotel.hotelTableId = :hotelId",{hotelId:hotelId})
        // .getOne();
        
        // let  filterHotel = user.wishHotels.filter((hotel) =>{
        //     return hotel.id !== hotelId;
        // });
        
        // console.log("find hotel : ", hotel);
        // console.log("filter after : ",filterHotel);
        
        // let a = await this.userRepo.manager.save(filterHotel);
        // console.log("A ", a);


        // const aa = await this.userRepo.createQueryBuilder("user")
        // .leftJoinAndSelect("user.wishHotels", "wishHotel")
        // .where("user_wishHotel.userTableId = :id", {id:userId})
        // //.andWhere("user_wishHotel.hotelTableId = :hotelId",{hotelId:hotelId})
        // .getOne();

        // console.log("ddddd : ", aa);
        return; 
    }

    async createHotelReservaton(userId: number, hotelId: number): Promise<void> {
        // 예약한 호텔
        const hotel = await this.hotelRepo.findOneBy({id: hotelId});
        if(!hotel){
            throw new NotFoundException("존재하지 않는 호텔입니다.");
        }
        const user = await this.userRepo.createQueryBuilder("user")
        .leftJoinAndSelect("user.reservationHotels", "reservationHotel")
        .where("user.id= : id ", {id : userId})
        .getOne();
        console.log("user : ", user);

        if (!user) {
            throw new NotFoundException("존재하지 않는 유저입니다.");
        }

        if (user.reservateionHotels.find(f => f.id === hotelId)) {
            throw new BadRequestException("이미 찜한 호텔입니다.");
        }

        user.reservateionHotels.push(hotel);
        await this.userRepo.save(user);

        return; 
    }

    async getReservedHotel(userID: number): Promise<UserEntity[]>{

        const hotel = await this.hotelRepo.createQueryBuilder("hotel")
        .leftJoinAndSelect("hotel.reservateionHotels", "reservateionHotels")
        .where("hotel.id = :hotelId", { hotelId: 1 })
        .getOne();

        return hotel.reservedUsers;
    }

}


