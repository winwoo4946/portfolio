import { Body, Controller, Get, Logger, Param, ParseIntPipe, Post, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from "@nestjs/common";
import { FilesInterceptor } from "@nestjs/platform-express";
import { GetAuthUserInfo, JwtAuthGuard } from "src/auth/auth.decorator";
import { AuthUserInfo } from "src/auth/login";
import { CreateUserDTO } from "./dto/createUser.dto";
import { UserService } from "./user.service";

@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    @Post('createUser')
    @UsePipes(ValidationPipe)
    async createUser(@Body() createUserDTO : CreateUserDTO) {
        return await this.userService.createUser(createUserDTO);
    }

    @Post('getLoginUser')
    @UseGuards(JwtAuthGuard)
    async getUserByUI(@GetAuthUserInfo() authInfo: AuthUserInfo){
        return await this.userService.getUserByUID(authInfo.uid); 
    }

    // 찜 목록
    @Post('/addHotelWish/:id')
    async addHotelWish(@Param('id', ParseIntPipe) userId: number, @Body('hotelId') hotelId: number) {
        return await this.userService.createHotelWish(userId, hotelId);
    }

    @Get('/hotelWish/:id')
    async getHotelWish(@Param('id', ParseIntPipe) userId: number) {
         return await this.userService.getHotelWish(userId);
    }

    // 예약 목록
    // 추후에 관리자 페이지 문제로 인한 보류
    @Post('/addHotelReservation/:id')
    async addReservationHotel(@Param('id', ParseIntPipe) userId: number, @Body('hotelId') hotelId: number) {
        return await this.userService.createHotelReservaton(userId, hotelId);
    }

    @Get('/reservedHotel/:id')
    async getReservedHotel(@Param('id', ParseIntPipe) userId: number) {
         return await this.userService.getReservedHotel(userId);
    }

}