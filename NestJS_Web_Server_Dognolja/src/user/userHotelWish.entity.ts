import { HotelEntity } from "src/hotel/hotel.entity";
import { BaseEntity, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany, PrimaryColumn } from "typeorm";

@Entity('user_hotel_wish_table')
export class UserHotelWishEntity extends BaseEntity {
    @PrimaryColumn()
    user_id: number;

    @PrimaryColumn()
    hotel_id: number;

}