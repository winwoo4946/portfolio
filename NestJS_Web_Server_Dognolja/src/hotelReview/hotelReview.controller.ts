import { Body, Controller, Get, Param, ParseIntPipe, Post, UploadedFiles, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from "@nestjs/common";
import { FilesInterceptor } from "@nestjs/platform-express";
import { GetAuthUserInfo, JwtAuthGuard } from "src/auth/auth.decorator";
import { AuthUserInfo } from "src/auth/login";
import { ShopReviewDTO } from "src/commonDto/beautyReview.dto";
import { HotelReviewService } from "./hotelReview.service";

@Controller('hotelReview')
export class HotelReviewController {
    constructor(private readonly hotelReivewService: HotelReviewService) { }

    @Get('/:id')
    getHotelReviews(@Param('id', ParseIntPipe) id: number) {
        return this.hotelReivewService.getHotelReviews(id);
    }

    @Post('createReview')
    @UseGuards(JwtAuthGuard)
    @UseInterceptors(FilesInterceptor('files', 5))
    addReview(
        @GetAuthUserInfo() authInfo: AuthUserInfo,
        @Body() reviewDTO: ShopReviewDTO, 
        @UploadedFiles() files: Array<Express.Multer.File>
    ) {
        return this.hotelReivewService.createReview(authInfo, reviewDTO, files);
    }


    /**
     *  유저가 작성한 호텔 리뷰 리스트 가져오기
     *  userId (or ownerId)로 조회하여 조회 되는 모든 데이터 배열로 리턴 
     * 
     *  create by jungyo
     */
    @Get('getReviews/:id')
    getReviews(@Param('id', ParseIntPipe) id: number){
        return this.hotelReivewService.getHotelReviewsByUser(id);
    }


    @Get('getWrittenHotel/:id')
    getWrittenHotel(@Param('id', ParseIntPipe) id: number){
        return this.hotelReivewService.getWrittenShopReviewList(id);
    }
}