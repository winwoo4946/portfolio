import { HotelEntity } from "src/hotel/hotel.entity";
import { UserEntity } from "src/auth/user.entity";
import { BaseEntity, Column, Entity, Index, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { HotelReviewPhotoEntity } from "./hotelReviewPhoto.entity";

@Entity('hotel_review_table')
export class HotelReviewEntity extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: number;

    @Column("text")
    content: string;

    @Column()
    score: number;

    @Column()
    date: Date;

    @Index()
    @ManyToOne(type => HotelEntity, hotel => hotel.reviews)
    hotel: HotelEntity;

    @ManyToOne(type => UserEntity, user => user.hotelReviews)
    owner: UserEntity;

    @OneToMany(type => HotelReviewPhotoEntity, photo => photo.review)
    photos: HotelReviewPhotoEntity[];
}