import { BaseEntity, Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { HotelReviewEntity } from "./hotelReview.entity";

@Entity('hotel_review_photo_table')
export class HotelReviewPhotoEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    url: string;

    @Index()
    @ManyToOne(type => HotelReviewEntity, review => review.photos)
    review: HotelReviewEntity;
}