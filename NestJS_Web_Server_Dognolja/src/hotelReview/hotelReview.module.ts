import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AuthModule } from "src/auth/auth.module";
import { UserEntity } from "src/auth/user.entity";
import { HotelEntity } from "src/hotel/hotel.entity";
import { HotelModule } from "src/hotel/hotel.module";
import { HotelKakaoEntity } from "src/hotel/hotelKakao.entity";
import { HotelNaverEntity } from "src/hotel/hotelNaver.entity";
import { HotelPhotoEntity } from "src/hotel/hotelPhoto.entity";
import { UploadsService } from "src/uploads/uploads.service";
import { UserModule } from "src/user/user.module";
import { HotelReviewController } from "./hotelReview.controller";
import { HotelReviewEntity } from "./hotelReview.entity";
import { HotelReviewService } from "./hotelReview.service";
import { HotelReviewPhotoEntity } from "./hotelReviewPhoto.entity";

@Module({
  imports: [
    AuthModule,
    TypeOrmModule.forFeature([
      HotelEntity,
      HotelPhotoEntity,
      HotelReviewEntity,
      HotelReviewPhotoEntity,
      UserEntity,
      HotelKakaoEntity,
      HotelNaverEntity
    ]),
    UserModule,
    HotelModule
  ],
  exports: [HotelReviewService],
  controllers: [HotelReviewController],
  providers: [HotelReviewService, UploadsService]
})
export class HotelReviewModule { }
