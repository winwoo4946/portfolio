import { Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { HotelEntity } from "src/hotel/hotel.entity";
import { HotelService } from "src/hotel/hotel.service";
import { UserEntity } from "src/auth/user.entity";
import { UserService } from "src/user/user.service";
import { HotelReviewEntity } from "./hotelReview.entity";
import { Repository } from "typeorm";
import { AuthUserInfo } from "src/auth/login";
import { UploadsService } from "src/uploads/uploads.service";
import { HotelReviewPhotoEntity } from "./hotelReviewPhoto.entity";
import { ShopReviewDTO } from "src/commonDto/beautyReview.dto";

@Injectable()
export class HotelReviewService {
    constructor(
        private readonly hotelService: HotelService,
        private readonly userService: UserService,
        private readonly uploadsService: UploadsService,
        @InjectRepository(HotelReviewEntity)
        private readonly hotelReviewRepo: Repository<HotelReviewEntity>,
        @InjectRepository(HotelReviewPhotoEntity)
        private readonly hotelReviewPhotoRepo: Repository<HotelReviewPhotoEntity>,
        @InjectRepository(HotelEntity)
        private readonly hotelRepo: Repository<HotelEntity>
    ) { }

    async createReview(
        authInfo: AuthUserInfo,
        hotelReviewDTO: ShopReviewDTO,
        files: Array<Express.Multer.File>
    ): Promise<HotelReviewEntity> {
        const count = await this.getHotelReviewCountByContent(hotelReviewDTO.content);
        if (count > 0)
            return;

        const hotel: HotelEntity = await this.hotelService.getHotelById(hotelReviewDTO.shopId);
        const user: MyUserInfo = await this.userService.getUserByUID(authInfo.uid);

        const photoUrls = await this.uploadsService.uploadReviewPhoto("hotel", hotelReviewDTO.shopId, files);

        const photos: HotelReviewPhotoEntity[] = [];
        for (let url of photoUrls) {
            const photo = this.hotelReviewPhotoRepo.create({
                url
            });
            await this.hotelReviewPhotoRepo.save(photo);
            photos.push(photo);
        }

        const review = this.hotelReviewRepo.create({
            ...hotelReviewDTO,
            date: new Date(),
            hotel: hotel,
            owner: user,
            photos
        });
        await this.hotelReviewRepo.save(review);

        return review;
    }

    async getHotelReviews(hotelId: number): Promise<ReviewBase[]> {
        const hotelReviewEntity = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .leftJoin('hotelReview.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("hotelReview.hotelId = :hotelId", { hotelId })
            .getMany();

        return hotelReviewEntity;
    }

    async getHotelReviewCountByContent(content: string): Promise<number> {
        const found = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .where("hotelReview.content = :content", { content })
            .getCount();

        return found;
    }

    async getHotelReviewsByUser(ownerId: number): Promise<ReviewBase[]> {
        // 마이페이지 리뷰 리스트 - 
        const reviews = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .where("hotelReview.ownerId = :ownerId", { ownerId })
            .getMany();

        console.log("reviews : ", reviews);
        return reviews;
    }


    async getHotelReviewsByHotelId(hotelId: number): Promise<ReviewBase[]> {
        //마이페이지 리뷰 - 호텔 아이디로 리뷰 
        // select * from hotel_review_table where hotelId = :hotelId
        const reviews = await this.hotelReviewRepo
            .createQueryBuilder("hotelReview")
            .where("hotelReview.hotelId= :hotelId", { hotelId })
            .getMany();

        return reviews;
    }
    async getWrittenShopReviewList(userId: number):Promise<HotelReviewEntity[]>{
        // 리뷰를 작성한 호텔 리스트 

        //     const revieList = await this.userRepo.createQueryBuilder("user")
        //     .select(['user.id'])
        //     .leftJoin("user.hotelReviews", 'hotelReviews').addSelect(["hotelReviews.id"])
        //     .leftJoinAndSelect("hotelReviews.hotel","hotel")
        //     .where("user.id= :id",{id:userId})
        //      .getOne();
        //   //  .getSql();
    

        const revieList = await this.hotelReviewRepo.createQueryBuilder("review")
        //AndSelect("review.hotel","hotel")
        .leftJoin("review.hotel","hotel")   
        .addSelect(["hotel.id","hotel.title", "hotel.phone","hotel.url", 
            "hotel.address","hotel.roadAddress","hotel.thumbnailPhotoId"])
        .leftJoinAndSelect('hotel.photos', 'photo')
        //.leftJoin('hotel.photos', 'photo').addSelect("photo.url").take(1)
        //.orderBy("photo", "DESC")
        .where("review.ownerId= :id",{id:userId})
        .getMany();

        console.log("revieList : ",revieList);
        return revieList;
    }

    async getWritableHotel (userId: number): Promise<HotelEntity[]>{
        // 리뷰 작성 안한 호텔 리스트
        // 필요한것 예약을 했고, 예약한곳이 완료까지 됐는지 확인하는 방법이 있어야 함
        // 우리 DB 테이블에 예약을 하고 완료까지 했는지 확인 불가 
        // user_hotel_wish_table 에 완료 여부 칼럼 추가 하여 완료 여부 판단?
        // 그럼 완료는 어케 판단? - 해당 가게 사장이 완료 해야? / 만약 결제가 미리 있을 경우 expairedate 같은 것으로 만료 시간이 지나면 리뷰 작성할 수 있게?

        return; 
    }
}