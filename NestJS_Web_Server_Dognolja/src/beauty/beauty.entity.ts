import { BeautyReviewEntity } from "src/beauty-review/beautyReview.entity";
import { BaseEntity, Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { BeautyPhotoEntity } from "./beautyPhoto.entity";

@Entity('beauty_table')
export class BeautyEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column("text")
    description: string;

    @Column("varchar", { length: 45, default: true })
    phone: string;

    @Column()
    categoryName: string;

    @Column()
    url: string;

    @Column()
    address: string;

    @Column()
    roadAddress: string;

    @Column()
    mapx: number;

    @Column()
    mapy: number;

    @Column("double")
    mapLat: number;

    @Column("double")
    mapLng: number;

    @Column({default: 0})
    thumbnailPhotoId: number;

    @Column({default: true})
    price1: number = 0;

    @Column({default: true})
    price2: number = 0;

    @Column({default: true})
    price3: number = 0;

    @Column({default: true})
    price4: number = 0;

    @OneToMany(type => BeautyReviewEntity, review => review.beauty)
    reviews: BeautyReviewEntity[];

    @OneToMany(type => BeautyPhotoEntity, photo => photo.beauty)
    photos: BeautyPhotoEntity[];
}