import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { NaverMapDTO } from 'src/map/dto/naverMapDTO';
import { Repository } from 'typeorm';
import { BeautyEntity } from './beauty.entity';
import { BeautyNaverEntity } from './beautyNaver.entity';
import { BeautyPhotoEntity } from './beautyPhoto.entity';
import { BeautyDTO } from './dto/beauty.dto';

@Injectable()
export class BeautyService {
    constructor(
        @InjectRepository(BeautyEntity)
        private readonly beautyRepository: Repository<BeautyEntity>,
        @InjectRepository(BeautyPhotoEntity)
        private readonly beautyPhotoRepository: Repository<BeautyPhotoEntity>,
        @InjectRepository(BeautyNaverEntity)
        private readonly beautyNaverRepository: Repository<BeautyNaverEntity>,
    ) { }

    async getAllBeauty(): Promise<BeautyEntity[]> {
        return this.beautyRepository.find();
    }

    async getBeautyById(id: number): Promise<BeautyEntity> {
        const found = await this.beautyRepository.findOneBy({ id });

        if (!found) {
            throw new NotFoundException(`Not found Beauty Info - id(${id})`);
        }
        return found;
    }

    async getBeautyByAddress(address: string): Promise<ShopBaseInfo[]> {
        const beautyEntities: BeautyEntity[] = await this.beautyRepository.createQueryBuilder('beauty')
            .leftJoinAndSelect('beauty.photos', 'photo')
            .leftJoinAndSelect('beauty.reviews', 'review')
            .where("beauty.address like :address", { address: `%${address}%` })
            .take(50)
            .getMany();

        const beautyInfos: ShopBaseInfo[] = []
        for (let entity of beautyEntities) {
            let score: number = 0;
            let reviewCount: number = entity.reviews.length;
            for (let review of entity.reviews) {
                score += review.score;
            }
            beautyInfos.push({
                id: entity.id,
                title: entity.title,
                mapLat: entity.mapLat,
                mapLng: entity.mapLng,
                price1: entity.price1,
                mapx: entity.mapx,
                mapy: entity.mapy,
                thumbnailUrl: entity.photos.length > 0 ? entity.photos[0].url : "",
                hashTags: [],
                score: reviewCount === 0 ? 0 : Math.floor((score / reviewCount) * 10) / 10,
                reviewCount
            });
        }
        return beautyInfos;
    }

    async getBeautyDetail(id: number): Promise<ShopInfo> {
        const beautyEntity = await this.beautyRepository.createQueryBuilder('beauty')
            .leftJoin('beauty.photos', 'photo').addSelect(['photo.id', 'photo.url'])
            .leftJoinAndSelect('beauty.reviews', 'review')
            .leftJoin('review.owner', 'owner').addSelect(['owner.id', 'owner.nickName', 'owner.thumbnailImgUrl', 'owner.profileImgUrl'])
            .where("beauty.id = :id", { id })
            .getOne();
        const shopInfo: ShopInfo = {
            ...this.getShopBaseInfo(beautyEntity),
            description: beautyEntity.description,
            phone: beautyEntity.phone,
            reviews: beautyEntity.reviews,
            photos: beautyEntity.photos,
            address: beautyEntity.address,
            roadAddress: beautyEntity.roadAddress,
            operatingTime: [],
            subInfos: ['~4.9kg', '호텔링', '(1일)', '25,000원']
        }
        return shopInfo;
    }

    private getShopBaseInfo(entity: BeautyEntity): ShopBaseInfo {
        let score: number = 0;
        let reviewCount: number = entity.reviews.length;
        for (let review of entity.reviews) {
            score += review.score;
        }
        const shopBaseInfo: ShopBaseInfo = {
            id: entity.id,
            title: entity.title,
            mapLat: entity.mapLat,
            mapLng: entity.mapLng,
            price1: entity.price1,
            mapx: entity.mapx,
            mapy: entity.mapy,
            thumbnailUrl: entity.photos.length > 0 ? entity.photos[0].url : "",
            hashTags: [],
            score: Math.floor((score / reviewCount) * 10) / 10,
            reviewCount
        }
        return shopBaseInfo;
    }

    async getBeautyCountByTitle(address: string): Promise<number> {
        return this.beautyRepository.createQueryBuilder("beauty")
            .where("beauty.address = :address", { address })
            .getCount();
    }

    async updateBeautyPosition(id: number, x: number, y: number) {
        return this.beautyRepository.createQueryBuilder()
            .update()
            .set({
                mapLat: y,
                mapLng: x,
            })
            .where("id = :id", { id })
            .execute();
    }

    async createBeauty(beautyDTO: BeautyDTO): Promise<BeautyEntity> {
        const beauty = this.beautyRepository.create(beautyDTO);
        await this.beautyRepository.save(beauty);
        return beauty;
    }

    async getAllBeautyNaver(): Promise<BeautyNaverEntity[]> {
        return await this.beautyNaverRepository
            .createQueryBuilder("beautyNaver")
            .getMany();
    }

    async getBeautyNaverCountByAddress(address: string): Promise<number> {
        const found = await this.beautyNaverRepository
            .createQueryBuilder("beautyNaver")
            .where("beautyNaver.address like :address", { address: `%${address}%` })
            .getCount();

        return found;
    }

    async createBeautyNaver(naverMapDTO: NaverMapDTO): Promise<BeautyNaverEntity> {
        const count = await this.getBeautyNaverCountByAddress(naverMapDTO.address);
        if (count > 0) {
            // 이미 존재하는 호텔
            return;
        }

        const beauty = this.beautyNaverRepository.create(naverMapDTO);
        await this.beautyNaverRepository.save(beauty);
        return beauty;
    }

    async getBeautyPhotoCountByUrl(url: string): Promise<number> {
        const found = await this.beautyPhotoRepository
            .createQueryBuilder("beautyPhoto")
            .where("beautyPhoto.url = :url", { url })
            .getCount();

        return found;
    }

    async updateBeautyNaverZeroResult(id: number, isZero: boolean) {
        const found = await this.beautyNaverRepository.findOneBy({ id });
        if (!found)
            return;

        await this.beautyNaverRepository
            .createQueryBuilder()
            .update(BeautyNaverEntity)
            .set({ zeroResult: isZero })
            .where("id = :id", { id: id })
            .execute();

        console.log(`update naver result - ${id} - ${isZero}`);
    }

    async createBeautyGoogle(beatuyDTO: BeautyDTO): Promise<BeautyEntity> {
        // 호텔 생성
        const beauty = this.beautyRepository.create(beatuyDTO);
        const beautyEntity = await this.beautyRepository.save(beatuyDTO);

        // 포토 생성
        // 포토<-호텔 연결
        let promises = [];
        for (let photoUrl of beatuyDTO.photoUrls) {
            const count = await this.getBeautyPhotoCountByUrl(photoUrl);
            if (count > 0)
                continue;

            const photo = this.beautyPhotoRepository.create({
                url: photoUrl,
                beauty: beauty
            });
            promises.push(this.beautyPhotoRepository.save(photo));
        }
        const photos = await Promise.all(promises);
        if (photos.length > 0) {
            await this.beautyRepository.createQueryBuilder()
                .update(BeautyEntity)
                .set({ thumbnailPhotoId: photos[photos.length - 1].id })
                .where("id = :id", { id: beautyEntity.id })
                .execute();
        }
        return beauty;
    }
}
