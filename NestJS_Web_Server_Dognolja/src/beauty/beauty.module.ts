import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BeautyController } from './beauty.controller';
import { BeautyEntity } from './beauty.entity';
import { BeautyService } from './beauty.service';
import { BeautyNaverEntity } from './beautyNaver.entity';
import { BeautyPhotoEntity } from './beautyPhoto.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      BeautyEntity,
      BeautyPhotoEntity,
      BeautyNaverEntity
    ])
  ],
  exports: [BeautyService],
  controllers: [BeautyController],
  providers: [BeautyService]
})
export class BeautyModule {}
