import { BaseEntity, Column, Entity, Index, ManyToOne, PrimaryGeneratedColumn } from "typeorm";
import { BeautyEntity } from "./beauty.entity";

@Entity('beauty_photo_table')
export class BeautyPhotoEntity extends BaseEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column("text")
    url: string;

    @Index()
    @ManyToOne(type => BeautyEntity, beauty => beauty.photos)
    beauty: BeautyEntity;
}