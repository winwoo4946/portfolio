import { IsNumber, IsString } from "class-validator";

export class BeautyDTO {
    @IsString()
    title: string;

    @IsString()
    description: string;

    @IsString()
    phone: string;

    @IsString()
    photoUrls: string[];

    @IsString()
    categoryName: string;

    @IsString()
    url: string;

    @IsString()
    address: string;

    @IsString()
    roadAddress: string;

    @IsNumber()
    mapx: number;

    @IsNumber()
    mapy: number;

    @IsNumber()
    mapLat: number;

    @IsNumber()
    mapLng: number;
}