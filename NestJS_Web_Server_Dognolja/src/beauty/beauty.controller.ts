import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common';
import { BeautyService } from './beauty.service';

@Controller('beauty')
export class BeautyController {
    constructor(private readonly beautyService: BeautyService) { }
    @Get('/:id')
    getBeautyById(@Param('id', ParseIntPipe) id: number) {
        return this.beautyService.getBeautyDetail(id);
    }
}
