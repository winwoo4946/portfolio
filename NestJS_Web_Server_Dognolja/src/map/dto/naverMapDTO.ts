import { IsNumber, IsString } from "class-validator";

export class NaverMapDTO {
    @IsString()
    title: string;

    @IsString()
    description: string;

    @IsString()
    categoryName: string;

    @IsString()
    naverUrl: string;

    @IsString()
    address: string;

    @IsString()
    roadAddress: string;

    @IsNumber()
    x: number;

    @IsNumber()
    y: number;
}