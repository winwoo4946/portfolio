import { IsNumber, IsString } from "class-validator";

export class KakaoMapDTO {
    @IsNumber()
    kakaoMapId: number;

    @IsString()
    title: string;

    @IsString()
    description: string;

    @IsString()
    phone: string;

    @IsString()
    categoryName: string;

    @IsString()
    kakaoPlaceUrl: string;

    @IsString()
    address: string;

    @IsString()
    roadAddress: string;

    @IsNumber()
    x: number;

    @IsNumber()
    y: number;

    
}