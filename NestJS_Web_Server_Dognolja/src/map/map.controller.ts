import { Controller, Get } from '@nestjs/common';
import { MapService } from './map.service';

@Controller('map')
export class MapController {
    constructor(private readonly mapService: MapService) { }
    @Get('naverMap')
    async getNaverMap() {
        return await this.mapService.naverMap();
    }

    @Get('kakaoMap')
    async getKakaoMap() {
        return await this.mapService.kakaoMap();
    }

    @Get('googleMap')
    async getGoogleMap() {
        return await this.mapService.googleMap();
    }

    @Get('checkKakaoToNaver')
    async checkKakaoToNaver() {
        return await this.mapService.checkKakaoToNaver();
    }

    @Get('checkNaverToKakao')
    async checkNaverToKakao() {
        return await this.mapService.checkNaverToKakao();
    }

    @Get('addressTest')
    async addressTest() {
        return await this.mapService.addressTest();
    }

    @Get('googleMapPosition')
    async googleMapPosition() {
        return await this.mapService.googleMapPosition();
    }
}
