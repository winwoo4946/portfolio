
import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios, { AxiosResponse } from 'axios';
import { map, Observable, timeout } from 'rxjs';
import { BeautyReviewService } from 'src/beauty-review/beauty-review.service';
import { BeautyEntity } from 'src/beauty/beauty.entity';
import { BeautyService } from 'src/beauty/beauty.service';
import { BeautyNaverEntity } from 'src/beauty/beautyNaver.entity';
import { BeautyDTO } from 'src/beauty/dto/beauty.dto';
import { HotelDTO } from 'src/hotel/dto/hotel.dto';
import { HotelEntity } from 'src/hotel/hotel.entity';
import { HotelService } from 'src/hotel/hotel.service';
import { HotelKakaoEntity } from 'src/hotel/hotelKakao.entity';
import { HotelNaverEntity } from 'src/hotel/hotelNaver.entity';
import { HotelReviewService } from 'src/hotelReview/hotelReview.service';
import { setTimeout } from 'timers/promises';
import { KakaoMapDTO } from './dto/KakaoMapDTO';
import { NaverMapDTO } from './dto/naverMapDTO';
import { emdReigon, sigRegion } from './region';


@Injectable()
export class MapService {
    private readonly CLIENT_ID: string;
    private readonly CLIENT_SECRET: string;
    private readonly KAKAO_API_KEY: string;

    constructor(
        private readonly configService: ConfigService,
        private readonly hotelService: HotelService,
        private readonly hotelReviewService: HotelReviewService,
        private readonly beautyService: BeautyService,
        private readonly beautyReviewService: BeautyReviewService
    ) {
        this.CLIENT_ID = this.configService.get('NAVER_CLIENT_ID');
        this.CLIENT_SECRET = this.configService.get('NAVER_CLIENT_SECRET');
        this.KAKAO_API_KEY = this.configService.get('KAKAO_REST_API_KEY');
    }

    async naverMap() {
        let map = new Map<string, NaverMapDTO>();
        let apiCount = 0;
        const regionList = emdReigon;
        for (let i = 0; i < regionList.length; ++i) {
            const regtionName = regionList[i].EMD_KOR_NM;
            const result = await axios.get('https://openapi.naver.com/v1/search/local.json', {
                headers: {
                    'X-Naver-Client-Id': this.CLIENT_ID,
                    'X-Naver-Client-Secret': this.CLIENT_SECRET
                },
                params: {
                    query: `${regtionName} 애견미용`,
                    display: 5,
                }
            });

            ++apiCount;
            await setTimeout(100);

            for (let item of result.data.items) {
                if (map.has(item.address))
                    continue;

                const dto: NaverMapDTO = {
                    title: item.title,
                    description: item.description,
                    categoryName: item.category,
                    naverUrl: item.link,
                    address: item.address,
                    roadAddress: item.roadAddress,
                    x: item.mapx,
                    y: item.mapy
                };
                map.set(item.address, dto);
            }
        }

        let addHotelCount = 0;
        for (let [key, value] of map) {
            this.beautyService.createBeautyNaver(value);
            addHotelCount++;
        }
        return `apiCount : ${apiCount} / addCount : ${addHotelCount}`;
    }

    async kakaoMap() {
        let map = new Map<string, KakaoMapDTO>();

        const regionList = sigRegion;
        let apiCount = 0;
        for (let i = 0; i < regionList.length; ++i) {
            const regtionName = regionList[i].SIG_KOR_NM;
            const result = await axios.get(`https://dapi.kakao.com/v2/local/search/keyword.json`, {
                headers: {
                    'Authorization': `KakaoAK ${this.KAKAO_API_KEY}`,
                },
                params: {
                    query: `${regtionName} 애견미용`,
                    size: 15
                }
            });
            return;
            ++apiCount;

            for (let item of result.data.documents) {
                if (map.has(item.address_name))
                    continue;

                const dto: KakaoMapDTO = {
                    kakaoMapId: item.id,
                    title: item.place_name,
                    description: "",
                    phone: item.phone,
                    categoryName: item.category_name,
                    kakaoPlaceUrl: item.place_url,
                    address: item.address_name,
                    roadAddress: item.road_address_name,
                    x: item.x,
                    y: item.y
                };
                map.set(item.address_name, dto);
            }
        }

        let addHotelCount = 0;
        for (let [key, value] of map) {
            this.hotelService.createHotelKakao(value);
            ++addHotelCount;
        }
        return `apiCount : ${apiCount} / addHotelCount : ${addHotelCount}`;
    }

    async googleMap() {
        const key = this.configService.get('GOOGLE_MAP_API_KEY');
        const navers: BeautyNaverEntity[] = await this.beautyService.getAllBeautyNaver();
        const mapDataList = [];

        const naverCount = navers.length;
        console.log(`naver count : ${naverCount}`);
        let count = 0;
        for (let naver of navers) {
            count++;
            if (naver.zeroResult) {
                // 이미 구글 검색결과에 없다고 나온경우
                continue;
            }

            const beautyCount = await this.beautyService.getBeautyCountByTitle(naver.address);
            if(beautyCount > 0) {
                // 이미 등록된 업체 주소일 경우
                continue;
            }

            const encoding: string = encodeURIComponent(naver.title);
            let placeInfo = await axios.get(`https://maps.googleapis.com/maps/api/place/textsearch/json?query=${encoding}&language=ko&key=${key}`);
            if (placeInfo.data.results.length <= 0) {
                await this.beautyService.updateBeautyNaverZeroResult(naver.id, true);
                continue;
            }

            const placeId = placeInfo.data.results[0].place_id;
            let placeDetail = await axios.get(`https://maps.googleapis.com/maps/api/place/details/json?place_id=${placeId}&language=ko&key=${key}`);

            const placeDetailResult = placeDetail.data.result;

            //#region photo
            let promises = [];
            const photoUrls: string[] = [];
            if (placeDetailResult.photos) {
                for (let photo of placeDetailResult.photos) {
                    // try {
                    //     const p = await axios.get(`https://maps.googleapis.com/maps/api/place/photo?photo_reference=${photo.photo_reference}&maxheight=${1600}&maxwidth=${1600}&key=${key}`);
                    //     photoUrls.push(p.request.res.responseUrl);
                    // } catch(e) {
                    //     console.log(e.message);
                    // }

                    promises.push(axios.get(`https://maps.googleapis.com/maps/api/place/photo?photo_reference=${photo.photo_reference}&maxheight=${1600}&maxwidth=${1600}&key=${key}`));
                }
                try {
                    const photoRefResult = await Promise.all(promises);
                    for (let photoRef of photoRefResult) {
                        photoUrls.push(photoRef.request.res.responseUrl);
                    }
                } catch (e) {
                    console.log(e.message);
                }
            }
            //#endregion

            const beautyDTO: BeautyDTO = {
                phone: placeDetailResult.formatted_phone_number,
                url: naver.naverUrl,
                photoUrls: photoUrls,
                mapx: naver.x,
                mapy: naver.y,
                mapLat: placeDetailResult.geometry.location.lat,
                mapLng: placeDetailResult.geometry.location.lng,
                ...naver
            };
            const beauty = await this.beautyService.createBeautyGoogle(beautyDTO);

            if (placeDetailResult.reviews) {
                for (let review of placeDetailResult.reviews) {
                    if (!review.text)
                        continue;

                    let date = new Date(review.time * 1000);
                    await this.beautyReviewService.createReviewByMap(beauty, review.text, review.rating, date);
                }
            }

            console.log(`count : ${count}/${naverCount}`)
            //formatted_phone_number
            //geometry.location.lat, lng
            //photos[index].photo_reference
            //rating 리뷰
            //reviews[index].text, rating, time,
        }
        return 'complete';
    }

    async googleMapPosition() {
        const key = this.configService.get('GOOGLE_MAP_API_KEY');
        const beautys: BeautyEntity[] = await this.beautyService.getAllBeauty();
        const promises = [];
        for (let beauty of beautys) {
            if (beauty.mapLat > 0 && beauty.mapLng > 0)
                continue;

            const s = beauty.address.split(' ');
            for (let i = s.length - 1; i >= 0; --i) {
                const lastWord = s[i];
                const lastChar = lastWord.at(lastWord.length - 1);
                if (isNaN(Number(lastChar))) {
                    s[i] = '';
                    continue;
                }
                break;
            }
            const address = s.join(' ');
            const result = await axios.get(`https://dapi.kakao.com/v2/local/search/keyword.json`, {
                headers: {
                    'Authorization': `KakaoAK ${this.KAKAO_API_KEY}`,
                },
                params: {
                    query: address,
                    size: 1
                }
            });
            if(result.data.documents.length > 0){
                const data = result.data.documents[0];
                promises.push(this.beautyService.updateBeautyPosition(beauty.id, data.x, data.y));
            }
        }

        let count = 0;
        for(let p of promises) {
            p.then(()=>{
                ++count;
                console.log(`${count} / ${promises.length} = ${Math.floor((count/promises.length) * 100)}%`);
            })
        }
        await Promise.all(promises);

        return "완료";
    }

    async checkKakaoToNaver() {
        const results: string[] = [];
        const kakakos: HotelKakaoEntity[] = await this.hotelService.getAllHotelKakao();
        for (let kakao of kakakos) {
            const addressList: string[] = kakao.address.split(' ');
            const address = `${addressList[addressList.length - 2]} ${addressList[addressList.length - 1]}`;
            let count = await this.hotelService.getHotelNaverCountByAddress(address);
            results.push(`kakao(${kakao.id}) - ${address} match naver count - ${count}`);
        }
        return results.join('<br>');
    }

    async checkNaverToKakao() {
        const results: string[] = [];
        const navers: HotelNaverEntity[] = await this.hotelService.getAllHotelNaver();
        for (let naver of navers) {
            let count = await this.hotelService.getHotelKakaoCountByAddress(naver.address);
            if (count > 0) {
                results.push(`naver(${naver.id}) - ${naver.address} match kakao count - ${count}`);
            }
        }
        return results.join('<br>');
    }

    async addressTest() {
        /***
         * [시, 도]
         * [구, 군, 면, 동, 시, 읍]
         * [동, 가, 읍, 면, 리, 구]
         * [리, 동, 가, 면, 읍]
         */
        const hotelList: HotelEntity[] = await this.hotelService.getAllHotel();

        const map = [{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}];

        for (let hotel of hotelList) {
            const addressComponents: string[] = hotel.address.split(' ');
            for (let i = 0; i < addressComponents.length; ++i) {
                const address = addressComponents[i];
                const lastChar = address.at(address.length - 1);
                if (!map[i][lastChar]) {
                    map[i][lastChar] = lastChar;
                }
            }
        }

        let result = '';
        for (let item of map) {
            result += `${Object.keys(item).join('/')}<br>`;
        }
        return result;
    }
}
