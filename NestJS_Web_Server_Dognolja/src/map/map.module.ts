import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BeautyReviewModule } from 'src/beauty-review/beauty-review.module';
import { BeautyModule } from 'src/beauty/beauty.module';
import { BeautyNaverEntity } from 'src/beauty/beautyNaver.entity';
import { HotelEntity } from 'src/hotel/hotel.entity';
import { HotelModule } from 'src/hotel/hotel.module';
import { HotelKakaoEntity } from 'src/hotel/hotelKakao.entity';
import { HotelNaverEntity } from 'src/hotel/hotelNaver.entity';
import { HotelPhotoEntity } from 'src/hotel/hotelPhoto.entity';
import { HotelReviewModule } from 'src/hotelReview/hotelReview.module';
import { MapController } from './map.controller';
import { MapService } from './map.service';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5
    }),
    TypeOrmModule.forFeature([
      HotelEntity,
      HotelPhotoEntity,
      HotelKakaoEntity,
      HotelNaverEntity,
      BeautyNaverEntity
    ]),
    HotelModule,
    HotelReviewModule,
    BeautyModule,
    BeautyReviewModule
  ],
  controllers: [MapController],
  providers: [MapService]
})
export class MapModule { }
