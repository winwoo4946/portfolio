import { LoggerService, Logger as NestLogger, Type } from "@nestjs/common";
import { type } from "os";

export class Logger {
    private static combineMessage(message: string, object?: object) {
        if(object) {
            return {
                ...object,
                message
            }
        }

        return message;

    }
    
    /**
     * level log
     * @param context 로그를 사용하는 클래스
     * @param message 로그 메세지
     * @param object 로그 메세지와 함께 출력할 오브젝트
     */
    static log(context: Type<any>, message: string, object?: object): void {
        NestLogger.log(this.combineMessage(message, object), context.name);
    }

    /**
     * level error
     * @param context 로그를 사용하는 클래스
     * @param message 로그 메세지
     * @param object 로그 메세지와 함께 출력할 오브젝트
     */
     static error(context: Type<any>, message: string, object?: object): void {
        NestLogger.error(this.combineMessage(message, object), context.name);
    }

    /**
     * level warn
     * @param context 로그를 사용하는 클래스
     * @param message 로그 메세지
     * @param object 로그 메세지와 함께 출력할 오브젝트
     */
     static warn(context: Type<any>, message: string, object?: object): void {
        NestLogger.warn(this.combineMessage(message, object), context.name);
    }

    /**
     * level debug
     * @param context 로그를 사용하는 클래스
     * @param message 로그 메세지
     * @param object 로그 메세지와 함께 출력할 오브젝트
     */
     static debug(context: Type<any>, message: string, object?: object): void {
        NestLogger.debug(this.combineMessage(message, object), context.name);
    }

    /**
     * level verbose
     * @param context 로그를 사용하는 클래스
     * @param message 로그 메세지
     * @param object 로그 메세지와 함께 출력할 오브젝트
     */
     static verbose(context: Type<any>, message: string, object?: object): void {
        NestLogger.verbose(this.combineMessage(message, object), context.name);
    }
}