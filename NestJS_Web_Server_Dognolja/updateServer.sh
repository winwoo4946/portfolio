#!/bin/bash

# pm2 종료
pm2 kill
# submodule update
cd ~/dognolja_server/server/src/dognolja_common/
git pull
# git pull
cd ~/dognolja_server/server/
git pull
# npm install
npm install
# build
#npm run build
# production 배포
npm run start:prod
